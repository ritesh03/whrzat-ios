//
//  Constants.swift
//  cozo
//
//  Created by Sierra 4 on 05/05/17.
//  Copyright © 2017 . All rights reserved.


import Foundation
import UIKit
import SwiftyJSON

extension String {
    
    enum FieldType : String{
        case hotspotName = "hotspot name"
        case email = "email"
        case password = "password"
        case mobile = "phone number"
        case description = "description"
        case firstName = "first name"
        case lastName = "last name"
        case bio = "bio"
        case eventName = "event name"
        case startTime = "start time"
        case endTime = "end time"
        case newpassword = " new password"
        case name = "name"
        
        var localized: String {
            return NSLocalizedString(self.rawValue, comment: "")
        }
    }
    
    enum Status : String {
        case empty = "Please enter "
        case allSpaces = "Enter the "
        case valid
        case inValid = " Please enter a valid "
        case allZeros = "Please enter a valid "
        case hasSpecialCharacter = " can only contain A-z, a-z characters only"
        case notANumber = " must be a number "
        case emptyCountrCode = "Enter country code "
        case mobileNumberLength = " Phone number should be of atleast 6 - 15 number"
        case pwd = "Password length should be between 6-15 characters"
        case pinCode = "PinCode length should be 6 characters long"
        case zip = "Pincode should not contain special characters"
        
        func message(type : FieldType) -> String? {
            switch self {
            case .hasSpecialCharacter: return type.rawValue + rawValue
            case .valid: return nil
            case .emptyCountrCode: return rawValue
            case .pwd: return rawValue
            case .mobileNumberLength : return rawValue
            case .pinCode , .zip : return rawValue
            default: return rawValue + type.rawValue
            }
        }
    }
}

enum Alert : String{
    case success = "Success"
    case oops = "Oops"
    case login = "Login Successfull"
    case ok = "Ok"
    case cancel = "Cancel"
    case error = "Alert"
    
    var localized: String {
        return NSLocalizedString(self.rawValue, comment: "")
    }
}

enum NotificationName : String{
    case receivePush = "receivePush"
    case activeApp = "activeApp"
    case refreshChat = "refreshChat"
    case detailPage = "detgailPage"
    case chatPage = "chatPage"
    case tabPage = "tabPage"
    case profileNotification = "profileNotification"
    case hotspotNotification = "hotspotNotification"
}


enum Fire : String{
    case red = "red"
    case orange = "orange"
    case blue = "blue"
    case yellow = "yellow"
    case empty = "empty"
    
    var listingIcon: UIImage {
        switch self {
        case .red: return #imageLiteral(resourceName: "flame-red-listing-icon")
        case .orange: return #imageLiteral(resourceName: "flame-orange-listing-icon")
        case .blue: return #imageLiteral(resourceName: "flame-blue-listing-icon")
        case .yellow: return #imageLiteral(resourceName: "flame-yellow-listing-icon")
        default :  return #imageLiteral(resourceName: "flame-red-listing-icon")
        }
    }
    //return #imageLiteral(resourceName: "flame-red-listing-icon")
    var colour : UIColor{
        switch self {
        case .red: return UIColor.red()
        case .orange: return UIColor.orange()
        case .blue: return UIColor.blue()
        case .yellow: return UIColor.yelow()
        default: return UIColor.red()

        }
    }
    
    var marker : UIImage{
        switch self {
        case .red: return #imageLiteral(resourceName: "flame-red-icon")
        case .orange: return #imageLiteral(resourceName: "flame-orange-icon")
        case .blue: return #imageLiteral(resourceName: "flame-blue-icon")
        case .yellow: return #imageLiteral(resourceName: "flame-yellow-icon")
        default: return #imageLiteral(resourceName: "flame-red-icon")
        }
    }

    var popularity : String{
        switch self {
        case .red: return Popularity.veryPopular.rawValue
        case .orange: return Popularity.popular.rawValue
        case .blue: return Popularity.chill.rawValue
        case .yellow: return Popularity.justIn.rawValue
        default : return Popularity.veryPopular.rawValue
        }
    }
    
    var heart : UIImage{
        switch self {
        case .red: return #imageLiteral(resourceName: "heart_red_icon")
        case .orange: return #imageLiteral(resourceName: "heart_orange_icon")
        case .blue: return #imageLiteral(resourceName: "heart_blue_icon")
        case .yellow: return #imageLiteral(resourceName: "heart_yellow_icon")
        case .empty : return #imageLiteral(resourceName: "heart-empty-icon")
        }
    }
}

enum Popularity : String{
    case veryPopular = "It's Lit"
    case popular = "Popular"
    case chill = "Chill"
    case justIn = "Just in"
    
    var localized: String {
        return NSLocalizedString(self.rawValue, comment: "")
    }
}

enum AlertMessage : String{
    
    case hotspotnotfound = "Hotspot has expired"
    case unblockFirst = "Please Unblock User to send Message"
    case blockedByOther = "You are Blocked by this User"
    case notAllowed = "You are not allowed to change password."
    case enterOldPassword = "Old Password Field is empty"
    case matchConfirmAndNew = "New Password And Confirm Password must be Same"
    case matchOldAndNew = "New Password And Old Password must be Different"
    case pwdChange = "Password changed Successfully"
    case syncSuccess = "To prevent contact from seeing your photos, turn feeds off in app settings"
    case unableToFetchContct = "Unable to Sync Contacts please try again"
    case deleteChat = "Are you sure you want to delete this Chat?"
    case sync = "Please sync your contacts in settings to get feeds of your friends!!"
    case sessionExpired = "Your Session has been expired, You need to Login again"
    case noInternet = "No Internet Connection"
    case allowfeed = "Switch on feeds in settings to see what your friends are up to"
    case noFeeds = "Your contacts haven't been out recently"
    case settingsUpdated = "Settings Updated Successfuly"
    case requiredImage = "You can't delete this Image. Upload another photo before deletion"
    case invalidTime = "Start Time must be less than End Time"
    case invalidEventDuration = "Event Duration can't be more than 24 hrs"
    case invalidEvent = "You cannot create an event for more than 24 hours from the time of event creation."
    case checkinSuccess = "Checkin Successful"
    case checkoutSuccess = "Checkout Successful"
    case imageAddedSuccessfully = "Image Added Successfully"
    case eventCreated = "Event Created Successfully"
    case reportSuccess = "Thank You for reporting this Image"
    case profileUpdate = "Profile Updated Successfully"
    case distanceIssue = "You must be located near the hotspot"
    case linkSend = "Password Sent to Your Mail Successfully"
    case successLogin = "Login Successful"
    case successSignUp = "SignUp Successful"
    case imgHotspot = "Please select a picture"
    case addedHotspotSuccess = "Hotspot Added Successfully"
    case radiusPermission = "To adjust radius, please allow location permission from Settings"
    var localized: String {
        return NSLocalizedString(self.rawValue, comment: "")
    }
  }

enum TitleType : String{
    case writeMessage = "Write your message"
    case version = "Version "
    case n1 = "New chat message"
    case n2 = "Daily Hotspot"
    case n3 = "When someone loves your photo"
    case n4 = "When your photo is popular"
    case n5 = "When someone posts a picture or event to a favorite spot"
    case n6 = "When your contact creates a new hotspot"
    case forceUpdate = "Your app seems to be outdated, you need to update your app"
    case normalUpdate = "New version of app is available. Do you want to update your app?"
    case update = "Update"
    case to =  "  to  "
    case WhrzAt = "WhrzAt"
    case thankYou = "Thank You"
    case syncSuccessfully = "Sync Successful"
    case help = "Help & Support"
    case terms = "Terms & Policy"
    case termsCondition = "Terms & Conditions"
    case noActivity = "No activity"
    case noNotification = " No notifications "
    case noFavourite = " No favorites "
    case tutorialHead1 = "Discover popular hotspots around you"
    case tutorialHead2 = "Checkin, upload and share with contacts "
    case tutorialHead3 = "Check what your friends are up to"
    case tutorialDesc1 = "Explore with WhrzAt to find exciting live activity as well as share places to go "
    case tutorialDesc2 = "Upload photos (which disappear after 12hrs) and create events at any place you wish!"
    case tutorialDesc3 = "See where your friends have been with friends' feed! "
    case login = "Login"
    case no = "NO"
    case yes = "YES"
    case alert = "ALERT!!"
    case removePhoto = "Are you sure you want to delete this image?"
    case remove = "Remove"
    case imagePost = "IMAGE_POST"
    case post = " posted at "
    case noData = "No Data"
    case noImages = "No Images"
    case addPic = "addPic"
    case needCheckin = "You need to Checkin First to add Event at this Hotspot"
    case checkoutAlert = "Are you sure you want to checkout?"
    case eventscount = " Events at this Hotspot"
    case eventcount = " Event at this Hotspot"
    case loves = " Likes"
    case love = " Like "
    case checkinAt = " checked in at "
    case lovesyourphotopostedto = " loves your photo posted to "
    case dailyHotspot = "Since the hottest red flame is very popular today! Check them out! "
    case Yourpictureisverypopularpostedto = " Your picture is popular posted to "
    case postedTo = " posted a new photo at "
    case postedAt = " posted a picture to your favourite place "
    case createNewHotspot = " put a new hotspot on the map. Find out why!"
    case referralReward = "Congratulations! You’ve earned a referral reward!"
    case firstHotspotReward = "Congratulations! You’ve earned a reward for creating your first hotspot "
    case firstPhotoReward = "Congratulations! You’ve earned a reward for posting your first picture "
    case popular = "Popular"
    case addPhoto = "Add Photo"
    case permissionRequired = "Alert"
    case sync = "Sync Your Contacts"
    case Confirmation = "Confirmation"
    case userDeleteOnMsg = "User has been deleted."
    case logoutMsg = "Are you sure you want to log out?"
    case Logout = "Log Out"
    case Cancel = "Cancel"
    case confirm = "Confirm"
    case allowPermission = "Please allow permission from Settings"
    case allowContact = "Please grant WhrzAt permission to sync your contacts.You can prevent your contacts from seeing your photos by turning off \"feeds\" in settings"
    case setting = "Settings"
    case camera = "Camera"
    case gallery = "Gallery"
    case ok = "Ok"
    case chooseCountry = "Choose Country"
    //case locationSetting = "Application can't be run without Location. Please allow location permission from Settings"
    case locationSetting = "Please switch on location services in Settings to get nearby hotspots."

    //case trueVal = "true"
    //case falseVal = "false"
    case FB_ID_NOT_FOUND = "FB_ID_NOT_FOUND"
    case done = "Done"
    case skip = "Skip"
    case Block = "Block"
    case unblock = "Unblock"
    case report = "Report"
    case removeBlocked = "Are you sure you want to remove this user from your Block list?"
    case addBlocked = "Are you sure you want to report this Image?"
    case addToBlocked = "Are you sure you want to Block this user?"
    case deleteProfile = "Do you want to delete profile?"
    case clearNotification = "Are you sure you want to clear all notifications?"
    case delete = "Delete"
    case checkin = "Checkin"
    case checkout = "Checkout"
    
    var localized: String {
        return NSLocalizedString(self.rawValue, comment: "")
    }
 }

enum TimePassed : String{
    //POST TIME
    case hundred = "100"
    case yearAgo = " years ago"
    case oneYearAgo = "1 year ago"
    case lastYear = "Last year"
    case monthAgo = " months ago"
    case oneMonthAgo = "1 month ago"
    case lastMonth = "Last month"
    case weekAgo =  "weeks ago"
    case oneWeekAgo = "1 week ago"
    case lastWeek = "Last week"
    case dayAgo = " days ago"
    case oneDayAgo = "1 day ago"
    case yesterday = "Yesterday"
     case hourAgo = " hours ago"
    case oneHourAgo = "1 hour ago"
    case minAgo = " minutes ago"
    case oneMinAgo = "1 minute ago"
    case secAgo =   " seconds ago"
    case justNow = "Just now"
    //CHAT SCREEN
    case year = " years"
    case oneYear = "1 year"
    case month = " months"
    case oneMonth = "1 month"
    case week =  "weeks"
    case oneWeek = "1 week"
    case day = " days"
    case oneDay = "1 day"
    case hour = " hours"
    case oneHour = "1 hour"
    case min = " min"
    case oneMin = "1 min"
    case sec =   " sec"
    
    var localized: String {
        return NSLocalizedString(self.rawValue, comment: "")
    }
}

struct AppKeys {
    struct AppsFlyer {
        static let DevKey = "aXSD8agVMt6xmLyn2A8fkH"
        static let AppleAppId = "1217538793"
    }
}

prefix operator /
prefix func /(value : String?) -> String {
    return value.unwrap()
}


enum ErrorConstant : String{
    case enterPhoneNumber = "Please enter phone number"
    case networkError = "Network error, please try again!"
    case verificationCode = "Please enter verification code"
    case referralcode = "Please enter referral code"
    case invalidReferral = "Invalid referral code! Please enter valid referral code"
   
    
    var localized: String {
        return NSLocalizedString(self.rawValue, comment: "")
    }
}
