
//
//  Created by Sierra 4 on 24/07/17.
//  Copyright © 2017 Sierra 4. All rights reserved.
//


import Optik
import Kingfisher

internal struct KFImageDownLoader: Optik.ImageDownloader {
    func downloadImage(from url: URL, completion: @escaping ImageDownloaderCompletion) {
        
        KingfisherManager.shared.retrieveImage(with: url, options: nil, progressBlock: nil) { (image, error, cacheType, imgURL) in
            if let img: UIImage = image {
                completion(img)
            }
        }
    }
}

