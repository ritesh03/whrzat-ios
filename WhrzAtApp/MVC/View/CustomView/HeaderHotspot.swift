//
//  HeaderHotspot.swift
//  WhrzAtApp
//
//  Created by Sierra 4 on 03/09/17.
//  Copyright © 2017 com.example. All rights reserved.
//

import UIKit
import EZSwiftExtensions
import CoreLocation

class HeaderHotspot: UIView {
  
    
    //MARK::- OUTLETS
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var imgCheckIn: UIImageView!
    @IBOutlet weak var favBtn: UIButton!
    @IBOutlet weak var favImageView: UIImageView!
    @IBOutlet weak var btnCheckin: UIButton?
    @IBOutlet weak var btnPhotos: UIButton?
    @IBOutlet weak var btnEvent: UIButton?
    @IBOutlet weak var lblCheckin: UILabel?
    @IBOutlet weak var lblPhotos: UILabel?
    @IBOutlet weak var lblAddEvent: UILabel?
     @IBOutlet weak var lblFav: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    private func commonInit(){
        Bundle.main.loadNibNamed("HeaderHotspot", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }
    //MARK::- PROPERTIES
    var data : HotspotData?
    var checkInData : CheckInData?

    var callback : (() -> Void)? = nil
    //MARK::- BUTTON ACTION
    @IBAction func btnCheckinAction(_ sender: UIButton) {
       // sender.isSelected.toggle() vivek
            if lblCheckin?.text == TitleType.checkin.rawValue{
                if isLocationOn(){
                if calculateDistance() ?? true{
                checkInUserAPI(isEvent: false, isPhoto: false , isCheckin : true)
                }else{
                    Alerts.shared.show(alert: .error, message: AlertMessage.distanceIssue.rawValue, type: .info)
                }
              }
            }else{
                checkOutUserAPI()
            }
    }
   
    @IBAction func favButtonTap(_ sender: UIButton) {
     callback!()
       
       
//        if favCount < 11 {
//            favImageView.tintColor = UIColor.blue
//            favImageViewots.image = #imageLiteral(resourceName: "filled_Star")
//        }else if favCount < 20 {
//            favImageView.tintColor = UIColor.yellow
//            favImageView.image = #imageLiteral(resourceName: "filled_Star")
//        }else if favCount < 30 {
//            favImageView.image = #imageLiteral(resourceName: "filled_Star")
//            favImageView.tintColor = UIColor.orange
//        }else if favCount < 40 {
//            favImageView.image = #imageLiteral(resourceName: "filled_Star")
//            favImageView.tintColor = UIColor.red
//        }
    }
    @IBAction func btnPhotosAction(_ sender: UIButton) {
        
        if isLocationOn(){
        
        if calculateDistance() ?? true{
//            if lblCheckin?.text == TitleType.checkin.rawValue{
//                AlertsClass.shared.showAlertController(withTitle: TitleType.Confirmation.rawValue, message: TitleType.needCheckin.rawValue, buttonTitles: [TitleType.checkin.rawValue , TitleType.Cancel.rawValue]) { (tag) in
//                    let type = tag as AlertTag
//                    switch type{
//                    case .yes:
 //                       self.checkInUserAPI(isEvent: false, isPhoto: true , isCheckin : true)
 //
//                        
//                    default:
//                        break
//                    }
//                }
//            }else{
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: TitleType.addPic.rawValue), object: nil)
                
   //         }
        }else{
            Alerts.shared.show(alert: .error, message: AlertMessage.distanceIssue.rawValue, type: .info)
            
        }
        }
    }
  
    @IBAction func btnEventsAction(_ sender: UIButton) {
        
        if isLocationOn(){
        if calculateDistance() ?? true{
                let vc = StoryboardScene.Home.instantiateAddEventVC()
                vc.hotspotId = /self.data?._id
                vc.hotness = /self.data?.hotness
                vc.name = self.data?.name
                ez.topMostVC?.pushVC(vc)
                
        }else{
            Alerts.shared.show(alert: .error, message: AlertMessage.distanceIssue.rawValue, type: .info)
        }
        }
    }
    
    //MARK::- FUNCTIONS
    func calculateDistance() -> Bool?{
        let coordinate0 = CLLocation(latitude: CLLocationDegrees(/UserSingleton.shared.lat) ?? 0.0, longitude: CLLocationDegrees(/UserSingleton.shared.long) ?? 0.0)
        let coordinate1 = CLLocation(latitude: CLLocationDegrees(/data?.location?.last?.description) ?? 0.0, longitude: CLLocationDegrees(/data?.location?.first?.description) ?? 0.0)
        let distanceInMeters = coordinate0.distance(from: coordinate1)
        let distanceInMiles = distanceInMeters/1609.344
        return distanceInMiles <= 0.5
    }
    
    //MARK::- API Checkin
    func checkInUserAPI(isEvent : Bool , isPhoto : Bool , isCheckin : Bool){
        guard let user = UserSingleton.shared.loggedInUser else{return}
        APIManager.shared.request(with: HomeEndpoint.checkin(userId: user._id, hotspotId: /self.data?._id), completion: { [weak self] (res) in
            switch res{
            case .success( _):
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "checkIn"), object: nil)
               
                                
               // guard let response = res as? CheckInDetailModel else{return}
                //self?.checkInData = response.data
                //let dict:[String: Bool] = ["val": true]
                //NotificationCenter.default.post(name: NSNotification.Name(rawValue: TitleType.checkin.rawValue), object: nil , userInfo : dict)
                
                self?.lblCheckin?.text = isCheckin ? TitleType.checkout.rawValue : TitleType.checkin.rawValue
                //Alerts.shared.show(alert: .success, message: AlertMessage.checkinSuccess.rawValue, type: .success)
                
//                if isEvent{
//                    let vc = StoryboardScene.Home.instantiateAddEventVC()
//                    vc.hotspotId = /self?.data?._id
//                    vc.hotness = /self?.data?.hotness
//                    vc.name = self?.data?.name
//                    
//                    
//                    ez.topMostVC?.pushVC(vc)
//                }else if isPhoto{
//                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: TitleType.addPic.rawValue), object: nil)
//                    
//                }
                
            case .failure(let msg):
                Alerts.shared.show(alert: .error, message: msg, type: .info)
            }
            }, isLoaderNeeded: true)
    }
    
    
    //MARK::- API Checkout
    func checkOutUserAPI(){
        self.checkout()
//        AlertsClass.shared.showAlertController(withTitle: TitleType.Confirmation.rawValue, message: TitleType.checkoutAlert.rawValue, buttonTitles: [TitleType.checkout.rawValue , TitleType.Cancel.rawValue]) { (tag) in
//            let type = tag as AlertTag
//            switch type{
//            case .yes:
//                self.checkout()
//            default:
//                break
//            }
//        }
    }
    
    func checkout(){
        guard let user = UserSingleton.shared.loggedInUser else{return}
        APIManager.shared.request(with: HomeEndpoint.checkOut(userId: user._id, hotspotId: /self.data?._id), completion: { [weak self] (res) in
            switch res{
            case .success( _):
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "checkOut"), object: nil)

                //let dict:[String: Bool] = ["val": false]
                // NotificationCenter.default.post(name: NSNotification.Name(rawValue: TitleType.checkin.rawValue), object: nil , userInfo : dict)
                self?.lblCheckin?.text = TitleType.checkin.rawValue
                //Alerts.shared.show(alert: .success, message: AlertMessage.checkoutSuccess.rawValue, type: .success)
                
                
                
            case .failure(let msg):
                Alerts.shared.show(alert: .error, message: msg, type: .info)
            }
            }, isLoaderNeeded: true)
        
    }
    
    func isLocationOn() -> Bool{
        if CLLocationManager.authorizationStatus() == .denied  {
            self.settingsAlert()
            return false
        }else{
            return true
        }
    }
    func settingsAlert(){
        AlertsClass.shared.showAlertController(withTitle: TitleType.permissionRequired.rawValue, message: "To add photos, events or to checkin, please confirm your location by turning on location services in settings", buttonTitles: [TitleType.setting.rawValue , TitleType.Cancel.rawValue]) { (value) in
            let type = value as AlertTag
            switch type {
            case .yes:
                UIApplication.shared.openURL(NSURL(string:UIApplication.openSettingsURLString)! as URL)
            default:
                break
            }
        }
    }

    
}
