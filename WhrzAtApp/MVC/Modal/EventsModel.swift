//
//  EventsModel.swift
//  WhrzAtApp
//
//  Created by Sierra 4 on 18/09/17.
//  Copyright © 2017 com.example. All rights reserved.
//

import UIKit
import ObjectMapper

class EventsModel: Mappable {
    
    var message: String?
    var data: EventData?
    var statusCode: Int?
    
    required init?(map: Map){
    }
    func mapping(map: Map){
        message <- map["message"]
        data <- map["data"]
        statusCode <- map["statusCode"]
    }
}

class EventData: Mappable{
    var _id: String?
    var checkin : [String]?
    var events : [EventArr]?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        _id <- map["_id"]
        checkin <- map["checkedIn"]
        events <- map["events"]
    
    }
}

class EventArr : Mappable{
    var startDate: Double?
    var endDate: Double?
    var name: String?
    var description: String?
    var registrationDate: Int?
    var _id : String?
    
    required init?(map: Map){
    }
    func mapping(map: Map){
        startDate <- map["startDate"]
        endDate <- map["endDate"]
        name <- map["name"]
        description <- map["description"]
        _id <- map["_id"]
        registrationDate <- map["registrationDate"]
    }


}
