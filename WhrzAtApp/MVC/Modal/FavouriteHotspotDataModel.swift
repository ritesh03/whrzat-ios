/* 
Copyright (c) 2020 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
import ObjectMapper

class FavouriteHotspotDataModel : Mappable {
	var statusCode : Int?
	var message : String?
	var data : FavouriteData?

	  required init?(map: Map){
              
          }

	 func mapping(map: Map) {

		statusCode <- map["statusCode"]
		message <- map["message"]
		data <- map["data"]
	}

}

class FavouriteData : Mappable {
    var listFavorite : [ListFavorite]?

     required init?(map: Map){
                 
             }

     func mapping(map: Map) {

        listFavorite <- map["listFavorite"]
    }

}
class ListFavorite : Mappable {
    var _id : String?
    var area : String?
    var createdBy : String?
    var description : String?
    var registrationDate : Int?
    var dummyCountExpiry : Int?
    var dummyCount : Int?
    var allCheckedIn : [String]?
    var favourites : [String]?
    var checkedIn : [String]?
    var eventName : [String]?
    var events : [String]?
    var blocked : Bool?
    var deleted : Bool?
    var tags : [String]?
    var addedBy : String?
    var picture : Picture?
    var name : String?
    var isFavouriteColor : String?
    var hotness : String?
    var isFavouriteCount : Int?

     required init?(map: Map){
                 
             }

     func mapping(map: Map) {

        _id <- map["_id"]
        area <- map["area"]
        createdBy <- map["createdBy"]
        description <- map["description"]
        registrationDate <- map["registrationDate"]
        dummyCountExpiry <- map["dummyCountExpiry"]
        dummyCount <- map["dummyCount"]
        allCheckedIn <- map["allCheckedIn"]
        favourites <- map["favourites"]
        checkedIn <- map["checkedIn"]
        eventName <- map["eventName"]
        events <- map["events"]
        blocked <- map["blocked"]
        deleted <- map["deleted"]
        tags <- map["tags"]
        addedBy <- map["addedBy"]
        picture <- map["picture"]
        name <- map["name"]
        isFavouriteColor <- map["isFavouriteColor"]
        hotness <- map["hotness"]
        isFavouriteCount <- map["isFavouriteCount"]
    }

}

class Picture : Mappable {
    var original : String?
    var thumbnail : String?

     required init?(map: Map){
                 
             }

     func mapping(map: Map) {

        original <- map["original"]
        thumbnail <- map["thumbnail"]
    }

}
