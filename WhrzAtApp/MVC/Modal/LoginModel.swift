//
//  LoginModel.swift
//  WhrzAtApp
//
//  Created by Sierra 4 on 04/09/17.
//  Copyright © 2017 com.example. All rights reserved.
//

import Foundation
import ObjectMapper

class LoginModel: Mappable{
    var message: String?
    var data: RegisterUser?
    var statusCode: Int?
    
    required init?(map: Map){
    }
    func mapping(map: Map){
        message <- map["message"]
        data <- map["data"]
        statusCode <- map["statusCode"]
    }
}

class RegisterUser: Mappable{
    var profilePicURL: ProfilepicUrl?
    var password: String?
    var name: String?
    var accessToken : String?
    var contact: String?
    var isContactVerified:Bool?
    var fromFacebook: Bool?
    var __v: Int?
    var facebookId: String?
    var _id: String?
    var email: String?
    var registrationDate: Int?
    var radius : Double?
    var bio  : String?
    var isFeed : Bool?
    var rewardStatus : Bool?
    var notifications : [String]?
    var contactSync = 0
    var code  : String?
    
    
    required init?(map: Map){
    }
    init() {
        
    }
    func mapping(map: Map){
        profilePicURL <- map["profilePicURL"]
        password <- map["password"]
        name <- map["name"]
        isContactVerified <- map["isContactVerified"]
        contact <- map["contact"]
        fromFacebook <- map["fromFacebook"]
        __v <- map["__v"]
        facebookId <- map["facebookId"]
        _id <- map["_id"]
        email <- map["email"]
        registrationDate <- map["registrationDate"]
        accessToken <- map["accessToken"]
        radius <- map["radius"]
        bio <- map["bio"]
        isFeed <- map["feedsOn"]
        rewardStatus <- map["rewardStatus"]
        notifications <- map["notifications"]
        contactSync <- map["contactSync"]
        code <- map["code"]
    }
}

class ProfilepicUrl: Mappable{
    var thumbnail: String?
    var original: String?
    
    required init?(map: Map){
    }
    func mapping(map: Map){
        thumbnail <- map["thumbnail"]
        original <- map["original"]
    }
}


