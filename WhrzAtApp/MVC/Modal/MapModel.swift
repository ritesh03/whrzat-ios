//
//  MapModel.swift
//  WhrzAtApp
//
//  Created by Sierra 4 on 13/09/17.
//  Copyright © 2017 com.example. All rights reserved.
//

import Foundation
import ObjectMapper

class MapModel: Mappable{
    var message: String?
    var data: MapData?
    var statusCode: Int?
    
    required init?( map: Map){
    }
    func mapping(map: Map){
        message <- map["message"]
        data <- map["data"]
        statusCode <- map["statusCode"]
    }
}



class MapData : Mappable{
    var count : Int?
    var dataMap : [MapListData]?
    var promoteData : [MapListData]?
    required init?( map: Map){
    }
    
    func mapping(map: Map){
        count <- map["count"]
        dataMap <- map["mapData"]
        promoteData <- map["events"]
    }
}


class MapListData: Mappable{
    var picture: ProfilepicUrl?
    var description: String?
    var tags: [String]?
    var registrationDate: Int?
    var __v: Int?
    var location: [Double]?
    var createdBy: String?
    var _id: String?
    var name: String?
    var hotness : String?
    var refund : String?
    var locationName : String?
    var deleted : Bool?
    var isCreatedFromWebsite : Bool?
    var addedBy : String?
    var email : String?
    var organizerName : String?
    var endDate: Double?
    var timeEnd : String?
    var startDate : Double?
    
    required init?( map: Map){
    }
    init() {
        
    }
    
    func mapping(map: Map){
        picture <- map["picture"]
        description <- map["description"]
        tags <- map["tags"]
        registrationDate <- map["registrationDate"]
        __v <- map["__v"]
        location <- map["location"]
        createdBy <- map["createdBy"]
        _id <- map["_id"]
        name <- map["name"]
        hotness <- map["hotness"]
        refund <- map["refund"]
        locationName <- map["locationName"]
        deleted <- map["deleted"]
        isCreatedFromWebsite <- map["isCreatedFromWebsite"]
        addedBy <- map["addedBy"]
        email <- map["email"]
        organizerName <- map["organizerName"]
        endDate <- map["endDate"]
        timeEnd <- map["timeEnd"]
        startDate <- map["startDate"]
    }
}
