//
//  NotificationModel.swift
//  WhrzAtApp
//
//  Created by Sierra 4 on 04/10/17.
//  Copyright © 2017 com.example. All rights reserved.
//
import Foundation
import ObjectMapper

class NotificationModel: Mappable{
    var message: String?
    var data: [String]?
    var statusCode: Int?
    
    required init?(map: Map){
    }
    func mapping(map: Map){
        message <- map["message"]
        data <- map["data"]
        statusCode <- map["statusCode"]
    }
}
