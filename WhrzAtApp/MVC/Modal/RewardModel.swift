//
//  LoginModel.swift
//  WhrzAtApp
//
//  Created by Sierra 4 on 04/09/17.
//  Copyright © 2017 com.example. All rights reserved.
//

import Foundation
import ObjectMapper

class RewardModel: Mappable{
    var data: DataModel?
    var message: String?
    var statusCode: Int?
    
    required init?(map: Map){
    }
    init() {
        
    }
    func mapping(map: Map){
        data <- map["data"]
    }
}

class DataModel: Mappable{

    var finalResponse: FinalResponse?
    
    required init?(map: Map){
    }
    init() {
        
    }
    func mapping(map: Map){
        finalResponse <- map["finalResponse"]
    }
}

class FinalResponse: Mappable{
    var rewardStatus: Bool?
    var locationStatus: Bool?
    var rewardAmount: Int?
    var referralCode : String?
    var redeemRequest : Bool?
    var isRedeem : Bool?
    
    

    required init?(map: Map){
    }
    func mapping(map: Map){
         rewardStatus <- map["rewardStatus"]
         locationStatus <- map["locationStatus"]
         rewardAmount <- map["rewardAmount"]
         referralCode <- map["referralCode"]
         redeemRequest <- map["redeemRequest"]
         isRedeem <- map["isRedeem"]
    }
}


