//
//  HotspotDetailModel.swift
//  WhrzAtApp
//
//  Created by Sierra 4 on 13/09/17.
//  Copyright © 2017 com.example. All rights reserved.
//

import Foundation
import ObjectMapper

class HotspotDetailModel: Mappable{
    var message: String?
    var data: HotspotData?
    var statusCode: Int?
    
    required init?(map: Map){
    }
    func mapping(map: Map){
        message <- map["message"]
        data <- map["data"]
        statusCode <- map["statusCode"]
    }
}

class HotspotData: Mappable{
    var picture: ProfilepicUrl?
    var _id: String?
    var description: String?
    var area: String?
    var registrationDate: Int?
    var checkedIn: [String]?
    var __v: Int?
    var location: [Double]?
    var createdBy: String?
    var images: [Images]?
    var hotness: String?
    var events: [String]?
    var tags: [String]?
    var name: String?
    var count : Int?
    var deleted : Bool?
    var isFavouriteColor : String?
    var isFavouriteCount : Int?
    var isFavourite : Bool?
    var allCheckedIn : [String]?
    required init?( map: Map){
    }
    func mapping(map: Map){
        picture <- map["picture"]
        _id <- map["_id"]
        description <- map["description"]
        area <- map["area"]
        registrationDate <- map["registrationDate"]
        checkedIn <- map["checkedIn"]
        __v <- map["__v"]
        location <- map["location"]
        createdBy <- map["createdBy"]
        images <- map["images"]
        hotness <- map["hotness"]
        events <- map["events"]
        tags <- map["tags"]
        name <- map["name"]
        count <- map["count"]
        deleted <- map["deleted"]
        isFavouriteColor <- map["isFavouriteColor"]
        isFavouriteCount <- map["isFavouriteCount"]
        isFavourite <- map["isFavourite"]
        allCheckedIn <- map["allCheckedIn"]
    }
}

class Images: Mappable{
    
    var eventId: String?
    var picture: ProfilepicUrl?
    var _id: String?
    var __v: Int?
    var createdBy: Createdby?
    var blocked: Int?
    var hotspotId: String?
    var registrationDate: Double?
    var likes : [String]?
    var isLike : Bool?
    var likeCount : Int?
    var addedBy : String?
    
    required init?( map: Map){
    }
    func mapping(map: Map){
        eventId <- map["eventId"]
        picture <- map["picture"]
        _id <- map["_id"]
        __v <- map["__v"]
        createdBy <- map["createdBy"]
        blocked <- map["blocked"]
        hotspotId <- map["hotspotId"]
        registrationDate <- map["registrationDate"]
        likes <- map["likes"]
        isLike <- map["isLiked"]
        addedBy <- map["addedBy"]
    }
}

class Createdby: Mappable{
    var _id: String?
    var name: String?
    var img : ProfilepicUrl?
    var timeStamp : Double?
    var msg : String?
    var deleted : Bool?
    
    
    required init?(map: Map){
    }
    func mapping(map: Map){
        
        _id <- map["_id"]
        name <- map["name"]
        img <- map["profilePicURL"]
        timeStamp <- map["timeStamp"]
        msg <- map["message"]
        deleted <- map["deleted"]

    }
}
