//
//  ProfileNotification.swift
//  WhrzAtApp
//
//  Created by Sierra 4 on 06/10/17.
//  Copyright © 2017 com.example. All rights reserved.
//

import UIKit
import ObjectMapper


    class ProfileNotification: Mappable{
        var message: String?
        var data: [NotificationDataa]?
        var statusCode: Int?
        
        required init?(map: Map){
        }
        func mapping(map: Map){
            message <- map["message"]
            data <- map["data"]
            statusCode <- map["statusCode"]
        }
    }


class NotificationDataa : Mappable{
    var id : String?
    var imageId : Imageid?
    var likedBy : Imageid?
    var userId : Imageid?
    var hotspotId : Hotspotid?
    var hotness : String?
    var event : String?
    var picture: ProfilepicUrl?
    
    required init?(map : Map){
        
    }
    func mapping(map : Map){
        id <- map["id"]
        userId <- map["userId"]
        imageId <- map["imageId"]
        likedBy <- map["likedBy"]
        hotness <- map["hotness"]
        event <- map["event"]
        hotspotId <- map["hotspotId"]

    }
    
}



