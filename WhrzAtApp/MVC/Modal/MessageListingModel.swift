//
//  MessageListingModel.swift
//  WhrzAtApp
//
//  Created by Sierra 4 on 13/10/17.
//  Copyright © 2017 com.example. All rights reserved.
//

import UIKit
import ObjectMapper

class MessageListingModel: Mappable {
    var message: String?
    var data: [MessageDataa]?
    var statusCode: Int?
    
    required init?(map: Map){
    }
    func mapping(map: Map){
        message <- map["message"]
        data <- map["data"]
        statusCode <- map["statusCode"]
    }
}

class MessageDataa : Mappable{
    var message: MessageDetail?
    var pro: Createdby?
    var unread : Int?
    
    required init?(map: Map){
    }
    func mapping(map: Map){
        message <- map["message"]
        pro <- map["pro"]
        unread <- map["unread"]
    }
}

class MessageDetail : Mappable{
    var id: String?
    var message: String?
    var timeStamp : Double?
    var senderDetails : SenderDetail?
    var receiverDetails : SenderDetail?
    
    required init?(map: Map){
    }
    func mapping(map: Map){
        id <- map["_id"]
        message <- map["message"]
        timeStamp <- map["timeStamp"]
        senderDetails <- map["senderId"]
        receiverDetails <- map["receiverId"]
    }
    
}

class SenderDetail : Mappable{
    var name: String?
    var senderId: String?
    var timeStamp : Double?
    var profilePicURL : senderProfilePicDetail?
    
    required init?(map: Map){
    }
    func mapping(map: Map){
        name <- map["name"]
        senderId <- map["_id"]
        timeStamp <- map["timeStamp"]
        profilePicURL <- map["profilePicURL"]
    }
    
}
class senderProfilePicDetail : Mappable {
    var thumbnail : String?
    var original : String?
    required init?(map: Map){
    }
    func mapping(map: Map){
        thumbnail <- map["thumbnail"]
        original <- map["original"]
    }
    
}
