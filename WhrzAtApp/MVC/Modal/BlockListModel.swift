//
//  BlockListModel.swift
//  WhrzAtApp
//
//  Created by Sierra 4 on 04/10/17.
//  Copyright © 2017 com.example. All rights reserved.
//

import UIKit
import ObjectMapper

class BlockListModel: Mappable {
    
    var data : [Blockedby]?
    var message : String?
    var statusCode : Int?
    
    required init?( map: Map){
    }
    func mapping(map: Map){
        message <- map["message"]
        data <- map["data"]
        statusCode <- map["statusCode"]
    }

}


class Blockedby: Mappable{
    var profilePicURL: ProfilepicUrl?
    var _id: String?
    var name: String?
    
    required init?(map: Map){
    }
    func mapping(map: Map){
        profilePicURL <- map["profilePicURL"]
        _id <- map["_id"]
        name <- map["name"]
    }
}

    
    
