//
//  FeedModel.swift
//  WhrzAtApp
//
//  Created by Sierra 4 on 19/09/17.
//  Copyright © 2017 com.example. All rights reserved.
//

import UIKit
import ObjectMapper

class FeedModel: Mappable {
    var message: String?
    var data: [FeedData]?
    var statusCode: Int?
    
    required init?( map: Map){
    
    }
    
    func mapping(map: Map){
        message <- map["message"]
        data <- map["data"]
        statusCode <- map["statusCode"]
    }
}

class FeedData: Mappable{
    var hotspotId: Hotspotid?
    var imageId: Imageid?
    var __v: Int?
    var createdBy: Createdby?
    var _id: String?
    var type: String?
    var registrationDate: Int?
    var isLike  : Bool?
    
    required init?(map: Map){
    }
    func mapping(map: Map){
        hotspotId <- map["hotspotId"]
        imageId <- map["imageId"]
        __v <- map["__v"]
        createdBy <- map["createdBy"]
        _id <- map["_id"]
        type <- map["type"]
        registrationDate <- map["registrationDate"]
        isLike <- map["isLiked"]

    }
}

class Hotspotid: Mappable{
    var _id: String?
    var name: String?
    var area: String?
    var hotness: String?
    var deleted : Bool?
    var picture: ProfilepicUrl?
    
    required init?(map: Map){
    }
    func mapping(map: Map){
        _id <- map["_id"]
        name <- map["name"]
        area <- map["area"]
        hotness <- map["hotness"]
        deleted <- map["deleted"]
        picture <- map["picture"]
    }
}

class Imageid: Mappable{
    var picture: ProfilepicUrl?
    var _id: String?
    var registrationDate: Int?
    var likes : [String]?
    var isLike : Bool?
    var likeCount : Int?
    var name : String?
    var pic : ProfilepicUrl?
    
    required init?(map: Map){
    
    }
    
    func mapping(map: Map){
        picture <- map["picture"]
        _id <- map["_id"]
        registrationDate <- map["registrationDate"]
        likes <- map["likes"]
        name <- map["name"]
        pic <- map["profilePicURL"]
        isLike <- map["isLiked"]
    }

}
