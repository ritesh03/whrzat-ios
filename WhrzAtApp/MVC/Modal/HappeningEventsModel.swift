/* 
Copyright (c) 2020 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
import ObjectMapper

class HappeningEventsModel : Mappable {
	var statusCode : Int?
	var message : String?
	var data : HappeningData?

	required init?(map: Map) {

	}

    func mapping(map: Map) {

		statusCode <- map["statusCode"]
		message <- map["message"]
		data <- map["data"]
	}

}

class CreatedBy : Mappable {
    var _id : String?
    var password : String?
    var timeZone : Int?
    var profilePicURL : ProfilePicURL?
    var registrationDate : Int?
    var allFavouritesOfUsers : [String]?
    var maxFavourites : Int?
    var notifications : [String]?
    var blockedContacts : [String]?
    var blockedBy : [String]?
    var activeChats : [String]?
    var feeds : [String]?
    var loves : Int?
    var radius : Int?
    var facebookId : String?
    var feedsOn : Bool?
    var isContactVerified : Bool?
    var isAdminCreated : Bool?
    var fromFacebook : Bool?
    var deviceId : String?
    var deviceType : String?
    var bio : String?
    var blocked : Bool?
    var deleted : Bool?
    var code : String?
    var contact : String?
    var email : String?
    var name : String?
    var __v : Int?
    var accessToken : String?
    var location : [Double]?

    required init?(map: Map) {

    }

     func mapping(map: Map) {

        _id <- map["_id"]
        password <- map["password"]
        timeZone <- map["timeZone"]
        profilePicURL <- map["profilePicURL"]
        registrationDate <- map["registrationDate"]
        allFavouritesOfUsers <- map["allFavouritesOfUsers"]
        maxFavourites <- map["maxFavourites"]
        notifications <- map["notifications"]
        blockedContacts <- map["blockedContacts"]
        blockedBy <- map["blockedBy"]
        activeChats <- map["activeChats"]
        feeds <- map["feeds"]
        loves <- map["loves"]
        radius <- map["radius"]
        facebookId <- map["facebookId"]
        feedsOn <- map["feedsOn"]
        isContactVerified <- map["isContactVerified"]
        isAdminCreated <- map["isAdminCreated"]
        fromFacebook <- map["fromFacebook"]
        deviceId <- map["deviceId"]
        deviceType <- map["deviceType"]
        bio <- map["bio"]
        blocked <- map["blocked"]
        deleted <- map["deleted"]
        code <- map["code"]
        contact <- map["contact"]
        email <- map["email"]
        name <- map["name"]
        __v <- map["__v"]
        accessToken <- map["accessToken"]
        location <- map["location"]
    }

}
class HappeningData : Mappable {
    var imageData : [ImageData]?
    var count : Int?

    required init?(map: Map) {

    }

     func mapping(map: Map) {

        imageData <- map["imageData"]
        count <- map["count"]
    }

}
class HotspotId : Mappable {
    var _id : String?
    var area : String?
    var createdBy : String?
    var description : String?
    var location : [Double]?
    var registrationDate : Int?
    var dummyCountExpiry : Int?
    var dummyCount : Int?
    var allCheckedIn : [String]?
    var favourites : [String]?
    var checkedIn : [String]?
    var eventName : [String]?
    var events : [String]?
    var hotness : String?
    var blocked : Bool?
    var deleted : Bool?
    var tags : [String]?
    var addedBy : String?
    var picture : EventPicture?
    var name : String?
    var __v : Int?
   
  
    required init?(map: Map) {

    }

     func mapping(map: Map) {

        _id <- map["_id"]
        area <- map["area"]
        createdBy <- map["createdBy"]
        description <- map["description"]
        location <- map["location"]
        registrationDate <- map["registrationDate"]
        dummyCountExpiry <- map["dummyCountExpiry"]
        dummyCount <- map["dummyCount"]
        allCheckedIn <- map["allCheckedIn"]
        favourites <- map["favourites"]
        checkedIn <- map["checkedIn"]
        eventName <- map["eventName"]
        events <- map["events"]
        hotness <- map["hotness"]
        blocked <- map["blocked"]
        deleted <- map["deleted"]
        tags <- map["tags"]
        addedBy <- map["addedBy"]
        picture <- map["picture"]
        name <- map["name"]
        __v <- map["__v"]
      
     
    }

}
class Likes: Mappable {
   required init?(map: Map) {

      }
    func mapping(map: Map) {
        
    }
}
class ImageData : Mappable {
    var _id : String?
    var createdBy : CreatedBy?
    var hotspotId : HotspotId?
    var expiry : Int?
    var registrationDate : Int?
    var picture : EventPicture?
    var isLikedCount : Int?
      var likes : [Likes]?
 var isLiked : Bool?
    required init?(map: Map) {

    }

     func mapping(map: Map) {

        _id <- map["_id"]
        createdBy <- map["createdBy"]
        hotspotId <- map["hotspotId"]
        expiry <- map["expiry"]
        registrationDate <- map["registrationDate"]
        picture <- map["picture"]
        isLikedCount <- map["isLikedCount"]
             likes <- map["likes"]
          isLiked <- map["isLiked"]
    }

}
class EventPicture : Mappable {
    var thumbnail : String?
    var original : String?

    required init?(map: Map) {

    }

     func mapping(map: Map) {

        thumbnail <- map["thumbnail"]
        original <- map["original"]
    }

}
class ProfilePicURL : Mappable {
    var thumbnail : String?
    var original : String?

    required init?(map: Map) {

    }

     func mapping(map: Map) {

        thumbnail <- map["thumbnail"]
        original <- map["original"]
    }

}
