//
//  HotspotImagesModel.swift
//  WhrzAtApp
//
//  Created by Sierra 4 on 25/09/17.
//  Copyright © 2017 com.example. All rights reserved.
//

import UIKit
import ObjectMapper

class HotspotImagesModel: Mappable {
        var message: String?
        var data: [ImagesData]?
        var statusCode: Int?
        
        required init?(map: Map){
            
        }
    
        func mapping(map: Map){
            message <- map["message"]
            data <- map["data"]
            statusCode <- map["statusCode"]
        }
}


class ImagesData: Mappable{
    var picture: ProfilepicUrl?
    var hotspotId: String?
    var registrationDate: Int?
    var __v: Int?
    var createdBy: String?
    var _id: String?
    var blocked: Int?
    
    required init?(map: Map){
    }
    func mapping(map: Map){
        picture <- map["picture"]
        hotspotId <- map["hotspotId"]
        registrationDate <- map["registrationDate"]
        __v <- map["__v"]
        createdBy <- map["createdBy"]
        _id <- map["_id"]
        blocked <- map["blocked"]
    }
}
