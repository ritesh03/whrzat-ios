//
//  VersioningModel.swift
//  WhrzAtApp
//
//  Created by Sierra 4 on 10/11/17.
//  Copyright © 2017 com.example. All rights reserved.
//

import Foundation
import ObjectMapper




class VersioningModel: Mappable{
    var message: String?
    var data: VersioningData?
    var statusCode: Int?
    
    required init?(map: Map){
    }
    func mapping(map: Map){
        message <- map["message"]
        data <- map["data"]
        statusCode <- map["statusCode"]
    }
}


class VersioningData: Mappable{
    var _id: String?
    var latestIOSVersion : String?
    var criticalIOSVersion : String?
    
    required init?(map: Map){
    }
    func mapping(map: Map){
        _id <- map["_id"]
        latestIOSVersion <- map["latestIOSVersion"]
        criticalIOSVersion <- map["criticalIOSVersion"]
    }
}


