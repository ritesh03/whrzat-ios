//
//  ChatHistoryModel.swift
//  WhrzAtApp
//
//  Created by Sierra 4 on 12/10/17.
//  Copyright © 2017 com.example. All rights reserved.
//

import Foundation
import ObjectMapper

class ChatHistoryModel : Mappable{
        var message: String?
        var data: MessageData?
        var statusCode: Int?
        
        required init?(map: Map){
        }
        func mapping(map: Map){
            message <- map["message"]
            data <- map["data"]
            statusCode <- map["statusCode"]
        }
}


class MessageData  : Mappable {
    var msg : [Message]?
    var detail : Createdby?
    var blocked : Bool?
    var blockedBy : Bool?
    
    
    required init?(map: Map){
    }
    func mapping(map: Map){
        msg <- map["arr"]
        detail <- map["profile"]
        blocked <- map["blocked"]
        blockedBy <- map["blockedBy"]
    }
    
}


class Message : Mappable{
    var from : String?
    var to : String?
    var msg : String?
    var time : Double?
    var id : String?
    
    required init?(from_ : String? , to_ : String? , msg_ : String? , time_ : Double?)
    {
        from = from_
        to = to_
        msg = msg_
        time = time_
        id = ""
    }
    required init?(map: Map){
        
    }
    func mapping(map: Map){
        from <- map["senderId"]
        to <- map["receiverId"]
        msg <- map["message"]
        time <- map["timeStamp"]
        id <- map["_id"]
        
    }
}

    class NotificationCount : Mappable{
        
        var count : Int?
        var _id : String?
        var blocked : Bool?
        
        required init?(count_ : Int?)
        {
            count = count_
        }
        required init?(id_ : String? , blocked_ : Bool?)
        {
            _id = id_
            blocked = blocked_
        }
        required init?(map: Map){
            
        }
        func mapping(map: Map){
            count <- map["count"]
            _id <- map["_id"]
            blocked <- map["blocked"]
            
        }

        
    }
    








