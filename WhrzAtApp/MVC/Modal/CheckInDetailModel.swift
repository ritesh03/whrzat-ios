//
//  CheckInDetailModel.swift
//  WhrzAtApp
//
//  Created by Ritesh chopra on 07/08/23.
//  Copyright © 2023 com.example. All rights reserved.
//

import Foundation
import ObjectMapper

class CheckInDetailModel: Mappable{
    var message: String?
    var data: [CheckInData]?
    var statusCode: Int?
    
    required init?(map: Map){
    }
    func mapping(map: Map){
        message <- map["message"]
        data <- map["data"]
        statusCode <- map["statusCode"]
    }
}

class CheckInData: Mappable{
    var hotness: String?
  
    required init?( map: Map){
    }
    func mapping(map: Map){
       hotness <- map["hotness"]

  }
}

