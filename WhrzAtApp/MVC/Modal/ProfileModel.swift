//
//  ProfileModel.swift
//  WhrzAtApp
//
//  Created by Sierra 4 on 05/10/17.
//  Copyright © 2017 com.example. All rights reserved.
//

import Foundation
import ObjectMapper

class ProfileModel: Mappable{
    var message: String?
    var data: ProfileData?
    var statusCode: Int?
    
    required init?( map: Map){
    }
    func mapping(map: Map){
        message <- map["message"]
        data <- map["data"]
        statusCode <- map["statusCode"]
    }
}

class ProfileData: Mappable{
    var profilePicURL: ProfilepicUrl?
    var password: String?
    var name: String?
    var accessToken: String?
    var bio: String?
    var registrationDate: Int?
    var fromFacebook: Int?
    var notifications: [String]?
    var contact: String?
    var radius: Int?
    var loves: Int?
    var __v: Int?
    var images: [FeedData]?
    var facebookId: String?
    var _id: String?
    var email: String?
    var feedsOn: Int?
    var blockedBy : [String]?

    
    required init?(map: Map){
    }
    func mapping(map: Map){
        profilePicURL <- map["profilePicURL"]
        password <- map["password"]
        name <- map["name"]
        accessToken <- map["accessToken"]
        bio <- map["bio"]
        registrationDate <- map["registrationDate"]
        fromFacebook <- map["fromFacebook"]
        notifications <- map["notifications"]
        contact <- map["contact"]
        radius <- map["radius"]
        loves <- map["loves"]
        __v <- map["__v"]
        images <- map["images"]
        images <- map["images"]
        facebookId <- map["facebookId"]
        _id <- map["_id"]
        email <- map["email"]
        feedsOn <- map["feedsOn"]
        blockedBy <- map["blockedBy"]
    }
}

class ImagesArr: Mappable{
    var hotspotId: Hotspotid?
    var isLiked: Bool?
    var imageId: Imageid?
    var __v: Int?
    var createdBy: Createdby?
    var _id: String?
    var type: String?
    var registrationDate: Int?
    
    required init?(map: Map){
    }
    func mapping(map: Map){
        hotspotId <- map["hotspotId"]
        isLiked <- map["isLiked"]
        imageId <- map["imageId"]
        __v <- map["__v"]
        createdBy <- map["createdBy"]
        _id <- map["_id"]
        type <- map["type"]
        registrationDate <- map["registrationDate"]
    }
}
