//
//  ChatVC.swift
//  WhrzAtApp
//
//  Created by Sierra 4 on 31/08/17.
//  Copyright © 2017 com.example. All rights reserved.
//

import UIKit

class ChatVC: BaseViewController , UISearchBarDelegate {

    //MARK::- OUTLETS
    
    @IBOutlet weak var searchBar: UISearchBar?
    @IBOutlet weak var tableView: UITableView?
    @IBOutlet weak var lblNoData: UILabel!
    
    //MARK::- PROPERTIES
    var tableViewDataSource : ChatListDataSource?
    var arrData : [MessageDataa] = []
    var arrSearch : [MessageDataa] = []
    var isFirst = true
    
    //MARK::- LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.getDataFromAPI), name: NSNotification.Name(rawValue: NotificationName.refreshChat.rawValue), object: nil)
        searchBar?.delegate = self
        searchBar?.showsCancelButton = false
        searchBar?.searchTextField.backgroundColor = UIColor(red: 232/255, green: 230/255, blue: 230/255, alpha: 1.0)
         searchBar?.delegate = self
         let textFieldInsideUISearchBar =  searchBar?.value(forKey: "searchField") as? UITextField
         textFieldInsideUISearchBar?.textColor = UIColor(red: 25/255, green: 26/255, blue: 25/255, alpha: 1.0)
         if let textFieldInsideSearchBar =  searchBar?.value(forKey: "searchField") as? UITextField,
            let glassIconView = textFieldInsideSearchBar.leftView as? UIImageView {
             //Magnifying glass
             glassIconView.image = UIImage(named: "ic_search")?.withRenderingMode(.alwaysTemplate)
             glassIconView.tintColor = UIColor(red: 25/255, green: 26/255, blue: 25/255, alpha: 1.0)
         }
        configureTableView()
       // manageReceiver()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
         getDataFromAPI()
    }
}

//MARK::- SEARCHBAR DELGATE
extension ChatVC{
    func searchBarSearchButtonClicked( _ searchBar: UISearchBar){
        self.view.endEditing(true)
        let text =  searchBar.text
        filterContentForSearchText(searchText: /text)
    }
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = true
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        let text =  searchText      //added
        filterContentForSearchText(searchText: /text)//added
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
        searchBar.text = ""
        filterContentForSearchText(searchText: "")

    }
}

//MARK::- EXTENSION CONFIGURE TABLEVIEW
extension ChatVC{
    func configureTableView() {
        
        tableViewDataSource = ChatListDataSource(items: arrSearch , height: UITableView.automaticDimension, estimatedHeight: 106, tableView: tableView, cellIdentifier: CellIdentifiers.ChatCell.rawValue, configureCellBlock: {(cell, item, indexpath) in
            let cell = cell as? ChatCell
            guard let item = item as? MessageDataa else {return}
            cell?.configureCell(data : item)
            
        }, aRowSelectedListener: { (indexPath) in
            self.didSelect(index: indexPath.row)
            
        }, willDisplayCell: { (indexPath) in
        } , deleteBlock : { (indexpath) in
            self.deleteFunc(index : indexpath.row)
            
        })
        
        tableView?.delegate = tableViewDataSource
        tableView?.dataSource = tableViewDataSource
        tableView?.reloadData()
    }
    
    func didSelect(index : Int){
        let vc = StoryboardScene.Home.instantiateChatUserVC()
        
        if UserSingleton.shared.loggedInUser?._id == /self.arrSearch[index].message?.receiverDetails?.senderId {
            vc.id = /self.arrSearch[index].message?.senderDetails?.senderId
            vc.name = /self.arrSearch[index].message?.senderDetails?.name
            vc.img = /self.arrSearch[index].message?.senderDetails?.profilePicURL?.thumbnail
        }else{
            vc.id = /self.arrSearch[index].message?.receiverDetails?.senderId
            vc.name = /self.arrSearch[index].message?.receiverDetails?.name
            vc.img = /self.arrSearch[index].message?.receiverDetails?.profilePicURL?.thumbnail
        }
        
        pushVC(vc)
    }
    
    func deleteFunc(index : Int){
        self.deleteChat(index : index)
    }
}

//MARK::- API
extension ChatVC{
   @objc func getDataFromAPI(){
        guard let user = UserSingleton.shared.loggedInUser else {return}
        APIManager.shared.request(with: HomeEndpoint.messageListing(id: /user._id, skip: "0", limit: "0"), completion: { [weak self] (response) in
            switch response{
            case .success(let res):
                guard let res = res as? MessageListingModel else {return}
                self?.arrData = res.data ?? []
                self?.arrSearch = self?.arrData ?? []
                self?.tableViewDataSource?.items = self?.arrSearch
                self?.lblNoData.isHidden = (self?.arrSearch.count ?? 0) > 0
                self?.isFirst = false
                self?.tableView?.reloadData()
                
            case .failure(let msg):
                Alerts.shared.show(alert: .error, message: msg, type: .info)

            }
        }, isLoaderNeeded: isFirst)
    }
    
    func deleteChat(index : Int){
        guard let user = UserSingleton.shared.loggedInUser else {return}
        APIManager.shared.request(with: HomeEndpoint.deleteChat(id: /user._id , otherUser : /self.arrSearch[index].message?.receiverDetails?.senderId), completion: { [weak self] (response) in
            switch response{
            case .success(_):
                self?.arrSearch.remove(at: index)
                self?.tableViewDataSource?.items = self?.arrSearch ?? []
                self?.tableView?.reloadData()
                self?.lblNoData.isHidden = (self?.arrSearch.count ?? 0) > 0

            case .failure(let msg):
                Alerts.shared.show(alert: .error, message: msg, type: .info)
            }
        }, isLoaderNeeded: true)
    }
}


//MARK::- SEARCHING FUNCTiON
extension ChatVC{
    func filterContentForSearchText(searchText: String, scope: String = "All") {
        if searchText != "" {
            self.arrSearch = arrData.filter { arr in
                var str = ""
                if UserSingleton.shared.loggedInUser?._id == arr.message?.receiverDetails?.senderId {
                    if let name = arr.message?.senderDetails?.name {
                        str = name
                    }
                   else{ return true}
                }else{
                    if let name = arr.message?.receiverDetails?.name {
                         str = name
                     }
                    else{ return true}
                }
                
              
                     return str.lowercased().contains(searchText.lowercased())
                
               
            }
        }else { self.arrSearch = self.arrData }
        
        self.tableViewDataSource?.items = self.arrSearch
        self.tableView?.reloadData()
        self.lblNoData?.isHidden =  self.arrSearch.count > 0
    }

}

