
//
//  ExtensionChatUser.swift
//  WhrzAtApp
//
//  Created by Sierra 4 on 06/11/17.
//  Copyright © 2017 com.example. All rights reserved.
//

import EZSwiftExtensions
import UIKit

//MARK::- KEYBOARD MANAGE
extension ChatUserVC{
    func registerForKeyboardNotifications(){
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWasShown(notification:)), name: UIApplication.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillBeHidden(notification:)), name: UIApplication.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWasShown(notification: NSNotification){
        if let userInfo = notification.userInfo {
            if let keyboardSize = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect {
                UIView.animate(withDuration: 0.1, animations: {
                    self.bottomConstraint?.constant = keyboardSize.height
                    self.view.layoutIfNeeded()
                    self.scrollToLast()
                })
            }
        }
    }
    
    @objc func keyboardWillBeHidden(notification: NSNotification){
        bottomConstraint?.constant = 0
        
    }
    
    func scrollToLast() {
        ez.runThisInMainThread {  [weak self] in
            guard let count = self?.tableViewDataSource?.items?.count , count > 0 else { return }
            let indexPath = IndexPath(row: count - 1, section: 0)
            self?.tableView?.scrollToRow(at: indexPath, at: .middle, animated: false)
            
        }
    }
    
    //MARK::- GET MESSAGES
    func getMessages(){
        guard let user = UserSingleton.shared.loggedInUser else {return}
        APIManager.shared.request(with: HomeEndpoint.chatHistory(sender: /user._id, receiver: /self.id, skip: "\(skip)", limit: "\(offset)"), completion: { [weak self] (res) in
            switch res{
                
            case .failure(let msg):
                if msg == "User does not exists" {
                    
                    
                    AlertsClass.shared.showAlertController(withTitle: TitleType.alert.rawValue, message: TitleType.userDeleteOnMsg.rawValue, buttonTitles: [TitleType.ok.rawValue]) { (tag) in
                            let type = tag as AlertTag
                            switch type{
                                case .yes:
                                    self!.popVC()
                                default:
                                break
                                
                            }
                        }
                    
                }else{
                    Alerts.shared.show(alert: .error, message: msg, type: .info)
                }
                
                
                self?.refreshControl.endRefreshing()
                
            case.success(let res):
                guard let res = res as? ChatHistoryModel else {return}
                self?.blocked = res.data?.blocked
                self?.blockedby = res.data?.blockedBy
                self?.skip = (self?.skip ?? 0) + (self?.offset ?? 0)
                if (res.data?.msg?.isEmpty ?? true){
                    self?.length = 1
                }
                for item in res.data?.msg ?? []{
                    self?.arrDataChat.insert(item, at: 0)
                }
                self?.lblName?.text = /res.data?.detail?.name
                if (self?.blocked ?? false || self?.blockedby ?? false){
                    self?.imgUser?.setImage(#imageLiteral(resourceName: "ic_profile"), for: .normal)
                    self?.imgUser?.isUserInteractionEnabled = false
                }else{
                    self?.imgUser?.kf.setImage(with: URL(string : /res.data?.detail?.img?.original), for: .normal, placeholder: #imageLiteral(resourceName: "ic_profile"), options: nil, progressBlock: nil, completionHandler: nil)
                    self?.imgUser?.isUserInteractionEnabled = true
                }
                //self?.lblNoData.isHidden = (self?.arrDataChat.count ?? 0) > 0
                self?.tableViewDataSource?.items = self?.arrDataChat
                self?.tableView?.reloadData()
                self?.refreshControl.endRefreshing()
                if !(self?.isPullDown ?? false){
                    guard let count = self?.tableViewDataSource?.items?.count , count > 0 else { return }
                    let indexPath = IndexPath(row: count - 1, section: 0)
                    self?.tableView?.scrollToRow(at: indexPath, at: .middle, animated: false)
                    self?.isPullDown = true
                }
            }
            }, isLoaderNeeded: true)
        isFirst = false
    }
    
    //MARK::- RECEIVE SOCKET
    @objc func manageSocketReceiver(){
        manageBlockNotification()
        SocketIOManager.sharedInstance.recieveMessage(completionHandler: { [weak self] (res) in
            guard let res = res else {return }
            guard let vc = ez.topMostVC else {return}
            if !(vc is ChatUserVC){
                return
            }
            //print(res)
            if /res.from != /self?.id{return}
            var dict = [String : Any]()
            dict["senderId"] = /self?.id
            dict["receiverId"] = /UserSingleton.shared.loggedInUser?._id
            SocketIOManager.sharedInstance.seen(dict)
            self?.arrDataChat.append(res)
            self?.tableViewDataSource?.items?.append(res)
            //            if self?.arrDataChat.count == 1{
            self?.tableView?.reloadData()
            //            }else{
            //self?.tableView?.beginUpdates()
            //                self?.tableView?.insertRows(at:[IndexPath.init(row: ((self?.arrDataChat.count ?? 1) - 1) , section: 0)], with: .none)
            //self?.tableView?.endUpdates()
            //            }
            //            self?.view.layoutIfNeeded()
            self?.scrollToLast()
            //self?.lblNoData.isHidden = true
            }, failure: { (msg) in
                //  print(msg)
        })
    }
    
    func manageBlockNotification(){
        SocketIOManager.sharedInstance.receiveBlocked(completionHandler: { [weak self] (res) in
            guard let res = res else {return}
            if (self?.id == res._id){
                self?.blockedby = res.blocked
                if (res.blocked ?? false){
                    self?.imgUser?.setImage(#imageLiteral(resourceName: "ic_profile"), for: .normal)
                    self?.imgUser?.isUserInteractionEnabled = false
                }else{
                    self?.imgUser?.kf.setImage(with: URL(string : /self?.img ), for: .normal, placeholder: #imageLiteral(resourceName: "ic_profile"), options: nil, progressBlock: nil, completionHandler: nil)
                    self?.imgUser?.isUserInteractionEnabled = true
                    
                }
            }
        }) { (msg) in
            //  print(msg)
        }
    }
}


