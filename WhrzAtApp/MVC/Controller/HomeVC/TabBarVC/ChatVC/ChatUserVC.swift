//
//  ChatUserVC.swift
//  WhrzAtApp
//
//  Created by Sierra 4 on 03/09/17.
//  Copyright © 2017 com.example. All rights reserved.

import UIKit
import GrowingTextView
import IQKeyboardManager
import EZSwiftExtensions
import Alamofire
import Optik
import ESPullToRefresh
import IBAnimatable

class ChatUserVC: BaseViewController,UITextViewDelegate {
    
    //MARK::- OUTLETS
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var tableView: UITableView?
    @IBOutlet weak var txtMsg: GrowingTextView?
    @IBOutlet weak var imgUser: UIButton?
    @IBOutlet weak var lblName: UILabel?
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint?
    @IBOutlet weak var lblNoData: UILabel!
    @IBOutlet weak var btnSend: AnimatableButton?
    
    //MARK::- PROPERTIES
    var tableViewDataSource : CustomTableDatasource?
    var arrDataChat : [Message] = []
    var id : String?
    var name : String?
    var img : String?
    var blocked : Bool?
    var blockedby : Bool?
    var fromProfile = false
    var isFirst = true
    let refreshControl = UIRefreshControl()
    var isPullDown = false
    var skip = 0
    var offset = 20
    var length = 0
    //var isPush = false
    
    //MARK::- LIFECYCLE
    override func viewDidLoad() {

        if UITraitCollection.current.userInterfaceStyle == .dark {
//            backgroundImageView.alpha = 0.3
//            backgroundImageView.image = UIImage(named: "background_chat_d")
            backgroundImageView.image = UIImage(named: "background_chat")
            backgroundImageView.alpha = 0.3
            }
            else {
                backgroundImageView.image = UIImage(named: "background_chat")
                backgroundImageView.alpha = 1.0
            }
        
        refreshControl.addTarget(self, action: #selector(ChatUserVC.refreshData), for: UIControl.Event.valueChanged)
        tableView?.addSubview(refreshControl)
        super.viewDidLoad()
        configureTableView()
        registerForKeyboardNotifications()
        manageSocketReceiver()
       // NotificationCenter.default.addObserver(self, selector: #selector(self.pullToRefresh), name: NSNotification.Name(rawValue: NotificationName.chatPage.rawValue), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.manageSocketReceiver), name: NSNotification.Name(rawValue: "chatAgain"), object: nil)
        
        txtMsg?.text = "Write your message"
        txtMsg?.textColor = UIColor.lightGray
        txtMsg?.delegate = self
        txtMsg?.contentInset = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 0)
    }
   
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        IQKeyboardManager.shared().isEnableAutoToolbar = true
        IQKeyboardManager.shared().isEnabled = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        isPullDown = false
        self.navigationController?.isNavigationBarHidden = true

        IQKeyboardManager.shared().isEnabled = false
        IQKeyboardManager.shared().isEnableAutoToolbar = false
        lblName?.text = /name
        self.imgUser?.setImage(#imageLiteral(resourceName: "ic_profile"), for: .normal)
        pullToRefresh()
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if txtMsg?.textColor == UIColor.lightGray {
            txtMsg?.text = nil
            txtMsg?.textColor = UIColor(named: "AppTextColor")
            //
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if (txtMsg?.text.isEmpty)! {
            txtMsg?.text = "Write your message"
            txtMsg?.textColor = UIColor.lightGray
        }
    }
    
    //MARK::- BUTTON ACTION
    @IBAction func btnActionBack(_ sender: Any) {
        self.view.endEditing(true)
        popVC()
    }
    
    @IBAction func btnActionImage(_ sender: Any) {
        self.view.endEditing(true)
        let imageDownLoader = KFImageDownLoader()
        var imageURLs = [URL]()
        guard let url = URL(string : /self.img) else {return}
        imageURLs.append(url)
        let destVC = Optik.imageViewer(withURLs: imageURLs, initialImageDisplayIndex: 0, imageDownloader: imageDownLoader, activityIndicatorColor: .white, dismissButtonImage: #imageLiteral(resourceName: "close.png"), dismissButtonPosition: .topTrailing)
        self.presentVC(destVC)
    }
    
    @IBAction func btnActionAbout(_ sender: Any) {
        
        let vc = StoryboardScene.Home.instantiateOtherUserProfileVC()
        vc.id = /self.id
        self.view.endEditing(true)
        self.pushVC(vc)
        
    }
    
    @IBAction func btnActionSend(_ sender: Any) {
        if (txtMsg?.text.isBlank)! || (txtMsg?.text.isEmpty)! || txtMsg?.text == TitleType.writeMessage.rawValue{
            btnSend?.animate(.pop(repeatCount: 1))
            return
        }
        if !Alamofire.NetworkReachabilityManager()!.isReachable {
            Alerts.shared.show(alert: .error, message: AlertMessage.noInternet.rawValue, type: .error)
            return}
        if (self.blocked ?? false){
            Alerts.shared.show(alert: .error, message: AlertMessage.unblockFirst.rawValue, type: .info)
            return
        }
        if (self.blockedby ?? false){
            Alerts.shared.show(alert: .error, message: AlertMessage.blockedByOther.rawValue, type: .info)
            return
        }
        let msg = /txtMsg?.text.trimmed()
        guard let message = Message.init(from_: /UserSingleton.shared.loggedInUser?._id, to_: /self.id, msg_: msg, time_: (Date().timeIntervalSince1970 * 1000)) else{ return }
        self.arrDataChat.append(message)
        tableViewDataSource?.items?.append(message)
//        if self.arrDataChat.count == 1{
            tableView?.reloadData()
//        }else{
//            //tableView?.beginUpdates()
//            tableView?.insertRows(at: [IndexPath.init(row: (arrDataChat.count - 1) , section: 0)], with: .none)
//            //tableView?.endUpdates()
//        }
       // self.view.layoutIfNeeded()
        self.scrollToLast()
        //lblNoData.isHidden = true
        txtMsg?.text = ""
        txtMsg?.placeholder = TitleType.writeMessage.rawValue
        var dict = [String : Any]()
        dict["to"] = /self.id
        dict["from"] = /UserSingleton.shared.loggedInUser?._id
        dict["timeStamp"] = (Date().timeIntervalSince1970 * 1000)
        dict["message"] = msg
        SocketIOManager.sharedInstance.sendMessage(dict)
    }
}

//MARK::- TABLEVIEW Configure
extension ChatUserVC{
    func configureTableView() {
        tableViewDataSource = CustomTableDatasource(items: arrDataChat, height: UITableView.automaticDimension, estimatedHeight: 106, tableView: tableView, cellIdentifier: [CellIdentifiers.ChatReceiveCell.rawValue , CellIdentifiers.ChatSendCell.rawValue], configureCellBlock: {(cell, item, indexpath) in
            guard let item = item as? Message else {return}
            if item.from == /UserSingleton.shared.loggedInUser?._id{
                let cell = cell as? ChatSendCell
                cell?.configureCell(item : item)
            }
            else{
                let cell = cell as? ChatReceiveCell
                cell?.configureCell(item : item)
            }
        }, aRowSelectedListener: { (indexPath) in
            self.didSelect(index: indexPath.row)
            
        }, willDisplayCell: { (indexPath) in
        } , viewforHeaderInSection : {
            (i) -> UIView? in
            return UIView()
        })
        tableView?.delegate = tableViewDataSource
        tableView?.dataSource = tableViewDataSource
        tableView?.reloadData()
        self.refreshControl.endRefreshing()
        //self.scrollToLast()
    }
    
    func didSelect(index : Int){
        self.view.endEditing(true)
    }
}

//MARK::- PAGING AND PULL TO REFRESH
extension ChatUserVC{
   @objc func refreshData(){
        if self.length == 0{
            getMessages()}else{
            self.refreshControl.endRefreshing()
        }
    }
    
    func pullToRefresh() {
        //if isPush{
            skip = 0
            offset = 20
            length = 0
            arrDataChat = []
            //isPush = false
        //}
        refreshData()
    }
    
}






