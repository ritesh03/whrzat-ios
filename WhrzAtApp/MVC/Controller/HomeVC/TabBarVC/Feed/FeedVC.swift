//
//  FeedVC.swift
//  WhrzAtApp
//
//  Created by Sierra 4 on 31/08/17.
//  Copyright © 2017 com.example. All rights reserved.
import UIKit
import EZSwiftExtensions
import Optik
import Contacts

class FeedVC: BaseViewController {
    
    //MARK::- OUTLETS
    @IBOutlet weak var contactsBtn: UIButton!
    @IBOutlet weak var tableView: UITableView?
    @IBOutlet weak var happeningBtn: UIButton!
    @IBOutlet weak var lblNoData: UILabel?
    @IBOutlet weak var lblAlert: UILabel!
    @IBOutlet var topTabBarBottomView: [UIView]!
    
    //MARK::- PROPERTIES
    var tableViewDataSource : TableViewCustomDatasource?
    var arrData : [FeedData]?
    var isFirst = true
    var contacts = [CNContact]()
    let refreshControl = UIRefreshControl()
    var store = CNContactStore()
    var phoneArray = [String]()
    var addressDict = [String : Any]()
    var isZoom = false
    var tab = 1
    var happeningArrData = [ImageData]()
    //MARK::- LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        refreshControl.addTarget(self, action: #selector(FeedVC.pullToRefresh), for: UIControl.Event.valueChanged)
        tableView?.addSubview(refreshControl)
        //  configureTableView()
        tableView?.delegate = self
        tableView?.dataSource = self
        topTabBarBottomView[0].isHidden = false
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        // lblAlert.isHidden = false
        if tab == 1 {
            getEventData()
            self.lblNoData!.text = "No recent images from nearby"
            if !isZoom{
                       pullToRefresh()
                   }else{
                       isZoom = false
                   }
        }else{
            if !(UserSingleton.shared.loggedInUser?.isFeed ?? true){
                self.arrData?.removeAll()
                self.tableView?.reloadData()
                self.lblNoData!.text = AlertMessage.allowfeed.rawValue
                lblNoData!.isHidden = false
            }else{
                if !isZoom{
                           pullToRefresh()
                       }else{
                           isZoom = false
                       }
                self.lblNoData!.text = AlertMessage.noFeeds.rawValue
            }
        }
        
        
       
    }
    
    
    @IBAction func topTabBarClick(_ sender: UIButton) {
        tab = sender.tag
        if sender.tag == 1 {
            if self.happeningArrData.count == 0 {
                getEventData()
                self.lblAlert.isHidden = false
                self.lblNoData!.isHidden = false
                self.lblNoData!.text = "No recent images from nearby"
            }
            
            happeningBtn.setTitleColor(UIColor.black, for: UIControl.State.normal)
            contactsBtn.setTitleColor(UIColor.darkGray, for: UIControl.State.normal)
            
            topTabBarBottomView[0].isHidden = false
            topTabBarBottomView[1].isHidden = true
            lblAlert.isHidden = false
            lblNoData!.isHidden = false
        }else{
            if !(UserSingleton.shared.loggedInUser?.isFeed ?? true){
                tableViewDataSource?.items = []
                tableView?.reloadData()
                lblNoData!.text = AlertMessage.allowfeed.rawValue
            }
                
            else if ((UserSingleton.shared.loggedInUser?.contactSync) == 0) || ((UserSingleton.shared.loggedInUser?.contactSync == 1)) {
                tableViewDataSource?.items = []
                tableView?.reloadData()
                self.fetchContactArray()
                lblNoData!.text = AlertMessage.noFeeds.rawValue
            }
            else{
                lblNoData!.text = AlertMessage.noFeeds.rawValue
            }
            
            happeningBtn.setTitleColor(UIColor.darkGray, for: UIControl.State.normal)
            contactsBtn.setTitleColor(UIColor.black, for: UIControl.State.normal)
            topTabBarBottomView[1].isHidden = false
            topTabBarBottomView[0].isHidden = true
            lblAlert.isHidden = false
            lblNoData!.isHidden = false
        }
        self.tableView?.reloadData()
    }
    
    
}

//MARK::- TABLEVIEW Configure
extension FeedVC: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tab == 2 {
            if let data = arrData {
                return data.count
            }
            else{
                return 0
            }
        }else{
            
            return happeningArrData.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tab == 1 {
            let cell = self.tableView?.dequeueReusableCell(withIdentifier: "FeedTableViewCell") as! FeedTableViewCell
            cell.eventPicCallBack = {
                let vc = StoryboardScene.Home.instantiateHotspotDetailVC()
                vc.hotspotId = /self.happeningArrData[indexPath.row].hotspotId?._id
                ez.topMostVC?.pushVC(vc)
            }
            cell.chatCalllBack = {
                
                let vc = StoryboardScene.Home.instantiateChatUserVC()
                vc.name = /self.happeningArrData[indexPath.row].createdBy?.name
                vc.img = /self.happeningArrData[indexPath.row].createdBy?.profilePicURL?.thumbnail
                vc.id = /self.happeningArrData[indexPath.row].createdBy?._id
                self.pushVC(vc)
                
            }
            
            cell.reportCalllBack = {
                AlertsClass.shared.showAlertController(withTitle: TitleType.Confirmation.rawValue, message: TitleType.addBlocked.rawValue, buttonTitles: [TitleType.report.rawValue , TitleType.Cancel.rawValue]) { (tag) in
                    let type = tag as AlertTag
                    switch type{
                    case .yes:
                        guard let user = UserSingleton.shared.loggedInUser else {return}
                        
                        APIManager.shared.request(with: HomeEndpoint.reportImage(imageId: /self.happeningArrData[indexPath.row]._id , id : /UserSingleton.shared.loggedInUser?._id), completion: { [weak self] (res) in
                            switch res{
                            case .success(_):
                                Alerts.shared.show(alert: .success, message: AlertMessage.reportSuccess.rawValue, type: .success)
                            case .failure(let msg):
                                Alerts.shared.show(alert: .error, message: msg, type: .info)
                            }
                            }, isLoaderNeeded: true)
                        
                    default:
                        break
                    }
                }
            }
            cell.profileCalllBack = {
                let vc = StoryboardScene.Home.instantiateOtherUserProfileVC()
                vc.id = /self.happeningArrData[indexPath.row].createdBy?._id
                self.pushVC(vc)
            }
            cell.eventNameCallBack = {
                 let vc = StoryboardScene.Home.instantiateHotspotDetailVC()
                              vc.hotspotId = /self.happeningArrData[indexPath.row].hotspotId?._id
                              ez.topMostVC?.pushVC(vc)
            }
            lblAlert.isHidden = true
            self.lblNoData!.isHidden = true
            cell.imgProfile?.kf.setImage(with: URL(string : ((self.happeningArrData[indexPath.row].createdBy?.profilePicURL!.thumbnail)!)), placeholder: #imageLiteral(resourceName: "ic_profile"), options: nil, progressBlock: nil, completionHandler: nil)
            cell.imgPost?.kf.setImage(with: URL(string : /self.happeningArrData[indexPath.row].hotspotId?.picture?.original), placeholder: UIImage(), options: nil, progressBlock: nil, completionHandler: nil)
            cell.eventNameLabel?.text = /self.happeningArrData[indexPath.row].hotspotId?.name
            cell.lblCheckin!.text = /self.happeningArrData[indexPath.row].createdBy?.name
            cell.eventNameLabel?.textColor = Utility.functions.getHotnessColor(val: self.happeningArrData[indexPath.row].hotspotId?.hotness ?? "")
            
            var loveCountt : Int  = self.happeningArrData[indexPath.row].isLikedCount!
            cell.imgPopular?.image = Utility.functions.getHotnessIcon(val: /self.happeningArrData[indexPath.row].hotspotId?.hotness)
            cell.lblLike?.text = loveCountt == 1 || loveCountt == 0 ? (loveCountt.toString + TitleType.love.rawValue) : ( loveCountt.toString + TitleType.loves.rawValue)
            
            cell.btnLove?.isSelected = (self.happeningArrData[indexPath.row].isLiked) ?? false
            
            cell.btnLove?.setImage(Utility.functions.HeartColor(val: self.happeningArrData[indexPath.row].likes?.count ?? 0), for: .normal)
            
            cell.btnLove!.setImage(Utility.functions.HeartColor(val: loveCountt), for: .normal)
            let user = UserSingleton.shared.loggedInUser
            
            if self.happeningArrData[indexPath.row].createdBy?._id != user!._id {
                cell.btnChat?.isHidden = false
                cell.btnLove?.isUserInteractionEnabled = true
                cell.imgChat?.isHidden = false
            }
            else{
                cell.imgChat?.isHidden = true
                cell.btnChat?.isHidden = true
                cell.btnLove?.isUserInteractionEnabled = false
            }
            
            cell.likeCallBack = {
                guard let user = UserSingleton.shared.loggedInUser else {return}
                
                if self.happeningArrData[indexPath.row].createdBy?._id != user._id {
                    loveCountt += ((!cell.btnLove!.isSelected) ? 1 : (-1))
                    cell.lblLike?.text = loveCountt == 1 || loveCountt == 0 ? (loveCountt.toString + TitleType.love.rawValue) :  (loveCountt.toString + TitleType.loves.rawValue)
                    
                    cell.btnLove!.setImage(Utility.functions.HeartColor(val: loveCountt), for: .normal)
                    if self.happeningArrData[indexPath.row].isLiked == true{
                        self.happeningArrData[indexPath.row].isLiked = false
                        self.happeningLike(index: indexPath.row, isLike: false)
                        cell.btnLove!.isSelected = false
                    }else{
                        self.happeningArrData[indexPath.row].isLiked = true
                        self.happeningLike(index: indexPath.row, isLike: true)
                        cell.btnLove!.isSelected = true
                    }
                    
                }
                
            }
            
            return cell
        }else{
            let cell = self.tableView?.dequeueReusableCell(withIdentifier: "FeedTableViewCell") as! FeedTableViewCell
            lblAlert.isHidden = true
            self.lblNoData!.isHidden = true
            var loveCount = self.arrData![indexPath.row].imageId!.likes?.count
            cell.eventNameLabel?.textColor = Utility.functions.getHotnessColor(val: self.arrData![indexPath.row].hotspotId!.hotness!)
            cell.eventNameLabel.text = self.arrData![indexPath.row].hotspotId!.name
            cell.imgPopular?.image = Utility.functions.getHotnessIcon(val: self.arrData![indexPath.row].hotspotId!.hotness!)
            cell.lblCheckin?.text = "\((self.arrData![indexPath.row].createdBy?.name)!)"
            cell.lblLike?.text = loveCount == 1 || loveCount == 0 ? (loveCount!.toString + TitleType.love.rawValue) : ( loveCount!.toString + TitleType.loves.rawValue)
            cell.imgProfile?.kf.setImage(with: URL(string : (self.arrData![indexPath.row].createdBy?.img!.original)!), placeholder: #imageLiteral(resourceName: "ic_profile"), options: nil, progressBlock: nil, completionHandler: nil)
            cell.imgPost?.kf.setImage(with: URL(string : /self.arrData![indexPath.row].imageId?.picture?.thumbnail), placeholder: UIImage(), options: nil, progressBlock: nil, completionHandler: nil)
            
            
            cell.btnLove?.isSelected = (self.arrData![indexPath.row].isLike) ?? false
            
            cell.btnChat?.isHidden = self.arrData![indexPath.row].createdBy?._id == /UserSingleton.shared.loggedInUser?._id
            cell.imgChat?.isHidden = self.arrData![indexPath.row].createdBy?._id == /UserSingleton.shared.loggedInUser?._id
            cell.btnReport?.isHidden = self.arrData![indexPath.row].createdBy?._id == /UserSingleton.shared.loggedInUser?._id
            cell.btnLove?.isUserInteractionEnabled = true
            cell.btnLove?.setImage(Utility.functions.HeartColor(val: self.arrData![indexPath.row].imageId?.likes?.count ?? 0), for: .normal)
            cell.likeCallBack = {
                loveCount! += ((!cell.btnLove!.isSelected) ? 1 : (-1))
                cell.lblLike?.text = loveCount == 1 || loveCount == 0 ? (loveCount!.toString + TitleType.love.rawValue) :  (loveCount!.toString + TitleType.loves.rawValue)
                cell.btnLove?.isUserInteractionEnabled = false
                cell.btnLove!.setImage(Utility.functions.HeartColor(val: loveCount!), for: .normal)
                if self.arrData![indexPath.row].isLike == true{
                    self.like(index: indexPath.row, isLike: false)
                }else{
                    self.like(index: indexPath.row, isLike: true)
                }
                
            }
            cell.chatCalllBack = {
                self.chat(index: indexPath.row)
            }
            cell.reportCalllBack = {
                AlertsClass.shared.showAlertController(withTitle: TitleType.Confirmation.rawValue, message: TitleType.addBlocked.rawValue, buttonTitles: [TitleType.report.rawValue , TitleType.Cancel.rawValue]) { (tag) in
                    let type = tag as AlertTag
                    switch type{
                    case .yes:
                        self.report(index: indexPath.row)
                    default:
                        break
                    }
                }
            }
            cell.profileCalllBack = {
                self.profileTap(index: indexPath.row)
            }
            cell.eventPicCallBack = {
                //   isImageZoom = true
                let imageDownLoader = KFImageDownLoader()
                var imageURLs = [URL]()
                guard let img = URL(string : (self.arrData![indexPath.row].imageId?.picture!.original)!) else {return}
                imageURLs.append( img )
                let destVC = Optik.imageViewer(withURLs: imageURLs, initialImageDisplayIndex: 0, imageDownloader: imageDownLoader, activityIndicatorColor: .white, dismissButtonImage: #imageLiteral(resourceName: "close.png"), dismissButtonPosition: .topTrailing)
                self.presentVC(destVC)
            }
            return cell
        }
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tab == 2 {
            return 340
        }else{
            return 340
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tab == 0  {
            let vc = StoryboardScene.Home.instantiatePromotedVC()
            
            ez.topMostVC?.pushVC(vc)
            
            
            
            
        }else if tab == 1 {
//            let vc = StoryboardScene.Home.instantiateHotspotDetailVC()
//            vc.hotspotId = /self.happeningArrData[indexPath.row].hotspotId?._id
//            ez.topMostVC?.pushVC(vc)
        }
    }
    //    func configureTableView() {
    //        tableViewDataSource = TableViewCustomDatasource(items: arrData as Array<AnyObject>?, height: UITableView.automaticDimension, estimatedHeight: 106, tableView: tableView, cellIdentifier: CellIdentifiers.FeedTableCell.rawValue, configureCellBlock: {(cell, item, indexpath) in
    //            let cell = cell as? FeedTableViewCell
    //            guard let item = item as? FeedData else {return}
    //            cell?.delegate = self
    //            cell?.btnPost?.addTarget(self, action: #selector(self.showEnlargeImageView(sender:)), for: .touchUpInside)
    //
    //            cell?.loveCount = item.imageId?.likeCount ?? 0
    //            cell?.btnLove?.isSelected = (item.isLike) ?? false
    //
    //            cell?.btnPost?.tag = indexpath.row
    //            cell?.configureCell(data : item)
    //            cell?.btnChat?.isHidden = item.createdBy?._id == /UserSingleton.shared.loggedInUser?._id
    //            cell?.imgChat?.isHidden = item.createdBy?._id == /UserSingleton.shared.loggedInUser?._id
    //            cell?.btnReport?.isHidden = item.createdBy?._id == /UserSingleton.shared.loggedInUser?._id
    //            cell?.btnLove?.isUserInteractionEnabled = item.createdBy?._id != /UserSingleton.shared.loggedInUser?._id//
    //
    //            cell?.btnLove?.setImage(Utility.functions.HeartColor(val: item.imageId?.likeCount ?? 1), for: .normal)
    //
    //
    //        }, aRowSelectedListener: { (indexPath) in
    //            self.didSelect(index: indexPath)
    //
    //        }, willDisplayCell: { (indexPath) in
    //        })
    //
    //        tableView?.delegate = tableViewDataSource
    //        tableView?.dataSource = tableViewDataSource
    //        tableView?.reloadData()
    //        self.refreshControl.endRefreshing()
    //    }
    //
    //    func didSelect(index : IndexPath?){
    //    }
}

//MARK::- IMAGETAP
extension FeedVC{
    @objc func showEnlargeImageView(sender : UIButton){
        isZoom = true
        let imageDownLoader = KFImageDownLoader()
        var imageURLs = [URL]()
        guard let img = URL(string : /self.arrData?[sender.tag].imageId?.picture?.original) else {return}
        imageURLs.append( img )
        let destVC = Optik.imageViewer(withURLs: imageURLs, initialImageDisplayIndex: 0, imageDownloader: imageDownLoader, activityIndicatorColor: .white, dismissButtonImage: #imageLiteral(resourceName: "close.png"), dismissButtonPosition: .topTrailing)
        self.presentVC(destVC)
    }
    
    func getEventData(){
        guard let user = UserSingleton.shared.loggedInUser else {return}
        
     
           var rad = (user.radius?.toInt ?? 100)
        let lat = UserSingleton.shared.lat
             let long = UserSingleton.shared.long
           if rad == -1{rad = 100}
           
       
        
        APIManager.shared.request(with: HomeEndpoint.getHappeningEvents(userId: /user._id, skip : "0" , limit : "100", radius: "\(rad)", lat: lat, long: long), completion: { [weak self] (res) in
            switch res{
            case .success(let res):
                guard let resp = res as? HappeningEventsModel else {return}
                self!.happeningArrData.removeAll()
                
                var tempArr : [ImageData] = []
                
                for item in resp.data!.imageData!{
                    self!.happeningArrData.append(item)
                    
                }
                self?.tableView?.reloadData()
                
                if self?.happeningArrData.count == 0 {
                    self?.lblNoData!.text = "No recent images from nearby"
                    self?.lblAlert.isHidden = false
                    self?.lblNoData!.isHidden = false
                }
                 self?.refreshControl.endRefreshing()
            case .failure(let msg):
               // Alerts.shared.show(alert: .error, message: msg, type: .info)
                self?.lblNoData!.text = "No recent images from nearby"
                self?.lblAlert.isHidden = false
                self?.lblNoData!.isHidden = false
                
                 self?.refreshControl.endRefreshing()
            }
            }, isLoaderNeeded:true)
    }
    
    func fetchContactArray(){
        guard let _ = UserSingleton.shared.loggedInUser else {return}
        
        Utility.functions.contactFetch(completionHandler: { [weak self] (res) in
            let res = res ?? [ ]
            var addressDict = [String : Any]()
            addressDict["userId"] = /UserSingleton.shared.loggedInUser?._id
            addressDict["contacts"] = res
            APIManager.shared.request(with: HomeEndpoint.syncContact(body: addressDict, id: /UserSingleton.shared.loggedInUser?._id , arr : String(describing : res)), completion: { [weak self] (res) in
                switch res{
                case .success(_):
                    guard let user = UserSingleton.shared.loggedInUser else{return}
                    user.contactSync = 1
                    UserSingleton.shared.loggedInUser = user
                    self?.pullToRefresh()
                case .failure(_):
                    Alerts.shared.show(alert: .error, message: AlertMessage.unableToFetchContct.rawValue, type: .info)
                    //    self?.lblNoData?.isHidden = (self?.arrData?.count ?? 0) > 0
                }
                }, isLoaderNeeded: false)
        })
    }
}

//MARK::- delegate
extension FeedVC : FeedDelegate{
    func profileTap(index: Int?) {
        let vc = StoryboardScene.Home.instantiateOtherUserProfileVC()
        vc.id = /self.arrData?[index ?? 0].createdBy?._id
        pushVC(vc)
    }
    func locationTap(index: Int?) {
        if (self.arrData?[index ?? 0].hotspotId?.deleted ?? false){
            Alerts.shared.show(alert: .error, message: AlertMessage.hotspotnotfound.rawValue, type: .info)
            return
        }
        let vc = StoryboardScene.Home.instantiateHotspotDetailVC()
        vc.hotspotId = /self.arrData?[index ?? 0].hotspotId?._id
        pushVC(vc)
    }
    func chat(index: Int?) {
        let vc = StoryboardScene.Home.instantiateChatUserVC()
        vc.name = /self.arrData?[index ?? 0].createdBy?.name
        vc.img = /self.arrData?[index ?? 0].createdBy?.img?.original
        vc.id = /self.arrData?[index ?? 0].createdBy?._id
        self.pushVC(vc)
    }
    
    func report(index: Int?) {
        guard let user = UserSingleton.shared.loggedInUser else {return}
        
        APIManager.shared.request(with: HomeEndpoint.reportImage(imageId: self.arrData?[index ?? 0].imageId?._id , id : /UserSingleton.shared.loggedInUser?._id), completion: { [weak self] (res) in
            switch res{
            case .success(_):
                Alerts.shared.show(alert: .success, message: AlertMessage.reportSuccess.rawValue, type: .success)
            case .failure(let msg):
                Alerts.shared.show(alert: .error, message: msg, type: .info)
            }
            }, isLoaderNeeded: true)
    }
    func like(index: Int? , isLike : Bool?) {
        guard let user = UserSingleton.shared.loggedInUser else {return}
        APIManager.shared.request(with: HomeEndpoint.likeImage(userId : /user._id , imageId: self.arrData?[index ?? 0].imageId?._id
            , isLike: (isLike ?? false) ? "true" : "false"), completion: { [weak self] (res) in
                switch res{
                case .success( _):
                    //                    self?.arrData?[index ?? 0].isLike = isLike
                    //                    self?.arrData?[index ?? 0].imageId?.likeCount = (self?.arrData?[index ?? 0].imageId?.likeCount ?? 0) + ((isLike ?? true) ? 1 : -1)
                    self!.getDataFromAPI()
                    
                case .failure( let msg):
                    Alerts.shared.show(alert: .error, message: msg, type: .info)
                    self?.tableView?.reloadRows(at: [IndexPath.init(row: index
                        ?? 0, section: 0)], with: .none)
                }
            }, isLoaderNeeded: true)
    }
    func happeningLike(index: Int? , isLike : Bool?) {
        guard let user = UserSingleton.shared.loggedInUser else {return}
        APIManager.shared.request(with: HomeEndpoint.likeImage(userId : /user._id , imageId: self.happeningArrData[index ?? 0]._id
            , isLike: (isLike ?? false) ? "true" : "false"), completion: { [weak self] (res) in
                switch res{
                case .success( _):
                    //                    self?.arrData?[index ?? 0].isLike = isLike
                    //                    self?.arrData?[index ?? 0].imageId?.likeCount = (self?.arrData?[index ?? 0].imageId?.likeCount ?? 0) + ((isLike ?? true) ? 1 : -1)
                    //                     self!.getEventData()
                    print("test")
                    
                case .failure( let msg):
                    Alerts.shared.show(alert: .error, message: msg, type: .info)
                    self?.tableView?.reloadRows(at: [IndexPath.init(row: index
                        ?? 0, section: 0)], with: .none)
                }
            }, isLoaderNeeded: true)
    }
}

//MARK::- API
extension FeedVC{
    func getDataFromAPI(){
        guard let user = UserSingleton.shared.loggedInUser else{return}
        APIManager.shared.request(with: HomeEndpoint.getFeeds(customerId: /user._id), completion: { [weak self] (response) in
            switch response{
            case .success(let res):
                guard let res = res as? FeedModel else {return}
                self?.arrData = res.data ?? []
                //                for item in self?.arrData ?? []{
                //                    //item.imageId?.isLike = item.imageId?.likes?.contains(/UserSingleton.shared.loggedInUser?._id)
                //                    item.imageId?.likeCount = item.imageId?.likes?.count ?? 0
                //                }
                //  self?.lblNoData?.isHidden = (self?.arrData?.count ?? 0) > 0
                //   self?.lblAlert?.isHidden = true
                //                self?.tableViewDataSource?.items = self?.arrData
                self?.tableView?.reloadData()
                self?.refreshControl.endRefreshing()
            case .failure(let msg):
                Alerts.shared.show(alert: .error, message: msg, type: .info)
                //    self?.lblNoData?.isHidden = (self?.arrData?.count ?? 0) > 0
                self?.refreshControl.endRefreshing()
            }
            }, isLoaderNeeded: true)
        isFirst = false
    }
}


//MARK::- PAGING AND PULL TO REFRESH
extension FeedVC{
    func handlePagination(){
    }
    
    func refreshData(){
        getDataFromAPI()
    }
    
    @objc func pullToRefresh() {
        if tab == 1 {
            getEventData()
        }else{
            if UserSingleton.shared.loggedInUser?.contactSync == 0{
                fetchContactArray()
            }else{
                refreshData()}
        }
        
    }
}



