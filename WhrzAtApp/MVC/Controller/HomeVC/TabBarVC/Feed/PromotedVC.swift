//
//  PromotedVC.swift
//  WhrzAtApp
//
//  Created by Vivek Dogra on 25/02/20.
//  Copyright © 2020 com.example. All rights reserved.
//

import UIKit

class PromotedVC: UIViewController,UITextViewDelegate {
    
    @IBOutlet weak var promoteImageView: UIImageView!
    @IBOutlet weak var eventDate: UILabel!
    @IBOutlet weak var locationLbl: UILabel!
    @IBOutlet weak var eventDescLbl: UILabel!
    @IBOutlet weak var eventTitle: UILabel!
    @IBOutlet weak var linkLabel: UILabel!
    @IBOutlet weak var descTextView: UITextView!
    var promoteData = MapListData()
  
    override func viewDidLoad() {
        super.viewDidLoad()
        eventDescLbl.isHidden =  true
        linkLabel.attributedText = NSAttributedString(string: promoteData.refund!, attributes:
            [.underlineStyle: NSUnderlineStyle.single.rawValue])
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.tapFunction))
        linkLabel.isUserInteractionEnabled = true
        linkLabel.addGestureRecognizer(tap)
        eventTitle.text = promoteData.name
        descTextView.text = ""
        let description = NSString(string: promoteData.description!).data(using: String.Encoding.unicode.rawValue)
        let attributedString = try! NSAttributedString(data: description!, options: [.documentType: NSAttributedString.DocumentType.html ], documentAttributes: nil)
        
      //  [NSAttributedString.Key.font: UIFont(name: "OpenSans-SemiBold",size: CGFloat(size)) as Any]
//        let newAttributedString = NSMutableAttributedString(attributedString: attributedString)
//
//        // Enumerate through all the font ranges
//        newAttributedString.enumerateAttribute(NSAttributedString.Key.font, in: NSMakeRange(0, newAttributedString.length), options: [])
//        {
//            value, range, stop in
//            guard let currentFont = value as? UIFont else {
//                return
//            }
//
//            // An NSFontDescriptor describes the attributes of a font: family name, face name, point size, etc.
//            // Here we describe the replacement font as coming from the "Hoefler Text" family
//            let fontDescriptor = currentFont.fontDescriptor.addingAttributes([UIFontDescriptor.AttributeName.family: UIFont(name:CustomFont.OpenSansSemiBold, size: 15)!])
//
//            // Ask the OS for an actual font that most closely matches the description above
//            if let newFontDescriptor = fontDescriptor.matchingFontDescriptors(withMandatoryKeys: [UIFontDescriptor.AttributeName.family]).first {
//                let newFont = UIFont(descriptor: newFontDescriptor, size: currentFont.pointSize)
//                newAttributedString.addAttributes([NSAttributedString.Key.font: newFont], range: range)
//            }
//        }
        descTextView.text = promoteData.description!
        descTextView.isSelectable = true
        descTextView.dataDetectorTypes = .link
       // descTextView.attributedText = newAttributedString
      
        descTextView.delegate = self
        
       // eventDescLbl.text = promoteData.description
        locationLbl.text = promoteData.locationName
        eventDate.text = self.getDateFromStamp(val: ((promoteData.startDate ?? 0) / 1000 )) + TitleType.to.rawValue + self.getDateFromStamp(val: ((promoteData.endDate ?? 0) / 1000 ))
        promoteImageView?.kf.setImage(with: URL(string : /promoteData.picture?.original))
    }
    
    func getDateFromStamp(val : Double?) -> String{
        let date = NSDate(timeIntervalSince1970: val ?? 0.0)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM d, h:mm a"
        let localDate = dateFormatter.string(from: date as Date)
        return localDate
    }
    
    @IBAction func btnActionBack(_ sender: Any) {
        popVC()
    }
    @objc func tapFunction()  {
        
        let urlString = promoteData.refund!
        let validUrlString = promoteData.refund!.hasPrefix("http") ? urlString : "http://\(urlString)"
        if let url = URL(string: validUrlString), UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.openURL(url)
        }
    }
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
      
         // check for the url string for performing your own custom actions here
        let urlString = URL.absoluteString
      
         // Return NO if you don't want iOS to open the link
          return true
       }
    
    
}

