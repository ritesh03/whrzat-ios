//
//  HomeFriendsVC.swift
//  WhrzAtApp
//
//  Created by Subhash Mehta on 25/06/21.
//  Copyright © 2021 com.example. All rights reserved.
//

import UIKit
import EZSwiftExtensions
import Optik
import Contacts

class HomeFriendsVC: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView?
    @IBOutlet weak var lblNoData: UILabel?
    var arrData : [FeedData]?
    var isFirst = true
    var contacts = [CNContact]()
    let refreshControl = UIRefreshControl()
    var store = CNContactStore()
    var phoneArray = [String]()
    var addressDict = [String : Any]()
    var isZoom = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.firebaseAnalyticsLogEvent(eventName: "homescr_friends")
        refreshControl.addTarget(self, action: #selector(self.pullToRefresh), for: UIControl.Event.valueChanged)
        tableView?.addSubview(refreshControl)
        //  configureTableView()
        tableView?.delegate = self
        tableView?.dataSource = self
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        guard let user = UserSingleton.shared.loggedInUser else{return}
        if user.contactSync == 0 {
            lblNoData?.isHidden = false
        }

        if !(user.isFeed ?? true){
            self.arrData?.removeAll()
            self.tableView?.reloadData()
            self.lblNoData!.text = AlertMessage.allowfeed.rawValue
            lblNoData!.isHidden = false
        }else{
            if !isZoom{
                 

                pullToRefresh()
            }else{
                isZoom = false
            }
            self.lblNoData!.text = AlertMessage.noFeeds.rawValue
        }
    }
    
}
//MARK::- TABLEVIEW Configure
extension HomeFriendsVC: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let data = arrData {
            return data.count
        }
        else{
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = self.tableView?.dequeueReusableCell(withIdentifier: "FeedTableViewCell") as! FeedTableViewCell
            self.lblNoData!.isHidden = true
            var loveCount = self.arrData![indexPath.row].imageId!.likes?.count
            cell.eventNameLabel?.textColor = Utility.functions.getHotnessColor(val: self.arrData![indexPath.row].hotspotId!.hotness!)
            cell.eventNameLabel.text = self.arrData![indexPath.row].hotspotId!.name
            cell.imgPopular?.image = Utility.functions.getHotnessIcon(val: self.arrData![indexPath.row].hotspotId!.hotness!)
        cell.btnFlame.tag = indexPath.row
           cell.btnFlame.addTarget(self, action: #selector(actionForFlame(sender:)), for: .touchUpInside)
            cell.lblCheckin?.text = "\((self.arrData![indexPath.row].createdBy?.name)!)"
            cell.lblLike?.text = loveCount == 1 || loveCount == 0 ? (loveCount!.toString + TitleType.love.rawValue) : ( loveCount!.toString + TitleType.loves.rawValue)
            cell.imgProfile?.kf.setImage(with: URL(string : (self.arrData![indexPath.row].createdBy?.img!.original)!), placeholder: #imageLiteral(resourceName: "ic_profile"), options: nil, progressBlock: nil, completionHandler: nil)
            cell.imgPost?.kf.setImage(with: URL(string : /self.arrData![indexPath.row].imageId?.picture?.thumbnail), placeholder: UIImage(), options: nil, progressBlock: nil, completionHandler: nil)
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(didTapView(_:)))

        tapGestureRecognizer.numberOfTapsRequired = 1
        cell.imgPost?.tag = indexPath.row
        cell.imgPost?.addGestureRecognizer(tapGestureRecognizer)
        cell.btnLove?.isSelected = (self.arrData![indexPath.row].isLike) ?? false
            
            cell.btnChat?.isHidden = self.arrData![indexPath.row].createdBy?._id == /UserSingleton.shared.loggedInUser?._id
            cell.imgChat?.isHidden = self.arrData![indexPath.row].createdBy?._id == /UserSingleton.shared.loggedInUser?._id
            cell.btnReport?.isHidden = self.arrData![indexPath.row].createdBy?._id == /UserSingleton.shared.loggedInUser?._id
            cell.btnLove?.isUserInteractionEnabled = true
            cell.btnLove?.setImage(Utility.functions.HeartColor(val: self.arrData![indexPath.row].imageId?.likes?.count ?? 0), for: .normal)
            cell.likeCallBack = {
                loveCount! += ((!cell.btnLove!.isSelected) ? 1 : (-1))
                cell.lblLike?.text = loveCount == 1 || loveCount == 0 ? (loveCount!.toString + TitleType.love.rawValue) :  (loveCount!.toString + TitleType.loves.rawValue)
                cell.btnLove?.isUserInteractionEnabled = false
                cell.btnLove!.setImage(Utility.functions.HeartColor(val: loveCount!), for: .normal)
                if self.arrData![indexPath.row].isLike == true{
                    self.like(index: indexPath.row, isLike: false)
                }else{
                    self.like(index: indexPath.row, isLike: true)
                }
                
            }
            cell.chatCalllBack = {
                self.chat(index: indexPath.row)
            }
            cell.reportCalllBack = {
                AlertsClass.shared.showAlertController(withTitle: TitleType.Confirmation.rawValue, message: TitleType.addBlocked.rawValue, buttonTitles: [TitleType.report.rawValue , TitleType.Cancel.rawValue]) { (tag) in
                    let type = tag as AlertTag
                    switch type{
                    case .yes:
                        self.report(index: indexPath.row)
                    default:
                        break
                    }
                }
            }
            cell.profileCalllBack = {
                self.profileTap(index: indexPath.row)
            }
            cell.eventPicCallBack = {
                //   isImageZoom = true
//                let imageDownLoader = KFImageDownLoader()
//                var imageURLs = [URL]()
//                guard let img = URL(string : (self.arrData![indexPath.row].imageId?.picture!.original)!) else {return}
//                imageURLs.append( img )
//                let destVC = Optik.imageViewer(withURLs: imageURLs, initialImageDisplayIndex: 0, imageDownloader: imageDownLoader, activityIndicatorColor: .white, dismissButtonImage: #imageLiteral(resourceName: "close.png"), dismissButtonPosition: .topTrailing)
//                self.presentVC(destVC)
            }
            return cell
        }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 510
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let vc = StoryboardScene.Home.instantiatePromotedVC()
//        
//        self.pushVC(vc)
    }
   
}
//MARK::- IMAGETAP
extension HomeFriendsVC{
    
    @IBAction func didTapView(_ sender: UITapGestureRecognizer) {
        let vC = StoryboardScene.Home.instantiateHotspotDetailVC()
        vC.hotspotId = self.arrData![sender.view!.tag].hotspotId?._id
        self.pushVC(vC)
    }
    
    @objc func actionForFlame(sender:UIButton){
        let vC = StoryboardScene.Home.instantiateHotspotDetailVC()
        vC.hotspotId = self.arrData![sender.tag].hotspotId?._id
        self.pushVC(vC)
    }
    @objc func showEnlargeImageView(sender : UIButton){
        isZoom = true
        let imageDownLoader = KFImageDownLoader()
        var imageURLs = [URL]()
        guard let img = URL(string : /self.arrData?[sender.tag].imageId?.picture?.original) else {return}
        imageURLs.append( img )
        let destVC = Optik.imageViewer(withURLs: imageURLs, initialImageDisplayIndex: 0, imageDownloader: imageDownLoader, activityIndicatorColor: .white, dismissButtonImage: #imageLiteral(resourceName: "close.png"), dismissButtonPosition: .topTrailing)
        self.presentVC(destVC)
    }
    
    func fetchContactArray(){
        guard let _ = UserSingleton.shared.loggedInUser else {return}
       // self.lblNoData?.isHidden = false
        //self.lblNoData!.text = "Syncing your contacts"
        Utility.functions.loader()
        Utility.functions.contactFetch(isAlert:true,completionHandler: { [weak self] (res) in
            let res = res ?? [ ]
            var addressDict = [String : Any]()
            addressDict["userId"] = /UserSingleton.shared.loggedInUser?._id
            addressDict["contacts"] = res
            APIManager.shared.request(with: HomeEndpoint.syncContact(body: addressDict, id: /UserSingleton.shared.loggedInUser?._id , arr : String(describing : res)), completion: { [weak self] (res) in
//                self?.lblNoData?.isHidden = true
//
//                self?.lblNoData!.text = ""
                switch res{
                case .success(_):
                    Utility.functions.removeLoader()
                    guard let user = UserSingleton.shared.loggedInUser else{return}
                    user.contactSync = 1
                    UserSingleton.shared.loggedInUser = user
                    self?.perform(#selector(self?.pullToRefresh), with: nil, afterDelay: 0.1)
                   
                case .failure(_):
                    Utility.functions.removeLoader()
                    Alerts.shared.show(alert: .error, message: AlertMessage.unableToFetchContct.rawValue, type: .info)
                    //    self?.lblNoData?.isHidden = (self?.arrData?.count ?? 0) > 0
                }
                }, isLoaderNeeded: true)
        })
    }
}
//MARK::- delegate
extension HomeFriendsVC : FeedDelegate{
    func profileTap(index: Int?) {
        let vc = StoryboardScene.Home.instantiateOtherUserProfileVC()
        vc.id = /self.arrData?[index ?? 0].createdBy?._id
        pushVC(vc)
    }
    func locationTap(index: Int?) {
        if (self.arrData?[index ?? 0].hotspotId?.deleted ?? false){
            Alerts.shared.show(alert: .error, message: AlertMessage.hotspotnotfound.rawValue, type: .info)
            return
        }
        let vc = StoryboardScene.Home.instantiateHotspotDetailVC()
        vc.hotspotId = /self.arrData?[index ?? 0].hotspotId?._id
        pushVC(vc)
    }
    func chat(index: Int?) {
        let vc = StoryboardScene.Home.instantiateChatUserVC()
        vc.name = /self.arrData?[index ?? 0].createdBy?.name
        vc.img = /self.arrData?[index ?? 0].createdBy?.img?.original
        vc.id = /self.arrData?[index ?? 0].createdBy?._id
        self.pushVC(vc)
    }

    func report(index: Int?) {
        guard let user = UserSingleton.shared.loggedInUser else {return}
        
        APIManager.shared.request(with: HomeEndpoint.reportImage(imageId: self.arrData?[index ?? 0].imageId?._id , id : /UserSingleton.shared.loggedInUser?._id), completion: { [weak self] (res) in
            switch res{
            case .success(_):
                Alerts.shared.show(alert: .success, message: AlertMessage.reportSuccess.rawValue, type: .success)
            case .failure(let msg):
                Alerts.shared.show(alert: .error, message: msg, type: .info)
            }
            }, isLoaderNeeded: true)
    }
    func like(index: Int? , isLike : Bool?) {
        guard let user = UserSingleton.shared.loggedInUser else {return}
        APIManager.shared.request(with: HomeEndpoint.likeImage(userId : /user._id , imageId: self.arrData?[index ?? 0].imageId?._id
            , isLike: (isLike ?? false) ? "true" : "false"), completion: { [weak self] (res) in
                switch res{
                case .success( _):
                    //                    self?.arrData?[index ?? 0].isLike = isLike
                    //                    self?.arrData?[index ?? 0].imageId?.likeCount = (self?.arrData?[index ?? 0].imageId?.likeCount ?? 0) + ((isLike ?? true) ? 1 : -1)
                    self!.getDataFromAPI()
                    
                case .failure( let msg):
                    Alerts.shared.show(alert: .error, message: msg, type: .info)
                    self?.tableView?.reloadRows(at: [IndexPath.init(row: index
                        ?? 0, section: 0)], with: .none)
                }
            }, isLoaderNeeded: true)
    }
    func happeningLike(index: Int? , isLike : Bool?) {
        guard let user = UserSingleton.shared.loggedInUser else {return}
//        APIManager.shared.request(with: HomeEndpoint.likeImage(userId : /user._id , imageId: self.happeningArrData[index ?? 0]._id
//            , isLike: (isLike ?? false) ? "true" : "false"), completion: { [weak self] (res) in
//                switch res{
//                case .success( _):
//                    //                    self?.arrData?[index ?? 0].isLike = isLike
//                    //                    self?.arrData?[index ?? 0].imageId?.likeCount = (self?.arrData?[index ?? 0].imageId?.likeCount ?? 0) + ((isLike ?? true) ? 1 : -1)
//                    //                     self!.getEventData()
//                    print("test")
//
//                case .failure( let msg):
//                    Alerts.shared.show(alert: .error, message: msg, type: .info)
//                    self?.tableView?.reloadRows(at: [IndexPath.init(row: index
//                        ?? 0, section: 0)], with: .none)
//                }
//            }, isLoaderNeeded: true)
    }
}
//MARK::- API
extension HomeFriendsVC{
    func getDataFromAPI(){
        guard let user = UserSingleton.shared.loggedInUser else{return}
        APIManager.shared.request(with: HomeEndpoint.getFeeds(customerId: /user._id), completion: { [weak self] (response) in
            self?.isFirst = false
           // Utility.functions.removeLoader()
            switch response{
            case .success(let res):
                guard let res = res as? FeedModel else {return}
                self?.arrData = res.data ?? []
                
                self?.lblNoData?.isHidden = (self?.arrData?.count ?? 0) > 0
                
                self?.tableView?.reloadData()
                self?.refreshControl.endRefreshing()
                
                
            case .failure(let msg):
                Alerts.shared.show(alert: .error, message: msg, type: .info)
                //    self?.lblNoData?.isHidden = (self?.arrData?.count ?? 0) > 0
                self?.refreshControl.endRefreshing()
            }
        }, isLoaderNeeded: isFirst)
        
    }
}
//MARK::- PAGING AND PULL TO REFRESH
extension HomeFriendsVC {
   
    func refreshData(){
        getDataFromAPI()
    }
    
    @objc func pullToRefresh() {
        if UserSingleton.shared.loggedInUser?.contactSync == 0{
            self.refreshControl.endRefreshing()

            fetchContactArray()
        }else{

            refreshData()}
    }
    
}
    
    

