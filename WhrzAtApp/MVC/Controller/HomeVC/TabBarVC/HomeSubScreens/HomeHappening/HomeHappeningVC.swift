//
//  HomeHappeningVC.swift
//  WhrzAtApp
//
//  Created by Subhash Mehta on 25/06/21.
//  Copyright © 2021 com.example. All rights reserved.
//

import UIKit
import EZSwiftExtensions
import Optik
import Contacts
import ESPullToRefresh

class HomeHappeningVC: BaseViewController {

    @IBOutlet weak var tableView: UITableView?
    @IBOutlet weak var lblNoData: UILabel?
    @IBOutlet weak var lblAlert: UILabel!
    var arrData : [FeedData]?
    var isFirst = true
    let refreshControl = UIRefreshControl()
    var isZoom = false
    var happeningArrData = [ImageData]()
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.firebaseAnalyticsLogEvent(eventName: "homescr_happening")
        refreshControl.addTarget(self, action: #selector(self.pullToRefresh), for: UIControl.Event.valueChanged)
        tableView?.addSubview(refreshControl)
        //  configureTableView()
        tableView?.delegate = self
        tableView?.dataSource = self
        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        getEventData()
        self.lblNoData!.text = "No recent images from nearby"
        if !isZoom{
            pullToRefresh()
        }else{
            isZoom = false
        }
    }
   

}
//MARK::- TABLEVIEW Configure
extension HomeHappeningVC: UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
            return happeningArrData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = self.tableView?.dequeueReusableCell(withIdentifier: "FeedTableViewCell") as! FeedTableViewCell
     
            cell.eventPicCallBack = {
                let vc = StoryboardScene.Home.instantiateHotspotDetailVC()
                vc.hotspotId = /self.happeningArrData[indexPath.row].hotspotId?._id
                ez.topMostVC?.pushVC(vc)
            }
            cell.chatCalllBack = {
                
                let vc = StoryboardScene.Home.instantiateChatUserVC()
                vc.name = /self.happeningArrData[indexPath.row].createdBy?.name
                vc.img = /self.happeningArrData[indexPath.row].createdBy?.profilePicURL?.thumbnail
                vc.id = /self.happeningArrData[indexPath.row].createdBy?._id
                self.pushVC(vc)
                
            }
            
            cell.reportCalllBack = {
                AlertsClass.shared.showAlertController(withTitle: TitleType.Confirmation.rawValue, message: TitleType.addBlocked.rawValue, buttonTitles: [TitleType.report.rawValue , TitleType.Cancel.rawValue]) { (tag) in
                    let type = tag as AlertTag
                    switch type{
                    case .yes:
                        guard let user = UserSingleton.shared.loggedInUser else {return}
                        
                        APIManager.shared.request(with: HomeEndpoint.reportImage(imageId: /self.happeningArrData[indexPath.row]._id , id : /UserSingleton.shared.loggedInUser?._id), completion: { [weak self] (res) in
                            switch res{
                            case .success(_):
                                Alerts.shared.show(alert: .success, message: AlertMessage.reportSuccess.rawValue, type: .success)
                            case .failure(let msg):
                                Alerts.shared.show(alert: .error, message: msg, type: .info)
                            }
                            }, isLoaderNeeded: true)
                        
                    default:
                        break
                    }
                }
            }
            cell.profileCalllBack = {
                let vc = StoryboardScene.Home.instantiateOtherUserProfileVC()
                vc.id = /self.happeningArrData[indexPath.row].createdBy?._id
                self.pushVC(vc)
            }
            cell.eventNameCallBack = {
                 let vc = StoryboardScene.Home.instantiateHotspotDetailVC()
                              vc.hotspotId = /self.happeningArrData[indexPath.row].hotspotId?._id
                              ez.topMostVC?.pushVC(vc)
            }
           
            cell.btnReport?.isHidden = self.happeningArrData[indexPath.row].createdBy?._id == /UserSingleton.shared.loggedInUser?._id
            self.lblNoData!.isHidden = true
            cell.imgProfile?.kf.setImage(with: URL(string : ((self.happeningArrData[indexPath.row].createdBy?.profilePicURL!.thumbnail)!)), placeholder: #imageLiteral(resourceName: "ic_profile"), options: nil, progressBlock: nil, completionHandler: nil)
            cell.imgPost?.kf.setImage(with: URL(string : /self.happeningArrData[indexPath.row].hotspotId?.picture?.original), placeholder: UIImage(), options: nil, progressBlock: nil, completionHandler: nil)
            cell.eventNameLabel?.textColor = Utility.functions.getHotnessColor(val: self.happeningArrData[indexPath.row].hotspotId!.hotness!)
            cell.eventNameLabel?.text = /self.happeningArrData[indexPath.row].hotspotId?.name
            cell.lblCheckin!.text = /self.happeningArrData[indexPath.row].createdBy?.name
            
            var loveCountt : Int  = self.happeningArrData[indexPath.row].isLikedCount!
            cell.imgPopular?.image = Utility.functions.getHotnessIcon(val: /self.happeningArrData[indexPath.row].hotspotId?.hotness)
            cell.lblLike?.text = loveCountt == 1 || loveCountt == 0 ? (loveCountt.toString + TitleType.love.rawValue) : (loveCountt.toString + TitleType.loves.rawValue)
            
            cell.btnLove?.isSelected = (self.happeningArrData[indexPath.row].isLiked) ?? false
            
            cell.btnLove?.setImage(Utility.functions.HeartColor(val: self.happeningArrData[indexPath.row].likes?.count ?? 0), for: .normal)
            
            cell.btnLove!.setImage(Utility.functions.HeartColor(val: loveCountt), for: .normal)
            let user = UserSingleton.shared.loggedInUser
            
            if self.happeningArrData[indexPath.row].createdBy?._id != user!._id {
                cell.btnChat?.isHidden = false
                cell.btnLove?.isUserInteractionEnabled = true
                cell.imgChat?.isHidden = false
            }
            else{
                cell.imgChat?.isHidden = true
                cell.btnChat?.isHidden = true
                cell.btnLove?.isUserInteractionEnabled = false
            }
            
            cell.likeCallBack = {
                guard let user = UserSingleton.shared.loggedInUser else {return}
                
                if self.happeningArrData[indexPath.row].createdBy?._id != user._id {
                    loveCountt += ((!cell.btnLove!.isSelected) ? 1 : (-1))
                    cell.lblLike?.text = loveCountt == 1 || loveCountt == 0 ? (loveCountt.toString + TitleType.love.rawValue) : (loveCountt.toString + TitleType.loves.rawValue)
                    
                    cell.btnLove!.setImage(Utility.functions.HeartColor(val: loveCountt), for: .normal)
                    if self.happeningArrData[indexPath.row].isLiked == true{
                        self.happeningArrData[indexPath.row].isLiked = false
                        self.happeningLike(index: indexPath.row, isLike: false)
                        cell.btnLove!.isSelected = false
                    }else{
                        self.happeningArrData[indexPath.row].isLiked = true
                        self.happeningLike(index: indexPath.row, isLike: true)
                        cell.btnLove!.isSelected = true
                    }
                    
                }
                
            }
            return cell

        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
      
            return 510
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
   //     let vc = StoryboardScene.Home.instantiatePromotedVC()
        
  //      self.pushVC(vc)
//        if tab == 0  {
//            let vc = StoryboardScene.Home.instantiatePromotedVC()
//
//            ez.topMostVC?.pushVC(vc)
//
//
//
//
//        }else if tab == 1 {
////            let vc = StoryboardScene.Home.instantiateHotspotDetailVC()
////            vc.hotspotId = /self.happeningArrData[indexPath.row].hotspotId?._id
////            ez.topMostVC?.pushVC(vc)
//        }
    }

}
//MARK::- IMAGETAP
extension HomeHappeningVC{
    @objc func showEnlargeImageView(sender : UIButton){
        isZoom = true
        let imageDownLoader = KFImageDownLoader()
        var imageURLs = [URL]()
        guard let img = URL(string : /self.arrData?[sender.tag].imageId?.picture?.original) else {return}
        imageURLs.append( img )
        let destVC = Optik.imageViewer(withURLs: imageURLs, initialImageDisplayIndex: 0, imageDownloader: imageDownLoader, activityIndicatorColor: .white, dismissButtonImage: #imageLiteral(resourceName: "close.png"), dismissButtonPosition: .topTrailing)
        self.presentVC(destVC)
    }
    
    func getEventData(){
        guard let user = UserSingleton.shared.loggedInUser else {return}
        var rad = (user.radius?.toInt ?? 100)
        let lat = UserSingleton.shared.lat
        let long = UserSingleton.shared.long
        if rad == -1{rad = 100}
        
        APIManager.shared.request(with: HomeEndpoint.getHappeningEvents(userId: /user._id, skip : "0" , limit : "100", radius: "\(rad)", lat: lat, long: long), completion: { [weak self] (res) in
            self?.isFirst = false
            switch res{
            case .success(let res):
                guard let resp = res as? HappeningEventsModel else {return}
                self!.happeningArrData.removeAll()
                self?.isFirst = false
                
                for item in resp.data!.imageData!{
                    self!.happeningArrData.append(item)
                    
                }
                self?.tableView?.reloadData()
                
                if self?.happeningArrData.count == 0 {
                    self?.lblNoData!.text = "No recent images from nearby"
                    self?.lblNoData!.isHidden = false
                }
                self?.refreshControl.endRefreshing()
            case .failure(let msg):
                // Alerts.shared.show(alert: .error, message: msg, type: .info)
                self?.lblNoData!.text = "No recent images from nearby"
                self?.lblNoData!.isHidden = false
                self?.refreshControl.endRefreshing()
            }
        }, isLoaderNeeded:isFirst)
        self.isFirst = false
    }
}
//MARK::- delegate
extension HomeHappeningVC : FeedDelegate{
    func profileTap(index: Int?) {
        let vc = StoryboardScene.Home.instantiateOtherUserProfileVC()
        vc.id = /self.arrData?[index ?? 0].createdBy?._id
        pushVC(vc)
    }
    func locationTap(index: Int?) {
        if (self.arrData?[index ?? 0].hotspotId?.deleted ?? false){
            Alerts.shared.show(alert: .error, message: AlertMessage.hotspotnotfound.rawValue, type: .info)
            return
        }
        let vc = StoryboardScene.Home.instantiateHotspotDetailVC()
        vc.hotspotId = /self.arrData?[index ?? 0].hotspotId?._id
        pushVC(vc)
    }
    func chat(index: Int?) {
        let vc = StoryboardScene.Home.instantiateChatUserVC()
        vc.name = /self.arrData?[index ?? 0].createdBy?.name
        vc.img = /self.arrData?[index ?? 0].createdBy?.img?.original
        vc.id = /self.arrData?[index ?? 0].createdBy?._id
        self.pushVC(vc)
    }
    
    func report(index: Int?) {
        guard let user = UserSingleton.shared.loggedInUser else {return}
        
        APIManager.shared.request(with: HomeEndpoint.reportImage(imageId: self.arrData?[index ?? 0].imageId?._id , id : /UserSingleton.shared.loggedInUser?._id), completion: { [weak self] (res) in
            switch res{
            case .success(_):
                Alerts.shared.show(alert: .success, message: AlertMessage.reportSuccess.rawValue, type: .success)
            case .failure(let msg):
                Alerts.shared.show(alert: .error, message: msg, type: .info)
            }
            }, isLoaderNeeded: true)
    }
    func like(index: Int? , isLike : Bool?) {
        guard let user = UserSingleton.shared.loggedInUser else {return}
        APIManager.shared.request(with: HomeEndpoint.likeImage(userId : /user._id , imageId: self.arrData?[index ?? 0].imageId?._id
            , isLike: (isLike ?? false) ? "true" : "false"), completion: { [weak self] (res) in
                switch res{
                case .success( _):
                    //                    self?.arrData?[index ?? 0].isLike = isLike
                    //                    self?.arrData?[index ?? 0].imageId?.likeCount = (self?.arrData?[index ?? 0].imageId?.likeCount ?? 0) + ((isLike ?? true) ? 1 : -1)
                    self!.getDataFromAPI()
                    
                case .failure( let msg):
                    Alerts.shared.show(alert: .error, message: msg, type: .info)
                    self?.tableView?.reloadRows(at: [IndexPath.init(row: index
                        ?? 0, section: 0)], with: .none)
                }
            }, isLoaderNeeded: true)
    }
    func happeningLike(index: Int? , isLike : Bool?) {
        guard let user = UserSingleton.shared.loggedInUser else {return}
        APIManager.shared.request(with: HomeEndpoint.likeImage(userId : /user._id , imageId: self.happeningArrData[index ?? 0]._id
            , isLike: (isLike ?? false) ? "true" : "false"), completion: { [weak self] (res) in
                switch res{
                case .success( _):
                    //                    self?.arrData?[index ?? 0].isLike = isLike
                    //                    self?.arrData?[index ?? 0].imageId?.likeCount = (self?.arrData?[index ?? 0].imageId?.likeCount ?? 0) + ((isLike ?? true) ? 1 : -1)
                    //                     self!.getEventData()
                    print("test")
                    
                case .failure( let msg):
                    Alerts.shared.show(alert: .error, message: msg, type: .info)
                    self?.tableView?.reloadRows(at: [IndexPath.init(row: index
                        ?? 0, section: 0)], with: .none)
                }
            }, isLoaderNeeded: true)
    }
}
//MARK::- API
extension HomeHappeningVC{
    func getDataFromAPI(){
        guard let user = UserSingleton.shared.loggedInUser else{return}
        APIManager.shared.request(with: HomeEndpoint.getFeeds(customerId: /user._id), completion: { [weak self] (response) in
            switch response{
            case .success(let res):
                guard let res = res as? FeedModel else {return}
                self?.arrData = res.data ?? []
                //                for item in self?.arrData ?? []{
                //                    //item.imageId?.isLike = item.imageId?.likes?.contains(/UserSingleton.shared.loggedInUser?._id)
                //                    item.imageId?.likeCount = item.imageId?.likes?.count ?? 0
                //                }
                //  self?.lblNoData?.isHidden = (self?.arrData?.count ?? 0) > 0
                //   self?.lblAlert?.isHidden = true
                //                self?.tableViewDataSource?.items = self?.arrData
                self?.tableView?.reloadData()
                self?.refreshControl.endRefreshing()
            case .failure(let msg):
                Alerts.shared.show(alert: .error, message: msg, type: .info)
                //    self?.lblNoData?.isHidden = (self?.arrData?.count ?? 0) > 0
                self?.refreshControl.endRefreshing()
            }
            }, isLoaderNeeded: isFirst)
        isFirst = false
    }
}

//MARK::- PAGING AND PULL TO REFRESH
extension HomeHappeningVC{
    
    @objc func pullToRefresh() {
        getEventData()
        
    }
}
