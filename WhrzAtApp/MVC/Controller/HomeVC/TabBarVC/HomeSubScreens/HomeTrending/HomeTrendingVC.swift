//
//  HomeTrendingVC.swift
//  WhrzAtApp
//
//  Created by Subhash Mehta on 25/06/21.
//  Copyright © 2021 com.example. All rights reserved.
//

import UIKit
import IQKeyboardManager
import EZSwiftExtensions
import ESPullToRefresh
import FirebaseDynamicLinks
import CoreLocation
import Contacts

class HomeTrendingVC: BaseViewController {
    
    @IBOutlet weak var syncingContactsLabel: UILabel!
    @IBOutlet weak var collectionViewTrend: UICollectionView?
    @IBOutlet weak var lblNoData: UILabel?
    
    let refreshControl = UIRefreshControl()
    var isLoadMore = false
    var isPullDown = true
    var arrData : [MapListData]?
    var skip = 0
    var length = 0
    var hotspotStr = ""
    var isFirst = true
    var offset = 10
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.firebaseAnalyticsLogEvent(eventName: "homescr_trending")
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateData(notification:)), name: NSNotification.Name(rawValue: "PostAdded"), object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.updateLocationData),
                                               name: NSNotification.Name(rawValue: "LocationReceived"),
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.fetchContact),
                                               name: NSNotification.Name(rawValue: "fetchContact"),
                                               object: nil)
        refreshControl.addTarget(self, action: #selector(self.pullToRefresh), for: UIControl.Event.valueChanged)
        collectionViewTrend?.addSubview(refreshControl)
        self.lblNoData?.text = "No hotspots found"
        handlePagination()
        guard let user = UserSingleton.shared.loggedInUser else{return}
        if user.contactSync == 0 {
            fetchContactArray(isFirstTime: true)

        }
    }

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
            self.pullToRefresh()
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        print("Disappear")
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func updateData(notification:NSNotification){
       
            isFirst = true
            Utility.functions.loader()
            
            self.perform(#selector(self.pullToRefresh), with: nil, afterDelay: 3.1)
     
    }
    
    @objc func updateLocationData(notification:NSNotification){
           isFirst = true
            Utility.functions.loader()
            self.pullToRefresh()
        
}
    
    
    @objc func fetchContact(notification:NSNotification){
       fetchContactArray()
}
//    @objc func refreshDataNotification() {
//        skip = 0
//        let text = hotspotStr.trimmed()
//        let url =  ((text.isBlank) || (text.isEmpty)) ? nil : text
//        self.getData(str: url)
////
////        // tableView?.tableFooterView = UIView()
////        resetNoMoreData()
////        length = 0
////
////        refreshData()
//    }
}
extension HomeTrendingVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrData?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionViewTrend?.dequeueReusableCell(withReuseIdentifier: "TrendingCollectionCell", for: indexPath) as! TrendingCollectionCell
        let item = arrData?[indexPath.item]
        cell.configureCell(data : item)
        cell.imgIcon?.image = Utility.functions.getHotnessIcon(val: /item?.hotness)
        cell.lblPopularity?.textColor = Utility.functions.getHotnessColor(val: /item?.hotness)
        cell.lblPopularity?.text = Utility.functions.getHotnessTitle(val: /item?.hotness)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vC = StoryboardScene.Home.instantiateHotspotDetailVC()
        vC.hotspotId = /arrData?[indexPath.item]._id
        self.pushVC(vC)
        //ez.topMostVC?.pushVC(vC)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width:((UIScreen.main.bounds.size.width - 56) / 2) , height: (((UIScreen.main.bounds.size.width - 56) / 2) * 1.37))
    }
    
}
//MARK::- PAGING AND PULL TO REFRESH
extension HomeTrendingVC {
    
    func handlePagination(){
        if (isLocationOn()) {
            let _ = collectionViewTrend?.es.addInfiniteScrolling { [weak self] in
                if (self?.arrData?.count ?? 0) < (self?.length ?? 0) {
                    self?.isPullDown = false
                    let text = self!.hotspotStr.trimmed()
                    let url =  ((text.isBlank ) || (text.isEmpty ?? false)) ? nil : text
                    self?.getData(str: url)
                }else{
                    self?.foundNoMoreData()
                    //self.refreshControl.endRefreshing()
                }
            }
        }else{
            self.lblNoData?.isHidden = self.arrData?.count ?? 0 > 0

            Utility.functions.removeLoader()
            refreshControl.endRefreshing()
        }
    }
    func foundNoMoreData(){
        self.collectionViewTrend?.es.stopLoadingMore()
        self.collectionViewTrend?.es.noticeNoMoreData()
    }
    func refreshData(){
        if(isLocationOn()) {
            isPullDown = true
            skip = 0
            let text = hotspotStr.trimmed()
            let url =  ((text.isBlank) || (text.isEmpty)) ? nil : text
            self.getData(str: url)
        }else{
            self.lblNoData?.isHidden = self.arrData?.count ?? 0 > 0

            Utility.functions.removeLoader()
            refreshControl.endRefreshing()
        }
    }
    
    @objc func pullToRefresh() {
       if (isLocationOn()) {
            resetNoMoreData()
            length = 0
            refreshData()
        }else{
            self.lblNoData?.isHidden = self.arrData?.count ?? 0 > 0

            Utility.functions.removeLoader()
            refreshControl.endRefreshing()
        }
    }
    func resetNoMoreData(){
        self.collectionViewTrend?.es.resetNoMoreData()
    }
    
    func isLocationOn() -> Bool{
        
        if CLLocationManager.authorizationStatus() == .authorizedAlways || CLLocationManager.authorizationStatus() == .authorizedWhenInUse  {

            return true
        }else{
            self.settingsAlert()

            return false
        }
    }
  //  "To create new hotspot, please turn on location services in settings"
    func settingsAlert(){
        AlertsClass.shared.showAlertController(withTitle: TitleType.permissionRequired.rawValue, message: TitleType.locationSetting.rawValue, buttonTitles: [TitleType.setting.rawValue , TitleType.Cancel.rawValue]) { (value) in
            let type = value as AlertTag
            self.lblNoData?.isHidden = self.arrData?.count ?? 0 > 0

            switch type {
            case .yes:
                UIApplication.shared.openURL(NSURL(string:UIApplication.openSettingsURLString)! as URL)
            default:
                break
            }
        }
    }
    
}
//MARK::- GET DATA
extension HomeTrendingVC {
    
    func getData(str : String?){
        if (isLocationOn)() {
            guard let user = UserSingleton.shared.loggedInUser else {return}
            let lat = UserSingleton.shared.lat
            let long = UserSingleton.shared.long
            var rad = (user.radius?.toInt ?? 100)
            if rad == -1{rad = 100}
            if /str != "" {
                rad = -1
            }
            
            APIManager.shared.request(with: HomeEndpoint.hotspotMapView(lat: lat, long: long, radius: rad.description, desc: nil, id: user._id, search: str , skip : "\(skip)" , limit : "\(offset)"), completion: { [weak self] (res) in
                switch res{
                case .success(let res):
                    guard let resp = res as? MapModel else {return}
                    self?.length = resp.data?.count ?? 0
                    var tempArr : [MapListData] = []
                    
                    for item in resp.data?.dataMap ?? []{
                        self?.arrData?.append(item)
                        tempArr.append(item)
                    }
                    if (self?.isPullDown ?? false){
                        self?.arrData = tempArr
                    }
                    self?.skip = (self?.skip ?? 0) + (self?.offset ?? 10)
                    self?.isLoadMore = (self?.arrData?.count ?? 0) > 0
                    self?.collectionViewTrend?.es.stopLoadingMore()
                    ( self?.isLoadMore ?? true ) ? self?.collectionViewTrend?.es.resetNoMoreData() : self?.collectionViewTrend?.es.noticeNoMoreData()
                    
                    // self?.tableViewDataSource?.items = self?.arrData ?? []
                    if self?.isFirst ?? false {
                        AnimatableReload.reload(collectionView: self?.collectionViewTrend ?? UICollectionView(), animationDirection: .right)
                        self?.isFirst = false
                    }
                    else{
                        self?.collectionViewTrend?.reloadData()
                    }
                    self?.lblNoData?.isHidden = self?.arrData?.count ?? 0 > 0
                    self?.refreshControl.endRefreshing()
                    
                case .failure(let msg):
                    Alerts.shared.show(alert: .error, message: msg, type: .info)
                    self?.lblNoData?.isHidden = self?.arrData?.count ?? 0 > 0
                    self?.refreshControl.endRefreshing()
                    
                }
            }, isLoaderNeeded: isFirst)
        }else{
            self.lblNoData?.isHidden = self.arrData?.count ?? 0 > 0
           Utility.functions.removeLoader()
            refreshControl.endRefreshing()
        }
    }
    
    
    func fetchContactArray(isFirstTime:Bool = false){

        guard let user = UserSingleton.shared.loggedInUser else {return}
        Utility.functions.contactFetch(completionHandler: { [weak self] (res) in
            
             let res = res ?? [ ]
            var addressDict = [String : Any]()
            addressDict["userId"] = /UserSingleton.shared.loggedInUser?._id
            addressDict["contacts"] = res
            user.contactSync = 1
            UserSingleton.shared.loggedInUser = user

            APIManager.shared.request(with: HomeEndpoint.syncContact(body: addressDict, id: /UserSingleton.shared.loggedInUser?._id , arr : String(describing : res)), completion: { [weak self] (res) in


                switch res{
                case .success(_):
                    user.contactSync = 1
                   UserSingleton.shared.loggedInUser = user
                   
                case .failure(_):

                   break
                }
                }, isLoaderNeeded: false)
        })
    }
    

}

