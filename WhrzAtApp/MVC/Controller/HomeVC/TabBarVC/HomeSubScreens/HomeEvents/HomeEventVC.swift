//
//  HomeEventVC.swift
//  WhrzAtApp
//
//  Created by Subhash Mehta on 25/06/21.
//  Copyright © 2021 com.example. All rights reserved.
//

import UIKit

class HomeEventVC: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView?
    @IBOutlet weak var lblNoData: UILabel?
    @IBOutlet weak var createEventButton: UIButton!
    var promoteArrData : [MapListData]?
    var isFirst = true
    var skip = 0
    var offset = 10
    var length = 0
    let refreshControl = UIRefreshControl()
    var isLoadMore = false
    var isPullDown = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.firebaseAnalyticsLogEvent(eventName: "homescr_events")
        refreshControl.addTarget(self, action: #selector(self.pullToRefresh), for: UIControl.Event.valueChanged)
        tableView?.addSubview(refreshControl)
        handlePagination()
        createEventButton.layer.cornerRadius = 4
        createEventButton.layer.shadowColor = UIColor.black.cgColor
        createEventButton.layer.shadowOffset = CGSize(width: 1, height: 1)
        createEventButton.layer.shadowOpacity = 3.0
        createEventButton.layer.shadowRadius = 2.5
      
        self.lblNoData?.text = "No events found"
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getPromoteData(str: "")
    }

    @IBAction func createEventBtnTap(_ sender: Any) {
        if let url = URL(string: "https://www.whrzat.com/promoteyourevent.html"), UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.openURL(url)
        }
    }
}
extension HomeEventVC:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return promoteArrData?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Promoted", for: indexPath) as! ExploreListCell
       // guard let item = item as? MapListData else {return}
        let item = promoteArrData?[indexPath.row]
        cell.configureCell(data : item)
        cell.lblLocation.text = /item?.locationName
    
        cell.lblForTime?.text =  self.getDateFromStamp(val: ((item?.startDate ?? 0) / 1000 )) + TitleType.to.rawValue + self.getDateFromStamp(val: ((item?.endDate ?? 0) / 1000 ))
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vC = StoryboardScene.Home.instantiatePromotedVC()
        vC.promoteData = self.promoteArrData![indexPath.row]
        self.pushVC(vC)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 295
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 295
    }
    
}
extension HomeEventVC{
    
    func getDateFromStamp(val : Double?) -> String{
        let date = NSDate(timeIntervalSince1970: val ?? 0.0)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM d, h:mm a"
        let localDate = dateFormatter.string(from: date as Date)
        return localDate
        
    }
    
    func getPromoteData(str: String?){
        guard let user = UserSingleton.shared.loggedInUser else {return}
        
        let lat = UserSingleton.shared.lat
        let long = UserSingleton.shared.long
        var rad = (user.radius?.toInt ?? 100)
        if rad == -1{rad = 100}
        
        if /str != ""{
            rad = -1
        }
        
        APIManager.shared.request(with: HomeEndpoint.promoteEventList(lat: lat, long: long, id: user._id, search: str, skip: "0", limit: "100", radius: "\(rad)"), completion: { [weak self] (res) in
            switch res{
            case .success(let res):
                guard let resp = res as? MapModel else {return}
                
                
                
                var tempArr : [MapListData] = []
                for item in resp.data?.promoteData ?? []{
                    
                    tempArr.append(item)
                }
                
                self?.promoteArrData = tempArr
                self?.lblNoData?.isHidden = (self?.promoteArrData?.count ?? 0) > 0
                print(self?.promoteArrData?.count)
                
               // self?.tableViewDataSource?.items = self?.promoteArrData ?? []
                if self?.isFirst ?? false {
                    AnimatableReload.reload(tableView: self?.tableView ?? UITableView(), animationDirection: .right)
                    self?.isFirst = false
                }
                else{
                    self?.tableView?.reloadData()
                }
                
                self?.lblNoData?.isHidden = self?.promoteArrData?.count ?? 0 > 0
                
                self?.tableView?.es.noticeNoMoreData()
                self?.refreshControl.endRefreshing()
            case .failure(let msg):
                Alerts.shared.show(alert: .error, message: msg, type: .info)
                
                
            }
            }, isLoaderNeeded: isFirst)
    }
}
//MARK::- PAGING AND PULL TO REFRESH
extension HomeEventVC {
    
    func handlePagination(){
//        let _ = tableView?.es.addInfiniteScrolling { [weak self] in
//            if (self?.arrData?.count ?? 0) < (self?.length ?? 0) {
//                self?.isPullDown = false
//                let text = self!.hotspotStr.trimmed()
//                let url =  ((text.isBlank ) || (text.isEmpty ?? false)) ? nil : text
//                self?.getData(str: url)
//            }else{
//                self?.foundNoMoreData()
//                //self.refreshControl.endRefreshing()
//            }
//        }
    }
    func foundNoMoreData(){
        self.tableView?.es.stopLoadingMore()
        self.tableView?.es.noticeNoMoreData()
    }
    
    @objc func pullToRefresh() {
        // tableView?.tableFooterView = UIView()
        resetNoMoreData()
        length = 0
        self.getPromoteData(str: "")
       
        
    }
    func resetNoMoreData(){
        self.tableView?.es.resetNoMoreData()
    }
    
}
