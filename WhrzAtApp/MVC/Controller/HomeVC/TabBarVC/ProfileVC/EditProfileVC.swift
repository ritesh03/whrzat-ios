//
//  EditProfileVC.swift
//  WhrzAtApp
//
//  Created by Sierra 4 on 04/09/17.
//  Copyright © 2017 com.example. All rights reserved.

import UIKit
import SZTextView
import ACFloatingTextfield_Swift

class EditProfileVC: BaseViewController {

    //MARK::- OUTLETS
    @IBOutlet weak var txtAbout: SZTextView?
    @IBOutlet weak var txtPhone: UITextField?
    @IBOutlet weak var txtName: UITextField?
    @IBOutlet weak var imgProfile: UIImageView?
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var countryCodeBtn: UIButton!
    
    //MARK::- LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        onViewDidLoad()
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    //MARK::- BUTON ACTION
    @IBAction func btnActionBack(_ sender: UIButton) {
       popVC()
    }
    @IBAction func btnActionSave(_ sender: UIButton) {
        self.view.endEditing(true)
       if ("".editProfile(name: nil , mobile: /txtPhone?.text)){
            hitAPI()}
    }
    @IBAction func btnActionPickImage(_ sender: Any) {
        selectImage()
    }
    
    //MARK::- FUNCTIONS
    override func donePicking(selectImage: UIImage) {
        imgProfile?.image = selectImage
    }
    
    func onViewDidLoad(){
        guard let user = UserSingleton.shared.loggedInUser else {return}
        txtPhone?.text = /user.contact
        txtName?.text = /user.name
        txtEmail?.text = /user.email
        countryCodeBtn.setTitle(/user.code, for: .normal)
//        if /user.bio == "" {
//            txtAbout?.text = "NA"
//        }else{
//            txtAbout?.text = /user.bio
//        }
        txtAbout?.text = /user.bio
        imgProfile?.image  = #imageLiteral(resourceName: "ic_profile")
        imgProfile?.kf.setImage(with: URL(string : /user.profilePicURL?.original), placeholder: #imageLiteral(resourceName: "ic_profile"), options: nil, progressBlock: nil, completionHandler: { (img, _, _, _) in
            self.selectedImage = img
        })
    }
}

//MARK::- API
extension EditProfileVC{
    func hitAPI(){
        guard let user = UserSingleton.shared.loggedInUser else {return}

        APIManager.shared.request(withImage: HomeEndpoint.editProfile(userId: UserSingleton.shared.loggedInUser?._id, name: txtName?.text, password: nil, contact: txtPhone?.text , radius : nil , bio : txtAbout?.text), image: selectedImage, completion: { [weak self] (res) in
            switch res{
            case .success(let res):
                guard let res = res as? LoginModel else {return}
                let data = res.data
                guard let user = UserSingleton.shared.loggedInUser else {return}
                data?.contactSync = user.contactSync
                data?.isFeed = user.isFeed
                UserSingleton.shared.loggedInUser = data
                Alerts.shared.show(alert: .success, message: AlertMessage.profileUpdate.rawValue, type: .success)
                _ = self?.navigationController?.popToRootViewController(animated: false)
            case .failure(let msg):
                Alerts.shared.show(alert: .error, message: msg, type: .info)
            }
        }, isLoaderNeeded: true)
    }
}
