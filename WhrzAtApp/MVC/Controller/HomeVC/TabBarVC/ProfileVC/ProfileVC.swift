//  ProfileVC.swift
//  WhrzAtApp
//
//  Created by Sierra 4 on 31/08/17.
//  Copyright © 2017 com.example. All rights reserved.

import UIKit
import Optik
import Kingfisher
import EZSwiftExtensions
import IBAnimatable

class ProfileVC: BaseViewController {
    
    //MARK::- OUTLETS
    
    @IBOutlet weak var btnFavorites: UIButton!
    
    @IBOutlet weak var viewFavourite: UIView!
    
    @IBOutlet weak var tableView: UITableView?
    @IBOutlet weak var imgProfile: UIImageView?
    @IBOutlet weak var lblName: UILabel?
    @IBOutlet weak var lblLocation: UILabel?
    @IBOutlet weak var lblLoves: UILabel?
    @IBOutlet weak var lblAbout: UILabel?
    @IBOutlet weak var btnActivity: UIButton?
    @IBOutlet weak var btnNotification: UIButton?
    @IBOutlet weak var lblNotificationCount: UILabel?
    @IBOutlet weak var viewActivity: UIView?
    @IBOutlet weak var viewNotification: UIView?
    @IBOutlet weak var imgHeart: UIImageView?
    @IBOutlet weak var lblNoData: UILabel?
    @IBOutlet weak var btnClear: UIButton?
    @IBOutlet weak var btnSetting: AnimatableButton?
    @IBOutlet weak var btnEdit: AnimatableButton!
    
    //MARK::- PROPERTIES
    var isActivity = false
    var isFav = false
    var tableViewDataSource : TableViewCustomDatasource?
    var arrDataActivity : [FeedData]?
    var arrDataNotification : [NotificationDataa]?
    var favouritesData : [ListFavorite]?
    var count = 0
    var isZoom = false
    
    //MARK::- LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        //rotateButton()
        configureTableView()
        isActivity = true
   
        NotificationCenter.default.addObserver(self, selector: #selector(self.updatePage), name: NSNotification.Name(rawValue: "RefeshPageForNewNotification"), object: nil)
       
        manageButton(btn1: btnActivity, btn2: btnNotification, view1: viewActivity, view2: viewNotification, btn3: btnFavorites, view3: viewFavourite)
        btnActivity?.alpha = 1.0
        btnFavorites.alpha = 0.4
    
        btnNotification?.alpha = 0.4
        viewActivity?.isHidden = false
        viewFavourite?.isHidden = true
        viewNotification?.isHidden = true
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        guard let headerView = tableView?.tableHeaderView else {
            return
        }
        let size = headerView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize)
        if headerView.frame.size.height != size.height {
            headerView.frame.size.height = size.height
            tableView?.tableHeaderView = headerView
            tableView?.layoutIfNeeded()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        NotificationCenter.default.addObserver(self, selector: #selector(self.receiveCount1), name: NSNotification.Name(rawValue: "activeNotificationCount"), object: nil)
        lblNotificationCount?.text = UserSingleton.shared.notificationCount ?? ""
        lblNotificationCount?.isHidden = UserSingleton.shared.notificationCount == ""
        lblNotificationCount?.clipsToBounds = true
        lblNotificationCount?.layer.cornerRadius = 10
        guard let user = UserSingleton.shared.loggedInUser else {return}
        imgProfile?.kf.setImage(with: URL(string : /user.profilePicURL?.original ), placeholder: #imageLiteral(resourceName: "ic_profile"), options: nil, progressBlock: nil, completionHandler: nil)
        lblName?.text = /user.name
        lblAbout?.text = /user.bio
        if !isZoom{
            if isActivity ?? true{
                getActivityDataFromAPI(val: true)
            }else if isFav ?? true {
                getFavouriteDataFromAPI(val: true)
            }
                else{
                getNotificationDataFromAPI(val : true)
            }
        }else{
            isZoom = false
        }
       
        
    }
    
    @objc func receiveCount1(){
        lblNotificationCount?.text = UserSingleton.shared.notificationCount ?? ""
        lblNotificationCount?.isHidden = UserSingleton.shared.notificationCount == ""


    }
    
    //MARK::- BUTTON ACTION
    @IBAction func btnActionSetting(_ sender: UIButton) {
        let vc = StoryboardScene.Home.instantiateSettingsVC()
        ez.topMostVC?.pushVC(vc)
        //        let img = imgProfile?.image?.cgImage?.cropping(to: CGRect.init(x: imgProfile.x, y: imgProfile.y, w: imgProfile?.frame.width, h: imgProfile?.frame.height))
    }
    @IBAction func btnActionEdit(_ sender: Any) {
        let vc = StoryboardScene.Home.instantiateEditProfileVC()
        ez.topMostVC?.pushVC(vc)
    }
    
    @IBAction func btnActionActivity(_ sender: Any) {
        btnActivity?.alpha = 1.0
        btnFavorites.alpha = 0.4
        btnNotification?.alpha = 0.4
        viewActivity?.isHidden = false
        viewFavourite?.isHidden = true
        viewNotification?.isHidden = true
        isActivity = true
        isFav = false
        btnClear?.isHidden = true
        if !(btnActivity?.isSelected ?? false){
            manageButton(btn1: btnActivity, btn2: btnNotification, view1: viewActivity, view2: viewNotification, btn3: btnFavorites, view3: viewFavourite)
            getActivityDataFromAPI(val : true)
        }
    }
    
    @IBAction func btnActionZoom(_ sender: UIButton) {
        isZoom = true
        let imageDownLoader = KFImageDownLoader()
        var imageURLs = [URL]()
        guard let img = URL(string : /UserSingleton.shared.loggedInUser?
            .profilePicURL?.original)else {return}
        imageURLs.append(img)
        let destVC = Optik.imageViewer(withURLs: imageURLs, initialImageDisplayIndex: 0, imageDownloader: imageDownLoader, activityIndicatorColor: .white, dismissButtonImage: #imageLiteral(resourceName: "close.png"), dismissButtonPosition: .topTrailing)
        self.presentVC(destVC)
    }
    
    @IBAction func btnActionNotification(_ sender: Any) {
        btnActivity?.alpha = 0.4
        btnFavorites.alpha = 0.4
        btnNotification?.alpha = 1.0
        viewActivity?.isHidden = true
        viewFavourite?.isHidden = true
        viewNotification?.isHidden = false
        isActivity = false
        isFav = false
        UserSingleton.shared.notificationCount = ""
        lblNotificationCount?.isHidden = true
        if !(btnNotification?.isSelected ?? false){
            manageButton(btn1: btnNotification, btn2: btnActivity, view1: viewNotification, view2: viewActivity, btn3: btnFavorites, view3: viewFavourite)
            getNotificationDataFromAPI(val : true)
        }
    }
    
    @IBAction func btnActionFavorites(_ sender: Any) {
        btnActivity?.alpha = 0.4
        btnFavorites.alpha = 1.0
        btnNotification?.alpha = 0.4
        viewActivity?.isHidden = true
        viewFavourite?.isHidden = false
        viewNotification?.isHidden = true
        isActivity = false
        isFav = true
        if !(btnFavorites?.isSelected ?? false){
            manageButton(btn1: btnFavorites, btn2: btnNotification, view1: viewFavourite, view2: viewNotification, btn3: btnActivity, view3: viewActivity)
            getFavouriteDataFromAPI(val: true)
        }
        
    }
    @IBAction func btnActionClear(_ sender: UIButton) {
        guard let res = UserSingleton.shared.loggedInUser else {return}
        AlertsClass.shared.showAlertController(withTitle: TitleType.Confirmation.rawValue, message: TitleType.clearNotification.rawValue, buttonTitles: [TitleType.confirm.rawValue,TitleType.Cancel.rawValue], responseBlock: { (val) in
            let value = val as AlertTag
            switch value{
            case .yes:
                APIManager.shared.request(with: HomeEndpoint.clear(id: /res._id), completion: { [weak self] (res) in
                    switch res{
                    case .success(_):
                        self?.arrDataNotification = []
                        self?.tableViewDataSource?.items =  []
                        self?.lblNoData?.isHidden = (self?.arrDataNotification?.count ?? 0) > 0
                        self?.btnClear?.isHidden = (self?.arrDataNotification?.count ?? 0) == 0
                        self?.lblNoData?.text = (self?.arrDataNotification?.count ?? 0) > 0 ? "" : TitleType.noNotification.rawValue
                        UserSingleton.shared.notificationCount = ""
                        self?.lblNotificationCount?.isHidden = true
                       
                        self?.tableView?.reloadSections([0], with: .automatic)
                    case .failure(let msg):
                        Alerts.shared.show(alert: .error, message: msg, type: .info)
                    }
                    }, isLoaderNeeded: true)
            default :
                break
            }
        })
    }
}

//MARK::- FUCTIONS
extension ProfileVC{
    func manageButton(btn1 : UIButton? , btn2 : UIButton? , view1 : UIView? , view2 : UIView?, btn3 : UIButton?, view3: UIView?){
        btn1?.isSelected = true
        btn2?.isSelected = false
        view1?.isHidden = false
        view2?.isHidden = true
        btn3?.isSelected = false
        view3?.isHidden = true
    }
    
    
    @objc func updatePage(){
        guard let vc = ez.topMostVC else {return}
        if !isActivity && vc is ProfileVC {
            getNotificationDataFromAPI(val : false)
        }
    }
    
    func rotateButton(){
        UIView.animate(withDuration: 1.5, delay: 0.0, options: .curveLinear, animations: {
            self.btnSetting?.transform = (self.btnSetting?.transform.rotated(by: CGFloat(M_PI)))!
        }) { finished in
            self.rotateButton()
        }
    }
}

//MARK::- TABLEVIEW Configure
extension ProfileVC{
    func configureTableView() {
        tableViewDataSource = TableViewCustomDatasource(items: arrDataActivity as Array<AnyObject>?, height: UITableView.automaticDimension, estimatedHeight: 106, tableView: tableView, cellIdentifier: CellIdentifiers.FeedTableCell.rawValue, configureCellBlock: {(cell, item, indexpath) in
            if self.isActivity {
                let cell = cell as? FeedTableViewCell
                guard let item = item as? FeedData else {return}
                cell?.delegate = self
               // cell?.btnPost?.addTarget(self, action: #selector(self.showEnlargeImageView(sender:)), for: .touchUpInside)
                cell?.loveCount = item.imageId?.likeCount ?? 0
                cell?.btnLove?.isSelected = (item.isLike) ?? false
                cell?.btnLove?.isUserInteractionEnabled = false//
               // cell?.btnPost?.tag = indexpath.row
                cell?.configureCell(data: item)
                cell?.btnLove?.setImage(Utility.functions.HeartColor(val: item.imageId?.likeCount ?? 0), for: .normal)
            }
            else if self.isFav {
                let cell = cell as? FavouritesTableViewCell
                guard let item = item as? ListFavorite else {return}
                cell?.configureCell(data : item)
                cell?.imgIcon?.image = Utility.functions.getHotnessIcon(val: /item.hotness)
                cell?.lblPopularity?.textColor = Utility.functions.getHotnessColor(val: /item.hotness)
                cell?.lblPopularity?.text = Utility.functions.getHotnessTitle(val: /item.hotness)
            }
            else{
                let cell = cell as? ProfileNotificationCell
                guard let item = item as? NotificationDataa else {return}
                cell?.configureCell(data : item , index : indexpath.row)
                cell?.btnProfile?.addTarget(self, action: #selector(self.showProfile(sender:)), for: .touchUpInside)
            }
            
        }, aRowSelectedListener: { (indexPath) in
            self.didSelect(index: indexPath.row)
            
        }, willDisplayCell: { (indexPath) in
        })
        tableView?.delegate = tableViewDataSource
        tableView?.dataSource = tableViewDataSource
        tableView?.reloadData()
    }
    
    func didSelect(index : Int){
        if !isFav && !isActivity{
            if (self.arrDataNotification?[index].hotspotId?.deleted ?? false){
                Alerts.shared.show(alert: .error, message: AlertMessage.hotspotnotfound.rawValue, type: .info)
                return
            }
            let vc = StoryboardScene.Home.instantiateHotspotDetailVC()
            vc.hotspotId = /self.arrDataNotification?[index].hotspotId?._id
            ez.topMostVC?.pushVC(vc)
        }else if isFav {
            if (self.favouritesData?[index].deleted ?? false){
                Alerts.shared.show(alert: .error, message: AlertMessage.hotspotnotfound.rawValue, type: .info)
                return
            }
            let vc = StoryboardScene.Home.instantiateHotspotDetailVC()
            vc.hotspotId = /self.favouritesData?[index]._id
            ez.topMostVC?.pushVC(vc)
        }else {
            if (self.arrDataActivity?[index].hotspotId?.deleted ?? false){
                Alerts.shared.show(alert: .error, message: AlertMessage.hotspotnotfound.rawValue, type: .info)
                return
            }
            let vc = StoryboardScene.Home.instantiateHotspotDetailVC()
            vc.hotspotId = /self.arrDataActivity?[index].hotspotId?._id
            ez.topMostVC?.pushVC(vc)
        }
    }
}

//6121f7dba1e5665f86f9848c
