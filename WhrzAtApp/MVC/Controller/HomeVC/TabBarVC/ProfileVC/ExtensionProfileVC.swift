//
//  ExtensionProfileVC.swift
//  WhrzAtApp
//
//  Created by Sierra 4 on 07/11/17.
//  Copyright © 2017 com.example. All rights reserved.
//

import EZSwiftExtensions
import Optik

//MARK::- IMAGETAP
extension ProfileVC{
    @objc func showProfile(sender : UIButton){
        let data = self.arrDataNotification?[sender.tag]
        if /UserSingleton.shared.loggedInUser?._id == data?.likedBy?._id{return}
        let vc = StoryboardScene.Home.instantiateOtherUserProfileVC()
       if let id = data?.likedBy?._id {
           vc.id = id

       }else{
           vc.id = data?.userId?._id
       }
         self.pushVC(vc)
    
    }
    @objc func showEnlargeImageView(sender : UIButton){
        isZoom = true
        let imageDownLoader = KFImageDownLoader()
        var imageURLs = [URL]()
        guard let img = URL(string : /self.arrDataActivity?[sender.tag].imageId?.picture?.original) else {return}
        imageURLs.append(img)
        let destVC = Optik.imageViewer(withURLs: imageURLs, initialImageDisplayIndex: 0, imageDownloader: imageDownLoader, activityIndicatorColor: .white, dismissButtonImage: #imageLiteral(resourceName: "ic_cancle_circle"), dismissButtonPosition: .topTrailing)
        self.presentVC(destVC)
    }
}

//MARK::- delegate
extension ProfileVC : FeedDelegate{
    func chat(index: Int?) {}
    func report(index: Int?) {}
    func like(index: Int? , isLike : Bool?) {
        guard let user = UserSingleton.shared.loggedInUser else {return}
        APIManager.shared.request(with: HomeEndpoint.likeImage(userId : /user._id , imageId: self.arrDataActivity?[index ?? 0].imageId?._id
            , isLike: (isLike ?? false) ? "true" : "false"), completion: { [weak self] (res) in
                switch res{
                case .success( _):
                    self?.arrDataActivity?[index ?? 0].isLike = isLike
                    self?.arrDataActivity?[index ?? 0].imageId?.likeCount = (self?.arrDataActivity?[index ?? 0].imageId?.likeCount ?? 0) + ((isLike ?? true) ? 1 : -1)
                    self?.count += ((isLike ?? true) ? 1 : (-1))
                    //self?.lblLoves?.text = /self?.count.description + TitleType.loves.rawValue
                    self?.lblLoves?.text = self?.count == 1 ? (/self?.count.description  + TitleType.love.rawValue) :  (/self?.count.description  + TitleType.loves.rawValue)                    
                    
                case .failure( let msg):
                    Alerts.shared.show(alert: .error, message: msg, type: .info)
                    self?.tableView?.reloadRows(at: [IndexPath.init(row: index
                        ?? 0, section: 0)], with: .automatic)
                }
            }, isLoaderNeeded: false)
    }
    func profileTap(index: Int?) {
    
    }
    
    func locationTap(index: Int?) {
        if (self.arrDataActivity?[index ?? 0].hotspotId?.deleted ?? false){
            Alerts.shared.show(alert: .error, message: AlertMessage.hotspotnotfound.rawValue, type: .info)
            return
        }
        let vc = StoryboardScene.Home.instantiateHotspotDetailVC()
        vc.hotspotId = /self.arrDataActivity?[index ?? 0].hotspotId?._id
        ez.topMostVC?.pushVC(vc)
    }
}

//MARK::- API
extension ProfileVC{
    func getActivityDataFromAPI(val : Bool){
        guard let user = UserSingleton.shared.loggedInUser else {return}
        APIManager.shared.request(with: HomeEndpoint.getProfile(id: /user._id  , profileId : /user._id), completion: { [weak self] (response) in
            switch response{
            case .success(let res):
                guard let res = res as? ProfileModel else{return}
                //self?.lblLoves?.text = /res.data?.loves?.toString  + TitleType.loves.rawValue
                self?.lblLoves?.text = res.data?.loves == 1 ? (/res.data?.loves?.toString  + TitleType.love.rawValue) :  (/res.data?.loves?.toString  + TitleType.loves.rawValue)

                self?.count = res.data?.loves ?? 0
                self?.arrDataActivity = res.data?.images ?? []
                for item in self?.arrDataActivity ?? []{
                    item.imageId?.likeCount = item.imageId?.likes?.count
                }
                self?.tableViewDataSource?.cellIdentifier = CellIdentifiers.FeedTableCell.rawValue
                self?.lblNoData?.isHidden = (self?.arrDataActivity?.count ?? 0) > 0
                self?.lblNoData?.text = (self?.arrDataActivity?.count ?? 0) > 0 ? "" : TitleType.noActivity.rawValue
                self?.tableViewDataSource?.items = self?.arrDataActivity
                self?.tableView?.reloadSections([0], with: .automatic)
            case .failure(let msg):
                Alerts.shared.show(alert: .error, message: msg, type: .info)
                self?.lblNoData?.isHidden = (self?.arrDataActivity?.count ?? 0) > 0
                self?.lblNoData?.text = (self?.arrDataActivity?.count ?? 0) > 0 ? "" : TitleType.noActivity.rawValue
                self?.tableViewDataSource?.items = self?.arrDataActivity
                self?.tableViewDataSource?.cellIdentifier = CellIdentifiers.FeedTableCell.rawValue
                self?.tableView?.reloadSections([0], with: .automatic)
                
            }
            }, isLoaderNeeded: val
        )
    }
    
    func getNotificationDataFromAPI(val : Bool){
        guard let user = UserSingleton.shared.loggedInUser else {return}
        APIManager.shared.request(with: HomeEndpoint.getNotifications(id: /user._id  , skip : nil , limit : nil), completion: { [weak self] (response) in
            switch response{
            case .success(let res):
                guard let res = res as? ProfileNotification else {return}
                self?.arrDataNotification = res.data ?? []
                self?.tableViewDataSource?.cellIdentifier = CellIdentifiers.ProfileNotificationCell.rawValue
                self?.tableViewDataSource?.items = self?.arrDataNotification ?? []
                self?.lblNoData?.isHidden = (self?.arrDataNotification?.count ?? 0) > 0
                self?.btnClear?.isHidden = (self?.arrDataNotification?.count ?? 0) == 0
                self?.lblNoData?.text = (self?.arrDataNotification?.count ?? 0) > 0 ? "no" : TitleType.noNotification.rawValue
                //self?.lblNotificationCount?.isHidden = (self?.arrDataNotification?.count ?? 0) > 0 ? false : true
                //self?.lblNotificationCount?.text = self?.arrDataNotification?.count.toString
                self?.tableView?.reloadSections([0], with: .automatic)
            case .failure(let msg):
                Alerts.shared.show(alert: .error, message: msg, type: .info)
                self?.btnClear?.isHidden = (self?.arrDataNotification?.count ?? 0) == 0
                self?.lblNoData?.isHidden = (self?.arrDataNotification?.count ?? 0) > 0
                self?.lblNoData?.text = (self?.arrDataNotification?.count ?? 0) > 0 ? "" : TitleType.noNotification.rawValue
                self?.tableViewDataSource?.items = self?.arrDataNotification ?? []
                self?.tableViewDataSource?.cellIdentifier = CellIdentifiers.ProfileNotificationCell.rawValue
                self?.tableView?.reloadSections([0], with: .automatic)
            }
            }, isLoaderNeeded: val
        )
    }
    func getFavouriteDataFromAPI(val : Bool){
      guard let user = UserSingleton.shared.loggedInUser else {return}
           APIManager.shared.request(with: HomeEndpoint.getFavourites(id: /user._id), completion: { [weak self] (response) in
               switch response{
               case .success(let res):
                   guard let res = res as? FavouriteHotspotDataModel else {return}
                   self?.favouritesData = res.data?.listFavorite ?? []
                   self?.tableViewDataSource?.cellIdentifier = CellIdentifiers.FavouritesTableViewCell.rawValue
                    self?.tableViewDataSource?.items = self?.favouritesData ?? []
                   self?.lblNoData?.isHidden = (self?.favouritesData?.count ?? 0) > 0
                   self?.btnClear?.isHidden = true
                   self?.lblNoData?.text = (self?.favouritesData?.count ?? 0) > 0 ? "no" : TitleType.noFavourite.rawValue
                   self?.tableView?.reloadSections([0], with: .automatic)
               case .failure(let msg):
                   Alerts.shared.show(alert: .error, message: msg, type: .info)
                   self?.btnClear?.isHidden = true
                   self?.lblNoData?.isHidden = (self?.favouritesData?.count ?? 0) > 0
                   self?.lblNoData?.text = (self?.favouritesData?.count ?? 0) > 0 ? "" : TitleType.noFavourite.rawValue
                   self?.tableViewDataSource?.items = self?.favouritesData ?? []
                   self?.tableViewDataSource?.cellIdentifier = CellIdentifiers.FavouritesTableViewCell.rawValue
                   self?.tableView?.reloadSections([0], with: .automatic)
               }
               }, isLoaderNeeded: val
           )
    }

}



