//  ProfileVC.swift
//  WhrzAtApp
//
//  Created by Sierra 4 on 31/08/17.
//  Copyright © 2017 com.example. All rights reserved.

import UIKit
import Optik
class OtherUserProfileVC: BaseViewController {
    
    //MARK::- OUTLETS
    @IBOutlet weak var tableView: UITableView?
    @IBOutlet weak var imgProfile: UIImageView?
    @IBOutlet weak var lblName: UILabel?
    @IBOutlet weak var lblLocation: UILabel?
    @IBOutlet weak var lblLoves: UILabel?
    @IBOutlet weak var lblAbout: UILabel?
    @IBOutlet weak var imgHeart: UIImageView?
    @IBOutlet weak var btnBlock: UIButton?
    @IBOutlet weak var lblNoData: UILabel?
    @IBOutlet weak var btnChat: UIButton?
    @IBOutlet weak var heightBtnBlock: NSLayoutConstraint!
    
    //MARK::- PROPERTIES
    var tableViewDataSource : TableViewCustomDatasource?
    var arrData : [FeedData]?
    var heroid : String?
    var id : String?
    var currentId : String?
    var detailData : ProfileData?
    var count = 0
    
    //MARK::- LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTableView()
        checkStatusUser()
        getDataFromAPI()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        guard let headerView = tableView?.tableHeaderView else {return}
        let size = headerView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize)
        if headerView.frame.size.height != size.height {
            headerView.frame.size.height = size.height
            tableView?.tableHeaderView = headerView
            tableView?.layoutIfNeeded()
        }
    }
    
    //MARK::- BUTTON ACTION
    @IBAction func btnActionCancel(_ sender: Any) {
        self.popVC()
    }
    
    @IBAction func btnActionChat(_ sender: UIButton) {
        let vc = StoryboardScene.Home.instantiateChatUserVC()
        vc.name = /detailData?.name
        vc.img = /detailData?.profilePicURL?.original
        vc.id = /detailData?._id
        vc.fromProfile = true
        self.pushVC(vc)
    }
    @IBAction func btnActionBlock(_ sender: UIButton) {
        AlertsClass.shared.showAlertController(withTitle: TitleType.Confirmation.rawValue, message: sender.isSelected ? TitleType.removeBlocked.rawValue :TitleType.addToBlocked.rawValue, buttonTitles: [sender.isSelected ? TitleType.unblock.rawValue : TitleType.Block.rawValue , TitleType.Cancel.rawValue]) { (tag) in
            let tag = tag as AlertTag
            switch tag{
            case .yes:
                self.manageBlock(isBlock: !(self.btnBlock?.isSelected ?? false))
            default:
                break
            }
        }
    }
    @IBAction func btnActionZoom(_ sender: UIButton) {
        let imageDownLoader = KFImageDownLoader()
        var imageURLs = [URL]()
        guard let img = URL(string : /detailData?.profilePicURL?.original) else {return}
        imageURLs.append( img )
        let destVC = Optik.imageViewer(withURLs: imageURLs, initialImageDisplayIndex: 0, imageDownloader: imageDownLoader, activityIndicatorColor: .white, dismissButtonImage: #imageLiteral(resourceName: "close.png"), dismissButtonPosition: .topTrailing)
        self.presentVC(destVC)
    }
}

//MARK::- TABLEVIEW Configure
extension OtherUserProfileVC {
    func configureTableView() {
        tableViewDataSource = TableViewCustomDatasource(items: arrData as Array<AnyObject>?, height: UITableView.automaticDimension, estimatedHeight: 106, tableView: tableView, cellIdentifier: CellIdentifiers.OtherProfileCell.rawValue, configureCellBlock: {(cell, item, indexpath) in
            let cell = cell as? OtherProfileCell
            guard let item = item as? FeedData else{return}
            cell?.delegate = self
            cell?.configureCell(data : item , index : indexpath.row)
            cell?.btnReport?.isHidden = (/self.id == /self.currentId)
            cell?.btnLove?.isUserInteractionEnabled = (/self.id != /self.currentId)//
            //if (/self.id == /self.currentId){
            cell?.btnLove?.setImage(Utility.functions.HeartColor(val: item.imageId?.likeCount ?? 1), for: .normal)
            //}
            cell?.btnPost?.addTarget(self, action: #selector(self.showEnlargeImageView(sender:)), for: .touchUpInside)
        }, aRowSelectedListener: { (indexPath) in
            self.didSelect(index: indexPath.row)
            
        }, willDisplayCell: { (indexPath) in
        })
        
        tableView?.delegate = tableViewDataSource
        tableView?.dataSource = tableViewDataSource
        tableView?.reloadData()
    }
    
    func didSelect(index : Int){}
    
    func backTo(id : String?){
        self.id = /id
    }
}

//MARK::- IMAGETAP EXTENSION
extension OtherUserProfileVC{
    @objc func showEnlargeImageView(sender : UIButton){
        let imageDownLoader = KFImageDownLoader()
        var imageURLs = [URL]()
        guard let img = URL(string : /self.arrData?[sender.tag].imageId?.picture?.original) else {return}
        imageURLs.append(img)
        let destVC = Optik.imageViewer(withURLs: imageURLs, initialImageDisplayIndex: 0, imageDownloader: imageDownLoader, activityIndicatorColor: .white, dismissButtonImage: #imageLiteral(resourceName: "close.png"), dismissButtonPosition: .topTrailing)
        self.presentVC(destVC)        
    }
    func setData(){
        lblName?.text = /detailData?.name
        lblLocation?.text = /detailData?.email   
        self.count = detailData?.loves ?? 0
        lblLoves?.text = detailData?.loves == 1 ? (/detailData?.loves?.toString  + TitleType.love.rawValue) :  (/detailData?.loves?.toString  + TitleType.loves.rawValue)
        imgProfile?.kf.setImage(with: URL(string : /detailData?.profilePicURL?.original), placeholder: #imageLiteral(resourceName: "ic_profile"), options: nil, progressBlock: nil, completionHandler: nil)
        lblAbout?.text = /detailData?.bio
        btnBlock?.isSelected = detailData?.blockedBy?.contains( /UserSingleton.shared.loggedInUser?._id) ?? false
    }
    
    func checkStatusUser(){
        currentId = /UserSingleton.shared.loggedInUser?._id
        if currentId == id{
            btnBlock?.isHidden = true
            btnChat?.isHidden = true
            heightBtnBlock.constant = 0
        }
    }
}

//MARK::- delegates
extension OtherUserProfileVC : OtherProfileDelegate{
    
    func report(index: Int?) {}
    
    func like(index: Int? , isLike : Bool?) {
        guard let user = UserSingleton.shared.loggedInUser else {return}
        APIManager.shared.request(with: HomeEndpoint.likeImage(userId : /user._id , imageId: self.arrData?[index ?? 0].imageId?._id
            , isLike: (isLike ?? false) ? "true" : "false"), completion: { [weak self] (res) in
                switch res{
                case .success( _):
                    self?.arrData?[index ?? 0].isLike = isLike
                    self?.arrData?[index ?? 0].imageId?.likeCount = (self?.arrData?[index ?? 0].imageId?.likeCount ?? 0) + ((isLike ?? true) ? 1 : -1)
                    self?.count += ((isLike ?? true) ? 1 : (-1))
                    self?.lblLoves?.text = self?.count == 1 ? (/self?.count.description  + TitleType.love.rawValue) :  (/self?.count.description  + TitleType.loves.rawValue)

                case .failure( let msg):
                    Alerts.shared.show(alert: .error, message: msg, type: .info)
                    self?.tableView?.reloadRows(at: [IndexPath.init(row: index
                        ?? 0, section: 0)], with: .none)
                }
            }, isLoaderNeeded: false)
    }

    func tapLocation(index: Int?) {
        if (self.arrData?[index ?? 0].hotspotId?.deleted ?? false){
            Alerts.shared.show(alert: .error, message: AlertMessage.hotspotnotfound.rawValue, type: .info)
            return
        }
        let vc = StoryboardScene.Home.instantiateHotspotDetailVC()
        vc.hotspotId = /self.arrData?[index ?? 0].hotspotId?._id
        pushVC(vc)
    }
}

//MARK::- FUNCTIONS APi Block
extension OtherUserProfileVC{
    func manageBlock(isBlock : Bool){
        guard let user = UserSingleton.shared.loggedInUser else { return }
        APIManager.shared.request(with : HomeEndpoint.blockUser(id : /user._id , isBlock : isBlock ? "true" : "false", contact: /self.id) , completion : { [weak self] (response) in
            switch response{
            case .success(_):
                self!.btnBlock?.isSelected = !self!.btnBlock!.isSelected
            case .failure(let msg):
                Alerts.shared.show(alert: .error, message: msg, type: .info)
            }
            }, isLoaderNeeded: true)
    }
}

//MARK::- API GetData
extension OtherUserProfileVC{
    func getDataFromAPI(){
        guard let user = UserSingleton.shared.loggedInUser else {return}
        APIManager.shared.request(with: HomeEndpoint.getProfile(id: /user._id  , profileId : /self.id), completion: { [weak self] (response) in
            switch response{
            case .success(let res):
                guard let res = res as? ProfileModel else{return}
                self?.detailData = res.data
                self?.setData()
                self?.arrData = res.data?.images ?? []
                for item in self?.arrData ?? []{
                    item.imageId?.likeCount = item.imageId?.likes?.count
                    item.isLike = item.imageId?.likes?.contains(/user._id)
                }
                self?.tableViewDataSource?.items = self?.arrData
                self?.lblNoData?.isHidden = (self?.arrData?.count ?? 0) > 0
                self?.tableView?.reloadData()
            case .failure(let msg):
                Alerts.shared.show(alert: .error, message: msg, type: .info)
            }
        }, isLoaderNeeded: true
        )
    }
}

