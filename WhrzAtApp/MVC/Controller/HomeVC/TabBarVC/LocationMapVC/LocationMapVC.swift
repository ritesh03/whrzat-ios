//
//  LocationMapVC.swift
//  WhrzAtApp
//
//  Created by Sierra 4 on 05/09/17.
//  Copyright © 2017 com.example. All rights reserved.
//

import UIKit
import GoogleMaps

class LocationMapVC: BaseViewController {

    //MARK::- OUTLETS
    
    @IBOutlet weak var mapView: GMSMapView?
    
    //MARK::- PROPERTIES
    var location : [Double]?
    var hotness : String?
    var data  : HotspotData?
    var happeningData : HotspotId?
    @IBOutlet weak var imgHotspot: UIImageView?
    @IBOutlet weak var lblName: UILabel?
    @IBOutlet weak var lblDesc: UILabel?
    @IBOutlet weak var lblPopularity: UILabel?
    @IBOutlet weak var imgPopularity: UIImageView?
    
    //MARK::- LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
       
    }
    override func viewWillAppear(_ animated: Bool) {
        setupMap()
    }
    
    //MARK::- BUTTON ACTION
    @IBAction func btnActionCancel(_ sender: UIButton) {
        self.dismissVC(completion: nil)
    }
    
    //MARK::- FUNCTIONS
    func setupMap(){
        //imgHotspot?.sd_setImage(with: URL(string : /data?.picture?.original))
        if let data = self.data {
            self.imgHotspot?.kf.setImage(with: URL(string : /data.picture?.original))

            lblName?.text = /data.name
            lblDesc?.text = /data.description
        }else{
            self.imgHotspot?.kf.setImage(with: URL(string : /happeningData?.picture?.thumbnail))

            lblName?.text =  /happeningData?.name
            lblDesc?.text =  /happeningData?.description
        }
       
        lblPopularity?.text = Utility.functions.getHotnessTitle(val: /hotness)
        imgPopularity?.image = Utility.functions.getHotnessIcon(val: /hotness)
        lblPopularity?.textColor = Utility.functions.getHotnessColor(val: /hotness)
        
        
        ///var bounds = GMSCoordinateBounds()
        let marker = GMSMarker()
        marker.icon = Utility.functions.getHotnessMarker(val : /hotness )
        marker.position = CLLocationCoordinate2D(latitude: CLLocationDegrees(location?.last ?? 0.0) , longitude: CLLocationDegrees(location?.first
            ?? 0.0))
        marker.map = self.mapView
        let camera: GMSCameraPosition = GMSCameraPosition.camera(withLatitude: CLLocationDegrees(location?.last ?? 0.0) , longitude: CLLocationDegrees(location?.first ?? 0.0) , zoom: 18.0)
        
        
       // mapView?.animate(to: camera)
        self.mapView?.camera = GMSCameraPosition.camera(withTarget: marker.position, zoom: 14.0)

    }
    
}
