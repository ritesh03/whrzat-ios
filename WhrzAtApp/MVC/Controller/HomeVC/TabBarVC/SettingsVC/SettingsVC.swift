//
//  SettingsVC.swift
//  WhrzAtApp
//
//  Created by Sierra 4 on 14/09/17.
//  Copyright © 2017 com.example. All rights reserved.
//

import UIKit
import Contacts
import CoreLocation

class SettingsVC: BaseViewController {
    
    //MARK::- OUTLETS
    
    @IBOutlet weak var switchFeed: UISwitch?
    @IBOutlet weak var switchRadius: UISwitch?
    @IBOutlet weak var lblVersion: UILabel?
    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var lblRadius: UILabel!
    @IBOutlet weak var imgSync: UIImageView!
    @IBOutlet weak var referFriendHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var rewardsHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var rewardsTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var rewardsBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var referFriendButton: UIButton!
    @IBOutlet weak var rewardsButton: UIButton!
    @IBOutlet weak var referFriendIndicator: UIImageView!
    @IBOutlet weak var rewardsIndicator: UIImageView!
    
    //MARK::- PROPERTIES
    var contacts = [CNContact]()
    var store = CNContactStore()
    var phoneArray = [String]()
    var addressDict = [String : Any]()
    var user : RegisterUser?
    var radius : Int?
    var feed : Bool?
    var rewardsResponse : FinalResponse?
    
    
    
    //MARK:- LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        onViewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        checkRewardStatus()
    }
    
    //MARK::- BUTTON ACTION
    @IBAction func btnActionCancel(_ sender: UIButton) {
        self.popVC()
    }
    
    @IBAction func btnActionLogout(_ sender: UIButton) {
        
        AlertsClass.shared.showAlertController(withTitle: TitleType.Confirmation.rawValue, message: TitleType.logoutMsg.rawValue, buttonTitles: [TitleType.Logout.rawValue , TitleType.Cancel.rawValue]) { [weak self](tag) in
            let type = tag as AlertTag
            switch type{
            case .yes:
                self?.logOut()
            default:
                break
            }
        }
    }
    
    @IBAction func btnActionEditProfile(_ sender: UIButton) {
        
        let vc = StoryboardScene.Home.instantiateEditProfileVC()
        self.pushVC(vc)
    }
    
    @IBAction func btnActionNotifications(_ sender: UIButton) {
        let vc = StoryboardScene.Home.instantiateNotificationsVC()
        pushVC(vc)
    }
    
    @IBAction func btnActionCentre(_ sender: Any) {
        slider.value = 100
        lblRadius.text = TimePassed.hundred.rawValue
    }
    
    
    @IBAction func btnActionBlockedContacts(_ sender: UIButton) {
        
        let vc = StoryboardScene.Home.instantiateBlockedContactVC()
        pushVC(vc)
    }
    
    @IBAction func btnActionFeed(_ sender: UIButton) {
        //DO NOTHING
    }
    
    @IBAction func btnActionSyncContact(_ sender: UIButton) {
    }
    
    @IBAction func btnActionTermsPolicy(_ sender: UIButton) {
        let vc = StoryboardScene.Home.instantiateWebPageViewController()
        vc.url = APIConstants.terms.rawValue
        vc.titleHead = TitleType.terms.rawValue
        self.pushVC(vc)
    }
    
    @IBAction func btnActionHelpSupport(_ sender: UIButton) {
        openURLPage(APIConstants.help.rawValue)
    }
    
    @IBAction func deleteProfileAction(_ sender: Any) {
      AlertsClass.shared.showAlertController(withTitle: TitleType.Confirmation.rawValue, message: TitleType.deleteProfile.rawValue, buttonTitles: [TitleType.yes.rawValue , TitleType.no.rawValue]) { (tag) in
          let type = tag as AlertTag
          switch type{
              case .yes:
                self.deleteProfile()
              default:
              break
              
          }
      }
    }
    
    
    @IBAction func switchFeedAction(_ sender: UISwitch) {
        updateSetting()
    }
    @IBAction func switchRadiusAction(_ sender: UISwitch) {
        //Do Nothing
    }
    
    @IBAction func sliderAction(_ sender: UISlider) {
//        if sender.value > 105{
//            slider.value = 200
//        }
//        if sender.value == 200{
//            lblRadius.text = ""
//        }else if sender.value > 100 && sender.value <= 105{
//            lblRadius.text = TimePassed.hundred.rawValue
//        }
        lblRadius.text = sender.value.description.toInt()?.description
        //set radius in the user object
        user = UserSingleton.shared.loggedInUser
        user?.radius = lblRadius.text?.toDouble() ?? 100
        UserSingleton.shared.loggedInUser = user
    }
    
    @IBAction func sliderEndAction(_ sender: UISlider) {
    }
    
    
    @IBAction func referFriendBtnAction(_ sender: Any) {
        
        let text = "Join me on WhrzAt, a perfect app to create Hotspots, share pictures and create events which guide other community members to where the most interesting places to be at a particular time is.\nEnter my code \(String(describing: self.rewardsResponse!.referralCode!)) to earn $5 back after posting your first picture or creating a hotspot!\nDownload the app:\nhttp://onelink.to/a3gr8n"
        let textShare = [ text ]
        let activityViewController = UIActivityViewController(activityItems: textShare , applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop ]

        self.present(activityViewController, animated: true, completion: nil)
    }
    
    @IBAction func rewardsBtnAction(_ sender: Any) {
    let vc = StoryboardScene.Home.instantiateRewardsVC()
    vc.rewardsResponse = rewardsResponse
    pushVC(vc)
    }
    
    
    @IBAction func btnActionChangePassword(_ sender: Any) {
        if (UserSingleton.shared.loggedInUser?.fromFacebook ?? false){
            Alerts.shared.show(alert: .error, message: AlertMessage.notAllowed.rawValue, type: .info)
            return
        }
         let vc = StoryboardScene.Home.instantiateChangePasswordVC()
         pushVC(vc)
    }
}

//MARK::- FUNCTIONS
extension SettingsVC {
    
    func onViewDidLoad(){
        guard let userr = UserSingleton.shared.loggedInUser else {return}
        user = userr
        switchFeed?.isOn = user?.isFeed ?? true
        slider.value = Float(user?.radius == -1 ? 100 : (user?.radius ?? 0))
        lblRadius.text = (user?.radius == -1 ? "100" : (user?.radius?.description.toInt()?.description))
        if let version = (Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String) {
            lblVersion?.text = TitleType.version.rawValue + version

        }
       // checkRewardStatus()
    }
    
    //MARK::- API
    func updateSetting(){
        guard let user = UserSingleton.shared.loggedInUser else {return}
        APIManager.shared.request(with: HomeEndpoint.feedUpdate(id: /user._id, feedStatus: (switchFeed?.isOn ?? true) ? "true" : "false" , radius: nil), completion: { [weak self] (res) in
            switch res{
            case .success(_):
                user.isFeed = self?.switchFeed?.isOn ?? true
                UserSingleton.shared.loggedInUser = user
                
            case .failure(let msg):
                Alerts.shared.show(alert: .error, message: msg, type: .info)
                self?.onViewDidLoad()
            }
            }, isLoaderNeeded: true)
    }
    
    func checkRewardStatus(){
        
        APIManager.shared.request(with: HomeEndpoint.getRewardDetail, completion: { [weak self] (response) in
         switch response{
             case.success(let res):
                guard let resp = res as? RewardModel else{return}
                guard let data = resp.data else{return}
                guard let response = data.finalResponse else{return}
                self!.rewardsResponse = response
               
            if !self!.rewardsResponse!.rewardStatus! { //hide refer a friend and rewards options
                    self!.referFriendHeightConstraint.constant = 0
                    self!.rewardsHeightConstraint.constant = 0
                    self!.rewardsTopConstraint.constant = 0
                    self!.rewardsBottomConstraint.constant = 0
                    self!.referFriendIndicator.isHidden = true
                    self!.rewardsIndicator.isHidden = true
                    self!.referFriendButton.isHidden = true
                    self!.rewardsButton.isHidden = true
               }
                
              case .failure(let msg):
                  Alerts.shared.show(alert: .error, message: msg, type: .error)
                 
              }
              }, isLoaderNeeded: true)
      }
    
    func deleteProfile(){
        guard let user = UserSingleton.shared.loggedInUser else {return}
        APIManager.shared.request(with: HomeEndpoint.deleteUser(id: /user._id), completion: { [weak self] (res) in
            Utility.functions.removeLoader()

              switch res {
              case .success(_):
                
                      UserSingleton.shared.loggedInUser = nil
                             let initialNavVC = StoryboardScene.Login.initialViewController()
                              let VC = StoryboardScene.Login.instantiateLoginSignUpVC()
                            initialNavVC.viewControllers = [VC]
                  
                   UIView.transition(with: UIApplication.shared.keyWindow!, duration: 0.5, options: .transitionFlipFromLeft , animations: { () -> Void in
                          UIApplication.shared.keyWindow?.rootViewController = initialNavVC
                      }) { (completed) -> Void in
                          Utility.functions.removeLoader()

                      }
                  
                            
              case .failure(let msg):

                  Alerts.shared.show(alert: .error, message: msg, type: .error)
                 
              }
              }, isLoaderNeeded: true)
      }
    
    func openURLPage(_ urlString: String?) {
        if let url = URL(string: /urlString) {
            UIApplication.shared.openURL(url)
        }
    }
    func isLocationOn() -> Bool{
        if CLLocationManager.authorizationStatus() == .denied {
            self.settingsAlert()
            return false
        }else{
            return true
        }
    }
    func settingsAlert(){
        AlertsClass.shared.showAlertController(withTitle: TitleType.permissionRequired.rawValue, message: AlertMessage.radiusPermission.rawValue, buttonTitles: [TitleType.setting.rawValue , TitleType.Cancel.rawValue]) { (value) in
            let type = value as AlertTag
            switch type {
            case .yes:
                UIApplication.shared.openURL(NSURL(string:UIApplication.openSettingsURLString)! as URL)
            default:
                break
            }
        }
    }
    

}

