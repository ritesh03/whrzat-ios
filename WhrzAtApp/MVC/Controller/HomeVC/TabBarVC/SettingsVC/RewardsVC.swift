//
//  RewardsVC.swift
//  WhrzAtApp
//
//  Created by Ritesh on 04/05/21.
//  Copyright © 2021 com.example. All rights reserved.
//

import UIKit
import Toast_Swift


class RewardsVC: BaseViewController {

    @IBOutlet weak var tableView: UITableView?
    @IBOutlet weak var redeemButton: UIButton!
    
     var rewardsResponse : FinalResponse?
    
    
    //MARK::- LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let isRedeem = rewardsResponse?.isRedeem else {
            return
        }
        
        guard let redeemRequest = rewardsResponse?.redeemRequest else {
            return
        }
        
        if  isRedeem && redeemRequest {
            self.redeemButton.isUserInteractionEnabled =  false
            self.redeemButton.setTitleColor(.lightGray, for: .normal)
        } else if !isRedeem {
            redeemButton.isHidden = true
        }
    
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    
    //MARK::- BUTTON ACTIONS
    @IBAction func btnActionBack(_ sender: UIButton) {
        popVC()
    }
    
    @IBAction func btnActionSave(_ sender: UIButton) {
        initiateRedeemRequest()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
//MARK::- CONFIGURE TABLEVIEW
extension RewardsVC: UITableViewDelegate, UITableViewDataSource {

func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return 6
}

func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    var cell : UITableViewCell?
    
    switch indexPath.row {
        case 0: //rewards aomount
        print("0")
            let rewardsCell = tableView.dequeueReusableCell(withIdentifier: "RewardsEarnedCell", for: indexPath) as? RewardsTableViewCell
            if let amount = rewardsResponse?.rewardAmount {
                rewardsCell?.amountLabel.text = "$\(String(describing: amount))"
            } else {
                rewardsCell?.amountLabel.text = "$0"
            }
            cell = rewardsCell
        
        case 1: //referal code
        print("1")
            let referalCell = tableView.dequeueReusableCell(withIdentifier: "ReferralCodeCell", for: indexPath) as? RewardsTableViewCell
             referalCell?.referralCodeField.text = rewardsResponse?.referralCode
             referalCell!.referralCodeField.delegate = self
             cell = referalCell
        
        case 2: // locations
        print("2")
            let locationCell = tableView.dequeueReusableCell(withIdentifier: "LocationsCell", for: indexPath) as? RewardsTableViewCell
            //cell.locationButton.addTarget(self, action:#selector(handleAction(_:)), for: .touchUpInside)
            cell = locationCell
        
        case 3: // info 1
        print("3")
            let infoCell = tableView.dequeueReusableCell(withIdentifier: "InfoCell", for: indexPath) as? RewardsTableViewCell
            infoCell!.headingLabel.text = "How to Earn Points"
            infoCell!.subheadingLabel.text = "Earn $5 when you ﬁrst create a hotspot or post a picture to an existing hotspot. Earn another $5 when you refer a friend who creates a new hotspot or posts a picture to an existing hotspot."
            cell = infoCell
        
        case 4: // info2
        print("4")
           let infoCell = tableView.dequeueReusableCell(withIdentifier: "InfoCell", for: indexPath) as? RewardsTableViewCell
           infoCell!.headingLabel.text = "How To Redeem Reward Points"
           infoCell!.subheadingLabel.text = "Cash earned will automatically be sent via Cash App to the number listed during registration. WhrzAt will cannot be held responsible for users who do not have Cash App to receive earned cash at which point user may lose the reward."
            cell = infoCell
         case 5: // info3
         print("5")
            let infoCell = tableView.dequeueReusableCell(withIdentifier: "InfoCell", for: indexPath) as? RewardsTableViewCell
             infoCell!.headingLabel.text = "DISCLAIMER"
             infoCell!.subheadingLabel.text = "Rewards for posting your ﬁrst hotspot/picture may be unavailable, inaccurate or interrupted from time to time for a variety of reasons. We are not responsible for any unavailability, interruptions or errors of your rewards. Furthermore, WhrzAt, makes no warranties, representations, or guarantees of any kind, express or implied, including but not limited to, accuracy, currency, or completeness, the operation of the Program, the information, materials, content, availability, and products."
             cell = infoCell
        
        default:
            print("nothing")
    }
    return cell!
}


    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            switch indexPath.row {
                case 1: //referral code
                    return 125
                case 2: //locations
//                if !rewardsResponse!.locationStatus! {
//                    return 0
//                }
                return 0
                case 3: //info1
                    return 150
                case 4: //info2
                    return 170
                case 5: //info3
                    return 280
                default:
                    break
            }
         return 65
    }
    
//    func handleAction(sender: UIButton){
//       //...
//    }
}
//MARK::-UITextFieldDelegate
extension RewardsVC {
 func endEditing(_ force: Bool) -> Bool { // use to make the view or any subview that is the first responder resign (optionally force)
  return true
}

func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
   // self.becomeFirstResponder()
   // let menuController = UIMenuController.shared
   // let copyItem = UIMenuItem(title: "Copy", action: #selector(copyTextFieldContent))
  //  UIMenuController.shared.menuItems = [copyItem]
    let pasteboard = UIPasteboard.general
    pasteboard.string = textField.text
    self.view.makeToast("copied to clipboard")
    return false
}

//@objc func copyTextFieldContent() {
//    let pasteboard = UIPasteboard.general
//    pasteboard.string = self.referralCodeField.text
// }

}
//MARK::-API
extension RewardsVC{
    
    func initiateRedeemRequest(){
        
        APIManager.shared.request(with: HomeEndpoint.redeemRequest, completion: { [ weak self ] (res) in
            switch res{
            case .success(let res):
                self!.view.makeToast("Redeem request successfully initiated!")
                self?.redeemButton.isUserInteractionEnabled =  false
                self?.redeemButton.setTitleColor(.lightGray, for: .normal)
              
            case .failure(let msg):
                Alerts.shared.show(alert: .error, message: msg, type: .info)
            }
        }, isLoaderNeeded: true)
    }
}
