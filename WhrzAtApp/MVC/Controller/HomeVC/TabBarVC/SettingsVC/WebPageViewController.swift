//
//  WebPageViewController.swift
//  WhrzAtApp
//
//  Created by Sierra 4 on 26/10/17.
//  Copyright © 2017 com.example. All rights reserved.
//

import UIKit

class WebPageViewController: UIViewController {
    
    //MARK::- OUTLETS
    @IBOutlet weak var webView: UIWebView?
    @IBOutlet weak var lbl: UILabel?
    
    
    //MARK::- PROPERTIES
    var titleHead : String?
    var url : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        webView?.delegate = self
        webView?.backgroundColor = .clear
        lbl?.text = /titleHead
        if let url = URL(string: /url) {
            let request = URLRequest(url: url)
            webView?.loadRequest(request)
        }
    }
    
    //MARK::- BUTTON ACTIONS
    
    @IBAction func btnActionBack(_ sender: Any) {
        popVC()
    }
}

//MARK:- Web view delegate
extension WebPageViewController : UIWebViewDelegate {
    func webViewDidStartLoad(_ webView: UIWebView) {
        Utility.functions.loader()
    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
        Utility.functions.removeLoader()
    }
}



