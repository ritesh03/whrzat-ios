//
//  ChangePasswordVC.swift
//  WhrzAtApp
//
//  Created by Sierra 4 on 08/11/17.
//  Copyright © 2017 com.example. All rights reserved.
//

import UIKit
import ACFloatingTextfield_Swift


class ChangePasswordVC: BaseViewController {

    //MARK::- OUTLETS
    
    @IBOutlet weak var txtOldPassword: ACFloatingTextfield?
    
    @IBOutlet weak var txtNewPassword: ACFloatingTextfield?
    
    @IBOutlet weak var txtConfirmPassword: ACFloatingTextfield?
    
    //MARK::- PROPERTIES
    
    //MARK::- LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        textArr = [txtOldPassword , txtNewPassword , txtConfirmPassword]
    }
    
    
    //MARK::- BUTTON ACTION
    
    @IBAction func btnActionSave(_ sender: Any) {
        guard let user = UserSingleton.shared.loggedInUser else {return}
        if "".validateChangePassword(old: txtOldPassword?.text, new: txtNewPassword?.text, confirm: txtConfirmPassword?.text){
            
            APIManager.shared.request(with: LoginEndpoint.changePassword(id : /user._id , old: /txtOldPassword?.text, new: /txtNewPassword?.text), completion: {[weak self] (response) in
                switch response
                {
                case .success(let res):
                    Alerts.shared.show(alert: .success, message: AlertMessage.pwdChange.rawValue, type: .success)
                    _ =  self?.navigationController?.popViewController(animated: false)
                case .failure(let error):
                    Alerts.shared.show(alert: .error, message: error, type: .info)
                }
                
                } , isLoaderNeeded : true)
        }
    }
    
    @IBAction func btnActionBack(_ sender: Any) {
       _ =  self.navigationController?.popViewController(animated: true)
    }
    
    

}
