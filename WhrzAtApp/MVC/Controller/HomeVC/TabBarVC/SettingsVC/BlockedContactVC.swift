//
//  BlockedContactVC.swift
//  WhrzAtApp
//
//  Created by Sierra 4 on 15/09/17.
//  Copyright © 2017 com.example. All rights reserved.
//

import UIKit

class BlockedContactVC: BaseViewController {
    
    //MARK::- OUTLETS
    
    @IBOutlet weak var lblNoData: UILabel?
    @IBOutlet weak var tableView: UITableView?
    
    //MNARK::- PROPERTIES
    var arrData : [Blockedby]?
    var tableViewDataSource : TableViewCustomDatasource?
    var id : String?
    //MARK::- LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        getDataFromAPI()

    }
    
    
    //MARK::- BUTTON ACTION
    
    @IBAction func btnActionBack(_ sender: UIButton) {
        popVC()
    }
}

//MARK::- CONFIGURE TABLEVIEW
extension BlockedContactVC{
    func configureTableView(){
        tableViewDataSource = TableViewCustomDatasource(items: arrData, height: UITableView.automaticDimension, estimatedHeight: 200, tableView: tableView, cellIdentifier: CellIdentifiers.BlockedContactCell.rawValue, configureCellBlock: { (cell, item, indexpath) in
            let cell = cell as? BlockedContactCell
            cell?.configureCell(data : item)
            cell?.delegate = self
        }, aRowSelectedListener: { (indexpath) in
            
        }, willDisplayCell: { (indexpath) in
            
        })
        tableView?.delegate = tableViewDataSource
        tableView?.dataSource = tableViewDataSource
        tableView?.reloadData()
    }
}

//MARK::- API
extension BlockedContactVC{
    func getDataFromAPI(){
        guard let user = UserSingleton.shared.loggedInUser else {return}
        APIManager.shared.request(with: HomeEndpoint.getBlockedList(id: /user._id), completion: { [weak self] (response) in
            switch response{
            case .success(let res):
                guard let res = res as? BlockListModel else{return}
                self?.arrData = res.data ?? []
                self?.tableViewDataSource?.items = self?.arrData
                self?.lblNoData?.isHidden = (self?.arrData?.count ?? 0) > 0
                //self.tableView?.reloadData()
                AnimatableReload.reload(tableView: self?.tableView ?? UITableView(), animationDirection: .down)
                
            case .failure(let msg):
                Alerts.shared.show(alert: .error, message: msg, type: .info)
                self?.lblNoData?.isHidden = (self?.arrData?.count ?? 0) > 0

            }
            }, isLoaderNeeded: true)
        
    }
    
    func unblockUserAPI(index : Int?){
        guard let user = UserSingleton.shared.loggedInUser else { return }
        APIManager.shared.request(with : HomeEndpoint.blockUser(id : /user._id , isBlock : "false", contact: /arrData?[index ?? 0]._id) , completion : { [weak self] (response) in
            switch response{
            case .success(let res):
                self?.arrData?.remove(at: index ?? 0)
                self?.tableViewDataSource?.items = self?.arrData
                self?.lblNoData?.isHidden = (self?.arrData?.count ?? 0) > 0
                self?.tableView?.reloadData()
            case .failure(let msg):
                Alerts.shared.show(alert: .error, message: msg, type: .info)
            }
        }, isLoaderNeeded: true)
    }
}

//MARK::- DELEGATE
extension BlockedContactVC : BlockDelegate{
    func unblockUser(index : Int?){
        AlertsClass.shared.showAlertController(withTitle: TitleType.Confirmation.rawValue, message: TitleType.removeBlocked.rawValue, buttonTitles: [TitleType.unblock.rawValue,TitleType.Cancel.rawValue]) { (val) in
            let value = val as AlertTag
            switch value{
            case .yes:
                self.unblockUserAPI(index : index)
            default:
                break
            }
        }
        
    }
}
