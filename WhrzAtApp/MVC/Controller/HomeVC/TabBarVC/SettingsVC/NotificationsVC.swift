//
//  NotificationsVC.swift
//  WhrzAtApp
//
//  Created by Sierra 4 on 15/09/17.
//  Copyright © 2017 com.example. All rights reserved.
//

import UIKit

class NotificationsVC: BaseViewController {

    //MARK::- OUTLETS
    
    @IBOutlet weak var tableView: UITableView?
    
    //MARK:::- PROPERTIES
    var tableViewDataSource : TableViewCustomDatasource?
    var arrData  = [TitleType.n1.rawValue , TitleType.n2.rawValue, TitleType.n3.rawValue ,TitleType.n4.rawValue ,TitleType.n5.rawValue , TitleType.n6.rawValue]
    var valBool = [false , false , false , false , false , false]
    
    //MARK::- LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        onViewDidLoad()
        configureTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    
    //MARK::- BUTTON ACTIONS
    
    @IBAction func btnActionBack(_ sender: UIButton) {
        popVC()
    }
    
    @IBAction func btnActionSave(_ sender: UIButton) {
        updateSetting()
    }

}

//MARK::- CONFIGURE TABLEVIEW       
extension NotificationsVC{
    func configureTableView(){
        tableViewDataSource = TableViewCustomDatasource(items: arrData as Array<AnyObject>, height: UITableView.automaticDimension, estimatedHeight: 200, tableView: tableView, cellIdentifier: CellIdentifiers.NotificationCell.rawValue, configureCellBlock: { (cell, item, indexpath) in
            let cell = cell as? NotificationCell
            cell?.configureCell(index : indexpath.row , val : self.arrData[indexpath.row])
            cell?.switch?.isOn = self.valBool[indexpath.row]
            cell?.delegate = self
        }, aRowSelectedListener: { (indexpath) in
            
        }, willDisplayCell: { (indexpath) in
            
        })
        tableView?.delegate = tableViewDataSource
        tableView?.dataSource = tableViewDataSource
        tableView?.reloadData()
        //AnimatableReload.reload(tableView: self.tableView ?? UITableView(), animationDirection: .right)

    }
    
    //MARK::- FUNCTION
    func onViewDidLoad(){
        guard let user = UserSingleton.shared.loggedInUser else{return}
        for item in user.notifications ?? []{
            if (item == "" || item == "999"){return}
            valBool[item.toInt() ?? 0] = true
        }
    }
}

//MARK::-API
extension NotificationsVC{
    func getDataFromAPI(){
    }
    
    func updateSetting(){
        guard let user = UserSingleton.shared.loggedInUser else{return}
        var res : [String] = []
        for (index, element) in valBool.enumerated(){
            if element{
                res.append(index.toString)
            }
        }
//        var dict = [String : Any]()
//        dict["userId"] = /user._id
//        dict["notifications"] =
//        res = res == [] ? [""] : res
        
        APIManager.shared.request(with: HomeEndpoint.editNotifications(notifications: res, userId: user._id), completion: { [ weak self ] (res) in
            switch res{
            case .success(let res):
                guard let res = res as? NotificationModel else{return}
//                let user = UserSingleton.shared.loggedInUser
                user.notifications = res.data ?? []
                UserSingleton.shared.loggedInUser  = user
                self?.popVC()
            case .failure(let msg):
                Alerts.shared.show(alert: .error, message: msg, type: .info)
            }
        }, isLoaderNeeded: true)
    }
}

//MARK::-- DELEGATE
extension NotificationsVC : DelegateNotification{
    func valueChange(index: Int?, val: Bool?) {
        valBool[index ?? 0] = val ?? true
    }
}
