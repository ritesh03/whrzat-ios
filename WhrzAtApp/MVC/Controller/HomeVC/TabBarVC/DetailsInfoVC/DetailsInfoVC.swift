//
//  DetailsInfoVC.swift
//  WhrzAtApp
//
//  Created by Subhash Mehta on 09/07/21.
//  Copyright © 2021 com.example. All rights reserved.
//

import UIKit
import IBAnimatable

class DetailsInfoVC: UIViewController {
    
    typealias typeCompletionHandler = (Int?) -> ()
    var complition : typeCompletionHandler = { _  in }
    func dismissVCCompletion(_ completionHandler: @escaping typeCompletionHandler) {
        self.complition = completionHandler
        
    }
    @IBOutlet weak var postImg: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var imgPopular: AnimatableImageView!
    @IBOutlet weak var lblPopularity: UILabel!
    @IBOutlet weak var favButton: UIButton!
    @IBOutlet weak var favCount: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var tagsLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    var detailData : HotspotData?
    @IBOutlet weak var topConstraints: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        if UIScreen.main.bounds.size.height < 670 {
//            topConstraints.constant = 180
//        }
//        
        if detailData?.images?.count ?? 0 > 0 {
            postImg.kf.setImage(with: URL(string : detailData?.images?[0].picture?.original ?? ""), placeholder: UIImage(), options: nil, progressBlock: nil, completionHandler: nil)
        }
        imgPopular.image = Utility.functions.getHotnessIcon(val: /detailData?.hotness)
        lblPopularity.text = Utility.functions.getHotnessTitle(val : /detailData?.hotness)
        lblPopularity.textColor = Utility.functions.getHotnessColor(val: /detailData?.hotness)
        titleLabel.text = /detailData?.name
        if let arr = detailData?.tags{
          let arr1 = arr.map{ $0.trimmingCharacters(in: .whitespaces) }
            tagsLabel?.text =  "#\(String(describing: arr1.joined(separator: " #")))"
        }
        addressLabel.text = /detailData?.area
        descriptionLabel.text = /detailData?.description
        favCount.text = String(detailData?.isFavouriteCount ?? 0)
        favButton.setImage(#imageLiteral(resourceName: "ic_new_favorite"), for: .normal)
        favCount.isHidden = (detailData?.isFavouriteCount == 0) ? true : false
        favButton.isHidden = (detailData?.isFavouriteCount == 0) ? true : false
        
        if detailData!.isFavouriteCount! < 11 {
            favButton.tintColor = UIColor(red: 99.0/255, green: 173.0/255, blue: 252.0/255, alpha: 1.0)
        } else if detailData!.isFavouriteCount! < 20 && detailData!.isFavouriteCount! > 10 {
            favButton.tintColor = UIColor.yellow
        } else if detailData!.isFavouriteCount! < 30 && detailData!.isFavouriteCount! > 20{
            favButton.tintColor = UIColor.orange
        } else if detailData!.isFavouriteCount! > 30 {
            favButton.tintColor = UIColor.red
        }
        
        // Do any additional setup after loading the view.
    }
    @IBAction func actionForCross(_ sender: Any) {
        self.dismissView(selection: 0)
    }
    
    private func dismissView(selection:Int) {
        self.view.removeFromSuperview()
        removeFromParent()
        complition(selection)
    }
   
    @IBAction func outsideTapAction(_ sender: UIButton) {
        self.dismissView(selection: 0)
}
    
}
