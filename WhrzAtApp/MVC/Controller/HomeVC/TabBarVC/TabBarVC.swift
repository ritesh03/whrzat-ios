//
//  TabBarVC.swift
//  WhrzAtApp
//
//  Created by Sierra 4 on 01/09/17.
//  Copyright © 2017 com.example. All rights reserved.

import UIKit
import EZSwiftExtensions
import CoreLocation

class TabBarVC: UITabBarController , UITabBarControllerDelegate {
    
    //MARK::- PROPERTIES
    var index = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    onViewDidLoad()
    }
    

      
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
         onviewWillAppear()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        ez.runThisAfterDelay(seconds: 2) {
            var data = [String: Any]()
            data["uid"] = /UserSingleton.shared.loggedInUser?._id
            SocketIOManager.sharedInstance.requestCount(data)
            SocketIOManager.sharedInstance.requestChat(data)
        }
    }
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {

        
            for (items,index) in (tabBar.items?.enumerated())! {
                if index == item{
                    
                    self.index = items
                }
            }
              let firstSubView = tabBar.subviews[self.index + 1]
                let secondSubView = firstSubView.subviews.first
                secondSubView?.springAnnimate()
            
        
    }
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        if viewController is AddVC{
            if !isLocationOn(){
                return false
            }else{
                return true
            }
           
        }
        
        if viewController is ProfileVC {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "activeNotificationCount"), object: nil)
        }
        return true
    }
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        
//        let animation: CATransition = CATransition()
//        animation.type = kCATransitionFade
//        animation.duration = 0.25
//        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseIn)
//        view.window?.layer.add(animation, forKey: "fadeTransition")
    }
    
    func onViewDidLoad(){
        self.delegate = self
        SocketIOManager.sharedInstance.establishConnection()
        NotificationCenter.default.addObserver(self, selector: #selector(self.receiveChat), name: NSNotification.Name(rawValue: NotificationName.tabPage.rawValue), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.receiveLikeNotification), name: NSNotification.Name(rawValue: NotificationName.profileNotification.rawValue), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.receiveHotspotNotification), name: NSNotification.Name(rawValue: NotificationName.hotspotNotification.rawValue), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.receiveCount), name: NSNotification.Name(rawValue: NotificationName.activeApp.rawValue), object: nil)
        receiveCount()
    }
    func onviewWillAppear(){
        var data = [String: Any]()
        data["uid"] = /UserSingleton.shared.loggedInUser?._id
        SocketIOManager.sharedInstance.requestCount(data)
        SocketIOManager.sharedInstance.requestChat(data)
        if UserSingleton.shared.isPush{
            self.index = 2
            UserSingleton.shared.isPush = false
        }else if UserSingleton.shared.isLikeNotification{
            self.index = 3
            UserSingleton.shared.isLikeNotification = false
        }else if UserSingleton.shared.isHotspotNotification{
            let when = DispatchTime.now() + 1 // change 1 to desired number of seconds
            DispatchQueue.main.asyncAfter(deadline: when) {
                let vc = StoryboardScene.Home.instantiateHotspotDetailVC()
                vc.hotspotId = UserSingleton.shared.id
                ez.topMostVC?.pushVC(vc)
                UserSingleton.shared.isHotspotNotification = false
            }
        }
        self.tabBarController?.selectedIndex = index
        self.selectedIndex = index
    }
    func isLocationOn() -> Bool{
        if CLLocationManager.authorizationStatus() == .denied {
            
            self.settingsAlert()
            return false
        }else{
            return true
        }
    }
    func settingsAlert(){
        if self.index == 1 {
            AlertsClass.shared.showAlertController(withTitle: TitleType.permissionRequired.rawValue, message: "To create new hotspot, please turn on location services in settings", buttonTitles: [TitleType.setting.rawValue , TitleType.Cancel.rawValue]) { (value) in
                let type = value as AlertTag
                switch type {
                case .yes:
                    UIApplication.shared.openURL(NSURL(string:UIApplication.openSettingsURLString)! as URL)
                default:
                    break
                }
            }
        }
    }

  @objc func receiveChat(){
        self.tabBarController?.selectedIndex = 2
        self.selectedIndex = 2
        UserSingleton.shared.isPush = false
        
    }
    
   @objc func receiveLikeNotification(){
        
        self.tabBarController?.selectedIndex = 3
        self.selectedIndex = 3
        UserSingleton.shared.isLikeNotification = false
    }
    
    @objc func receiveHotspotNotification(){
            let vc = StoryboardScene.Home.instantiateHotspotDetailVC()
            vc.hotspotId = UserSingleton.shared.id
            ez.topMostVC?.pushVC(vc)
            UserSingleton.shared.isHotspotNotification = false
    }
    
    @objc func receiveCount(){
        SocketIOManager.sharedInstance.receiveNotificationCount(completionHandler: { [weak self] (res) in
            guard let notification = res else {return}
        self?.tabBar.items?.last?.badgeValue = notification.count == 0 ? nil : /notification.count?.toString
            UserSingleton.shared.notificationCount = self?.tabBar.items?.last?.badgeValue

            if notification.count != 0 {
                guard let _ = UserSingleton.shared.loggedInUser else {return}
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "RefeshPageForNewNotification"), object: nil)}
            }, failure: { (msg) in
                //            print(msg)
        })
        SocketIOManager.sharedInstance.receiveChatCount(completionHandler: { [weak self] (res) in
            guard let chatNotification = res else {return}
            self?.tabBar.items?[2].badgeValue = chatNotification.count == 0 ? nil : /chatNotification.count?.toString
            if ez.topMostVC  is ChatVC && chatNotification.count != 0{
                guard let _ = UserSingleton.shared.loggedInUser else {return}
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.refreshChat.rawValue), object: nil)
            }
            
            }, failure: { (msg) in
                //          print(msg)
        })
        
    }
    
    
  
}

