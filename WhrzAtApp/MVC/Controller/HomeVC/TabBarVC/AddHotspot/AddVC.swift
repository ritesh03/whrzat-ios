//
//  AddVC.swift
//  WhrzAtApp
//
//  Created by Sierra 4 on 19/09/17.
//  Copyright © 2017 com.example. All rights reserved.
//

import UIKit
import EZSwiftExtensions
import CoreLocation

class AddVC: UIViewController {
    
    var isPresnt:Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
            
    }
    override func viewWillAppear(_ animated: Bool) {
        if isPresnt {
            if isLocationOn() {
                let vc  = StoryboardScene.Home.instantiateAddHotspotVC()
                vc.modalPresentationStyle = .fullScreen
                isPresnt = false
                self.presentVC(vc)
            }else{
                isPresnt = true
                let vc = StoryboardScene.Home.initialViewController()
                vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: false, completion: nil)
                self.tabBarController?.selectedIndex = 0
            }
        }else{
            isPresnt = true
            let vc = StoryboardScene.Home.initialViewController()
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: false, completion: nil)
            self.tabBarController?.selectedIndex = 0
        }
        
        //ez.topMostVC?.presentVC(vc)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func isLocationOn() -> Bool{
        if CLLocationManager.authorizationStatus() == .denied {
            return false
        }else{
            return true
        }
    }
    
}
