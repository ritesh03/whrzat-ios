//
//  AddHotspotVC.swift
//  WhrzAtApp
//
//  Created by Sierra 4 on 31/08/17.
//  Copyright © 2017 com.example. All rights reserved.
//

import UIKit
import SZTextView
import ACFloatingTextfield_Swift
import GooglePlacePicker
//import GoogleMapsCore

class AddHotspotVC: BaseViewController {
    
    //MARK::- OUTLETS
    @IBOutlet weak var img: UIImageView?
    @IBOutlet weak var lblAddress: UILabel?
    @IBOutlet weak var txtHotspotName: UITextField?
    @IBOutlet weak var txtTag: ACFloatingTextfield?
    @IBOutlet weak var txtViewDesc: SZTextView?
    
    //MARK::- PROPERTIES
    var lat : String?
    var long : String?
    var area : String?
    
    //MARK::- LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        txtTag?.addLeftTextPadding(16)
        txtHotspotName?.addLeftTextPadding(16)
        txtViewDesc?.contentInset = UIEdgeInsets(top: 0, left: 11, bottom: 0, right: 0)
        NotificationCenter.default.addObserver(self, selector: #selector(updateLabel), name: NSNotification.Name(rawValue: TitleType.locationSetting.rawValue), object: nil)
        lblAddress?.text = /UserSingleton.shared.location
        lat = /UserSingleton.shared.lat?.description
        long = /UserSingleton.shared.long?.description
        area = /UserSingleton.shared.location
    
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        textArr = [txtHotspotName , txtTag ]
        LocationManager.shared.updateHotspotLocation()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        LocationManager.shared.updateUserLocation()
    }
    
    
    override func donePicking(selectImage: UIImage) {
        img?.contentMode = .scaleAspectFill
        img?.image = selectImage
        
    }
    
    
    
    //MARK::- BUTTON ACTION
    @IBAction func btnAction(_ sender: Any) {
        dismiss(animated: false, completion: nil)
    }
    @IBAction func btnACtionCreate(_ sender: Any) {
        if  UserSingleton.shared.lat != "0.0"{
            if "".validateAddHotSpot(name : txtHotspotName?.text , description : txtViewDesc?.text){
                if selectedImage == nil{
                    Alerts.shared.show(alert: .error, message: AlertMessage.imgHotspot.rawValue, type: .info)
                    return
                }
                hitAPI()
            }
        }else{
            settingsAlert()
        }
    }
    @IBAction func btnActionPickImage(_ sender: UIButton) {
        selectImage()
    }
    @IBAction func btnActionLocation(_ sender: UIButton) {
        //        let config = GMSPlacePickerConfig(viewport: nil)
        //        let placePicker = GMSPlacePickerViewController(config: config)
        //        placePicker.delegate = self
        //        self.present(placePicker, animated: true, completion: nil)
    }
    
}
//MARK::- EXTENSION FUNCTIONS

extension AddHotspotVC{
    
    //MARK::- API
    
    func hitAPI(){
        guard let user = UserSingleton.shared.loggedInUser else {return}
        DispatchQueue.main.async {
            //Do UI Code here.
            APIManager.shared.request(withImage: HomeEndpoint.createHotspot(name: /self.txtHotspotName?.text, lat: /self.lat, long: /self.long, description: self.txtViewDesc?.text, createdBy: /user._id, registrationDate: self.txtTag?.text == "" ? nil : /self.txtTag?.text , area: /self.area), image: self.selectedImage, completion: { [weak self](response) in
                switch response{
                case .failure(let msg):
                    Alerts.shared.show(alert: .error, message: msg, type: .info)
                case.success( _):
                    Alerts.shared.show(alert: .success, message: AlertMessage.addedHotspotSuccess.rawValue, type: .success)
                    DispatchQueue.main.async {
                        self?.perform(#selector(self?.dismissView), with: nil, afterDelay: 1.0)
                    }
                  
                }
                }, isLoaderNeeded: true)
        }
       
    }
    @objc func dismissView(){
        self.dismiss(animated: false, completion: {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "PostAdded"), object: nil)
           
                                })
    }

   @objc func updateLabel(){
        lblAddress?.text = /UserSingleton.shared.location
        area = /UserSingleton.shared.location
        lat = /UserSingleton.shared.lat?.description
        long = /UserSingleton.shared.long?.description
    }
    
    func isLocationOn() -> Bool{
        if CLLocationManager.authorizationStatus() == .denied {
            self.settingsAlert()
            return false
        }else{
            return true
        }
    }
    func settingsAlert(){
        AlertsClass.shared.showAlertController(withTitle: TitleType.permissionRequired.rawValue, message: "To create new hotspot, please turn on location services in settings", buttonTitles: [TitleType.setting.rawValue , TitleType.Cancel.rawValue]) { (value) in
            let type = value as AlertTag
            switch type {
            case .yes:
                UIApplication.shared.openURL(NSURL(string:UIApplication.openSettingsURLString)! as URL)
            default:
                break
            }
        }
    }
    
}

//MARK::- GMSPLACEPICKER DELEGATE
extension AddHotspotVC : GMSPlacePickerViewControllerDelegate {
    
    func placePicker(_ viewController: GMSPlacePickerViewController, didPick place: GMSPlace) {
        viewController.dismiss(animated: true, completion: nil)
        self.lat = place.coordinate.latitude.description
        self.long = place.coordinate.longitude.description
        lblAddress?.text = /place.formattedAddress
        self.area = /place.formattedAddress
    }
    
    func placePickerDidCancel(_ viewController: GMSPlacePickerViewController) {
        viewController.dismiss(animated: true, completion: nil)
    }
}

