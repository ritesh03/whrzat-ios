//
//  HomeVC.swift
//  WhrzAtApp
//
//  Created by Sierra 4 on 03/09/17.
//  Copyright © 2017 com.example. All rights reserved.
//

import UIKit
import CoreLocation
import FirebaseDynamicLinks

class HomeVC: BaseViewController {
    
    fileprivate var pageViewController : UIPageViewController!
    @IBOutlet weak var pageView: UIView!
  
    @IBOutlet weak var borderTrending: UILabel!
  //  @IBOutlet weak var borderHappening: UILabel!
    @IBOutlet weak var borderFriends: UILabel!
    @IBOutlet weak var borderEvent: UILabel!
    @IBOutlet weak var btnTrending: UIButton!
   // @IBOutlet weak var btnHappening: UIButton!
    @IBOutlet weak var btnFriend: UIButton!
    @IBOutlet weak var btnEvent: UIButton!
    
    var controllers = [UIViewController]()
    var selectedIndex:Int = 0
    
    //MARK::- LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        if let homeTrendingVC = storyboard.instantiateViewController(withIdentifier: "HomeTrendingVC") as? HomeTrendingVC{
            controllers.append(homeTrendingVC)
        }
        if let homeHappeningVC = storyboard.instantiateViewController(withIdentifier: "HomeHappeningVC") as? HomeHappeningVC {
            controllers.append(homeHappeningVC)
        }
        if let homeFriendsVC = storyboard.instantiateViewController(withIdentifier: "HomeFriendsVC") as? HomeFriendsVC{
            controllers.append(homeFriendsVC)
        }
        if let homeEventVC = storyboard.instantiateViewController(withIdentifier: "HomeEventVC") as? HomeEventVC {
            controllers.append(homeEventVC)
        }
        borderTrending.isHidden = false
      //  borderHappening.isHidden = true
        borderFriends.isHidden = true
        borderEvent.isHidden = true
        
        btnTrending.alpha = 1.0
      //  btnHappening.alpha = 0.4
        btnFriend.alpha = 0.4
        btnEvent.alpha = 0.4
        self.loadContentView()
    }
    

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        _ = LocationManager.shared.currentLoc
        LocationManager.shared.updateUserLocation()
        //LocationManager.shared.requestContactPermissions()
//        Utility.functions.requestForAccess(onlyPermission:true) { [weak self] (val) in
//            
//            print(val)
//
//        }
    }
    
    private func loadContentView(){
        self.pageViewController = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
      //  self.pageViewController.dataSource = self
       // self.pageViewController.delegate = self
        let contentController = viewControllerAtIndex(index: 0)
        let contentControllers = [contentController]
        self.pageViewController.setViewControllers(contentControllers as [UIViewController], direction: .forward, animated: true, completion: nil)
        self.addChild(self.pageViewController)
        self.pageViewController.didMove(toParent: self)
        self.pageViewController.view.translatesAutoresizingMaskIntoConstraints = false
        self.pageViewController.view.frame = CGRect(x: 0, y: 0, width: self.pageView.frame.size.width, height: self.pageView.frame.size.height)
        self.pageView.addSubview(self.pageViewController.view)
      
    }
    private func viewControllerAtIndex(index:Int)-> UIViewController{
        
       let controller = controllers[index]
       
        return controller
    }
    @IBAction func actionForTap(_ sender: UIButton) {
        borderTrending.isHidden = true
      //  borderHappening.isHidden = true
        borderFriends.isHidden = true
        borderEvent.isHidden = true
        btnTrending.alpha = 0.4
       // btnHappening.alpha = 0.4
        btnFriend.alpha = 0.4
        btnEvent.alpha = 0.4
        
        if sender.tag == 0 {
            borderTrending.isHidden = false
            btnTrending.alpha = 1.0
        }else if sender.tag == 1{
           // borderHappening.isHidden = false
            //btnHappening.alpha = 1.0
        }else if sender.tag == 2{
            borderFriends.isHidden = false
            btnFriend.alpha = 1.0
        }else{
            borderEvent.isHidden = false
            btnEvent.alpha = 1.0
        }
       
        let contentController = viewControllerAtIndex(index: sender.tag)
        let contentControllers = [contentController]
        if selectedIndex > sender.tag {
            self.pageViewController.setViewControllers(contentControllers , direction: .reverse, animated: true, completion: nil)
        }else{
            self.pageViewController.setViewControllers(contentControllers , direction: .forward, animated: true, completion: nil)
        }
        selectedIndex = sender.tag
    }
    @IBAction func actionForMap(_ sender: UIButton) {
        let vc = StoryboardScene.Home.instantiateExploreMapVC()
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func actionForExplore(_ sender: UIButton) {
        let vc = StoryboardScene.Home.instantiateExploreListVC()
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
       
    }
}




