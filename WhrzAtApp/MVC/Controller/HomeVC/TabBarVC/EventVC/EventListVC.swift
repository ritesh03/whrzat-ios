//
//  EventListVC.swift
//  WhrzAtApp
//
//  Created by Sierra 4 on 03/09/17.
//  Copyright © 2017 com.example. All rights reserved.
//

import UIKit
import CoreLocation

class EventListVC: BaseViewController {
    
    //MARK::- OUTLETS
    @IBOutlet weak var lblNameHotspot: UILabel?
    @IBOutlet weak var imgStar: UIImageView?
    @IBOutlet weak var tableView: UITableView?
    @IBOutlet weak var lblNoData: UILabel?
    
    //MARK::- PROPERTIES
    var tableViewDataSource : TableViewCustomDatasource?
    var arrData: [EventArr]?
    var hotspotId  : String?
    var location : [Double]?
    var name : String?
    var hotness : String?
    
    //MARK::- LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        lblNameHotspot?.text = /name
        configureTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        getDataFromAPI()
    }
    
    //MARK::- BUTTON ACTION
    @IBAction func btnActionBack(_ sender: Any) {
        popVC()
    }
    @IBAction func btnActionAddEvent(_ sender: Any) {
        if isLocationOn(){
            let vc = StoryboardScene.Home.instantiateAddEventVC()
            vc.hotspotId = /self.hotspotId
            vc.hotness = /self.hotness
            vc.name = /name
            pushVC(vc)
          }
    }
}

//MARK::- EXTENSION TABLEVIEW CONFIGURE
extension EventListVC{
    func configureTableView() {
        
        tableViewDataSource = TableViewCustomDatasource(items: arrData as Array<AnyObject>? , height: UITableView.automaticDimension, estimatedHeight: 130, tableView: tableView, cellIdentifier: CellIdentifiers.EventListCell.rawValue, configureCellBlock: {(cell, item, indexpath) in
            let cell = cell as? EventListCell
            guard let item = item as? EventArr else {return}
            cell?.configureCell(data  : item)
            cell?.lblEventTime?.text = self.getDateFromStamp(val: ((item.startDate ?? 0) / 1000 )) + TitleType.to.rawValue + self.getDateFromStamp(val: ((item.endDate ?? 0) / 1000 ))
            
        }, aRowSelectedListener: { (indexPath) in
            self.didSelect(index: indexPath.row)
            
        }, willDisplayCell: { (indexPath) in
        })
        tableView?.delegate = tableViewDataSource
        tableView?.dataSource = tableViewDataSource
        AnimatableReload.reload(tableView: tableView!, animationDirection: .right)
    }
    
    func didSelect(index : Int){
    }
}

//MARK::- API
extension EventListVC{
    
    func getDataFromAPI(){
        guard let user = UserSingleton.shared.loggedInUser else{return}
        APIManager.shared.request(with: HomeEndpoint.getEvents(hotspotId: /self.hotspotId , userId : /user._id), completion: { [weak self] (res) in
            switch res{
            case .success(let res):
                guard let resp = res as? EventsModel else {return}
                self?.arrData = resp.data?.events ?? []
                self?.lblNoData?.isHidden = (self?.arrData?.count ?? 0 > 0)
                self?.tableViewDataSource?.items = self?.arrData ?? []
                self?.tableView?.reloadData()
                
            case .failure(let msg):
                Alerts.shared.show(alert: .error, message: msg, type: .info)
            }
            }, isLoaderNeeded: true)
    }

}

//MARK::- FUNCTIONS
extension EventListVC{
    //MARK::- GET DATE FROM TIMESTAMP
    func getDateFromStamp(val : Double?) -> String{
        let date = NSDate(timeIntervalSince1970: val ?? 0.0)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM d, h:mm a"
        let localDate = dateFormatter.string(from: date as Date)
        return localDate
        
    }
    
    
    func calculateDistance() -> Bool?{
        let coordinate0 = CLLocation(latitude: CLLocationDegrees(/UserSingleton.shared.lat) ?? 0.0, longitude: CLLocationDegrees(/UserSingleton.shared.long) ?? 0.0)
        let coordinate1 = CLLocation(latitude: CLLocationDegrees((location?.last?.description) ?? "0.0") ?? 0.0, longitude: CLLocationDegrees(location?.first?.description ?? "0.0") ?? 0.0)
        let distanceInMeters = coordinate0.distance(from: coordinate1)
        let distanceInMiles = distanceInMeters/1609.344
        return distanceInMiles <= 0.1
    }
    func isLocationOn() -> Bool{
        if UserSingleton.shared.lat == "0.0" {
            self.settingsAlert()
            return false
        }else{
            return true
        }
    }
    func settingsAlert(){
        AlertsClass.shared.showAlertController(withTitle: TitleType.permissionRequired.rawValue, message: "To add events or to checkin, please confirm your location by turning on location services in settings", buttonTitles: [TitleType.setting.rawValue , TitleType.Cancel.rawValue]) { (value) in
            let type = value as AlertTag
            switch type {
            case .yes:
                UIApplication.shared.openURL(NSURL(string:UIApplication.openSettingsURLString)! as URL)
            default:
                break
            }
        }
    }
    

}

