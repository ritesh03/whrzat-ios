//
//  AddEventVC.swift
//  WhrzAtApp
//
//  Created by Sierra 4 on 03/09/17.
//  Copyright © 2017 com.example. All rights reserved.

import UIKit
import SZTextView
import ACFloatingTextfield_Swift
import IBAnimatable

class AddEventVC: BaseViewController {
    
    //MARK::- OUTLETS
    @IBOutlet weak var imgPopular: UIImageView?
    @IBOutlet weak var lblPopular: UILabel?
    @IBOutlet weak var lblNameHotspot: UILabel?
    @IBOutlet weak var txtEventName: ACFloatingTextfield?
    @IBOutlet weak var txtViewDesc: SZTextView?
    @IBOutlet weak var txtStart: ACFloatingTextfield?
    @IBOutlet weak var txtEnd: ACFloatingTextfield?
    @IBOutlet weak var txtTag: ACFloatingTextfield?
    
    //MARK::- PROPERTIES
    var datePicker = MIDatePicker.getFromNib()
    var dateFormatter = DateFormatter()
    var val : String?
    var startTime : String?
    var endTime : String?
    var hotspotId : String?
    var hotness : String?
    var name : String?
    
    //MARK::- LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDatePicker()
        dateFormatter.dateFormat = "MMM d, h:mm a"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        textArr = [txtEventName]
    }
    
    //MARK::- BUTTON ACTION
    @IBAction func btnActionCancel(_ sender: Any) {
        _ =  self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func btnActionCreate(_ sender: Any) {
        if "".validateAddEvent(eventName : txtEventName?.text , description : txtViewDesc?.text , startTime : /startTime , endTime : /endTime){
            if (endTime?.toDouble()?.isLess(than: startTime?.toDouble() ?? 0)) ?? false || endTime == startTime{
                Alerts.shared.show(alert: .error, message: AlertMessage.invalidTime.rawValue, type: .info)
                return
            }
            if ((endTime?.toDouble() ?? 0) - (startTime?.toDouble() ?? 0) > 86400000){
                Alerts.shared.show(alert: .error, message: AlertMessage.invalidEventDuration.rawValue, type: .info)
                return
            }
            let currentDate = (Date().timeIntervalSince1970 * 1000).description
            if ((endTime?.toDouble() ?? 0) - (currentDate.toDouble() ?? 0) > 86400000){
                Alerts.shared.show(alert: .error, message: AlertMessage.invalidEvent.rawValue, type: .info)
                return
            }
            
            
            hitAPI()
        }
    }
    
    @IBAction func btnActionStartTime(_ sender: Any) {
        val = "start"
        self.view.endEditing(true)
        datePicker.show(inVC: self)
    }
    
    @IBAction func btnActionEndTime(_ sender: Any) {
        val = "end"
        self.view.endEditing(true)
        datePicker.show(inVC: self)
    }
 }

extension AddEventVC: MIDatePickerDelegate {
    
    func miDatePicker(_ amDatePicker: MIDatePicker, didSelect date: Date) {
        if val == "start"{
            txtStart?.text = dateFormatter.string(from: date)
            startTime = (date.timeIntervalSince1970 * 1000).description
        }else{
            txtEnd?.text = dateFormatter.string(from: date)
            endTime = (date.timeIntervalSince1970 * 1000).description
        }
      //  print(dateFormatter.string(from: date))
    }
    
    fileprivate func setupDatePicker() {
        datePicker.delegate = self
        datePicker.config.startDate = Date()
        datePicker.config.animationDuration = 0.25
        datePicker.config.cancelButtonTitle = TitleType.Cancel.rawValue
        datePicker.config.confirmButtonTitle = TitleType.confirm.rawValue
        datePicker.config.contentBackgroundColor = UIColor(red: 253/255.0, green: 253/255.0, blue: 253/255.0, alpha: 1)
        datePicker.config.headerBackgroundColor = UIColor(red: 244/255.0, green: 244/255.0, blue: 244/255.0, alpha: 1)
        datePicker.config.confirmButtonColor = UIColor.red
        datePicker.config.cancelButtonColor = UIColor.red
        imgPopular?.image = Utility.functions.getHotnessIcon(val: /hotness)
        lblPopular?.text = Utility.functions.getHotnessTitle(val: /hotness)
        lblPopular?.textColor = Utility.functions.getHotnessColor(val: /hotness)
        lblNameHotspot?.text = /name
    }

    func miDatePickerDidCancelSelection(_ amDatePicker: MIDatePicker) {
    }
    
    func hitAPI(){
        guard let user = UserSingleton.shared.loggedInUser else {return}
        APIManager.shared.request(with: HomeEndpoint.createEvent(name: /txtEventName?.text, description: /txtViewDesc?.text, start: /startTime, end: /endTime, createdBy: user._id, hotspotId: /self.hotspotId , tags : (txtTag?.text?.isBlank)! ? nil : /txtTag?.text), completion: { [weak self] (res) in
            switch res{
            case .success(_):
                Alerts.shared.show(alert: .success, message: AlertMessage.eventCreated.rawValue, type: .success)
                _ =  self?.navigationController?.popViewController(animated: false)
            case .failure(let msg):
                Alerts.shared.show(alert: .error, message: msg, type: .info)
            }
        }, isLoaderNeeded: true)
    }
    
}


