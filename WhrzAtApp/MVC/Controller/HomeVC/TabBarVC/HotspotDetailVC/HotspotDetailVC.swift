//
//  HotspotDetailVC.swift
//  WhrzAtApp
//  Created by Sierra 4 on 03/09/17.
//  Copyright © 2017 com.example. All rights reserved.

import UIKit
import CoreLocation
import Optik
import FirebaseDynamicLinks
import Firebase

class HotspotDetailVC: BaseViewController {
    
    //MARK::- OUTLETS
    @IBOutlet weak var favouriteUserLabel: UILabel!
    @IBOutlet weak var imgArrow: UIImageView?
    @IBOutlet weak var lblLocationType: UILabel?
    @IBOutlet weak var imgPopular: UIImageView?
    @IBOutlet weak var lblPopularity: UILabel?
    @IBOutlet weak var lblHotSpotName: UILabel?
    @IBOutlet weak var lblDesc: UILabel?
    @IBOutlet weak var lblAddress: UILabel?
    @IBOutlet weak var tableView: UITableView?
    @IBOutlet weak var btnEvents: UIButton?
    @IBOutlet weak var lblNoData: UILabel?
    @IBOutlet weak var heighBtnEvent: NSLayoutConstraint!
    @IBOutlet weak var favButton: UIButton!
    
    
    //MARK::- PROPERTIES
    var isCheckedIn = false
    var detailData : HotspotData?
    var userName:String?
    var hotspotId : String?
    var linkTab:Bool = false
    var chkinTxt : String?
    var arrImagesPost : [Images]?
    var isFirst = false
    var isImageZoom = false
    let refreshControl = UIRefreshControl()
    var favCount = 0
    @IBOutlet weak var hotspotsHeaderView: HeaderHotspot!
    @IBOutlet weak var eventBorder: UILabel!
    
    //MARK::- LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()

       // self.setUpBottomView()
        let nc = NotificationCenter.default
        nc.addObserver(self, selector: #selector(favClick), name: Notification.Name("favClick"), object: nil)
        nc.addObserver(self, selector: #selector(checkIn), name: Notification.Name("checkIn"), object: nil)
        nc.addObserver(self, selector: #selector(checkOut), name: Notification.Name("checkOut"), object: nil)

    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        guard let headerView = tableView?.tableHeaderView else {
            return
        }
        let size = headerView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize)
        if headerView.frame.size.height != size.height {
            headerView.frame.size.height = size.height
            tableView?.tableHeaderView = headerView
            tableView?.layoutIfNeeded()
        }
     }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden = true

         onViewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        //LocationManager.shared.updateUserLocation()
        if !isImageZoom {pullToRefresh()}
        isImageZoom = false
        
    }
    
    
    //MARK::- BUTTON ACTION
    @IBAction func btnActionEvent(_ sender: Any) {
        let vc = StoryboardScene.Home.instantiateEventListVC()
        vc.hotspotId = /self.hotspotId
        vc.location = self.detailData?.location
        vc.name = self.detailData?.name
        vc.hotness = detailData?.hotness
        pushVC(vc)
    }
    func createDynamicLink() {
        let domainLink = APIConstants.fireabasedynamicLink.rawValue
        guard let link = URL(string: "\(domainLink)/?hotspotId=" + self.hotspotId!) else { return }
       let linkbuilder = DynamicLinkComponents(link: link, domainURIPrefix: domainLink)
        linkbuilder?.iOSParameters = DynamicLinkIOSParameters(bundleID: "com.sam.erminesoft.whrzat")
        linkbuilder?.iOSParameters?.appStoreID = APIConstants.appStoreid.rawValue
        
        linkbuilder?.socialMetaTagParameters?.title = APIConstants.appTitle.rawValue
        linkbuilder?.navigationInfoParameters = DynamicLinkNavigationInfoParameters()
        linkbuilder?.navigationInfoParameters?.isForcedRedirectEnabled = false
        linkbuilder?.androidParameters = DynamicLinkAndroidParameters(packageName: "com.codebrew.whrzat")
        linkbuilder?.options = DynamicLinkComponentsOptions()
        linkbuilder?.options?.pathLength = .short
        linkbuilder?.shorten() { url, warnings, error in
          print("The short URL is: \(url!)")
         self.shareLink(url: "\(url!)")

        }
    }

    
    func shareLink(url:String) {

        let  firstname = UserSingleton.shared.loggedInUser!.name!.components(separatedBy: " ").first
        let firstName1 = firstname!.replacingOccurrences(of: "\"", with: "")
        let url = "Check out this hotspot \(firstName1) shared with you \(URL(string: url)!)"
        let activity = UIActivityViewController(activityItems: [url], applicationActivities: nil)
        present(activity, animated: true)

    }
    
    
    @IBAction func btnActionBack(_ sender: Any) {
        if linkTab {
            let vc = StoryboardScene.Home.initialViewController()
             vc.modalPresentationStyle = .fullScreen
            presentVC(vc)
        }else{
            popVC()
        }
    }
    @IBAction func btnActionInfo(_ sender: Any) {
        let controller = StoryboardScene.Home.instantiateDetailsInfoVC()
        controller.detailData = detailData
        navigationController?.addChild(controller)
        controller.navigationItem.hidesBackButton = true
        controller.view?.frame = (navigationController?.view?.frame)!
        controller.hidesBottomBarWhenPushed = true
        
        navigationController?.view.window?.addSubview((controller.view)!)
        controller.dismissVCCompletion { result  in
            // self.showConfirmation()
        }
    }
    @IBAction func favBtnTap(_ sender: Any) {
     
      
    }
    
    
    @IBAction func shareButtonAction(_ sender: UIButton) {
        createDynamicLink()
    }
    
    @IBAction func btnActionAddress(_ sender: UIButton) {
//        let vc = StoryboardScene.Home.instantiateLocationMapVC()
//        vc.hotness = /self.detailData?.hotness
//        vc.location = self.detailData?.location ?? []
//        vc.data = self.detailData
//        presentVC(vc)
    }
    private func setUpBottomView(){
         hotspotsHeaderView.data = self.detailData
          hotspotsHeaderView.lblCheckin?.text = chkinTxt ?? TitleType.checkin.rawValue

         if self.detailData?.hotness == Fire.blue.rawValue {
             
             hotspotsHeaderView.imgCheckIn.image = UIImage(named: "emptyFlame")

         }else{
            if hotspotsHeaderView.lblCheckin?.text == TitleType.checkin.rawValue {
            // if (detailData?.checkedIn?.contains(/UserSingleton.shared.loggedInUser?._id)) ?? false {
           
                 hotspotsHeaderView.imgCheckIn.image = UIImage(named: "emptyFlame")

             }else{
                 hotspotsHeaderView.imgCheckIn.image = Utility.functions.getHotnessIcon(val: /detailData?.hotness)
             }
             

         }

         hotspotsHeaderView.callback = {
         self.favUnfav(hotspotId: self.hotspotId, isFav: !self.detailData!.isFavourite!, cell: self.hotspotsHeaderView)
         }
         
         if let favCount = self.detailData?.isFavouriteCount! {
             setFav(cell: self.hotspotsHeaderView, count: favCount)
         }
     }
    
}

//MARK::- TABLEVIEW DATASOURCE DELEGATE
extension HotspotDetailVC : UITableViewDelegate , UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        lblNoData?.isHidden = (arrImagesPost?.count ?? 0) > 0
        lblNoData?.text = (arrImagesPost?.count ?? 0) > 0 ? "" : TitleType.noImages.rawValue
        return arrImagesPost?.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.HotspotDetailCell.rawValue , for: indexPath) as? HotspotDetailCell else{return UITableViewCell()}
        cell.btnPost?.tag = indexPath.row
        cell.hotness = /detailData?.hotness
       // cell.btnPost?.addTarget(self, action: #selector(self.showEnlargeImageView(sender:)), for: .touchUpInside)
       // cell.btnPost?.backgroundColor = .red
        cell.imgPost?.kf.setImage(with: URL(string : /arrImagesPost?[indexPath.row].picture?.original), placeholder: UIImage(), options: nil, progressBlock: nil, completionHandler: nil)
        
        cell.imgProfile?.kf.setImage(with: URL(string : /arrImagesPost?[indexPath.row].createdBy?.img?.original), placeholder: #imageLiteral(resourceName: "ic_profile"), options: nil, progressBlock: nil, completionHandler: nil)
        let date = Utility.functions.dateFromMilliseconds(ms : Int64((arrImagesPost?[indexPath.row].registrationDate) ?? 0))
        cell.lblTIme?.text = calculateTimeSince(time: /date, isChat: false)
            //detailData?.area
        cell.lblLike?.text = ((arrImagesPost?[indexPath.row].likeCount == 1) ? ( /arrImagesPost?[indexPath.row].likeCount?.toString + TitleType.love.rawValue ) : ( /arrImagesPost?[indexPath.row].likeCount?.toString + TitleType.loves.rawValue))
        cell.btnLike?.isSelected = (arrImagesPost?[indexPath.row].isLike) ?? false
        cell.btnLike?.tag = indexPath.row
        
        cell.lblCheckin?.text = /arrImagesPost?[indexPath.row].createdBy?.name
        userName = /arrImagesPost?[indexPath.row].createdBy?.name
        cell.id = /arrImagesPost?[indexPath.row]._id
        cell.loveCount = arrImagesPost?[indexPath.row].likeCount ?? 0
        cell.btnChat?.isHidden = arrImagesPost?[indexPath.row].createdBy?._id == /UserSingleton.shared.loggedInUser?._id
        cell.btnReport?.isHidden = arrImagesPost?[indexPath.row].createdBy?._id == /UserSingleton.shared.loggedInUser?._id
        cell.btnRemove?.isHidden = arrImagesPost?[indexPath.row].createdBy?._id != /UserSingleton.shared.loggedInUser?._id
        cell.btnLike?.isUserInteractionEnabled = arrImagesPost?[indexPath.row].createdBy?._id != /UserSingleton.shared.loggedInUser?._id//
        cell.btnLike?.setImage(Utility.functions.HeartColor(val: arrImagesPost?[indexPath.row].likeCount ?? 1), for: .normal)
        cell.imgChat?.isHidden = (arrImagesPost?[indexPath.row].createdBy?._id == /UserSingleton.shared.loggedInUser?._id)
        
        if arrImagesPost?[indexPath.row].createdBy == nil ||  (arrImagesPost?[indexPath.row].createdBy!.deleted)! {
         cell.imgChat?.isHidden = true
         cell.btnChat?.isHidden = true
         cell.lblCheckin?.text = "WhrzAt"
         cell.imgProfile?.kf.setImage(with: URL(string :""), placeholder: #imageLiteral(resourceName: "ic_logo"), options: nil, progressBlock: nil, completionHandler: nil)
        }
       
        if arrImagesPost?[indexPath.row].addedBy == "ADMIN"{
            cell.btnChat?.isHidden = true
            cell.btnRemove?.isHidden = true
            cell.btnReport?.isHidden = true
            cell.imgChat?.isHidden = true
            cell.lblCheckin?.text = "WhrzAt"
        }
        
        cell.delegate = self
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 510
            //UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
       //let cell = self.tableView?.dequeueReusableHeaderFooterView(withIdentifier: "HeaderHotspots1")
       // let cell = self.tableView?.dequeueReusableCell(withIdentifier: CellIdentifiers.HeaderHotspot.rawValue) as! HeaderHotspot
      //  let header = cell as! HeaderHotspots1
      //  header.data = self.detailData
      //  header.lblCheckin?.text = chkinTxt ?? TitleType.checkin.rawValue
      //  header.callback = {

//        self.favUnfav(hotspotId: self.hotspotId, isFav: !self.detailData!.isFavourite!, cell: header)
//
//
  //      }
//
//        if let favCount = self.detailData?.isFavouriteCount! {
//        setFav(cell: header, count: favCount)
//        }
//
        
        return UIView()
    }
    
    
    func setFav(cell : HeaderHotspot,count:Int)  {
          
          cell.favImageView.springAnnimate()
        
          if detailData != nil {
              if detailData?.isFavourite == false {
                  cell.favImageView.image = #imageLiteral(resourceName: "ic_favorite")
                  cell.lblFav.text = "Favorite"
                cell.favImageView.tintColor = UIColor(named: "AppTextColor")
              } else {
                  cell.favImageView.image = #imageLiteral(resourceName: "ic_new_favorite")
                  cell.lblFav.text = "Unfavorite"
                  if count < 11 || count == 1 {
                      cell.favImageView.tintColor = UIColor(red: 99.0/255, green: 173.0/255, blue: 252.0/255, alpha: 1.0)
                       }else if count < 20 && count > 10 {
                           cell.favImageView.tintColor = UIColor.yellow
                       }else if count < 30 && count > 20{
                           cell.favImageView.tintColor = UIColor.orange
                       }else if count > 30 {
                           cell.favImageView.tintColor = UIColor.red
                       }
                   }
               }
        }
    

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
}

//MARK::- API
extension HotspotDetailVC{
    //MARK::- API
    func getDetailAPI(isRefresh : Bool?){
        guard let user = UserSingleton.shared.loggedInUser else{return}
        if isRefresh! {
            Utility.functions.loader()
        }
        APIManager.shared.request(with: HomeEndpoint.hotspotDetails(hotspotId: /self.hotspotId , userId : /user._id), completion: { [weak self] (res) in
            Utility.functions.removeLoader()

            switch res{
            case .success(let res):
                guard let response = res as? HotspotDetailModel else {return}
                self?.detailData = response.data
                self?.setHeaderData()
                self?.setUpBottomView()

                self?.arrImagesPost = self?.detailData?.images
                for item in self?.arrImagesPost ?? []{
                    item.likeCount = item.likes?.count
                }
                self?.tableView?.reloadData()
                self?.refreshControl.endRefreshing()
            case .failure(let msg):
                Alerts.shared.show(alert: .error, message: msg, type: .info)
                self?.refreshControl.endRefreshing()
                if msg == AlertMessage.hotspotnotfound.rawValue{
                    self?.navigationController?.popViewController(animated: false)
                }
                
            }
            }, isLoaderNeeded: (isRefresh ?? true))
        isFirst = true
    }
    
    func addImageAPI(){
        isImageZoom = true
        guard let user = UserSingleton.shared.loggedInUser else {return}
        APIManager.shared.request(withImage: HomeEndpoint.addImages(createdBy : user._id , hotspotId : self.hotspotId  , eventId : nil), image: selectedImage, completion: { [weak self] (response) in
            switch response{
            case .failure(let msg):
                Alerts.shared.show(alert: .error, message: msg, type: .info)
            case.success(_):
                self?.getDetailAPI(isRefresh: true)
            }
            }, isLoaderNeeded: true)
    }
    @objc func favClick() {
        
        
        
    }
    
    @objc func checkOut() {
self.getDetailAPI(isRefresh: true)

}
   
    
    @objc func checkIn() {
        self.getDetailAPI(isRefresh: true)

}
    
    //MARK:- FUNCTION
    override func donePicking(selectImage: UIImage) {
        if selectedImage == nil{
            return
        }else{
            addImageAPI()}
    }
    
    func setHeaderData(){
       
        if (detailData?.checkedIn?.contains(/UserSingleton.shared.loggedInUser?._id)) ?? false{
            chkinTxt = TitleType.checkout.rawValue
        }else{
            chkinTxt = TitleType.checkin.rawValue
        }
        lblNoData?.textColor = Utility.functions.getHotnessColor(val: /detailData?.hotness)
        imgPopular?.image = Utility.functions.getHotnessIcon(val: /detailData?.hotness)
        lblPopularity?.text = Utility.functions.getHotnessTitle(val : /detailData?.hotness)
        lblPopularity?.textColor = Utility.functions.getHotnessColor(val: /detailData?.hotness)
        lblHotSpotName?.text = /detailData?.name
        btnEvents?.setTitle((detailData?.count == 0 ? TitleType.no.rawValue : /detailData?.count?.toString) + (detailData?.count == 1 ? TitleType.eventcount.rawValue : TitleType.eventscount.rawValue), for: .normal)
        btnEvents?.isHidden = detailData?.count == 0 ? true : false
        eventBorder?.isHidden = detailData?.count == 0 ? true : false
        
        heighBtnEvent.constant = detailData?.count == 0 ? 0 : 48
        
        imgArrow?.isHidden = (detailData?.count == 0)
        
      //  lblDesc?.text = /detailData?.description
        
      //  self.lblAddress?.text = /detailData?.area
        
        
//        favButton.setImage(#imageLiteral(resourceName: "filled_Star"), for: .normal)
//
//        favButton.isHidden = (detailData?.isFavouriteCount == 0) ? true : false
        
//        if detailData!.isFavouriteCount! < 11 {
//            favButton.tintColor = UIColor(red: 99.0/255, green: 173.0/255, blue: 252.0/255, alpha: 1.0)
//        } else if detailData!.isFavouriteCount! < 20 && detailData!.isFavouriteCount! > 10 {
//            favButton.tintColor = UIColor.yellow
//        } else if detailData!.isFavouriteCount! < 30 && detailData!.isFavouriteCount! > 20{
//            favButton.tintColor = UIColor.orange
//        } else if detailData!.isFavouriteCount! > 30 {
//            favButton.tintColor = UIColor.red
//        }
        
    }
    
    func onViewDidLoad(){
        lblNoData?.text =  "" 
        NotificationCenter.default.addObserver(self, selector: #selector(viewWillAppear(_:)), name: NSNotification.Name(rawValue: NotificationName.detailPage.rawValue), object: nil)
        let nib = UINib(nibName: CellIdentifiers.HeaderHotspot.rawValue, bundle: nil)
        tableView?.register(nib, forHeaderFooterViewReuseIdentifier: CellIdentifiers.HeaderHotspot.rawValue)
        
        tableView?.delegate = self
        tableView?.dataSource = self
        NotificationCenter.default.addObserver(self, selector: #selector(selectImage), name: NSNotification.Name(rawValue: TitleType.addPic.rawValue), object: nil)
        refreshControl.addTarget(self, action: #selector(HotspotDetailVC.pullToRefresh), for: UIControl.Event.valueChanged)
        tableView?.addSubview(refreshControl)
    }
}

//MARK::- IMAGE TAP
extension HotspotDetailVC{
    @objc func showEnlargeImageView(sender : UIButton){
        isImageZoom = true
        let imageDownLoader = KFImageDownLoader()
        var imageURLs = [URL]()
        guard let img = URL(string : /self.arrImagesPost?[sender.tag].picture?.original) else {return}
        imageURLs.append( img )
        let destVC = Optik.imageViewer(withURLs: imageURLs, initialImageDisplayIndex: 0, imageDownloader: imageDownLoader, activityIndicatorColor: .white, dismissButtonImage: #imageLiteral(resourceName: "close.png"), dismissButtonPosition: .topTrailing)
        self.presentVC(destVC)
    }
}

//MARK::- delegate
extension HotspotDetailVC : HotspotDetail{
    func tapName(index : Int?){
        if self.arrImagesPost?[index ?? 0].addedBy == "ADMIN"{return}
        else if self.arrImagesPost?[index ?? 0].createdBy == nil {return}
        else if (self.arrImagesPost?[index ?? 0].createdBy!.deleted)! {return}
        let vc = StoryboardScene.Home.instantiateOtherUserProfileVC()
        vc.id = /self.arrImagesPost?[index ?? 0].createdBy?._id
        pushVC(vc)
    }
    
    func like(index: Int?, isLike: Bool?) {
        guard let user = UserSingleton.shared.loggedInUser else {return}
        APIManager.shared.request(with: HomeEndpoint.likeImage(userId : /user._id , imageId: /self.arrImagesPost?[index ?? 0]._id, isLike: (isLike ?? false) ? "true" : "false"), completion: { [weak self] (res) in
            switch res{
            case .success( _):
                self?.arrImagesPost?[index ?? 0].isLike = (isLike ?? true)
                self?.arrImagesPost?[index ?? 0].likeCount = (self?.arrImagesPost?[index ?? 0].likeCount ?? 0) + ((isLike ?? true) ? 1 : -1)
            case .failure( let msg):
                Alerts.shared.show(alert: .error, message: msg, type: .info)
                self?.tableView?.reloadRows(at: [IndexPath.init(row: index
                    ?? 0, section: 0)], with: .none)
            }
            }, isLoaderNeeded: false)
    }
    
    func favUnfav(hotspotId: String?, isFav: Bool?, cell : HeaderHotspot) {
        guard let user = UserSingleton.shared.loggedInUser else {return}
        APIManager.shared.request(with: HomeEndpoint.favUnfav(hotspotId: hotspotId, userId: /user._id, favourite: isFav), completion: { [weak self] (res) in
            
            switch res{
            case .success( _):
                self!.getDetailAPI(isRefresh: false)
                
            case .failure( let msg):
                //cell.favImageView.image = #imageLiteral(resourceName: "unfilled_star")
                //cell.lblFav.text = "Favourite"
                Alerts.shared.show(alert: .error, message: msg, type: .info)
            }
            }, isLoaderNeeded: true)
    }
    
    func chat(index: Int?) {
        let vc = StoryboardScene.Home.instantiateChatUserVC()
        vc.name = /arrImagesPost?[index ?? 0].createdBy?.name
        vc.img = /arrImagesPost?[index ?? 0].createdBy?.img?.original
        vc.id = /arrImagesPost?[index ?? 0].createdBy?._id
        self.pushVC(vc)
    }
    
    func report(index: Int?) {}
    
    func delete(index: Int?) {
        if self.arrImagesPost?.count == 1{
            Alerts.shared.show(alert: .error, message: AlertMessage.requiredImage.rawValue, type: .info)
            return
        }
        AlertsClass.shared.showAlertController(withTitle: TitleType.Confirmation.rawValue, message: TitleType.removePhoto.rawValue, buttonTitles: [TitleType.delete.rawValue , TitleType.Cancel.rawValue]) { (tag) in
            let type = tag as AlertTag
            switch type{
            case .yes:
                guard let user = UserSingleton.shared.loggedInUser else {
                    return
                }
                
                APIManager.shared.request(with: HomeEndpoint.deletePic(id: /self.arrImagesPost?[index ?? 0]._id  , hotspotid: /self.hotspotId), completion: { [weak self] (res) in
                    switch res{
                    case .success(_):
                        self?.arrImagesPost?.remove(at: index ?? 0)
                        self?.tableView?.reloadSections([0], with: .none)
                    case .failure(let msg):
                        Alerts.shared.show(alert: .error, message: msg, type: .info)
                    }
                    }, isLoaderNeeded: true)
            default:
                break
            }}}
}

//MARK::- PAGING AND PULL TO REFRESH
extension HotspotDetailVC{
    func handlePagination(){
    }
    func refreshData(){
        getDetailAPI(isRefresh: !isFirst)
    }
    @objc func pullToRefresh() {
        refreshData()
    }
}


