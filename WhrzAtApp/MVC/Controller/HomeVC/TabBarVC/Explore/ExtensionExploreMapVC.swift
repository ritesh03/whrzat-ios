
import UIKit
import GoogleMaps
import EZSwiftExtensions
import EZSwiftExtensions
import MapKit

//MARK::- EXTENSION SETTING MARKER
extension ExploreMapVC{
    func setUpMapView() {
        mapView?.clear()
        var bounds = GMSCoordinateBounds()
        if isLocationOn(){
        let marker = GMSMarker()
        marker.icon = #imageLiteral(resourceName: "current-location")
        marker.zIndex = 1000
        marker.position = CLLocationCoordinate2D(latitude: CLLocationDegrees(/UserSingleton.shared.lat) ?? CLLocationDegrees() , longitude: CLLocationDegrees(/UserSingleton.shared.long) ?? CLLocationDegrees())
            marker.map = self.mapView}
        markeres = []
        var i = 0
        self.arrData?.forEach({ (valu) in
            let lat = CLLocationDegrees(/valu.location?.last?.description)
            let long = CLLocationDegrees(/valu.location?.first?.description)
            let val = CLLocationCoordinate2DMake(lat ?? 0.0, long ?? 0.0)
            ez.runThisInMainThread {
                self.setupMarkerIcon(isTap : false , foregroundImage: Utility.functions.getHotnessMarker(val: /valu.hotness), backgroundImage: /valu.picture?.original, finalMarker: { [weak self] (finalImage) in
                    let marker = GMSMarker()
                    marker.iconView  = finalImage
                    marker.position = CLLocationCoordinate2D(latitude: val.latitude , longitude: val.longitude)
                    marker.zIndex = Int32(i)
                    marker.map = self?.mapView
                    self?.markeres?.append(marker)
                    //bounds = bounds.includingCoordinate(val)
                })
                i = i+1
            }
        })
        if (isFirst && /UserSingleton.shared.lat != "0.0"){
            let camera: GMSCameraPosition = GMSCameraPosition.camera(withLatitude: CLLocationDegrees(/UserSingleton.shared.lat) ?? 0.0 , longitude: CLLocationDegrees(/UserSingleton.shared.long) ?? 0.0  , zoom: 14.0)
            mapView?.animate(to: camera)
            isFirst = false
        }
    }
    
    func setupMarkerIcon( isTap : Bool , foregroundImage: UIImage, backgroundImage: String?, finalMarker: @escaping (UIView) -> ()) {
        let DynamicView=UIView(frame: CGRect(x: 0,y: 0,width: 60,height: 60))
        DynamicView.backgroundColor = UIColor.clear
        //Creating Marker Pin imageview for Backgroung
        var imageViewForMarker : UIImageView
        imageViewForMarker  = UIImageView(frame:CGRect(x:0 ,y: 0,width: 40,height: 40))
        imageViewForMarker.backgroundColor = UIColor.lightGray
        imageViewForMarker.center = DynamicView.center
        imageViewForMarker.kf.setImage(with: URL(string : /backgroundImage))
        imageViewForMarker.layer.borderWidth = 2
        imageViewForMarker.layer.borderColor = isTap ? UIColor.red().cgColor : UIColor.white.cgColor
        imageViewForMarker.contentMode = .scaleAspectFill
        imageViewForMarker.layer.cornerRadius = 8
        imageViewForMarker.layer.masksToBounds = true
        imageViewForMarker.isUserInteractionEnabled = false
        var imageViewForPin : UIImageView
        imageViewForPin  = UIImageView(frame:CGRect(x:32,y: 0,width: 20,height: 20))
        imageViewForPin.contentMode = .scaleAspectFill
        imageViewForPin.image = foregroundImage
        DynamicView.addSubview(imageViewForMarker)
        DynamicView.addSubview(imageViewForPin)
        finalMarker(DynamicView)
    }
    
    func isLocationOn() -> Bool{
        if CLLocationManager.authorizationStatus() == .denied {
            return false
        }else{
            return true
        }
    }

}

