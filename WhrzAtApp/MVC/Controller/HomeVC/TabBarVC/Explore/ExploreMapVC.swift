//  ExploreMapVC.swift
//  WhrzAtApp
//
//  Created by Sierra 4 on 31/08/17.
//  Copyright © 2017 com.example. All rights reserved.

import UIKit
import GoogleMaps
import EZSwiftExtensions 
import EZSwiftExtensions
import MapKit

class ExploreMapVC: BaseViewController {
    
    //MARK::- OUTLETS
    @IBOutlet weak var mapView: GMSMapView?
    @IBOutlet weak var btnList: UIButton?
    @IBOutlet weak var bottomCollectionView: NSLayoutConstraint?
    @IBOutlet weak var collectionView: UICollectionView?
    @IBOutlet weak var searchBar: UISearchBar?
    
    //MARK::- PROPERTIES
    var happeningData : [HappeningEventsModel]?
    var arrData : [MapListData]?
    var markeres: [GMSMarker]?
    var coordinatsArray : [CLLocationCoordinate2D]?
    var isFirst = true
    
    var collectionViewdataSource : CollectionViewDataSource?{
        didSet{
            collectionView?.dataSource = collectionViewdataSource
            collectionView?.delegate = collectionViewdataSource
        }
    }
    
    //MARK::- LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        mapView?.isMyLocationEnabled = false
        onViewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        LocationManager.shared.updateUserLocation()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        let val =  ((searchBar?.text?.isEmpty ?? false) || (searchBar?.text?.isBlank ?? false)) ? nil : searchBar?.text
        getData(val : val)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        hideCollectionView()
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    //MARK::- BUTTON ACTION
    @IBAction func btnActionListView(_ sender: UIButton) {
        self.view.endEditing(true)
        let vc = StoryboardScene.Home.instantiateExploreListVC()
        _ = navigationController?.pushViewController(vc, animated: false)
    }
    
    @IBAction func btnActionSearchList(_ sender: UIButton) {
        let vc = StoryboardScene.Home.instantiateExploreListVC()
        vc.isSearch = true
        _ = navigationController?.pushViewController(vc, animated: false)
    }
    
    @IBAction func btnActionCurrentLocation(_ sender: UIButton) {
        if isLocationOn(){
        let camera: GMSCameraPosition = GMSCameraPosition.camera(withLatitude: CLLocationDegrees(/UserSingleton.shared.lat) ?? 0.0 , longitude: CLLocationDegrees(/UserSingleton.shared.long) ?? 0.0  , zoom: 14.0)
            mapView?.animate(to: camera)
        }else{
            settingsAlert()
        }
    }
}

//MARK::- COLLECTIONVIEW
extension ExploreMapVC{
    func configureCollectionView(){
        let width = UIScreen.main.bounds.width
        collectionViewdataSource = CollectionViewDataSource(items: arrData , collectionView: collectionView, cellIdentifier: CellIdentifiers.ExploreMapCell.rawValue, headerIdentifier: "", cellHeight: 122, cellWidth: (width  )  , cellSpacing: 0, configureCellBlock: { (cell, item, indexpath) in
            let cell = cell as? ExploreMapCell
            guard let item = item as? MapListData else{return}
            cell?.configureCell(data : item)
        }, aRowSelectedListener: { (indexPath) in
            self.didSelectFunc(index : indexPath.item)
        }, willDisplayCell: { (indexPath) in
            
        }, scrollViewListener: { [weak self] (UIScrollView) in
            self?.managePageControl()
        })
        collectionView?.reloadData()
    }
    
    //MARK::- DID SELECT
    func didSelectFunc(index : Int){
        let vc = StoryboardScene.Home.instantiateHotspotDetailVC()
        vc.hotspotId = /arrData?[index]._id
        ez.topMostVC?.pushVC(vc)
    }
}

//MARK::- FUNCTIONS
extension ExploreMapVC{
    func onViewDidLoad(){
        let swipeGesture = UISwipeGestureRecognizer(target: self, action: #selector(self.hideCollectionView))
        swipeGesture.direction = .down
        collectionView?.addGestureRecognizer(swipeGesture)
        configureCollectionView()
        mapView?.delegate = self
    }
    
    @objc func hideCollectionView(){
        UIView.animate(withDuration: 0.5, animations: {
            self.bottomCollectionView?.constant = -130
            self.view.layoutIfNeeded()
        })
    }
    func settingsAlert(){
        
        AlertsClass.shared.showAlertController(withTitle: TitleType.permissionRequired.rawValue, message: TitleType.locationSetting.rawValue, buttonTitles: [TitleType.setting.rawValue,TitleType.Cancel.rawValue]) { (value) in
            let type = value as AlertTag
            switch type {
            case .yes:
                UIApplication.shared.openURL(NSURL(string:UIApplication.openSettingsURLString)! as URL)
            default:
                break
            }
        }
    }
}

//MARK:-  Delegate
extension ExploreMapVC: GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        self.view.endEditing(true)
        var isFlag : Bool = false
        self.markeres?.forEachEnumerated({ (index, marker_) in
            if marker_ == marker {
                self.scrollToSelectedIndexPath(index)
                isFlag = true
            }
        })
        if isFlag{
            UIView.animate(withDuration: 0.5, animations: {
                self.bottomCollectionView?.constant = 8
                self.view.layoutIfNeeded()
            })}
        return true
    }
    
    func scrollToSelectedIndexPath(_ index: Int) {
        collectionView?.scrollToItem(at: IndexPath.init(item: index, section: 0), at: .centeredHorizontally, animated: false)
    }
}

//MARK::- API GET DATA
extension ExploreMapVC{
    func getData(val : String?){
        guard let user = UserSingleton.shared.loggedInUser else {return}
        let lat = UserSingleton.shared.lat
        let long = UserSingleton.shared.long
        var rad = user.radius?.toInt ?? 100
        if rad == -1{rad = 100}
        APIManager.shared.request(with: HomeEndpoint.hotspotMapView(lat: lat, long: long, radius: rad.description, desc: nil, id: user._id, search: val , skip : "0" , limit : "100"), completion: { [weak self] (res) in
            switch res{
            case .success(let res):
                guard let resp = res as? MapModel else {return}
                self?.arrData = resp.data?.dataMap ?? []
                self?.collectionViewdataSource?.items = self?.arrData
                self?.collectionView?.reloadData()
                self?.setUpMapView()
            case .failure(let msg):
                self?.setUpMapView()
                Alerts.shared.show(alert: .error, message: msg, type: .info)
            }
            }, isLoaderNeeded: isFirst)
    }
    
    //MARK::- PAGE SCROLL
    func managePageControl(){
                guard let origin = self.collectionView?.contentOffset else{return}
                guard let size = self.collectionView?.bounds.size else{return}
                let visibleRect = CGRect(origin: origin, size: size )
                let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
                let indexPath = self.collectionView?.indexPathForItem(at: visiblePoint)
                let lat = CLLocationDegrees(/(self.arrData?[indexPath?.item ?? 0].location?.last?.description))
                let long = CLLocationDegrees(/(self.arrData?[indexPath?.item ?? 0].location?.first?.description))
                let camera: GMSCameraPosition = GMSCameraPosition.camera(withLatitude: lat ?? 0.0 , longitude: long ?? 0.0  , zoom: 14.0)
                mapView?.animate(to: camera)
    }
}


