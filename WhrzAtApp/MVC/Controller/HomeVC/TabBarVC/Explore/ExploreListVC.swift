//
//  ExploreListVC.swift
//  WhrzAtApp
//
//  Created by Sierra 4 on 31/08/17.
//  Copyright © 2017 com.example. All rights reserved.
//

import UIKit
import IQKeyboardManager
import EZSwiftExtensions
import ESPullToRefresh
import CoreLocation

class ExploreListVC: BaseViewController , UISearchBarDelegate {
    
    //MARK::- OUTLETS
    @IBOutlet weak var collectionViewTrend: UICollectionView?
    @IBOutlet var tabBarView: [UIView]!
    @IBOutlet weak var tableView: UITableView?
    @IBOutlet weak var btnMap: UIButton?
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var lblNoData: UILabel?
    @IBOutlet weak var createEventButton: UIButton!
    
    //MARK::- PROPERTIES
    var tableViewDataSource : TableViewCustomDatasource?
    var arrData : [MapListData]?
    var promoteArrData : [MapListData]?
    var isSearch = false
    var isFirst = true
    var isFirstEvent = true
    var skip = 0
    var offset = 10
    var length = 0
    let refreshControl = UIRefreshControl()
    let refreshControl1 = UIRefreshControl()
    var isLoadMore = false
    var isPullDown = true
    var tab = 1
    var eventSrchStr = ""
    var hotspotStr = ""
    var didTapDeleteKey = false
    @IBOutlet weak var trendingBtn: UIButton!
    @IBOutlet weak var eventBtn: UIButton!
    
    //MARK::- LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
       self.searchBar.searchTextField.backgroundColor = UIColor(red: 232/255, green: 230/255, blue: 230/255, alpha: 1.0)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.updateLocationData),
                                               name: NSNotification.Name(rawValue: "LocationReceived"),
                                               object: nil)
        searchBar?.delegate = self
        let textFieldInsideUISearchBar = searchBar.value(forKey: "searchField") as? UITextField
        textFieldInsideUISearchBar?.textColor = UIColor(red: 25/255, green: 26/255, blue: 25/255, alpha: 1.0)
        if let textFieldInsideSearchBar = self.searchBar.value(forKey: "searchField") as? UITextField,
           let glassIconView = textFieldInsideSearchBar.leftView as? UIImageView {
            //Magnifying glass
            glassIconView.image = UIImage(named: "ic_search")?.withRenderingMode(.alwaysTemplate)
            glassIconView.tintColor = UIColor(red: 25/255, green: 26/255, blue: 25/255, alpha: 1.0)
        }

//        // SearchBar placeholder
//        let labelInsideUISearchBar = textFieldInsideUISearchBar!.value(forKey: "placeholderLabel") as? UILabel
//        labelInsideUISearchBar?.textColor = UIColor.red
        
        if (isSearch) {searchBar?.becomeFirstResponder()}
        configureTableView()
        refreshControl.addTarget(self, action: #selector(self.pullToRefresh), for: UIControl.Event.valueChanged)
        tableView?.addSubview(refreshControl)
        refreshControl1.addTarget(self, action: #selector(self.pullToRefresh), for: UIControl.Event.valueChanged)
        collectionViewTrend?.addSubview(refreshControl1)
       // if isLocationOn() {
            handlePagination()
            handleCollectionPagination()
       // }
        createEventButton.layer.cornerRadius = 4
        createEventButton.layer.shadowColor = UIColor.black.cgColor
        createEventButton.layer.shadowOffset = CGSize(width: 1, height: 1)
        createEventButton.layer.shadowOpacity = 3.0
        createEventButton.layer.shadowRadius = 2.5
        createEventButton.isHidden = true
        //getPromoteData(str: "")
      //  self.pullToRefresh()
        self.lblNoData?.text = "No hotspots found"
        collectionViewTrend?.isHidden = false
        tableView?.isHidden = true
        trendingBtn.alpha = 1.0
        eventBtn.alpha = 0.4
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if self.tab == 0 {
            self.lblNoData?.text = "No events found"
            self.tabBarView[1].isHidden = false
            self.tabBarView[0].isHidden = true
            self.tab = 0
          //  createEventButton.isHidden = false
            if isLocationOn() {
                
                getPromoteData(str: eventSrchStr)
            }
            trendingBtn.alpha = 0.4
            eventBtn.alpha = 1.0
        }else{
            self.lblNoData?.text = "No hotspots found"
            self.tabBarView[0].isHidden = false
            self.tabBarView[1].isHidden = true
            self.tab = 1
           // createEventButton.isHidden = true
            if isLocationOn() {
                
                self.pullToRefresh()
            }
            trendingBtn.alpha = 1.0
            eventBtn.alpha = 0.4
        }
        //  pullToRefresh()
    }
    
    @objc func updateLocationData(notification:NSNotification){
            Utility.functions.loader()
            self.pullToRefresh()
        
}
    
    //MARK::- BUTTON ACTION
    @IBAction func btnActionMap(_ sender: Any) {
       // self.view.endEditing(true)
        //_ =  navigationController?.popViewController(animated: false)
        self.navigationController?.popViewController(animated: true)
        
    }
    @IBAction func tabButtonAction(_ sender: UIButton) {
        self.lblNoData?.isHidden = true
        if sender.tag == 0 {
            self.lblNoData?.text = "No events found"
            self.tabBarView[1].isHidden = false
            self.tabBarView[0].isHidden = true
            self.tab = 0
          //  createEventButton.isHidden = false
            getPromoteData(str: eventSrchStr)
            trendingBtn.alpha = 0.4
            eventBtn.alpha = 1.0
        }else{
            self.lblNoData?.text = "No hotspots found"
            self.tabBarView[0].isHidden = false
            self.tabBarView[1].isHidden = true
            self.tab = 1
           // createEventButton.isHidden = true
            self.pullToRefresh()
            trendingBtn.alpha = 1.0
            eventBtn.alpha = 0.4
        }
        configureTableView()
        self.tableView!.setContentOffset(.zero, animated: true)
        
        
    }
    
    @IBAction func createEventBtnTap(_ sender: Any) {
        if let url = URL(string: "https://whrzat.com/even.html"), UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.openURL(url)
        }
    }
    
    func searchBarSearchButtonClicked( _ searchBar: UISearchBar){
        self.view.endEditing(true)
        if tab == 1 {
            hotspotStr = searchBar.text!
            pullToRefresh()
        }else{
            eventSrchStr = searchBar.text!
            getPromoteData(str: eventSrchStr)
        }
        
        
    }
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = true
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
        
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
       
        if !didTapDeleteKey && searchText.isEmpty {
            hotspotStr = ""
            eventSrchStr = ""
            if tab == 1 {
                pullToRefresh()
            }else{
                
                getPromoteData(str: eventSrchStr)
            }
        }else{
            
            if tab == 1 {
                if searchText.count > 3{
                    hotspotStr = searchText
                }else{
                     hotspotStr = ""
                }
               
                pullToRefresh()
            }else{
                if searchText.count > 3{
                    eventSrchStr = searchText
                }
                else{
                    eventSrchStr = ""
                }
                getPromoteData(str: eventSrchStr)
            }
        }
        didTapDeleteKey = false
    }
    
    func searchBar(_ searchBar: UISearchBar,
                   shouldChangeTextIn range: NSRange,
                   replacementText text: String) -> Bool
    {
        didTapDeleteKey = text.isEmpty
        
        return true
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
        hotspotStr = searchBar.text!
        eventSrchStr = searchBar.text!
        searchBar.text = ""
        if tab == 1 {
            pullToRefresh()
        }else{
            
            getPromoteData(str: eventSrchStr)
        }
        
    }
    
    
    func isLocationOn() -> Bool{
        if CLLocationManager.authorizationStatus() == .denied {
            self.settingsAlert()
            return false
        }else{
            return true
        }
    }
    
    func settingsAlert(){
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5)
        {
            
            AlertsClass.shared.showAlertController(withTitle: TitleType.permissionRequired.rawValue, message: TitleType.locationSetting.rawValue, buttonTitles: [TitleType.setting.rawValue , TitleType.Cancel.rawValue]) { (value) in
                let type = value as AlertTag
                self.lblNoData?.isHidden = self.arrData?.count ?? 0 > 0

                switch type {
                case .yes:
                    UIApplication.shared.openURL(NSURL(string:UIApplication.openSettingsURLString)! as URL)
                default:
                    break
                }
            }
        }
    }
}


//MARK::- TABLEVIEW Configure
extension ExploreListVC{
    
   
    func configureTableView() {
        if tab == 0 {
            tableView?.isHidden = false
            collectionViewTrend?.isHidden = true
            
            
            tableViewDataSource = TableViewCustomDatasource(items: promoteArrData, height: 295, estimatedHeight: 106, tableView: tableView, cellIdentifier: "Promoted", configureCellBlock: {(cell, item, indexpath) in
                let cell = cell as? ExploreListCell
                guard let item = item as? MapListData else {return}
                
                cell?.configureCell(data : item)
                cell?.lblLocation.text = /item.locationName
                cell?.lblForTime?.text =  self.getDateFromStamp(val: ((item.startDate ?? 0) / 1000 )) + TitleType.to.rawValue + self.getDateFromStamp(val: ((item.endDate ?? 0) / 1000 ))
               // cell?.lblForTime?.text =  self.getDateFromStamp(val: ((/item.startDate ?? 0) / 1000 )) + TitleType.to.rawValue + self.getDateFromStamp(val: ((/item.endDate ?? 0) / 1000 ))
                
            }, aRowSelectedListener: { (indexPath) in
                self.promoteDetail(index: indexPath.row)
                
            }, willDisplayCell: { (indexPath) in
            })
        }else{
            tableView?.isHidden = true
            collectionViewTrend?.isHidden = false
            collectionViewTrend?.reloadData()
//            tableViewDataSource = TableViewCustomDatasource(items: arrData, height: UITableView.automaticDimension, estimatedHeight: 106, tableView: tableView, cellIdentifier: CellIdentifiers.ExploreListCell.rawValue, configureCellBlock: {(cell, item, indexpath) in
//                let cell = cell as? ExploreListCell
//                guard let item = item as? MapListData else {return}
//                cell?.configureCell(data : item)
//                cell?.imgIcon?.image = Utility.functions.getHotnessIcon(val: /item.hotness)
//                cell?.lblPopularity?.textColor = Utility.functions.getHotnessColor(val: /item.hotness)
//                cell?.lblPopularity?.text = Utility.functions.getHotnessTitle(val: /item.hotness)
//            }, aRowSelectedListener: { (indexPath) in
//                self.didSelect(index: indexPath.row)
//
//            }, willDisplayCell: { (indexPath) in
//            })
        }
        
        
        tableView?.delegate = tableViewDataSource
        tableView?.dataSource = tableViewDataSource
        AnimatableReload.reload(tableView: tableView ?? UITableView(), animationDirection: .right)
        self.refreshControl.endRefreshing()
    }
    func promoteDetail(index : Int) {
        let vC = StoryboardScene.Home.instantiatePromotedVC()
        vC.promoteData = self.promoteArrData![index]
        ez.topMostVC?.pushVC(vC)
    }
    func didSelect(index : Int){
        let vC = StoryboardScene.Home.instantiateHotspotDetailVC()
        vC.hotspotId = /arrData?[index]._id
        ez.topMostVC?.pushVC(vC)
    }
    func getDateFromStamp(val : Double?) -> String{
        let date = NSDate(timeIntervalSince1970: val ?? 0.0)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM d, h:mm a"
        let localDate = dateFormatter.string(from: date as Date)
        return localDate
        
    }
}
extension ExploreListVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrData?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionViewTrend?.dequeueReusableCell(withReuseIdentifier: "TrendingCollectionCell", for: indexPath) as! TrendingCollectionCell
        let item = arrData?[indexPath.item]
        cell.configureCell(data : item)
        cell.imgIcon?.image = Utility.functions.getHotnessIcon(val: /item?.hotness)
        cell.lblPopularity?.textColor = Utility.functions.getHotnessColor(val: /item?.hotness)
        cell.lblPopularity?.text = Utility.functions.getHotnessTitle(val: /item?.hotness)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vC = StoryboardScene.Home.instantiateHotspotDetailVC()
        vC.hotspotId = /arrData?[indexPath.item]._id
        self.pushVC(vC)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width:((UIScreen.main.bounds.size.width - 56) / 2) , height: (((UIScreen.main.bounds.size.width - 56) / 2) * 1.37))
    }
    
}

//MARK::- GET DATA
extension ExploreListVC{
    
    func getPromoteData(str: String?){
        //if isLocationOn() {
            guard let user = UserSingleton.shared.loggedInUser else {return}
            
            let lat = UserSingleton.shared.lat
            let long = UserSingleton.shared.long
            var rad = (user.radius?.toInt ?? 100)
            if rad == -1{rad = 100}
            
            if /str != ""{
                rad = -1
            }
            
            APIManager.shared.request(with: HomeEndpoint.promoteEventList(lat: lat, long: long, id: user._id, search: str, skip: "0", limit: "100", radius: "\(rad)"), completion: { [weak self] (res) in
                switch res{
                case .success(let res):
                    guard let resp = res as? MapModel else {return}
                    
                    
                    
                    var tempArr : [MapListData] = []
                    for item in resp.data?.promoteData ?? []{
                        
                        tempArr.append(item)
                    }
                    
                    self?.promoteArrData = tempArr
                    
                    print(self?.promoteArrData?.count)
                    
                    self?.tableViewDataSource?.items = self?.promoteArrData ?? []
                    if self?.isFirstEvent ?? false {
                        AnimatableReload.reload(tableView: self?.tableView ?? UITableView(), animationDirection: .right)
                        self?.isFirstEvent = false
                    }
                    else{
                        self?.tableView?.reloadData()
                    }
                    
                    self?.lblNoData?.isHidden = self?.promoteArrData?.count ?? 0 > 0
                    
                    self?.tableView?.es.noticeNoMoreData()
                    self?.refreshControl.endRefreshing()
                case .failure(let msg):
                    Alerts.shared.show(alert: .error, message: msg, type: .info)
                    
                    
                }
            }, isLoaderNeeded: isFirstEvent)
        //}
    }
    
    func getData(str : String?){
     //   if isLocationOn() {
            guard let user = UserSingleton.shared.loggedInUser else {return}
            let lat = UserSingleton.shared.lat
            let long = UserSingleton.shared.long
            var rad = (user.radius?.toInt ?? 100)
            if rad == -1{rad = 100}
            
            if /str != ""{
                rad = -1
            }
            
            APIManager.shared.request(with: HomeEndpoint.hotspotMapView(lat: lat, long: long, radius: rad.description, desc: nil, id: user._id, search: str , skip : "\(skip)" , limit : "\(offset)"), completion: { [weak self] (res) in
                switch res{
                case .success(let res):
                    guard let resp = res as? MapModel else {return}
                    self?.length = resp.data?.count ?? 0
                    var tempArr : [MapListData] = []
                    
                    for item in resp.data?.dataMap ?? []{
                        self?.arrData?.append(item)
                        tempArr.append(item)
                    }
                    if (self?.isPullDown ?? false){
                        self?.arrData = tempArr
                    }
                    self?.skip = (self?.skip ?? 0) + (self?.offset ?? 10)
                    self?.isLoadMore = (self?.arrData?.count ?? 0) > 0
                    self?.collectionViewTrend?.es.stopLoadingMore()
                    ( self?.isLoadMore ?? true ) ? self?.collectionViewTrend?.es.resetNoMoreData() : self?.collectionViewTrend?.es.noticeNoMoreData()
                    
                    // self?.tableViewDataSource?.items = self?.arrData ?? []
                    if self?.isFirst ?? false {
                        AnimatableReload.reload(collectionView: self?.collectionViewTrend ?? UICollectionView(), animationDirection: .right)
                        self?.isFirst = false
                    }
                    else{
                        //self?.tableView?.reloadData()
                        self?.collectionViewTrend?.reloadData()
                    }
                    self?.lblNoData?.isHidden = self?.arrData?.count ?? 0 > 0
                    self?.refreshControl1.endRefreshing()
                    
                case .failure(let msg):
                    Alerts.shared.show(alert: .error, message: msg, type: .info)
                    self?.lblNoData?.isHidden = self?.arrData?.count ?? 0 > 0
                    self?.refreshControl1.endRefreshing()
                    
                }
            }, isLoaderNeeded: isFirst)
        //}
    }
}


//MARK::- PAGING AND PULL TO REFRESH
extension ExploreListVC {
    
    func handlePagination(){
       // if isLocationOn() {
            let _ = tableView?.es.addInfiniteScrolling { [weak self] in
                if (self?.arrData?.count ?? 0) < (self?.length ?? 0) {
                    self?.isPullDown = false
                    let text = self!.hotspotStr.trimmed()
                    let url =  ((text.isBlank ) || (text.isEmpty ?? false)) ? nil : text
                    self?.getData(str: url)
                }else{
                    self?.foundNoMoreData()
                    //self.refreshControl.endRefreshing()
                }
           // }
        }
    }
    
    func handleCollectionPagination(){
        let _ = collectionViewTrend?.es.addInfiniteScrolling { [weak self] in
            if (self?.arrData?.count ?? 0) < (self?.length ?? 0) {
                self?.isPullDown = false
                let text = self!.hotspotStr.trimmed()
                let url =  ((text.isBlank ) || (text.isEmpty ?? false)) ? nil : text
                self?.getData(str: url)
            }else{
                self?.foundNoMoreData()
                //self.refreshControl.endRefreshing()
            }
        }
    }
    
    func foundNoMoreData(){
        self.tableView?.es.stopLoadingMore()
        self.tableView?.es.noticeNoMoreData()
    }
    
    func refreshData(){
        
            isPullDown = true
            skip = 0
            let text = hotspotStr.trimmed()
            let url =  ((text.isBlank) || (text.isEmpty)) ? nil : text
            self.getData(str: url)
        
    }
    
    @objc func pullToRefresh() {
        // tableView?.tableFooterView = UIView()
            resetNoMoreData()
            length = 0
            if tab == 0 {
                self.getPromoteData(str: "")
            }else{
                refreshData()
            }
        
    }
    
    func resetNoMoreData(){
        self.tableView?.es.resetNoMoreData()
        self.collectionViewTrend?.es.resetNoMoreData()
    }
    
}



