//
//  BaseViewController.swift
//  WhrzAtApp
//
//  Created by Sierra 4 on 31/08/17.
//  Copyright © 2017 com.example. All rights reserved.


import UIKit
import Photos
import ADCountryPicker
import FirebaseAnalytics

class BaseViewController: UIViewController , UITextFieldDelegate {
    
    //MARK::- PROPERTIES
    var textArr =  [UITextField?]()
    var selectedImage : UIImage?
    let picker = UIImagePickerController()
    var image : UIImage? = nil
    
    
    //MARK::- OVERRIDE FUNCTION / DELEGATES
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        ISMessages.hideAlert(animated: true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        for index in 0 ..< textArr.count{
            if textArr[index] == textField{
                textArr[index]?.resignFirstResponder()
                if index == textArr.count - 1{
                    return true
                }else{
                    textArr[(index + 1)]?.becomeFirstResponder()}
            }
        }
        return true
    }
    
    func firebaseAnalyticsLogEvent(eventName:String){
        Analytics.logEvent(eventName, parameters: nil)
    }
    
    
    //MARK::- FUNCTION OPEN SETTING
    func openSetting(){
        UIApplication.shared.openURL(NSURL(string:UIApplication.openSettingsURLString)! as URL)
    }
    
    //MARK::- FUNCTION SHOW ALERT
    func showAlertConfirmation(title : String , desc : String, success : @escaping () -> ()){
        let refreshAlert = UIAlertController(title: title, message: desc, preferredStyle: UIAlertController.Style.alert)
        
        refreshAlert.addAction(UIAlertAction(title: TitleType.ok.rawValue, style: .default, handler: { (action: UIAlertAction!) in
            refreshAlert .dismiss(animated: true, completion: nil)
            success()
        }))
        refreshAlert.addAction(UIAlertAction(title: TitleType.Cancel.rawValue, style: .default, handler: { (action: UIAlertAction!) in
            refreshAlert .dismiss(animated: true, completion: nil)
        }))
        self.present(refreshAlert, animated: true, completion: nil)
    }
    
    func showCustomAlert(title : String? , desc : String? , buttons : [String]? , success : @escaping () -> ()){
        AlertsClass.shared.showAlertController(withTitle: /title, message: /desc, buttonTitles: buttons ?? []) { (value) in
            let type = value as AlertTag
            switch type {
            case .yes:
                success()
            default:
                break
            }
        }
    }
}

extension BaseViewController : UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    @objc func selectImage() {
        self.view.endEditing(true)
        let alert:UIAlertController=UIAlertController(title: TitleType.addPhoto.rawValue, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        alert.view.tintColor = UIColor.red()
        let cameraAction = UIAlertAction(title: TitleType.camera.rawValue, style: UIAlertAction.Style.default) {
            UIAlertAction in
            self.checkCameraStatus()
        }
        let galleryAction = UIAlertAction(title: TitleType.gallery.rawValue, style: UIAlertAction.Style.default) {
            UIAlertAction in
            self.openGallery()
        }
        let cancelAction = UIAlertAction(title: TitleType.Cancel.rawValue, style: UIAlertAction.Style.cancel) {
            UIAlertAction in
        }
        self.picker.delegate = self
        alert.addAction(cameraAction)
        alert.addAction(galleryAction)
        alert.addAction(cancelAction)
        
        if UIDevice.current.userInterfaceIdiom == .phone {
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]){
        guard let chosenImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage else { return }
        selectedImage = chosenImage
        self.donePicking(selectImage : chosenImage)
        dismiss(animated:true, completion: nil) //5
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    func openCamera() {
        // grantAccessForCamera()
        let cameraMediaType = AVMediaType.video
        let cameraAuthorizationStatus = AVCaptureDevice.authorizationStatus(for: cameraMediaType)
        if cameraAuthorizationStatus == .authorized || cameraAuthorizationStatus == .notDetermined{
            if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)) {
                picker.sourceType = UIImagePickerController.SourceType.camera
                picker.delegate = self
                picker.allowsEditing = true
                self .present(picker, animated: true, completion: nil)
            }
        }
        else if cameraAuthorizationStatus == .denied {
            showCustomAlert(title: TitleType.permissionRequired.rawValue, desc: TitleType.allowPermission.rawValue, buttons: [TitleType.setting.rawValue , TitleType.Cancel.rawValue], success: {
                self.openSetting()
            })
            
        }
    }
    
    func grantAccessForCamera() {
        AVCaptureDevice.requestAccess(for: AVMediaType.video) { granted in
            DispatchQueue.main.async() { self.checkCameraStatus() } }
    }
    
    func openGallery() {
        let status = PHPhotoLibrary.authorizationStatus()
        if status == .authorized || status == .notDetermined {
            picker.sourceType = UIImagePickerController.SourceType.photoLibrary
            picker.allowsEditing = true
            picker.delegate = self
            if UIDevice.current.userInterfaceIdiom == .phone {
                self.present(picker, animated: true, completion: nil)
            }
        }       else if status == .denied {
            showCustomAlert(title: TitleType.permissionRequired.rawValue, desc: TitleType.allowPermission.rawValue, buttons: [TitleType.setting.rawValue , TitleType.Cancel.rawValue], success: {
                self.openSetting()
            })
            
        }
    }
    
    func checkCameraStatus() {
        
        let authStatus = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
        switch authStatus {
        case .authorized: openCamera()
        case .denied: alertToEnableCameraViaSetting()
        case .notDetermined: grantAccessForCamera()
        default: alertToEnableCameraViaSetting()
            
        }
    }
    
   @objc func donePicking(selectImage : UIImage){
    }
    
    func alertToEnableCameraViaSetting() {
        showCustomAlert(title: TitleType.permissionRequired.rawValue, desc: TitleType.allowPermission.rawValue, buttons: [TitleType.setting.rawValue , TitleType.Cancel.rawValue], success: {
            self.openSetting()
        })
    }
}

extension BaseViewController : ADCountryPickerDelegate{
    
    func showCountryPicker(){
        let picker = ADCountryPicker()
        let pickerNavigationController = UINavigationController(rootViewController: picker)
        self.present(pickerNavigationController, animated: true, completion: nil)
        picker.delegate = self
        picker.showCallingCodes = true
        picker.showFlags = true
        picker.pickerTitle = TitleType.chooseCountry.rawValue
        //picker.defaultCountryCode = "+91"
        picker.alphabetScrollBarTintColor = UIColor.black
        picker.alphabetScrollBarBackgroundColor = UIColor.clear
        picker.closeButtonTintColor = UIColor.black
        picker.font = UIFont(name: "Helvetica Neue", size: 14)
        picker.flagHeight = 40
        picker.hidesNavigationBarWhenPresentingSearch = true
        picker.searchBarBackgroundColor = UIColor.lightGray
    }
    
    @objc func pickedCountry(name : String , code : String , dialCode : String){
        
    }
    func countryPicker(_ picker: ADCountryPicker, didSelectCountryWithName name: String, code: String, dialCode: String) {
        self.pickedCountry(name : name , code : code , dialCode : dialCode)
        self.dismiss(animated: true)
    }
    
    func logOut(){
        guard let user = UserSingleton.shared.loggedInUser else {return}

        APIManager.shared.request(with: LoginEndpoint.logOut, completion: { [weak self] (res) in
            Utility.functions.removeLoader()

            switch res{

            case .success(_):
                UserSingleton.shared.loggedInUser = nil

                let initialNavVC = StoryboardScene.Login.initialViewController()
                 let VC = StoryboardScene.Login.instantiateLoginSignUpVC()
                 initialNavVC.viewControllers = [VC]

                    UIView.transition(with: UIApplication.shared.keyWindow!, duration: 0.5, options: .transitionFlipFromLeft , animations: { () -> Void in

                        UIApplication.shared.keyWindow?.rootViewController = initialNavVC
                    }) { (completed) -> Void in
                        Utility.functions.removeLoader()

                    }
                
                
            case .failure(let msg):

                Alerts.shared.show(alert: .error, message: msg, type: .info)
            }
            }, isLoaderNeeded: true)

    }
    
}
