//
//  TutorialVCViewController.swift
//  WhrzAtApp
//
//  Created by Sierra 4 on 18/09/17.
//  Copyright © 2017 com.example. All rights reserved.
//

import UIKit

class TutorialVCViewController: UIViewController {
    
    //MARK::- OUTLETS
    @IBOutlet weak var btnSkip: UIButton?
    @IBOutlet weak var collectionView: UICollectionView?
    @IBOutlet weak var pageControl: UIPageControl?
    
    //MARK::- PROPERTIES
    var arrData = ["","",""]
    
    var collectionViewdataSource : CollectionViewDataSource?{
        didSet{
            collectionView?.dataSource = collectionViewdataSource
            collectionView?.delegate = collectionViewdataSource
        }
    }
    
    //MARK::- LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        configureCollectionView()
    }
    
    //MARK::- BUTTON ACTION
    @IBAction func btnActionSkip(_ sender: UIButton) {
        let vc = StoryboardScene.Login.instantiateLoginSignUpVC()
        pushVC(vc)
    }
}

//MARK::- EXTENSION
extension TutorialVCViewController{
    //MARK::- CONFIGURE COLLECTIONVIEW
    func configureCollectionView(){
        self.pageControl?.currentPage = 0
        let width = UIScreen.main.bounds.width
        let height = UIScreen.main.bounds.height - 64
        collectionViewdataSource = CollectionViewDataSource(items: arrData as Array<AnyObject> , collectionView: collectionView, cellIdentifier: CellIdentifiers.TutorialCollectionViewCell.rawValue, headerIdentifier: "", cellHeight: height , cellWidth: width , cellSpacing: 0, configureCellBlock: {[unowned self] (cell, item, indexpath) in
            let cell = cell as? TutorialCollectionViewCell
            cell?.configureCell(index : indexpath.item)
            }, aRowSelectedListener: { (indexPath) in
        }, willDisplayCell: { (indexPath) in
            
        }, scrollViewListener: { [unowned self] (UIScrollView) in
            self.managePageControl()
        })
        collectionView?.reloadData()
    }
    
    //MAR::- FUNCTION
    func managePageControl(){
        guard let origin = self.collectionView?.contentOffset else{return}
        guard let size = self.collectionView?.bounds.size else{return}
        let visibleRect = CGRect(origin: origin, size: size )
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        let indexPath = self.collectionView?.indexPathForItem(at: visiblePoint)
        self.pageControl?.currentPage = indexPath?.item ?? 0
        btnSkip?.setTitle(indexPath?.item == 2 ? TitleType.done.rawValue : TitleType.skip.rawValue, for: .normal)
    }
}
