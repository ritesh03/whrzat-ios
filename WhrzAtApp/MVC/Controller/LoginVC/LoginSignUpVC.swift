//
//  LoginSignUpVC.swift
//  WhrzAtApp
//
//  Created by Sierra 4 on 01/09/17.
//  Copyright © 2017 com.example. All rights reserved.

import UIKit
import CoreLocation
import EZSwiftExtensions
import IBAnimatable
import Photos

typealias socialLoginDict = (String?,String?,String?,String?,String?)?

class LoginSignUpVC: BaseViewController {
    
    //MARK::- OUTLETS
    @IBOutlet weak var btn: AnimatableButton?
    
    @IBOutlet weak var logoBottomConstraints: NSLayoutConstraint!
    @IBOutlet weak var fbLoginBtn: UIButton!
    //MARK::- PROPERTIES
    var socialDict : socialLoginDict?
    
    //MARK::- OVERRIDE FUNCTION
    override func viewDidLoad() {
        super.viewDidLoad()
        fbLoginBtn.layer.borderWidth = 1
        fbLoginBtn.layer.borderColor = UIColor(red: 59.0/255, green: 89.0/255, blue: 152.0/255, alpha: 1.0).cgColor
        if UIScreen.main.bounds.size.height == 568 {
            logoBottomConstraints.constant = -120
        }
       
    }
    
    //MARK::- BUTTON ACTIONS
    @IBAction func btnActionFacebook(_ sender: UIButton) {
       // if isLocationOn(){
            Utility.functions.loader()
            SocialNetworkClass.shared.facebookLogin {[weak self](fbId, name, email, img) in
                Utility.functions.removeLoader()
                self?.socialDict = (fbId, nil, email, img, name)
                self?.socialLogin()
            }
       // }
    }
    
    @IBAction func btnActionFire(_ sender: AnimatableButton) {
        btn?.animate(.pop(repeatCount: 1))
    }
    @IBAction func btnActionLogin(_ sender: UIButton) {
        //if isLocationOn(){
            let vc = StoryboardScene.Login.instantiateLoginVC()
            self.navigationController?.pushViewController(vc, animated: true)
        //}
    }
    
    @IBAction func btnActionSignUp(_ sender: Any) {
        //if isLocationOn(){
            let vc = StoryboardScene.Login.instantiateSignUpSecondVC()
            self.navigationController?.pushViewController(vc, animated: true)
        //}
    }
    
    @IBAction func btnActionTerms(_ sender: Any) {
        let vc = StoryboardScene.Home.instantiateWebPageViewController()
        vc.url = APIConstants.terms.rawValue
        vc.titleHead = TitleType.termsCondition.rawValue
        ez.topMostVC?.pushVC(vc)
    }
}

//MARK::- FUNCTIONS

extension LoginSignUpVC{
    func facebookLogin(){
        Utility.functions.loader()
        SocialNetworkClass.shared.facebookLogin {[weak self](fbId, name, email, img) in
            Utility.functions.removeLoader()
            self?.socialDict = (fbId, nil, email, img, name)
            self?.socialLogin()
        }
    }
    
    func socialLogin(){
        checkFb()
    }
    
    func checkFb(){
        APIManager.shared.request(with: LoginEndpoint.facebookIdCheck(id: self.socialDict??.0), completion: { [ weak self ] (res) in
            switch res{
            case .failure(let msg):
                if msg == TitleType.FB_ID_NOT_FOUND.rawValue{
                    let vc = StoryboardScene.Login.instantiateSignUpSecondVC()
                    vc.name = self?.socialDict??.4
                    vc.fbId = self?.socialDict??.0
                    vc.email = self?.socialDict??.2
                    vc.imgStr = self?.socialDict??.3
                    vc.fromFb = "true"
                    vc.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
                    
                    self?.pushVC(vc)}
                else{
                    Alerts.shared.show(alert: .error, message: msg, type: .info)
                }
            case.success(let res):
                guard let res = res as? LoginModel else {return}
                UserSingleton.shared.loggedInUser = res.data
                let vc = StoryboardScene.Home.initialViewController()
                vc.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
                
                self?.presentVC(vc)
            }
        }, isLoaderNeeded: true)
    }
}
