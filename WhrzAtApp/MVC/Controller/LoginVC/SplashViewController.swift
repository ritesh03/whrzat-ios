//
//  SplashViewController.swift
//  WhrzAtApp
//
//  Created by Sierra 4 on 31/08/17.
//  Copyright © 2017 com.example. All rights reserved.
//

import UIKit
import EZSwiftExtensions
//import BranchSDK

class SplashViewController: BaseViewController {
    
    //MARK::- PROPERTIES
    var btnClicked : Bool = true  // Switch it to "false" to enable versioning
    var urlAppStore : String = APIConstants.version.rawValue // Update link to appstore
    
    //MARK::- LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if btnClicked == false{
            checkVersioning()
        }else{
            normalFlow()
        }
    }
    
    //MARK::- FUNCTIONS
    func normalFlow(){
        if UserSingleton.shared.loggedInUser?.accessToken != nil{
//            let hotspotId = UserDefaults.standard.string(forKey: "hotspotId")
//            if (hotspotId != "" && hotspotId != nil) {
//                let vc = StoryboardScene.Home.instantiateHotspotDetailVC()
//
//                vc.modalPresentationSptyle = .fullScreen
//                presentVC(vc)
//            }else{
                let vc = StoryboardScene.Home.initialViewController()
                
                vc.modalPresentationStyle = .fullScreen
                presentVC(vc)
            //}
        }
        else{
            let vc = StoryboardScene.Login.instantiateTutorialVCViewController()
            pushVC(vc)
        }
    }
}

//MARK::- EXTENSION VERSIONING FUNCTIONS
extension SplashViewController {
    func checkVersioning() {
        APIManager.shared.request(with: LoginEndpoint.versioning, completion: { [weak self] (response) in
            switch response{
            case .success(let responseValue):
                guard let temp = responseValue as? VersioningModel else{return}
                guard let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String else{return}
                guard let ver = Double(version) else{return}
                guard let criticalVersion = temp.data?.criticalIOSVersion?.toDouble() else {return}
                guard let latestVersion = temp.data?.latestIOSVersion?.toDouble() else {return}
                print(ver)
                if ver < (criticalVersion){
                    self?.forceUpdate(title: TitleType.update.rawValue, description: TitleType.forceUpdate.rawValue)
                }
                else if  ver < (latestVersion){
                    self?.normalUpdate(title: TitleType.update.rawValue, description: TitleType.normalUpdate.rawValue)
                }
                else{
                    self?.normalFlow()
                }
            case .failure(_):
                self?.normalFlow()
            }
            }, isLoaderNeeded: true)
    }
    
    func forceUpdate(title : String? ,description : String?){
        showAlert(title : title ,description : description ,buttonTitles : [TitleType.update.rawValue])
    }
    
    func normalUpdate(title : String? ,description : String?){
        showAlert(title : title ,description : description , buttonTitles : [TitleType.update.rawValue , TitleType.skip.rawValue] )
    }
    
    func showAlert(title : String? , description : String? , buttonTitles :[String]){
        AlertsClass.shared.showAlertController(withTitle: /title, message: /description, buttonTitles: buttonTitles) {[weak self] (value) in
            let type = value as AlertTag
            switch type {
            case .yes:
                if self?.btnClicked == false {
                    self?.checkVersioning()
                }
                guard let urlStr = URL(string: /self?.urlAppStore) else {return}
                UIApplication.shared.openURL(urlStr)
            default:
                self?.normalFlow()
                return
            }
        }
    }
}


/*
 
 do {
 try self.store.enumerateContacts(with: request) {
 (contact, stop) in
 self.contacts.append(contact)
 
 for specificPhoneNum in contact.phoneNumbers{
 
 if ((!contact.givenName.trimmingCharacters(in: .whitespaces).isEmpty) && (specificPhoneNum.value.stringValue.digits != /UserData.shared.registeredUser?.countryCode?.digits + /UserData.shared.registeredUser?.phoneNo?.digits)){
 
 
 self.allContacts.append(["name" : (contact.givenName.trimmingCharacters(in: .whitespaces) + " " + contact.middleName.trimmingCharacters(in: .whitespaces) + contact.familyName.trimmingCharacters(in: .whitespaces))  ,
 "phoneNo": /(specificPhoneNum.value.stringValue.digits) , "idSameContact" : idSameCon.toString])
 }
 }
 idSameCon += 1
 }
 
 UserData.shared.contactsData = SavedContacts(attributes : allContacts)
 sendContacts(fetchedContacts: self.allContacts)
 }
 catch {
 self.uiContacts()
 
 }
 */
