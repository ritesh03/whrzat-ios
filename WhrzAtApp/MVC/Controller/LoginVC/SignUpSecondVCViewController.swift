//
//  SignUpSecondVCViewController.swift
//  WhrzAtApp
//
//  Created by Sierra 4 on 15/09/17.
//  Copyright © 2017 com.example. All rights reserved.

import UIKit
import ACFloatingTextfield_Swift
import SZTextView
import FirebaseAuth

class SignUpSecondVC: BaseViewController {
    
    //MARK::- OUTLETS
    @IBOutlet weak var txtFirst: UITextField?
    @IBOutlet weak var txtPhone: ACFloatingTextfield?
    @IBOutlet weak var txtBio: SZTextView?
    @IBOutlet weak var btnCode: UIButton?
    @IBOutlet weak var img: UIImageView?
    @IBOutlet weak var btnShowPassword: UIButton?
    
    //MARK::- PROPERTIES
    var email : String?
    var password : String?
    var name : String?
    var imgStr : String?
    var fbId : String?
    var fromFb = "false"
    
    //MARK::- LIFECYCLE / OVERRIDE FUNCTIONS
    override func viewDidLoad() {
        super.viewDidLoad()
        txtBio?.contentInset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 0)
        onViewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
      
    }
//    
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
//        if textField == txtFirst{
//            if string == " " {
//                return false
//            }
//        }
//     
//        return true
//    }
//    
    override func pickedCountry(name : String , code : String , dialCode : String){
        btnCode?.setTitle(dialCode, for: .normal)
    }
    
    override func donePicking(selectImage: UIImage) {
        img?.contentMode = .scaleAspectFill
        img?.image = selectedImage
    }
    
    //MARK::- BUTTON ACTION
    @IBAction func btnActionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnActionFinish(_ sender: UIButton) {
        self.view.endEditing(true)
        hitAPI()
    }
    @IBAction func btnActionPickCode(_ sender: UIButton) {
        self.view.endEditing(true)
        self.showCountryPicker()
    }
    
    @IBAction func btnActionPickImage(_ sender: UIButton) {
        self.view.endEditing(true)
        selectImage()
    }
    @IBAction func btnActionForLogin(_ sender: Any) {
        if let viewControllers = self.navigationController?.viewControllers
        {
            if viewControllers.contains(where: {
                return $0 is LoginVC
            }){
                for vc in viewControllers {
                    if vc is LoginVC {
                        _ = self.navigationController?.popToViewController(vc as! LoginVC, animated: true)
                    }
                }
            }else{
                let vc = StoryboardScene.Login.instantiateLoginVC()
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    @IBAction func btnActionShowPassword(_ sender: UIButton) {
      //  sender.isSelected.toggle() vivek
    }
}

//MARK::- API AND FUNCTION
extension SignUpSecondVC{
    
    //MARK::- API
    func hitAPI(){
        if "".signup(name: /txtFirst?.text, mobile: /txtPhone?.text){
                  if selectedImage == nil {
                      Alerts.shared.show(alert: .error, message: AlertMessage.imgHotspot.rawValue, type: .info)
                      return}
            let name = /txtFirst?.text
            APIManager.shared.request(withImage: LoginEndpoint.signUp(name: name, email: "", password: "", phoneNo: txtPhone?.text, profilePic: nil, fromFacebook: fromFb , fbId : fbId , bio : txtBio?.text , code : /btnCode?.title(for: .normal)), image: selectedImage, completion: { [weak self] (response) in
                switch response{
                case .failure(let msg):
                    Alerts.shared.show(alert: .error, message: msg, type: .info)
                case.success(let res):
                    self?.firebaseAnalyticsLogEvent(eventName: "signupscr_signup_click")
                    guard let resp = res as? LoginModel else {return}
                    if !(resp.data?.isContactVerified ?? false) {
                    Utility.functions.loader()
                    PhoneAuthProvider.provider().verifyPhoneNumber("\(self?.btnCode?.titleLabel?.text ?? "")\(self?.txtPhone?.text ?? "")", uiDelegate: nil) { (verificationID, error) in
                        Utility.functions.removeLoader()
                        if let error = error {
                            // self.showMessagePrompt(error.localizedDescription)
                            Alerts.shared.show(alert: .error, message: ErrorConstant.networkError.rawValue , type : .error)
                            return
                        }
                        // Sign in using the verificationID and the code sent to the user
                        // ...
                        let vc = StoryboardScene.Login.instantiateOTPVC()
                        guard let res = res as? LoginModel else {return}
                        guard let user = res.data else{return}
                        //UserSingleton.shared.loggedInUser = user
                        UserSingleton.shared.loggedInUser = user
                        vc.comeFrom = APIConstants.signUp
                        vc.user = user
                        vc.verificationID = verificationID
                        self?.pushVC(vc)
                        
                    }
                }
                  
//                    guard let res = res as? LoginModel else {return}
//                    guard let user = res.data else{return}
//                   // UserSingleton.shared.loggedInUser = user
//                    let vc = StoryboardScene.Login.instantiateOTPVC()
//                    vc.user = user
//                    self?.pushVC(vc)
                }
            }, isLoaderNeeded: true)
        }
        
    }
    
    //MARK::- FUNCTION
    func onViewDidLoad(){
        let str = "+1"
            //Utility.functions.getCC(/((Locale.current as? NSLocale)?.object(forKey: .countryCode) as? String))
        btnCode?.setTitle(str, for: .normal)
        if fromFb == "true"{
            splitName(name: /name)
           // btnShowPassword?.isHidden = true
            //txtEmail?.isUserInteractionEnabled = txtEmail?.text == ""
            if imgStr != "" && imgStr != nil{
                img?.contentMode = .scaleAspectFill
                img?.kf.setImage(with: URL(string : /imgStr), placeholder: #imageLiteral(resourceName: "ic_profile"), options: nil, progressBlock: nil, completionHandler: { (img, _, _, _) in
                    self.selectedImage = img
                })
            }
        }
    }
    
    func splitName(name : String?){
        let fullName = /name
        var components = fullName.components(separatedBy: " ")
        if(components.count > 0){
            let firstName = components.removeFirst()
          //  let lastName = components.joined(separator: " ")
            txtFirst?.text = firstName
        }
    }
}
