//
//  ForgetPasswordVC.swift
//  WhrzAtApp
//
//  Created by Sierra 4 on 01/09/17.
//  Copyright © 2017 com.example. All rights reserved.
//

import UIKit
import ACFloatingTextfield_Swift

class ForgetPasswordVC: BaseViewController {
    
    //MARK::- OUTLETS
    @IBOutlet weak var txtEmail: ACFloatingTextfield?
    
    //MARK::- LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        textArr = [txtEmail ]
    }
    
    //MARK::- BUTTON ACTIONS
    @IBAction func btnActionBack(_ sender: Any) {
        dismissVC(completion: nil)
    }
    
    @IBAction func btnActionSubmit(_ sender: UIButton) {        
        self.view.endEditing(true)
        if "".login(email: txtEmail?.text, password: nil){
            APIManager.shared.request(with: LoginEndpoint.forgetPassword(email: /txtEmail?.text), completion: { [weak self] (response) in
                switch response{
                case .success(_):
                    Alerts.shared.show(alert: .success, message: AlertMessage.linkSend.rawValue, type: .success)
                    self?.dismiss(animated: false, completion: nil)
                    
                case .failure(let msg):
                    Alerts.shared.show(alert: .error, message: msg, type: .info)
                }
                }, isLoaderNeeded: true)
        }
    }
}



extension ForgetPasswordVC{
    
}
