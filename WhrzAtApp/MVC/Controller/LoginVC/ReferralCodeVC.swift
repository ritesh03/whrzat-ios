//
//  ReferralCodeVC.swift
//  WhrzAtApp
//
//  Created by Ritesh on 09/03/21.
//  Copyright © 2021 com.example. All rights reserved.
//

import UIKit
import ACFloatingTextfield_Swift

class ReferralCodeVC: BaseViewController {

    @IBOutlet weak var referralCodeTF: ACFloatingTextfield!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func backButtonTap(_ sender: Any) {
           popVC()
       }
    
        @IBAction func submitBtnAction(_ sender: Any) {
            
            if referralCodeTF!.text == "" {
                Alerts.shared.show(alert: .error, message: ErrorConstant.referralcode.rawValue , type : .info)
                return
            }
            self.view.endEditing(true)
            
            APIManager.shared.request(with: LoginEndpoint.verifyReferralCode(code: referralCodeTF.text), completion: { [weak self] (response) in
                switch response{
                case .failure(_):
                    Alerts.shared.show(alert: .error, message: ErrorConstant.invalidReferral.rawValue, type: .info)
                case.success(_):
                   let vc = StoryboardScene.Home.initialViewController()
                   vc.modalPresentationStyle = .fullScreen
                   self?.presentVC(vc)
                  }
                }, isLoaderNeeded: true)
            
        }

    @IBAction func skipBtnAction(_ sender: Any) {
   
    let vc = StoryboardScene.Home.initialViewController()
        vc.modalPresentationStyle = .fullScreen
        self.presentVC(vc)
    
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
