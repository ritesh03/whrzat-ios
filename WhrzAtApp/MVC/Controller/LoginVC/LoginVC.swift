
//  LoginVC.swift
//  WhrzAtApp
//
//  Created by Sierra 4 on 01/09/17.
//  Copyright © 2017 com.example. All rights reserved.

import UIKit
import ACFloatingTextfield_Swift
import EZSwiftExtensions
import IBAnimatable
import ADCountryPicker
import FirebaseAuth

class LoginVC: BaseViewController {
    
    //MARK::- OUTLETS
    @IBOutlet weak var txtEmail: ACFloatingTextfield?
    @IBOutlet weak var btnCode: UIButton!
    @IBOutlet weak var txtPassword: ACFloatingTextfield?
    @IBOutlet weak var lblLogin: UILabel?
    
    //MARK::- LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        let str = "+1"
            //Utility.functions.getCC(/((Locale.current as? NSLocale)?.object(forKey: .countryCode) as? String))
               btnCode?.setTitle(str, for: .normal)
    }
    

    
    @IBAction func btnCodePicker(_ sender: Any) {
        
        self.view.endEditing(true)
             self.showCountryPicker()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        textArr = [txtEmail , txtPassword]
    }
    
    override func pickedCountry(name : String , code : String , dialCode : String){
           btnCode?.setTitle(dialCode, for: .normal)
       }
    
    @IBAction func btnActionSignUp(_ sender: Any) {
        
        if let viewControllers = self.navigationController?.viewControllers
        {
            if viewControllers.contains(where: {
                return $0 is SignUpSecondVC
            }){
                for vc in viewControllers {
                    if vc is SignUpSecondVC {
                        _ = self.navigationController?.popToViewController(vc as! SignUpSecondVC, animated: true)
                    }
                }
            }else{
                let vc = StoryboardScene.Login.instantiateSignUpSecondVC()
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        
        
    }
    //MARK::- BUTTON ACTIONS
    @IBAction func btnActionLogin(_ sender: UIButton) {
        
        if txtEmail!.text == "" {
            Alerts.shared.show(alert: .error, message: ErrorConstant.enterPhoneNumber.rawValue , type : .info)
            return
        }
        self.view.endEditing(true)
        Utility.functions.loader()

        APIManager.shared.request(with: LoginEndpoint.login(email: /txtEmail?.text, password: txtPassword?.text, code: btnCode.titleLabel?.text), completion: { [weak self] (response) in
            Utility.functions.removeLoader()
                switch response{
                
                case .failure(let msg):
                    Alerts.shared.show(alert: .error, message: msg, type: .info)
                    
                case.success(let res):
                    self?.firebaseAnalyticsLogEvent(eventName: "loginscr_login_click")
                    Utility.functions.loader()
                    PhoneAuthProvider.provider().verifyPhoneNumber("\(self?.btnCode.titleLabel?.text ?? "")\(self?.txtEmail?.text ?? "")", uiDelegate: nil) { (verificationID, error) in
                        Utility.functions.removeLoader()
                      if let error = error {
                        print(error.localizedDescription)
                       // self.showMessagePrompt(error.localizedDescription)
                        Alerts.shared.show(alert: .error, message: error.localizedDescription , type : .warning)
                        return
                      }
                      // Sign in using the verificationID and the code sent to the user
                      // ...
                        let vc = StoryboardScene.Login.instantiateOTPVC()
                        guard let res = res as? LoginModel else {return}
                        guard let user = res.data else{return}
                        //UserSingleton.shared.loggedInUser = user
                        vc.comeFrom = APIConstants.login
                        vc.user = user
                        vc.verificationID = verificationID
                        self?.pushVC(vc)
                        
                    }
                    

//                    let vc = StoryboardScene.Login.instantiateOTPVC()
//                    guard let res = res as? LoginModel else {return}
//                    guard let user = res.data else{return}
//                    UserSingleton.shared.loggedInUser = user
//                    self?.pushVC(vc)
                }
                }, isLoaderNeeded: true)
        
    }
    
    @IBAction func btnActionForgetPassword(_ sender: Any) {
        let vc = StoryboardScene.Login.instantiateForgetPasswordVC()
        presentVC(vc)
    }
    
    @IBAction func btnActionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnActionShowPassword(_ sender: UIButton) {
       // sender.isSelected.toggle() vivek
        txtPassword?.isSecureTextEntry = !sender.isSelected
    }
}


//MARK::- EXTENSION TEXTFIELD DELEGATE
extension LoginVC{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        if textField == txtPassword{
            let maxLength = 15
            let currentString : NSString = textField.text as NSString? ?? ""
            let newString = currentString.replacingCharacters(in: range, with: string)
            return newString.length <= maxLength
        }
        return true
    }
}
