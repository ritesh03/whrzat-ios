//
//  OtpVC.swift
//  WhrzAtApp
//
//  Created by Vivek Dogra on 20/03/20.
//  Copyright © 2020 com.example. All rights reserved.
//

import UIKit
import ACFloatingTextfield_Swift
import FirebaseAuth
class OtpVC: BaseViewController {
    
    @IBOutlet weak var otpTF: ACFloatingTextfield!
    var user : RegisterUser?
    var comeFrom = APIConstants.login
    var verificationID:String?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func backButtonTap(_ sender: Any) {
        popVC()
    }
    @IBAction func submitBtnAction(_ sender: Any) {
        
        if otpTF!.text == "" {
            Alerts.shared.show(alert: .error, message: ErrorConstant.verificationCode.rawValue , type : .info)
            return
        }
        self.view.endEditing(true)
        
        let credential = PhoneAuthProvider.provider().credential(
            withVerificationID: verificationID ?? "",
            verificationCode: otpTF.text ?? "")
        Utility.functions.loader()
        Auth.auth().signIn(with: credential) { (authResult, error) in
            Utility.functions.removeLoader()
            if let error = error {
                
                Alerts.shared.show(alert: .error, message: error.localizedDescription , type : .info)
                return
            }
            if self.comeFrom == APIConstants.login {
                UserSingleton.shared.loggedInUser = self.user
                let vc = StoryboardScene.Home.initialViewController()
                vc.modalPresentationStyle = .fullScreen
                self.presentVC(vc)
            } else{
                self.verifyOTP()
            }
        }
        
    }
    
    func verifyOTP(){
  
        APIManager.shared.request(with: LoginEndpoint.verifyOtp, completion: { [weak self] (response) in
                switch response{
                case .failure(let msg):
                    Alerts.shared.show(alert: .error, message: msg, type: .info)
                case.success(let res):
                    UserSingleton.shared.loggedInUser = self?.user
                    guard let user = UserSingleton.shared.loggedInUser else{return}
                    if user.rewardStatus != nil && user.rewardStatus == true { //go to referral code when coming from signup
                        let vc = StoryboardScene.Login.instantiateReferralCodeVC()
                        self?.pushVC(vc)
                        
                    } else {
                        let vc = StoryboardScene.Home.initialViewController()
                        vc.modalPresentationStyle = .fullScreen
                        self?.presentVC(vc)
                    }
              
                }
            }, isLoaderNeeded: true)
        
    }
}
