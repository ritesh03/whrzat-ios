import UIKit

class AttributedString {
    
    static let shared = AttributedString()
    
    func attributeStringFont(value: String?, size: Int) -> NSMutableAttributedString {

        if let str = value {
            return NSMutableAttributedString(string: str, attributes: [NSAttributedString.Key.font: UIFont(name: "OpenSans-SemiBold",size: CGFloat(size)) as Any])
        } else {
            return NSMutableAttributedString(string: " ", attributes: [NSAttributedString.Key.font: UIFont(name: "OpenSans-SemiBold",size: CGFloat(size)) as Any])
        }
    }
    
    
    func attributeString(value: String?, size: Int, color: UIColor) -> NSMutableAttributedString {
        var string:NSMutableAttributedString? = NSMutableAttributedString(string: " ", attributes: [NSAttributedString.Key.font: UIFont(name: CustomFont.OpenSansRegular,size: CGFloat(size)) as Any])
        
        if let str = value {
            string = NSMutableAttributedString(string: str, attributes: [NSAttributedString.Key.font: UIFont(name: CustomFont.OpenSansRegular,size: CGFloat(size)) as Any])
            string?.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: NSRange(location: 0, length: (str.count)))
            return string!
        } else {
            return string!
        }
    }
}
