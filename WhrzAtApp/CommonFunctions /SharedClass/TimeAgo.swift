
import Foundation



public func calculateTimeSince(time : String , isChat : Bool) -> String {
    let dateFormatter: DateFormatter = DateFormatter()
    dateFormatter.timeZone = TimeZone(abbreviation: "GMT")
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
    guard let locationDate: NSDate =  dateFormatter.date(from: time) as NSDate? else { return "" }
   // print(locationDate)
    if isChat{
        return  timeAgoSinceDateChat(date: locationDate,numericDates: true)
    }else{
        return  timeAgoSinceDate(date: locationDate,numericDates: true)}
}

public func dateConvert(time : Date) -> String{
    let dateFormatter : DateFormatter = DateFormatter()
    dateFormatter.dateFormat = "h:mm a"
    let date = dateFormatter.string(from: time)
    return date
}
public func dateConvertFull(time : String) -> String{
    let dateFormatter : DateFormatter = DateFormatter()
    let dateFormatter1 : DateFormatter = DateFormatter()
    
    dateFormatter.dateFormat = "h:mm a"
    let date = dateFormatter.date(from: time)
    dateFormatter1.dateFormat = "HH:mm"
    let dateNew = dateFormatter1.string(from: date ?? Date())
    return dateNew
}

public func calculateDate(time : String) -> String {
    let dateFormatter: DateFormatter = DateFormatter()
    dateFormatter.timeZone = TimeZone(abbreviation: "GMT")
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
    guard let locationDate: Date =  dateFormatter.date(from: time) as Date? else { return "" }
  //  print(locationDate)
    dateFormatter.dateFormat = "EEE, MMM d, yyyy - h:mm a"
    dateFormatter.timeZone = NSTimeZone.local
    let timeStamp = dateFormatter.string(from: locationDate)
  //  print(timeStamp)
    return  timeStamp
}

public func timeAgoSinceDate(date:NSDate, numericDates:Bool) -> String {
    let calendar = NSCalendar.current
    let unitFlags: Set<Calendar.Component> = [.minute, .hour, .day, .weekOfYear, .month, .year, .second]
    let now = NSDate()
    let earliest = now.earlierDate(date as Date)
    let latest = (earliest == now as Date) ? date : now
    let components = calendar.dateComponents(unitFlags, from: earliest as Date,  to: latest as Date)
    
    if (components.year! >= 2) {
        return "\(components.year!)" + TimePassed.yearAgo.rawValue
    } else if (components.year! >= 1){
        if (numericDates){
            return TimePassed.oneYearAgo.rawValue
        } else {
            return TimePassed.lastYear.rawValue
        }
    } else if (components.month! >= 2) {
        return "\(components.month!)" + TimePassed.monthAgo.rawValue
    } else if (components.month! >= 1){
        if (numericDates){
            return TimePassed.oneMonthAgo.rawValue
        } else {
            return TimePassed.lastMonth.rawValue
        }
    } else if (components.weekOfYear! >= 2) {
        return "\(components.weekOfYear!)" + TimePassed.weekAgo.rawValue
    } else if (components.weekOfYear! >= 1){
        if (numericDates){
            return TimePassed.oneWeekAgo.rawValue
        } else {
            return TimePassed.lastWeek.rawValue
        }
    } else if (components.day! >= 2) {
        return "\(components.day!)" + TimePassed.dayAgo.rawValue
    } else if (components.day! >= 1){
        if (numericDates){
            return TimePassed.oneDayAgo.rawValue
        } else {
            return TimePassed.yesterday.rawValue
        }
    } else if (components.hour! >= 2) {
        return "\(components.hour!)" + TimePassed.hourAgo.rawValue
    } else if (components.hour! >= 1){
        if (numericDates){
            return TimePassed.oneHourAgo.rawValue
        } else {
            return TimePassed.oneHourAgo.rawValue
        }
    } else if (components.minute! >= 2) {
        return "\(components.minute!)" + TimePassed.minAgo.rawValue
    } else if (components.minute! >= 1){
        if (numericDates){
            return TimePassed.oneMinAgo.rawValue
        } else {
            return TimePassed.oneMinAgo.rawValue
        }
    } else if (components.second! >= 3) {
        return "\(components.second!)" + TimePassed.secAgo.rawValue
    } else {
        return TimePassed.justNow.rawValue
    }
}

public func timeAgoSinceDateChat(date:NSDate, numericDates:Bool) -> String {
    let calendar = NSCalendar.current
    let unitFlags: Set<Calendar.Component> = [.minute, .hour, .day, .weekOfYear, .month, .year, .second]
    let now = NSDate()
    let earliest = now.earlierDate(date as Date)
    let latest = (earliest == now as Date) ? date : now
    let components = calendar.dateComponents(unitFlags, from: earliest as Date,  to: latest as Date)
    
    if (components.year! >= 2) {
        return "\(components.year!)" + TimePassed.year.rawValue
    } else if (components.year! >= 1){
        if (numericDates){
            return TimePassed.oneYear.rawValue
        } else {
            return TimePassed.lastYear.rawValue
        }
    } else if (components.month! >= 2) {
        return "\(components.month!)" + TimePassed.month.rawValue
    } else if (components.month! >= 1){
        if (numericDates){
            return TimePassed.oneMonth.rawValue
        } else {
            return TimePassed.lastMonth.rawValue
        }
    } else if (components.weekOfYear! >= 2) {
        return "\(components.weekOfYear!)" + TimePassed.week.rawValue
    } else if (components.weekOfYear! >= 1){
        if (numericDates){
            return TimePassed.oneWeek.rawValue
        } else {
            return TimePassed.lastWeek.rawValue
        }
    } else if (components.day! >= 2) {
        return "\(components.day!)" + TimePassed.day.rawValue
    } else if (components.day! >= 1){
        if (numericDates){
            return TimePassed.oneDay.rawValue
        } else {
            return TimePassed.yesterday.rawValue
        }
    } else if (components.hour! >= 2) {
        return "\(components.hour!)" + TimePassed.hour.rawValue
    } else if (components.hour! >= 1){
        if (numericDates){
            return TimePassed.oneHour.rawValue
        } else {
            return TimePassed.oneHour.rawValue
        }
    } else if (components.minute! >= 2) {
        return "\(components.minute!)" + TimePassed.min.rawValue
    } else if (components.minute! >= 1){
        if (numericDates){
            return TimePassed.oneMin.rawValue
        } else {
            return TimePassed.oneMin.rawValue
        }
    } else if (components.second! >= 3) {
        return "\(components.second!)" + TimePassed.sec.rawValue
    } else {
        return TimePassed.justNow.rawValue
    }
    
}
