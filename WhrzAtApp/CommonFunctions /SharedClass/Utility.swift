
import UIKit
import NVActivityIndicatorView
import CoreLocation
import Contacts
import EZSwiftExtensions

typealias success = (_ coordinates: CLLocationCoordinate2D, _ fullAddress: String?, _ name : String?, _ city : String?,_ state : String?, _ subLocality: String? ) -> ()

class Utility: NSObject, NVActivityIndicatorViewable {
    static let functions = Utility()
    let geoCoder = CLGeocoder()
   // var currentLat = LocationManager.shared.latitude, currentLng =  LocationManager.shared.longitude
    
    let formatter = DateFormatter()
    var store = CNContactStore()
    var addressDict = [String : Any]()
    
    override init() {
        super.init()
    }
    
    static func shared() -> AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    func loader()  {
        Utility.shared().window?.rootViewController?.startAnimating(nil, message: nil, messageFont: nil, type: .ballClipRotate, color: UIColor.red(), padding: nil, displayTimeThreshold: nil, minimumDisplayTime: nil)
    }
    
    func removeLoader()  {
        Utility.shared().window?.rootViewController?.stopAnimating()
    }
    
    
    func calculateAddress(lat : CLLocationDegrees , long : CLLocationDegrees , responseBlock : @escaping success){
        geoCoder.reverseGeocodeLocation( CLLocation(latitude: lat, longitude: long), completionHandler: { [weak self] (placemarks, error) -> Void in
            let placeMark = placemarks?.first
            guard let address = placeMark?.addressDictionary?["FormattedAddressLines"] as? [String] else {return}
            let fullAddress = address.joined(separator: ", ")
            let name = placeMark?.addressDictionary?["Name"] as? String
            let subLocality = placeMark?.addressDictionary?["SubLocality"] as? String
            let city = placeMark?.addressDictionary?["City"] as? String
            let state = placeMark?.addressDictionary?["State"] as? String
            
//            // Location name
//                       if let name = placeMark?.addressDictionary!["Name"] as? NSString {
//                           print(name)
//                       }
//
////                       // Street address
////                       if let street = placeMark?.addressDictionary!["Thoroughfare"] as? NSString {
////                           print(street)
////                       }
//
//                       // City
//                       if let city = placeMark?.addressDictionary!["City"] as? NSString {
//                           print(city)
//                       }
//
//                       // Zip code
//                       if let zip = placeMark?.addressDictionary!["ZIP"] as? NSString {
//                           print(zip)
//                       }
//
//                       // Country
//                       if let country = placeMark?.addressDictionary!["Country"] as? NSString {
//                           print(country)
//                       }
            
            
            
            responseBlock(CLLocationCoordinate2D(latitude: lat, longitude: long), fullAddress,name,city,state,subLocality)
        })
        
    }
    
    func dateFromMilliseconds(ms: Int64) -> String {
        let temp = Date(timeIntervalSince1970:Double(ms) / 1000.0)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        //dateFormatter.timeZone = TimeZone.current
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        return  dateFormatter.string(from: temp)
    }
    
    func convertArrayIntoJson(array: [String]?) -> NSString? {
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: array ?? [], options: JSONSerialization.WritingOptions.prettyPrinted)
            var string = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue) ?? ""
            print(string)
            string = string.replacingOccurrences(of: "\n", with: "") as NSString
            string = string.replacingOccurrences(of: "\\\"", with: "\"") as NSString
            string = string.replacingOccurrences(of: "\\", with: "") as NSString // removes \
            string = string.replacingOccurrences(of: "/", with: "") as NSString
            return string
        }
        catch let error as NSError{
            print(error.description)
            return ""
        }
    }
    
    func getHotnessIcon(val : String) -> UIImage{
        switch val{
        case Fire.red.rawValue:
            return Fire.red.listingIcon
        case Fire.blue.rawValue:
            return Fire.blue.listingIcon
        case Fire.yellow.rawValue:
            return Fire.yellow.listingIcon
        case Fire.orange.rawValue:
            return Fire.orange.listingIcon
        default :
            return Fire.orange.listingIcon
        }
    }
    
    func getHotnessColor(val : String) -> UIColor{
        switch val{
        case Fire.red.rawValue:
            return Fire.red.colour
            
        case Fire.blue.rawValue:
            return Fire.blue.colour
            
            
        case Fire.yellow.rawValue:
            return Fire.yellow.colour
            
            
        case Fire.orange.rawValue:
            return Fire.orange.colour
            
        default :
            return Fire.orange.colour
            
        }
    }
    func getHotnessTitle(val : String) -> String{
        switch val{
        case Fire.red.rawValue:
            return Fire.red.popularity
            
        case Fire.blue.rawValue:
            return Fire.blue.popularity
            
            
        case Fire.yellow.rawValue:
            return Fire.yellow.popularity
            
            
        case Fire.orange.rawValue:
            return Fire.orange.popularity
            
        default :
            return Fire.orange.popularity
            
        }
    }
    
    
    
    func HeartColor(val : Int) -> UIImage{
        if val == 0 {
            return Fire.empty.heart}
        else if val <= 3 {
            return Fire.blue.heart}
            
        else if val <= 6{
            return Fire.yellow.heart}
            
            
        else if val <= 9{
            return Fire.orange.heart}
            
        else{
            return Fire.red.heart}
        
    }
    
    func getHotnessMarker(val : String) -> UIImage{
        switch val{
        case Fire.red.rawValue:
            return Fire.red.marker
            
        case Fire.blue.rawValue:
            return Fire.blue.marker
            
            
        case Fire.yellow.rawValue:
            return Fire.yellow.marker
            
            
        case Fire.orange.rawValue:
            return Fire.orange.marker
            
        default :
            return Fire.orange.marker
            
        }
        
    }
    func getCC(_ countryCode: String) -> String {
        let ccDict = ["AF":"93",
                      "AL":"355",
                      "DZ":"213",
                      "AS":"1",
                      "AD":"376",
                      "AO":"244",
                      "AI":"1",
                      "AG":"1",
                      "AR":"54",
                      "AM":"374",
                      "AW":"297",
                      "AU":"61",
                      "AT":"43",
                      "AZ":"994",
                      "BS":"1",
                      "BH":"973",
                      "BD":"880",
                      "BB":"1",
                      "BY":"375",
                      "BE":"32",
                      "BZ":"501",
                      "BJ":"229",
                      "BM":"1",
                      "BT":"975",
                      "BA":"387",
                      "BW":"267",
                      "BR":"55",
                      "IO":"246",
                      "BG":"359",
                      "BF":"226",
                      "BI":"257",
                      "KH":"855",
                      "CM":"237",
                      "CA":"1",
                      "CV":"238",
                      "KY":"345",
                      "CF":"236",
                      "TD":"235",
                      "CL":"56",
                      "CN":"86",
                      "CX":"61",
                      "CO":"57",
                      "KM":"269",
                      "CG":"242",
                      "CK":"682",
                      "CR":"506",
                      "HR":"385",
                      "CU":"53",
                      "CY":"537",
                      "CZ":"420",
                      "DK":"45",
                      "DJ":"253",
                      "DM":"1",
                      "DO":"1",
                      "EC":"593",
                      "EG":"20",
                      "SV":"503",
                      "GQ":"240",
                      "ER":"291",
                      "EE":"372",
                      "ET":"251",
                      "FO":"298",
                      "FJ":"679",
                      "FI":"358",
                      "FR":"33",
                      "GF":"594",
                      "PF":"689",
                      "GA":"241",
                      "GM":"220",
                      "GE":"995",
                      "DE":"49",
                      "GH":"233",
                      "GI":"350",
                      "GR":"30",
                      "GL":"299",
                      "GD":"1",
                      "GP":"590",
                      "GU":"1",
                      "GT":"502",
                      "GN":"224",
                      "GW":"245",
                      "GY":"595",
                      "HT":"509",
                      "HN":"504",
                      "HU":"36",
                      "IS":"354",
                      "IN":"91",
                      "ID":"62",
                      "IQ":"964",
                      "IE":"353",
                      "IL":"972",
                      "IT":"39",
                      "JM":"1",
                      "JP":"81",
                      "JO":"962",
                      "KZ":"77",
                      "KE":"254",
                      "KI":"686",
                      "KW":"965",
                      "KG":"996",
                      "LV":"371",
                      "LB":"961",
                      "LS":"266",
                      "LR":"231",
                      "LI":"423",
                      "LT":"370",
                      "LU":"352",
                      "MG":"261",
                      "MW":"265",
                      "MY":"60",
                      "MV":"960",
                      "ML":"223",
                      "MT":"356",
                      "MH":"692",
                      "MQ":"596",
                      "MR":"222",
                      "MU":"230",
                      "YT":"262",
                      "MX":"52",
                      "MC":"377",
                      "MN":"976",
                      "ME":"382",
                      "MS":"1",
                      "MA":"212",
                      "MM":"95",
                      "NA":"264",
                      "NR":"674",
                      "NP":"977",
                      "NL":"31",
                      "AN":"599",
                      "NC":"687",
                      "NZ":"64",
                      "NI":"505",
                      "NE":"227",
                      "NG":"234",
                      "NU":"683",
                      "NF":"672",
                      "MP":"1",
                      "NO":"47",
                      "OM":"968",
                      "PK":"92",
                      "PW":"680",
                      "PA":"507",
                      "PG":"675",
                      "PY":"595",
                      "PE":"51",
                      "PH":"63",
                      "PL":"48",
                      "PT":"351",
                      "PR":"1",
                      "QA":"974",
                      "RO":"40",
                      "RW":"250",
                      "WS":"685",
                      "SM":"378",
                      "SA":"966",
                      "SN":"221",
                      "RS":"381",
                      "SC":"248",
                      "SL":"232",
                      "SG":"65",
                      "SK":"421",
                      "SI":"386",
                      "SB":"677",
                      "ZA":"27",
                      "GS":"500",
                      "ES":"34",
                      "LK":"94",
                      "SD":"249",
                      "SR":"597",
                      "SZ":"268",
                      "SE":"46",
                      "CH":"41",
                      "TJ":"992",
                      "TH":"66",
                      "TG":"228",
                      "TK":"690",
                      "TO":"676",
                      "TT":"1",
                      "TN":"216",
                      "TR":"90",
                      "TM":"993",
                      "TC":"1",
                      "TV":"688",
                      "UG":"256",
                      "UA":"380",
                      "AE":"971",
                      "GB":"44",
                      "US":"1",
                      "UY":"598",
                      "UZ":"998",
                      "VU":"678",
                      "WF":"681",
                      "YE":"967",
                      "ZM":"260",
                      "ZW":"263",
                      "BO":"591",
                      "BN":"673",
                      "CC":"61",
                      "CD":"243",
                      "CI":"225",
                      "FK":"500",
                      "GG":"44",
                      "VA":"379",
                      "HK":"852",
                      "IR":"98",
                      "IM":"44",
                      "JE":"44",
                      "KP":"850",
                      "KR":"82",
                      "LA":"856",
                      "LY":"218",
                      "MO":"853",
                      "MK":"389",
                      "FM":"691",
                      "MD":"373",
                      "MZ":"258",
                      "PS":"970",
                      "PN":"872",
                      "RE":"262",
                      "RU":"7",
                      "BL":"590",
                      "SH":"290",
                      "KN":"1",
                      "LC":"1",
                      "MF":"590",
                      "PM":"508",
                      "VC":"1",
                      "ST":"239",
                      "SO":"252",
                      "SJ":"47",
                      "SY":"963",
                      "TW":"886",
                      "TZ":"255",
                      "TL":"670",
                      "VE":"58",
                      "VN":"84",
                      "VG":"284",
                      "VI":"340"]
        var requiredCC: String?
        ccDict.forEach { (value: (key: String, value: String)) in
            if value.key == countryCode {
                requiredCC = value.value
            }
            return
        }
        return ("+" + /requiredCC)
    }
    
    func getChatTime(timestamp : Double?) -> String{
        let date = NSDate(timeIntervalSince1970: timestamp ?? 0.0)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM d, h:mm a"
        let localDate = dateFormatter.string(from: date as Date)
        return localDate
        
    }
    
    func getCurTime() -> String {
        let date = Date(timeIntervalSince1970: Date.timeIntervalSinceReferenceDate)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM d, h:mm a"
        return dateFormatter.string(from: date)
    }
    
    
    func contactFetch(isAlert:Bool = false,completionHandler : @escaping ([String]?) -> ()){
        var phoneeArray : [String] = []
        let strCode = Utility.functions.getCC(/((Locale.current as? NSLocale)?.object(forKey: .countryCode) as? String))
        
        requestForAccess(isAlert:isAlert) { [weak self] (val) in
            if val {
                let keysToFetch = [CNContactPhoneNumbersKey]
                let request = CNContactFetchRequest(keysToFetch: keysToFetch as [CNKeyDescriptor])
                do {
                    try self?.store.enumerateContacts(with: request) {  contact, stop in
                        if contact.phoneNumbers.count > 0{
                            for item in contact.phoneNumbers {
                                if let MobNumVar = (item.value).value(forKey: "digits") as? String {
                                    //phoneeArray.append(MobNumVar)
                                    phoneeArray.append((MobNumVar.replacingOccurrences(of: strCode, with: "")))
                                }
                            }
                        }
                    }
                } catch {
                    print(error)
                }
                completionHandler(phoneeArray )
            }else{
                completionHandler([])

            }
        }
    }
    
    func requestForAccess(isAlert:Bool = false ,onlyPermission:Bool = false,completionHandler: @escaping (_ accessGranted: Bool) -> Void) {
        let authorizationStatus = CNContactStore.authorizationStatus(for: CNEntityType.contacts)
        switch authorizationStatus {
        case .authorized:
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "LocationReceived"), object: nil, userInfo: nil)
            completionHandler(true)
            
        case .denied, .notDetermined:
            self.store.requestAccess(for: CNEntityType.contacts, completionHandler: { (access, accessError) -> Void in
                if access {
                    
                    completionHandler(access)
                }
                else {
                    if authorizationStatus == CNAuthorizationStatus.denied || authorizationStatus == .notDetermined{
                        DispatchQueue.main.async {
                        if ez.topMostVC is HomeVC{
                            if !onlyPermission && isAlert {
                                self.showAlertConfirmation()
                            }
                            Utility.functions.removeLoader()

                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "LocationReceived"), object: nil, userInfo: nil)
                            //Alerts.shared.show(alert: .error, message: TitleType.allowContact.rawValue, type: .info)
//                           DispatchQueue.main.async {
//                                if !onlyPermission && isAlert {
//                                    self.showAlertConfirmation()
//                                }

                           }

                        }
                    }
                }
               
            })
        default:
            completionHandler(false)
        }
    }
    
    func showAlertConfirmation(){
        AlertsClass.shared.showAlertController(withTitle: TitleType.sync.rawValue, message: TitleType.allowContact.rawValue, buttonTitles: [TitleType.setting.rawValue , TitleType.Cancel.rawValue]) { (value) in
            let type = value as AlertTag
            switch type {
            case .yes:
                UIApplication.shared.openURL(NSURL(string:UIApplication.openSettingsURLString)! as URL)
            default:
                break
            }
        }
      //  ez.topMostVC?.present(refreshAlert, animated: true, completion: nil)

        
        
//        let refreshAlert = UIAlertController(title: TitleType.sync.rawValue, message: TitleType.allowContact.rawValue, preferredStyle: UIAlertController.Style.alert)
//        
//        refreshAlert.addAction(UIAlertAction(title: TitleType.setting.rawValue, style: .default, handler: { (action: UIAlertAction!) in
//            refreshAlert .dismiss(animated: true, completion: nil)
//            UIApplication.shared.openURL(NSURL(string:UIApplication.openSettingsURLString)! as URL)
//            
//        }))
//        
//        ez.topMostVC?.present(refreshAlert, animated: true, completion: nil)
    }
    
}



//    public func calculateDate(time : String) -> String {
//        let dateFormatter: DateFormatter = DateFormatter()
//        dateFormatter.timeZone = TimeZone(abbreviation: "GMT")
//        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
//        guard let locationDate: Date =  dateFormatter.date(from: time) as Date? else { return "" }
//        print(locationDate)
//        dateFormatter.dateFormat = "MMM d, yyyy"
//        dateFormatter.timeZone = NSTimeZone.local
//        let timeStamp = dateFormatter.string(from: locationDate)
//        print(timeStamp)
//        return  timeStamp
//    }
//
//    public func calculateShipmentDate(time : String?) -> String {
//        let dateFormatter: DateFormatter = DateFormatter()
//        dateFormatter.timeZone = TimeZone(abbreviation: "GMT")
//        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
//        guard let tempTime = time else { return "" }
//        guard let locationDate: Date =  dateFormatter.date(from: tempTime) as Date? else { return "" }
//        print(locationDate)
//        dateFormatter.dateFormat = "MMM d,yyyy"
//        dateFormatter.timeZone = NSTimeZone.local
//        let timeStamp = dateFormatter.string(from: locationDate)
//        print(timeStamp)
//        return  timeStamp
//    }
//
//
//
//    static func getHourMinuteFromSeconds(seconds: NSNumber) -> String {
//        var str = ""
//        let hours = seconds.intValue / 3600
//        let remainder = seconds.intValue % 3600
//        let minutes = remainder / 60
//        //let seconds = remainder % 60
//
//        if hours > 0 {
//            str = "\(hours) Hr "
//        }
//        if minutes > 0 {
//            str = str + "\(minutes) Min"
//        }
//        return str
//    }
//
//   static func appendOptionalStrings(strings : [String?],separator : String) -> String{
//        return strings.flatMap{$0}.joined(separator: separator)
//    }
//
//    static func get24HrTimeFromTimestamp(timestamp: Int64) -> String {
//        let date = Date(timeIntervalSince1970: TimeInterval(timestamp))
//        print(date)
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "HH:mm"
//        var day = 0
//        var hr = 0
//        let min = 0
//        let str = dateFormatter.string(from: date)
//        let dateNew = dateFormatter.date(from: str)
//        if (dateNew?.hour)! > 23 {
//            day = hr/24
//            hr = hr - (day*24)
//        }
//
//        return "\(day)" + "-d" + "\(hr)" + "-h" + "\(min)" + "-m"
//    }
//
//    static func getDateMonthFromTimestamp(timestamp: NSNumber) -> String {
//        let date = Date(timeIntervalSince1970: timestamp.doubleValue)
//
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "dd MMM"
//
//        return dateFormatter.string(from: date)
//    }
//
//    static func get12HrTimeFromTimestamp1(timestamp: Double) -> String {
//        let date = Date(timeIntervalSince1970: timestamp)
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "hh:mm a"
//
//        return dateFormatter.string(from: date)
//    }
//
//
//
//
//
//    static func getDateTimestampInDBFormat(date:Date) -> Double {
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateStyle = .short
//        dateFormatter.dateFormat = "MM/dd/yyyy"
//
//        let dateString = dateFormatter.string(from: date)
//        let date = dateFormatter.date(from: dateString)
//        let timeInDouble = Double(date!.timeIntervalSince1970)
//        let rnd = round(timeInDouble)
//        return (round(timeInDouble))
//    }
//
//    static func getCurrentMillis()->Int64 {
//        return Int64(Date().timeIntervalSince1970 * 1000)
//    }
//

