//
//  SocialNetworkClass.swift
//  BusinessDirectory
//
//  Created by Aseem 13 on 06/01/17.
//

import UIKit
import FBSDKLoginKit
import FBSDKCoreKit
import EZSwiftExtensions
import SwiftyJSON



typealias SuccessBlock = (_ fbId: String?,_ name : String?, _ email : String?,_ img : String? ) -> ()

class SocialNetworkClass: NSObject{
    
    static let shared = SocialNetworkClass()
    var responseBack : SuccessBlock?
    
    //MARK: Facebook
    
    func facebookLogin(responseBlock : @escaping SuccessBlock) {
        responseBack = responseBlock
        let fbLoginManager : LoginManager = LoginManager()
        fbLoginManager.logOut()
        Utility.functions.removeLoader()

        fbLoginManager.logIn(permissions: ["email","public_profile"],from: ez.topMostVC, handler: {[weak self] (result, error) -> Void in
            if (error == nil){
                if let fbloginresult = result{
                    if(fbloginresult.isCancelled) {
                        Utility.functions.removeLoader()
                    } else if(fbloginresult.grantedPermissions.contains("email")) {
                        self?.handleFbResult()
                    }
                    else{
                        //Utility.functions.removeLoader()
                        self?.handleFbResult()
                    }
                }else{Utility.functions.removeLoader()
                }
            }else{Utility.functions.removeLoader()
            }
        })
        
    }
    
    
    func handleFbResult()  {
        
        if (AccessToken.current != nil){
            GraphRequest.init(graphPath: "me", parameters: ["fields": "id, name,email,picture.width(300).height(300)"]).start{(connection, result, error) in
                self.hanleResponse(result:(result as AnyObject?))
            }
        }else{Utility.functions.removeLoader()}
    }
    
    func hanleResponse(result : AnyObject?)
    {
        if let dict = result as? Dictionary<String, AnyObject>{
            let response = JSON(dict)
            //print(response)
            let facebookId = dict["id"] as? String
            let email = dict["email"] as? String
            let name = dict["name"] as? String

            let imageUrl = response["picture"]["data"]["url"].string

            self.responseBack?(facebookId,name,email,imageUrl)

        }else{
            Utility.functions.removeLoader()

        }
    }
}

