//
//  SocketManager.swift
//  WhrzAtApp
//
//  Created by Sierra 4 on 09/10/17.
//  Copyright © 2017 com.example. All rights reserved.

import UIKit
import SocketIO
import ObjectMapper
import SwiftyJSON

internal struct ChatConstants {
    static let send = "sendMessage"
    static let receive = "receiveMessage"
    static let countNotification = "Count"
    static let requestNotificationsCount = "requestCount"
    static let countChat = "msgC"
    static let getUnread = "getUnread"
    static let seen = "makeRead"
    static let blockNotify = "blockNotify"
}

class SocketIOManager: NSObject {
    
    static let sharedInstance = SocketIOManager()
    
    //var socket: SocketIOClient = SocketIOClient(socketURL: URL(string: /APIConstants.socketBasePath.rawValue)! )
    
    var socket : SocketIOClient = SocketIOClient.init(socketURL: URL(string: /APIConstants.socketBasePath.rawValue)!, config: [.connectParams(["access_token" : /UserSingleton.shared.loggedInUser?._id])])
    
    override init() {
        super.init()
    }
    
    //Connect SOcket
    func establishConnection() {
        guard let user = UserSingleton.shared.loggedInUser else {return}
        socket = SocketIOClient.init(socketURL: URL(string: /APIConstants.socketBasePath.rawValue)!, config: [.connectParams(["access_token" : /user._id])])
        setupSocketEvents()
        socket.connect()
    }
    
    func setupSocketEvents() {
        
        socket.on(clientEvent: .connect, callback: { data, ack in
                   print("SocketManager: connected")
        })
        
        socket.on(clientEvent: .disconnect, callback: { data, ack in
                    print("SocketManager: disconnected")
        })
        
        socket.onAny {event in
            print(event.event)
        }
    }
    
    //Discpnnect Socket
    func closeConnection() {
        socket.disconnect()
    }
    
    
    //SEND RECEIVE EVENT
    func sendMessage(_ data: [String: Any]) {
        print(data)
        socket.emit(ChatConstants.send, data)
    }
    
    //Receive Message
    func recieveMessage(completionHandler: @escaping (_ messageInfo: Message?) -> Void, failure: @escaping (_ err: String) -> ()) {
        socket.on(ChatConstants.receive) { (dataArray, socketAck) in
            print(dataArray[0])
            let json = JSON(dataArray[0])
            let mappedModel = Message.init(from_: json["from"].string, to_: /json["to"].string, msg_: (json["message"].string ), time_: json["timeStamp"].description.toDouble())
            guard let mappedModell = mappedModel else{return}
            completionHandler(mappedModell)
        }
    }
    
    //RECEIVING Notification COUNT
    func receiveNotificationCount(completionHandler: @escaping (_ messageInfo: NotificationCount?) -> Void, failure: @escaping (_ err: String) -> ()) {
        socket.on(ChatConstants.countNotification + /UserSingleton.shared.loggedInUser?._id) { (dataArray, socketAck) in
            print(dataArray)
            let json = JSON(dataArray[0])
            let mappedModel = NotificationCount.init(count_: json["count"].description.toInt())
            completionHandler(mappedModel)
        }
    }
    
    //RECEIVING Block Notification
    func receiveBlocked(completionHandler: @escaping (_ messageInfo: NotificationCount?) -> Void, failure: @escaping (_ err: String) -> ()) {
        socket.on(ChatConstants.blockNotify + /UserSingleton.shared.loggedInUser?._id) { (dataArray, socketAck) in
            print(dataArray)
            let json = JSON(dataArray[0])
            let mappedModel = NotificationCount.init(id_: json["_id"].stringValue , blocked_ : json["blocked"].boolValue)
            completionHandler(mappedModel)
        }
    }
    
    
    //Receive Chat Tab Count
    func receiveChatCount(completionHandler: @escaping (_ messageInfo: NotificationCount?) -> Void, failure: @escaping (_ err: String) -> ()) {
        socket.on(ChatConstants.countChat + /UserSingleton.shared.loggedInUser?._id) { (dataArray, socketAck) in
            print(dataArray)
            let json = JSON(dataArray[0])
            let mappedModel = NotificationCount.init(count_: json["count"].description.toInt())
            completionHandler(mappedModel)
        }
    }
    
    //Requesting Notification Count
    func requestCount(_ data : [String : Any]){
        socket.emit(ChatConstants.requestNotificationsCount, data)
    }
    
    //Requesting for Chat Cont
    func requestChat(_ data : [String : Any]){
        socket.emit(ChatConstants.getUnread, data)
    }
    
    func seen(_ data : [String : Any]){
        socket.emit(ChatConstants.seen, data)
    }
    
    
}
