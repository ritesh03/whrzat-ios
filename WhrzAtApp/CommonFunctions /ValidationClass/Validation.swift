//
//  Validation.swift
//  Paramount
//
//  Created by cbl24 on 15/02/17.
//  Copyright © 2017 Codebrew. All rights reserved.
//

import UIKit

extension String {
    
    
    func login(email : String? , password : String?) -> Bool {
        if  isValid(type: .email, info: email) && isValid(type: .password, info: password)
        {
            return true
        }
        return false
    }
    
    func validateAddEvent(eventName : String? , description : String? , startTime : String? , endTime : String?) -> Bool{
        if isValid (type : .eventName , info : eventName) && isValid (type : .description , info : description) && isValid (type : .startTime , info : startTime) && isValid (type : .endTime , info : endTime){
            return true
        }
        return false
    }
    
    
    func signup(name : String?,mobile : String?) -> Bool{
        if isValid(type: .name, info: name)  && isValid(type: .mobile, info: mobile ){
            return true
        }
        return false
    }
    
//    func editProfile(name : String? , bio : String , mobile : String?) -> Bool{
//        if isValid(type : .name , info : name) && isValid(type : .bio , info : bio) && isValid(type: .mobile, info: mobile ) {
//            return true
//        }
//        return false
//    }
    func editProfile(name : String? , mobile : String?) -> Bool{
        if isValid(type : .name , info : name) && isValid(type: .mobile, info: mobile ) {
            return true
        }
        return false
    }
    
    func validateAddHotSpot(name : String? , description : String?) -> Bool{
        if isValid(type : .hotspotName , info: name) && isValid(type : .description , info : description){
            return true
        }
        return false
    }
    
    private func isValid(type : FieldType , info: String?) -> Bool {
        guard let validStatus = info?.handleStatus(fieldType : type) else {
            return true
        }
        let errorMessage = validStatus
       // print(errorMessage)
        Alerts.shared.show(alert: .error, message: errorMessage , type : .info)
        return false
    }
    
    func validateChangePassword(old : String? , new : String? , confirm : String?) -> Bool{
        
        if old == ""{
            Alerts.shared.show(alert: .error, message: AlertMessage.enterOldPassword.rawValue , type : .info)
            return false
        }else if !isValid(type: .password, info: new){
            return false
        }else if new != confirm{
            Alerts.shared.show(alert: .error, message: AlertMessage.matchConfirmAndNew.rawValue , type : .info)
            return false
        }else if new == old{
            Alerts.shared.show(alert: .error, message:AlertMessage.matchOldAndNew.rawValue , type : .info)
            return false
        }
        else{
            return true
        }
    }

    
    func handleStatus(fieldType : FieldType) -> String? {
        
        switch fieldType {
        case .firstName , .lastName , .name:
            return  isValidName.message(type: fieldType)
        case .email   :
            return  isValidEmail.message(type: fieldType)
        case .password , .newpassword:
            return  isValid(password: 6, max: 15).message(type: fieldType)
        case .mobile:
            return  isValidPhoneNumber.message(type: fieldType)
      case .description  ,  .hotspotName , .bio , .eventName , .startTime , .endTime:
            return  isValidInformation.message(type: fieldType)
        default:
            return  isValidInformation.message(type: fieldType)
        }
    }
    
    
    
    var isNumber : Bool {
        if let _ = NumberFormatter().number(from: self) {
            return true
        }
        return false
    }
    
    var hasSpecialCharcters : Bool {
        return rangeOfCharacter(from: CharacterSet.alphanumerics.inverted) != nil || rangeOfCharacter(from: CharacterSet.letters.inverted) != nil
    }
    
    var isEveryCharcterZero : Bool{
        var count = 0
        self.forEach {
            if $0 == "0"{
                count += 1
            }
        }
        if count == self.count{
            return true
        }else{
            return false
        }
    }
    
    
    
    public func toString(format: String , date: Date) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.string(from: date)
    }
    
    public var length: Int {
        return self.count
    }
    
    public var isEmail: Bool {
        let dataDetector = try? NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
        let firstMatch = dataDetector?.firstMatch(in: self, options: NSRegularExpression.MatchingOptions.reportCompletion, range: NSRange(location: 0, length: length))
        return (firstMatch?.range.location != NSNotFound && firstMatch?.url?.scheme == "mailto")
    }
    
    public var isBlank: Bool {
        get {
            let trimmed = trimmingCharacters(in: .whitespacesAndNewlines)
            return trimmed.isEmpty
        }
    }
    
    func isValid(password min: Int , max: Int) -> Status {
        if length < 0 { return .empty }
        if isBlank  { return .allSpaces }
        if count >= min && count <= max{
            return .valid
        }
        return .pwd
    }
    
    var isValidEmail : Status {
        if length < 0 { return .empty }
        if isBlank { return .allSpaces }
        if isEmail { return .valid }
        return .inValid
    }
    
    var isValidInformation : Status {
        if length < 0 { return .empty }
        if isBlank { return .allSpaces }
        return .valid
    }
    
    var isValidExtension : Status {
        if hasSpecialCharcters { return .hasSpecialCharacter }
        if self.count < 6  && isNumber { return .valid }
        if self.count == 0 { return .valid }
        return .inValid
    }
    
    var isValidPhoneNumber : Status {
        if length <= 0 { return .empty }
        if isBlank { return .allSpaces }
        if isEveryCharcterZero { return .allZeros }
        // if !hasSpecialCharcters { return .emptyCountrCode }
        if count >= 6 && self.count <= 15 { return .valid
        }else{
            return .mobileNumberLength
        }
    }
    
    var isValidName : Status {
        if length < 0 { return .empty }
        if isBlank { return .allSpaces }
//        if hasSpecialCharcters { return .hasSpecialCharacter }
        return .valid
    }
    
    func isValidCardNumber(length max:Int ) -> Status {
        if length < 0 { return .empty }
        if isBlank { return .allSpaces }
        if hasSpecialCharcters { return .hasSpecialCharacter }
        if isEveryCharcterZero { return .allZeros }
        if count >= 16 && count <= max{
            return .valid
        }
        return .inValid
    }
    
    var isValidCVV : Status {
        if hasSpecialCharcters { return .hasSpecialCharacter }
        if isEveryCharcterZero { return .allZeros }
        if isNumber{
            if self.count >= 3 && self.count <= 4{
                return .valid
            }else{ return .inValid }
        }else { return .notANumber }
    }
    
    var isValidZipCode : Status {
        if length < 0 { return .empty }
        if isEveryCharcterZero { return .allZeros }
        if isBlank { return .allSpaces }
        if hasSpecialCharcters {return.zip}
        if length != 6 {return .pinCode}
        // if !isNumber{ return .notANumber }
        
        return .valid
    }
    
    var isValidAmount :  Status {
        if length < 0 { return .empty }
        if isBlank { return .allSpaces }
        if !isNumber{ return .notANumber }
        return .valid
    }
    
}

