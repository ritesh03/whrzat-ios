////
////  ViewController+Dimable.swift
////  
////
//
//
//import UIKit
//
//enum Direction { case `in`, out }
//
//protocol Dimmable { }
//
//extension UIViewController : Dimmable {}
//extension Dimmable where Self: UIViewController {
//    
//    func dim(direction: Direction, color: UIColor = .black, alpha: Float = 1.0, speed: Double = 0.0) {
//        
//        switch direction {
//        case .in:
//            
//            // Create and add a dim view
//            let dimView = UIView(frame: view.frame)
//            dimView.backgroundColor = color
//            dimView.alpha = 0.0
//            view.addSubview(dimView)
//            
//            // Deal with Auto Layout
//            dimView.translatesAutoresizingMaskIntoConstraints = false
//            view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "|[dimView]|", options: [], metrics: nil, views: ["dimView": dimView]))
//            view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[dimView]|", options: [], metrics: nil, views: ["dimView": dimView]))
//            
//            // Animate alpha (the actual "dimming" effect)
//            UIView.animate(withDuration: speed) { () -> Void in
//                dimView.alpha = CGFloat(alpha)
//            }
//            
//        case .out:
//            UIView.animate(withDuration: speed, animations: { () -> Void in
//                self.view.subviews.last?.alpha = CGFloat(alpha) 
//                }, completion: { (complete) -> Void in
//                    self.view.subviews.last?.removeFromSuperview()
//            })
//        }
//    }
//}
//
//extension Dimmable where Self: UIWindow {
//    
//    func dim(direction: Direction, color: UIColor = .black, alpha: Float = 0.0, speed: Double = 0.0) {
//        
//        switch direction {
//        case .in:
//            
//            // Create and add a dim view
//            let dimView = UIView(frame: self.frame)
//            dimView.backgroundColor = color
//            dimView.alpha = 0.0
//            self.addSubview(dimView)
//            
//            // Deal with Auto Layout
////            dimView.translatesAutoresizingMaskIntoConstraints = false
////            self.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("|[dimView]|", options: [], metrics: nil, views: ["dimView": dimView]))
////            self.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[dimView]|", options: [], metrics: nil, views: ["dimView": dimView]))
//            
//            // Animate alpha (the actual "dimming" effect)
//            UIView.animate(withDuration: speed) { () -> Void in
//                dimView.alpha = CGFloat(alpha)
//            }
//            
//        case .out:
//            UIView.animate(withDuration: speed, animations: { () -> Void in
//                self.subviews.last?.alpha = CGFloat(alpha)
//                }, completion: { (complete) -> Void in
//                    self.subviews.last?.removeFromSuperview()
//            })
//        }
//    }
//}
