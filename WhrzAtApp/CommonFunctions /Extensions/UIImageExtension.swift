////
////  UIImageExtension.swift
////  WhrzAtApp
////
////  Created by Sierra 4 on 04/09/17.
////  Copyright © 2017 com.example. All rights reserved.
////
//
//import Foundation
//import UIKit
//
//extension UIImage{
//    
//    func getImageFromURL(url: String) -> UIImage?{
//        
//        if let url = NSURL(string: url) {
//            if let data = NSData(contentsOf: url as URL) {
//               return  UIImage(data: data as Data)
//            }
//        }
//        return nil
//    }
//    
//}
