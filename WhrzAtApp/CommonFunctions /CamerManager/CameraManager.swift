////
////  CameraManager.swift
////  WhrzAtApp
////
////  Created by Sierra 4 on 13/11/17.
////  Copyright © 2017 com.example. All rights reserved.
////
//
//import Foundation
//import UIKit
//import Photos
//import EZSwiftExtensions
//
//typealias imagePicked = (_ img : UIImage) -> ()
//
//
//class CameraManager: NSObject {
//
//    var imagePicked : imagePicked?
//    var selectedImage : UIImage?
//    var picker : UIImagePickerController?
//    var image : UIImage? = nil
//  
//    
//    init (pickrr :UIImagePickerController? ,  imagePicked : imagePicked?) {
//        self.imagePicked = imagePicked
//        self.picker = pickrr
//    }
//    
//    override init() {
//        super.init()
//        
//    }
//}
//
//
//
//extension CameraManager : UIImagePickerControllerDelegate, UINavigationControllerDelegate{
//    
//    func selectImage(imagePicked : imagePicked?) {
//        let alert:UIAlertController=UIAlertController(title: TitleType.addPhoto.rawValue, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
//        alert.view.tintColor = UIColor.red()
//        let cameraAction = UIAlertAction(title: TitleType.camera.rawValue, style: UIAlertActionStyle.default) {
//            UIAlertAction in
//            self.checkCameraStatus()
//        }
//        let gallaryAction = UIAlertAction(title: TitleType.gallery.rawValue, style: UIAlertActionStyle.default) {
//            UIAlertAction in
//            self.openGallary()
//        }
//        let cancelAction = UIAlertAction(title: TitleType.Cancel.rawValue, style: UIAlertActionStyle.cancel) {
//            UIAlertAction in
//        }
//       // self.picker.delegate = self
//        alert.addAction(cameraAction)
//        alert.addAction(gallaryAction)
//        alert.addAction(cancelAction)
//        
//        if UIDevice.current.userInterfaceIdiom == .phone {
//            //self.present(alert, animated: true, completion: nil)
//            ez.topMostVC?.present(alert
//                , animated: true, completion: nil)
//        }
//    }
//    
//    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any] ) {
//        guard let chosenImage = info[UIImagePickerControllerEditedImage] as? UIImage else { return }
//        selectedImage = chosenImage
//        if let block = self.imagePicked {
//            block(chosenImage)
//        }
//       // self.donePicking(selectImage : chosenImage)
//        //dismiss(animated:true, completion: nil) //5
//        ez.topMostVC?.dismiss(animated: true, completion: nil)
//    }
//    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
//        ez.topMostVC?.dismiss(animated: true, completion: nil)
//    }
//    func openCamera() {
//        // grantAccessForCamera()
//        let cameraMediaType = AVMediaTypeVideo
//        let cameraAuthorizationStatus = AVCaptureDevice.authorizationStatus(forMediaType: cameraMediaType)
//        if cameraAuthorizationStatus == .authorized || cameraAuthorizationStatus == .notDetermined{
//            if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)) {
//                picker?.sourceType = UIImagePickerControllerSourceType.camera
//              //  picker.delegate = self
//                picker?.allowsEditing = true
//                //self .present(picker, animated: true, completion: nil)
//                ez.topMostVC?.present(picker!, animated: true, completion: nil)
//            }
//        }
//        else if cameraAuthorizationStatus == .denied {
//            showCustomAlert(title: TitleType.permissionRequired.rawValue, desc: TitleType.allowPermission.rawValue, buttons: [TitleType.setting.rawValue , TitleType.Cancel.rawValue], success: {
//                UIApplication.shared.openURL(NSURL(string:UIApplicationOpenSettingsURLString)! as URL)
//            })
//            
//        }
//    }
//    
//    func grantAccessForCamera() {
//        AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo) { granted in
//            DispatchQueue.main.async() { self.checkCameraStatus() } }
//    }
//    
//    func openGallary() {
//        
//        let status = PHPhotoLibrary.authorizationStatus()
//        if status == .authorized || status == .notDetermined {
//            picker?.sourceType = UIImagePickerControllerSourceType.photoLibrary
//            picker?.allowsEditing = true
//           // picker.delegate = self
//            if UIDevice.current.userInterfaceIdiom == .phone {
//                ez.topMostVC?.present(picker!, animated: true, completion: nil)
//            }
//        }       else if status == .denied {
//            showCustomAlert(title: TitleType.permissionRequired.rawValue, desc: TitleType.allowPermission.rawValue, buttons: [TitleType.setting.rawValue , TitleType.Cancel.rawValue], success: {
//                UIApplication.shared.openURL(NSURL(string:UIApplicationOpenSettingsURLString)! as URL)
//            })
//            
//        }
//    }
//    
//    func checkCameraStatus() {
//        
//        let authStatus = AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo)
//        switch authStatus {
//        case .authorized: openCamera()
//        case .denied: alertToEnableCameraViaSetting()
//        case .notDetermined: grantAccessForCamera()
//        default: alertToEnableCameraViaSetting()
//            
//        }
//    }
//    
//
//    
//    func alertToEnableCameraViaSetting() {
//        showCustomAlert(title: TitleType.permissionRequired.rawValue, desc: TitleType.allowPermission.rawValue, buttons: [TitleType.setting.rawValue , TitleType.Cancel.rawValue], success: {
//            UIApplication.shared.openURL(NSURL(string:UIApplicationOpenSettingsURLString)! as URL)
//        })
//    }
//    
//     func showCustomAlert(title : String? , desc : String? , buttons : [String]? , success : @escaping () -> ()){
//        AlertsClass.shared.showAlertController(withTitle: /title, message: /desc, buttonTitles: buttons ?? []) { (value) in
//            let type = value as AlertTag
//            switch type {
//            case .yes:
//                success()
//            default:
//                break
//            }
//        }
//    }
//
//}
