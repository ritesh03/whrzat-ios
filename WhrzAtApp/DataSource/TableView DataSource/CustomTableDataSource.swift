//
//  TableViewCustomDatasource.swift
//  SwimpyNew
//
//  Created by Aseem 10 on 11/24/16.
//  Copyright © 2016 Aseem 10. All rights reserved.
//

import UIKit



class CustomTableDatasource: NSObject {
    
    var items : Array<AnyObject>?
    var cellIdentifier : [String]?
    var tableView  : UITableView?
    var tableViewRowHeight : CGFloat?
    var tableViewEstimatedRowHeight : CGFloat?
    var configureCellBlock : ListCellConfigureBlock?
    var aRowSelectedListener : DidSelectedRow?
    var viewforHeaderInSection : ViewForHeaderInSection?
    var willDisplayCell : WillDisplayCell?
    var headerHeight : CGFloat? = 0.0
    
    init (items : Array<AnyObject>? , height : CGFloat ,estimatedHeight :CGFloat , tableView : UITableView? , cellIdentifier : [String]?  , configureCellBlock : ListCellConfigureBlock? , aRowSelectedListener :   DidSelectedRow?,willDisplayCell : WillDisplayCell? , viewforHeaderInSection : ViewForHeaderInSection? ) {
        
        self.tableView = tableView
        self.items = items
        self.tableViewRowHeight = height
        self.cellIdentifier = cellIdentifier ?? []

        self.tableViewEstimatedRowHeight = estimatedHeight
        self.configureCellBlock = configureCellBlock
        self.aRowSelectedListener = aRowSelectedListener
        self.willDisplayCell = willDisplayCell
        self.tableView?.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        self.viewforHeaderInSection = viewforHeaderInSection
    }
    
    override init() {
        super.init()
    }
}
extension CustomTableDatasource : UITableViewDelegate , UITableViewDataSource{
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //let id = indexPath.row % 2 == 0 ? 0 : 1
        
        guard let item = items as? [Message] else {return UITableViewCell()}
        let isMe = item[indexPath.row].from == UserSingleton.shared.loggedInUser?._id
        guard let identifier = isMe ? cellIdentifier?[1] : cellIdentifier?[0] else{
            fatalError("Cell identifier not provided")
        }
        let cell: UITableViewCell = tableView.dequeueReusableCell(withIdentifier: identifier , for: indexPath) as UITableViewCell
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        
        if let block = self.configureCellBlock , let item: AnyObject = self.items?[indexPath.row] as AnyObject?{
            block(cell , item , indexPath )
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let block = self.willDisplayCell {
            block(indexPath)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let block = self.aRowSelectedListener{
            block(indexPath)
        }
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableViewEstimatedRowHeight!
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items?.count ?? 0
    }
    
    private func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.tableViewRowHeight!
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let block = viewforHeaderInSection else { return nil }
        return block(section)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return headerHeight ?? 0.0
    }
    
}

