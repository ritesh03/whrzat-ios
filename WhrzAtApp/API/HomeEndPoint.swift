
//  Home.swift
//  APISampleClass
//
//  Created by Aseem 13 on 23/02/17.
//  Copyright © 2017 . All rights reserved.


import UIKit
import Alamofire

enum HomeEndpoint{
    
    case editProfile(userId : String? , name : String? , password : String? , contact : String? ,  radius : String? , bio : String?)
    case createHotspot(name : String? , lat : String? , long : String? , description : String? , createdBy : String? , registrationDate : String? , area : String?)
    case hotspotMapView(lat : String? , long : String? , radius : String? , desc : String? , id : String? , search : String? , skip : String? , limit : String? )
    case promoteEventList(lat : String? , long : String? , id : String? , search : String? , skip : String? , limit : String?, radius : String? )
    case getHappeningEvents(userId : String?,skip : String? , limit : String? , radius : String?,lat :String?, long: String?)
    case hotspotDetails(hotspotId : String? , userId : String?)
    case createEvent(name : String? , description : String? , start : String? , end : String? , createdBy : String? , hotspotId : String? , tags : String?)
    case checkin(userId : String? , hotspotId : String?)
    case checkOut(userId : String? , hotspotId : String?)
    case getEvents(hotspotId : String?  , userId : String?)
    case addImages(createdBy : String? , hotspotId : String? , eventId : String?)
    case getImages(hotspotId : String?)
    case reportImage(imageId : String? , id : String?)
    case likeImage(userId :String? , imageId : String? , isLike  : String?)
    case favUnfav(hotspotId: String?,userId: String, favourite : Bool?)
    case syncContact(body : [String : Any]? , id : String? , arr : String?)
    case getFeeds(customerId : String?)
    case deletePic(id : String? , hotspotid : String?)
    case blockUser(id : String? , isBlock : String? , contact : String?)
    case getBlockedList(id : String?)
    case feedUpdate(id : String? , feedStatus : String? , radius : String? )
    case deleteUser(id : String?)
    case notificationslisting(id : String?)
    case editNotifications(notifications:[String],userId:String?)

    //case editNotifications(body : [String : Any]?)
    case getProfile(id  : String? , profileId : String?)
    case getNotifications(id : String? , skip : String? , limit : String?)
    case getFavourites(id : String?)
    case chatHistory(sender : String? , receiver : String? , skip : String? , limit : String?)
    case messageListing(id : String? , skip : String? , limit : String?)
    case deleteChat(id : String? , otherUser : String?)
    case updateToken
    case clear(id : String?)
    case getRewardDetail
    case redeemRequest
}

extension HomeEndpoint : Router{
    
    var route : String  {
        switch self {
        case .getFavourites(_):         return APIConstants.getFavourites.rawValue
        case .editProfile(_):           return APIConstants.updateProfile.rawValue
        case .createHotspot(_):         return APIConstants.createHotspot.rawValue
        case .hotspotMapView(_):        return APIConstants.hotspotMapView.rawValue
        case .promoteEventList(_):      return APIConstants.promoteEvent.rawValue
        case .getHappeningEvents(_):    return APIConstants.happeningEvent.rawValue
        case .hotspotDetails(_):        return APIConstants.hotspotDetails.rawValue
        case .createEvent(_):           return APIConstants.createEvent.rawValue
        case .checkin(_):               return APIConstants.checkin.rawValue
        case .checkOut(_):              return APIConstants.checkout.rawValue
        case .getEvents(_):             return APIConstants.getEvents.rawValue
        case .addImages(_):             return APIConstants.addImages.rawValue
        case .getImages(_):             return APIConstants.getImages.rawValue
        case .reportImage(_):           return APIConstants.reportImage.rawValue
        case .likeImage(_):             return APIConstants.likeImage.rawValue
        case .favUnfav(_):              return APIConstants.favUnfav.rawValue
        case .syncContact(_):           return APIConstants.syncContact.rawValue
        case .getFeeds(_):              return APIConstants.getFeeds.rawValue
        case .deletePic(_):             return APIConstants.deletepic.rawValue
        case .blockUser(_):             return APIConstants.blockUser.rawValue
        case .getBlockedList(_):        return APIConstants.getBlockedList.rawValue
        case .feedUpdate(_):            return APIConstants.updateFeed.rawValue
        case .deleteUser(_):            return APIConstants.deleteUser.rawValue
        case .notificationslisting(_):  return APIConstants.notificationslisting.rawValue
        case .editNotifications(_):     return APIConstants.editNotifications.rawValue
        case .getProfile(_):            return APIConstants.getProfile.rawValue
        case .getNotifications(_):      return APIConstants.getNotifications.rawValue
        case .chatHistory(_):           return APIConstants.chatHistory.rawValue
        case .messageListing(_):        return APIConstants.messageListing.rawValue
        case .deleteChat(_):            return APIConstants.deleteChat.rawValue
        case .updateToken:              return APIConstants.updateToken.rawValue
        case .clear(_):                 return APIConstants.clear.rawValue
        case .getRewardDetail:          return APIConstants.getRewardDetail.rawValue
        case .redeemRequest:            return APIConstants.redeemRequest.rawValue
        }
    }
    
        
    var parameters: OptionalDictionary{
        return format()
    }
    
    func format() -> OptionalDictionary {
        
        switch self {
            
        case .editProfile(let userId , let name , let password  , let contact , let radius , let bio ):
            return Parameters.editProfile.map(values : [userId , name , password , contact , radius , bio])
        case .createHotspot(let name, let lat, let long, let description, let createdBy, let registrationDate , let area):
            return Parameters.createHotspot.map(values: [name , lat , long , description , createdBy , registrationDate , area])
        case .hotspotMapView(let lat, let long, let radius, let desc , let id , let search , let skip , let limit):
            return Parameters.hotspotMapView.map(values : [lat , long , radius , desc , id , search , skip , limit])
        case .promoteEventList(let lat , let long , let id , let search , let skip, let limit, let radius):
            return Parameters.promoteList.map(values: [lat , long , id , search , skip , limit, radius])
        case .getHappeningEvents(let userID ,let skip , let limit, let radius, let lat , let long):
            return Parameters.happeningEvent.map(values: [userID , skip , limit, radius, lat, long])
        case .hotspotDetails(let hotspotId , let userId):
            return Parameters.hotspotDetails.map(values : [hotspotId , userId])
        case .createEvent(let name , let description , let start , let end , let createdBy , let hotspotId , let tags):
            return Parameters.createEvent.map(values : [name , description , start , end , createdBy , hotspotId , tags])
        case .checkin(let userId, let hotspotId):
            return Parameters.checkin.map(values : [userId , hotspotId])
        case .checkOut(let userId, let hotspotId):
            return Parameters.checkOut.map(values : [userId , hotspotId])
        case .getEvents(let hotspotId , let userId):
            return Parameters.getEvents.map(values : [hotspotId , userId])
        case .addImages(let createdBy , let hotspotid , let eventId):
            return Parameters.addImages.map(values : [createdBy , hotspotid, eventId])
        case .getImages(let hotspotId):
            return Parameters.getImages.map(values : [hotspotId])
        case .reportImage(let imageId , let id):
            return Parameters.reportImage.map(values: [imageId , id])
        case .likeImage(let userId , let imageId, let isLike):
            return Parameters.likeImage.map(values : [userId , imageId , isLike])
        case .favUnfav(let hotspotId , let userId , let favourite):
            return Parameters.favUnfav.map(values : [hotspotId , userId , favourite])
        case .syncContact(let body , let id , let arr):
            return Parameters.syncContact.map(values : [id , arr])
        case .getFeeds(let id):
            return Parameters.getFeeds.map(values : [id])
        case .deletePic(let id , let hotspotid):
            return Parameters.deletePic.map(values: [id , hotspotid])
        case .blockUser(let id , let isBlocked , let contact):
            return Parameters.blockUser.map(values : [id , isBlocked , contact])
        case .getBlockedList(let id):
            return Parameters.getBlockedList.map(values : [id])
        case .feedUpdate(let id , let feedStatus , let radius):
            return Parameters.feedUpdate.map(values: [id , feedStatus , radius])
        case .deleteUser(let id):
            return Parameters.deleteUser.map(values: [id])
        case .notificationslisting(let id):
            return Parameters.notificationslisting.map(values : [id])
            
        case .editNotifications(let notifications, let userId):
            return Parameters.editNotifications.map(values: [notifications,userId])
//        case .editNotifications(let body):
//            return body
        case .getProfile(let id , let profileId):
            return Parameters.getProfile.map(values : [id , profileId])
            case .getFavourites(let id):
            return Parameters.getFavourite.map(values : [id])
        case .getNotifications(let id, let skip, let limit):
            return Parameters.getNotifications.map(values: [id , skip , limit])
        case .chatHistory(let sender , let receiverr , let skip , let limit):
            return Parameters.chatHistory.map(values : [sender , receiverr , skip , limit])
        case . messageListing(let id , let skip , let limit):
            return Parameters.messageListing.map(values : [id , skip , limit])
        case .deleteChat(let id , let otherUser):
            return Parameters.deleteChat.map(values : [id , otherUser])
        case .updateToken:
            return Parameters.updateToken.map(values: [/UserSingleton.shared.loggedInUser?._id ,UserSingleton.shared.token == "" ? "token" : UserSingleton.shared.token])
        case .clear(let id):
            return Parameters.clear.map(values : [id])
        case .getRewardDetail:
            return Parameters.getRewardDetail.map(values : [])
        case .redeemRequest:
            return Parameters.redeemRequest.map(values : [])
        }
    }
    
    var method : Alamofire.HTTPMethod {
        switch self {
        case .getRewardDetail, .redeemRequest:
            return .get
            default:
            return .post
        }
    }
    
    var baseURL: String{
        return APIConstants.basePath.rawValue
    }
}
