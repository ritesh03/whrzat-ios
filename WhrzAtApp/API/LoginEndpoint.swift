//
//  Login.swift
//  APISampleClass
//
//  Created by cbl20 on 2/23/17.
//  Copyright © 2017 . All rights reserved.
//

import UIKit
import Alamofire

enum LoginEndpoint {
    
    case signUp( name : String? ,email : String? , password : String?  , phoneNo : String? , profilePic : String? , fromFacebook : String? , fbId : String? , bio : String? , code : String?)
    case facebookIdCheck(id : String?)
    case login(email : String? , password : String?,code : String?)
    case forgetPassword(email : String?)
    case logOut
    case verifyOtp
    case changePassword(id : String? , old : String? , new : String?)
    case versioning
    case verifyReferralCode(code: String?)
}


extension LoginEndpoint : Router{
    
    var route : String  {
        
        switch self {
        case .login(_ ):
            return APIConstants.login.rawValue
        case .signUp(_):
            return APIConstants.signUp.rawValue
        case .facebookIdCheck(_):
            return APIConstants.facebookIdCheck.rawValue
        case .logOut:
            return APIConstants.logout.rawValue
        case .forgetPassword(_):
            return APIConstants.forgetPassword.rawValue
        case .changePassword(_):
            return APIConstants.changePassword.rawValue
        case .versioning:
            return APIConstants.versioning.rawValue
        case .verifyOtp:
            return APIConstants.verifyOtp.rawValue
        case .verifyReferralCode(_):
            return APIConstants.verifyReferralCode.rawValue
            
        }
        
    }
    
    var parameters: OptionalDictionary{
        return format()
    }
    
      
    func format() -> OptionalDictionary {
        switch self {
           
        case .forgetPassword(let email):
            return Parameters.forgetPassword.map(values : [email])
        case .login(let email, let password, let code ):
            return Parameters.login.map(values: [email , password ,  DeviceConstants.deviceType.rawValue , UserSingleton.shared.token == "" ? "noTokenGeneratedYet" : UserSingleton.shared.token , (NSTimeZone.local as NSTimeZone).secondsFromGMT.description, code])
        case .signUp( let name, let email, let password, let phoneNo, let profilePic, let fromFacebook , let fbId , let bio  , let code ):
            return Parameters.signUp.map(values: [ name , email , password  , phoneNo , profilePic , fromFacebook , fbId , bio , DeviceConstants.deviceType.rawValue , UserSingleton.shared.token == "" ? "noTokenGeneratedYet" : UserSingleton.shared.token , code , (NSTimeZone.local as NSTimeZone).secondsFromGMT.description ])
        case .facebookIdCheck(let id):
            return Parameters.facebookIdCheck.map(values: [id , UserSingleton.shared.token == "" ? "noTokenGeneratedYet" : UserSingleton.shared.token , (NSTimeZone.local as NSTimeZone).secondsFromGMT.description])
        case .logOut:
            return Parameters.logout.map(values : [])

        case .changePassword(let id , let old , let new):
            return Parameters.changePassword.map(values : [id , old , new])
        case .versioning:
            return Parameters.versioning.map(values : [])
        case .verifyOtp:
            return Parameters.verifyOTP.map(values : [])
        case .verifyReferralCode(let referralCode):
            return Parameters.verifyReferralCode.map(values : [referralCode])
        }
        
    }
    
    
    var method : Alamofire.HTTPMethod {
        switch self {
        case .versioning:
            return .get
        default :
            return .post
        }
    }
    
    var baseURL: String{
        return APIConstants.basePath.rawValue
    }
    
}
