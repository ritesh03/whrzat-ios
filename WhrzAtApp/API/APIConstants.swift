//donskelon@gmail.com -> //apple developer acc
//whrzat@gmail.com ->       //firebase account


import Foundation

enum DeviceConstants : String{
    case deviceType = "IOS"
}

enum APIConstants : String {
    
    //live store
    case appTitle = "WhrzAt"
    case appStoreid = "1217538793"
    case fireabasedynamicLink = "https://whrzat.page.link"
    case socketBasePath =       "https://whrzat.com:8001/"
    case basePath =             "https://whrzat.com:8001/api/customer/"
    case help = "https://whrzat.com"
    case terms = "https://whrzat.com:8001/api/customer/tc"
    case version = "https://itunes.apple.com/us/app/apple-store/id1217538793?ls=1&mt=8"// App Store Link
    case message = "message"
    case success = "success"
    case statusCode = "statusCode"
    case signUp = "customerSignUp"
    case facebookIdCheck = "facebookIdCheck"
    case login = "customerLogIn"
    case forgetPassword = "resetPassword"
    case updateProfile = "EditProfile"
    case getFavourites = "favouriteHotspotListing"
    case createHotspot = "createHotspot"
    case hotspotMapView = "hotspotMapView"
    case promoteEvent = "PromotedEventLisiting"
    case happeningEvent = "Happening"
    case hotspotDetails = "hotspotDetails"
    case createEvent = "createEvent"
    case logout = "logout"

    case checkin = "checkIn"
    case getEvents = "getEvents"
    case addImages = "addImages"
    case checkout = "checkOut"
    case getImages = "getImages"
    case reportImage = "reportImage"
    case likeImage = "likeImage"
    case favUnfav = "favouriteHotspot"
    case syncContact = "SyncContacs"
    case getFeeds = "getFeeds"
    case deletepic = "deletePic"
    case blockUser = "blockListAdd"
    case getBlockedList = "blockListing"
    case updateFeed = "editFeedRadius"
    case notificationslisting = "notificationslisting"
    case editNotifications = "notificationsEdit"
    case getProfile = "getProfile"
    case getNotifications = "getNotification"
    case chatHistory = "chatHistory"
    case messageListing = "messageListing"
    case deleteChat = "deleteMessage"
    case updateToken  = "updateToken"
    case clear = "clearNotifications"
    case changePassword = "changePassword"
    case versioning = "appVersion"
    case verifyOtp = "verifyOtp"
    case deleteUser = "deleteUser"
    case verifyReferralCode = "signupWithReferralCode"
    case getRewardDetail = "getRewardDetail"
    case redeemRequest = "redeemRequest"
}

enum Keys : String
{
    //signup
    case fbId = "facebookId"
    case name = "name"
    case email = "email"
    case password = "password"
    case fcm_id = "fcm_id"
    case phone = "contact"
    case countryCode = "phone_code"
    case profilePic = "image"
    case fromFb = "fromFacebook"
    case userId = "userId"
    case notifications = "notifications"

    case bio = "bio"
    case deviceType = "deviceType"
    case deviceId = "deviceId"
    case lat = "latitude"
    case long = "longitude"
    case createdBy = "createdBy"
    case registrationDate = "registrationDate"
    case description = "description"
    case radius = "radius"
    case tags = "tags"
    case search = "search"
    case hotspotId = "hotspotId"
    case startDate = "startDate"
    case endDate = "endDate"
    case eventId = "eventId"
    case area = "area"
    case imageId = "imageId"
    case isLike = "like"
    case contacts = "contacts"
    case body = "body"
    case customerId = "customerId"
    case isBlock = "block"
    case feedStatus = "feed"
    case blockContact = "blockContact"
    case profileId = "profileId"
    case skip = "skip"
    case limit = "limit"
    case sender = "sender"
    case receiver = "receiver"
    case otherUser = "otherUser"
    case code = "code"
    case referralCode = "referralCode"
    case old = "oldPassword"
    case new = "newPassword"
    case timeZone = "timeZone"
    case favourite = "favourite"
    
}

enum Validate : String {
    
    case none
    case success = "200"
    case failure = "Failure"
    case invalidAccessToken = "401"
    case blockUser = "403"
    
    func map(response message : String?) -> String? {
        switch self {
        case .success:
            return message
        case .failure :
            return message
        case .invalidAccessToken :
            return message
        case .blockUser :
            return message
        default:
            return nil
        }
    }
}


enum Response {
    case success(AnyObject?)
    case failure(String? )
}

typealias OptionalDictionary = [String : Any]?


struct Parameters {
    
    static let signUp : [Keys]  = [ .name , .email , .password , .phone , .profilePic , .fromFb , .fbId , .bio , .deviceType , .deviceId , .code , .timeZone]
    static let facebookIdCheck : [Keys] = [.fbId , .deviceId , .timeZone]
    static let login : [Keys] = [.phone , .code , .deviceType , .deviceId , .timeZone, .code]
    static let forgetPassword : [Keys] = [.email]
    static let editProfile : [Keys] = [.userId , .name , .password , .phone , .radius , .bio]
    static let createHotspot : [Keys] = [.name , .lat , .long ,   .description , .createdBy , .tags , .area]
    static let hotspotMapView : [Keys] = [.lat , .long , .radius , .description , .userId , .search , .skip , .limit]
    static let happeningEvent : [Keys] = [.userId,.skip , .limit, .radius, .lat, .long]
    static let promoteList : [Keys] = [.lat, .long, .userId, .search, .skip, .limit , .radius]
    static let hotspotDetails : [Keys] = [.hotspotId, .userId]
    static let createEvent : [Keys] = [.name  , .description , .startDate , .endDate , .createdBy , .hotspotId , .tags]
    static let logout : [Keys] = []
    static let checkin : [Keys] = [.userId , .hotspotId]
    static let getEvents : [Keys] = [.hotspotId , .userId]
    static let addImages : [Keys] = [.createdBy , .hotspotId , .eventId]
    static let checkOut : [Keys] = [.userId , .hotspotId]
    static let getImages : [Keys] = [.hotspotId]
    static let reportImage : [Keys] = [.imageId , .userId]
    static let likeImage : [Keys] = [.userId , .imageId , .isLike]
    static let favUnfav : [Keys] = [.hotspotId , .userId , .favourite]
    static let syncContact : [Keys] = [.userId , .contacts]
    static let getFeeds : [Keys] = [.customerId]
    static let deletePic : [Keys] = [.imageId , .hotspotId]
    static let blockUser : [Keys] = [.userId , .isBlock , .blockContact]
    static let getBlockedList : [Keys] = [.userId]
    static let feedUpdate : [Keys] = [.userId , .feedStatus , .radius]
    static let deleteUser : [Keys] = [.userId]
    static let notificationslisting : [Keys] = [.userId]
    static let editNotifications : [Keys] = [.notifications,.userId]

    //static let editNotifications : [Keys] = [.body]
    static let getProfile : [Keys] = [.userId , .profileId]
    static let getNotifications : [Keys] = [.userId , .skip , .limit]
    static let chatHistory : [Keys] = [.userId , .otherUser , .skip , .limit]
    static let messageListing : [Keys] = [.userId , .skip , .limit]
    static let deleteChat : [Keys] = [.userId , .otherUser]
    static let updateToken : [Keys] = [.userId , .deviceId]
    static let clear : [Keys] = [.userId]
    static let changePassword : [Keys] = [.userId , .old , .new]
    static let versioning : [Keys] = []
    static let getFavourite: [Keys] = [.userId]
    static let verifyOTP : [Keys] = [.code]
    static let verifyReferralCode : [Keys] = [.referralCode]
    static let getRewardDetail : [Keys] = []
    static let redeemRequest : [Keys] = []
}


