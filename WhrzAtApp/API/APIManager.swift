//
//  APIManager.swift
//  BusinessDirectory
//
//  Created by Aseem 13 on 04/01/17.
//  Copyright © 2017 . All rights reserved.
//


import Foundation
import SwiftyJSON
import NVActivityIndicatorView
import ObjectMapper
import Alamofire
import EZSwiftExtensions

class APIManager : NSObject, NVActivityIndicatorViewable{
    
    var tokenExpiredFlag = 0
    typealias Completion = (Response) -> ()
    static let shared = APIManager()
    private lazy var httpClient : HTTPClient = HTTPClient()
    func request(with api : Router , completion : @escaping Completion , isLoaderNeeded : Bool)  {
        if isLoaderNeeded {
            Utility.functions.loader()
        }
        if !Alamofire.NetworkReachabilityManager()!.isReachable {
            Utility.functions.removeLoader()
            return completion(Response.failure(AlertMessage.noInternet.rawValue))
            
        }
        httpClient.postRequest(withApi: api,success: {[weak self] (data) in
            Utility.functions.removeLoader()
            guard let response = data else {
                completion(Response.failure(.none))
                return
            }
            var mappedModel:AnyObject?
            switch api.route {
            case APIConstants.forgetPassword.rawValue , APIConstants.login.rawValue , APIConstants.signUp.rawValue , APIConstants.facebookIdCheck.rawValue , APIConstants.logout.rawValue , APIConstants.updateProfile.rawValue , APIConstants.clear.rawValue:
                mappedModel = Mapper<LoginModel>().map(JSONObject: data)! as LoginModel
            case APIConstants.hotspotMapView.rawValue:
                mappedModel = Mapper<MapModel>().map(JSONObject: data)! as MapModel
                case APIConstants.promoteEvent.rawValue:
                mappedModel = Mapper<MapModel>().map(JSONObject: data)! as MapModel
            case APIConstants.happeningEvent.rawValue:
                mappedModel = Mapper<HappeningEventsModel>().map(JSONObject: data)! as HappeningEventsModel
            case APIConstants.hotspotDetails.rawValue:
                mappedModel = Mapper<HotspotDetailModel>().map(JSONObject: data)! as HotspotDetailModel
            case APIConstants.getFavourites.rawValue:
                mappedModel = Mapper<FavouriteHotspotDataModel>().map(JSONObject: data)! as FavouriteHotspotDataModel
//            case APIConstants.checkin.rawValue:
//                mappedModel = Mapper<CheckInDetailModel>().map(JSONObject: data)! as CheckInDetailModel
            case APIConstants.getEvents.rawValue:
                mappedModel = Mapper<EventsModel>().map(JSONObject: data)! as EventsModel
            case APIConstants.getImages.rawValue:
                mappedModel = Mapper<HotspotImagesModel>().map(JSONObject: data)! as HotspotImagesModel
            case APIConstants.getFeeds.rawValue:
                mappedModel = Mapper<FeedModel>().map(JSONObject: data)! as FeedModel
            case APIConstants.getBlockedList.rawValue :
                mappedModel = Mapper<BlockListModel>().map(JSONObject: data)! as BlockListModel
            case APIConstants.editNotifications.rawValue:
                mappedModel = Mapper<NotificationModel>().map(JSONObject: data)! as NotificationModel
            case APIConstants.getProfile.rawValue:
                mappedModel = Mapper<ProfileModel>().map(JSONObject: data)! as ProfileModel
            case APIConstants.getNotifications.rawValue:
                mappedModel = Mapper<ProfileNotification>().map(JSONObject: data)! as ProfileNotification
            case APIConstants.chatHistory.rawValue:
                mappedModel = Mapper<ChatHistoryModel>().map(JSONObject: data)! as ChatHistoryModel
            case APIConstants.messageListing.rawValue:
                mappedModel = Mapper<MessageListingModel>().map(JSONObject : data)! as MessageListingModel
            case APIConstants.versioning.rawValue:
                mappedModel = Mapper<VersioningModel>().map(JSONObject : data)! as VersioningModel
            case APIConstants.getRewardDetail.rawValue:
                mappedModel = Mapper<RewardModel>().map(JSONObject : data)! as RewardModel
            case APIConstants.redeemRequest.rawValue:
                mappedModel = Mapper<RewardModel>().map(JSONObject : data)! as RewardModel
            default :
                break
            }
            
            let json = JSON(response)
            print(json)
            if json[APIConstants.statusCode.rawValue].stringValue == Validate.invalidAccessToken.rawValue{
                self?.tokenExpired()
                print(Validate.invalidAccessToken.rawValue)
                return
            }
            
            let responseType = Validate(rawValue: json[APIConstants.statusCode.rawValue].stringValue) ?? .failure
            if responseType == Validate.success{
                completion(Response.success(mappedModel))
                return
            }
            else{
                completion(Response.failure(json[APIConstants.message.rawValue].stringValue))
            }
            
            }, failure: {[weak self] (message) in
                
                Utility.functions.removeLoader()
                completion(Response.failure(message))
                
        })
    }
    
    //Request With Image
    
    func request(withImage api : Router , image : UIImage? , completion: @escaping Completion , isLoaderNeeded : Bool ){
       
        if isLoaderNeeded {
            Utility.functions.loader()
        }
        
        httpClient.postRequestWithSingleImage(withApi: api, image: image, success: {[weak self] (data) in
            switch (api.route){
            case APIConstants.addImages.rawValue:
                break
            default :
                Utility.functions.removeLoader()
            }
            
            guard let response = data else {
                Utility.functions.removeLoader()
                completion(Response.failure(.none)
                )
                return
            }
            var mappedModal:AnyObject?
            switch(api.route){
                
            case APIConstants.login.rawValue , APIConstants.signUp.rawValue , APIConstants.forgetPassword.rawValue , APIConstants.updateProfile.rawValue
                :
                mappedModal = Mapper<LoginModel>().map(JSONObject: data)! as LoginModel
                
            case APIConstants.signUp.rawValue:
                mappedModal = Mapper<LoginModel>().map(JSONObject: data)! as LoginModel
                
                
            default: break
                //print("error")
            }
            let json = JSON(response)
            print("Json response : " , json)
            if json[APIConstants.statusCode.rawValue].stringValue == Validate.invalidAccessToken.rawValue{
                Utility.functions.removeLoader()
                self?.tokenExpired()
                return
            }
            let responseType = Validate(rawValue: json[APIConstants.statusCode.rawValue].stringValue) ?? .failure
            if responseType == Validate.success{
                completion(Response.success(mappedModal))
                return
            }
            else{
                Utility.functions.removeLoader()
                completion(Response.failure(json[APIConstants.message.rawValue].stringValue))
            }
            
        }) { [weak self](message) in
            Utility.functions.removeLoader()
            completion(Response.failure(message))
        }
    }
    
    
    func requestWithDoc(withDoc api : Router , documentUrl : URL? , completion: @escaping Completion , isLoaderNeeded : Bool ){
        if isLoaderNeeded {
            Utility.functions.loader()
        }
        httpClient.postRequestWithDocument(withApi: api, documentUrl: documentUrl, success: {[weak self] (data) in
            Utility.functions.removeLoader()
            
            guard let response = data else {
                completion(Response.failure(.none))
                return
            }
            var mappedModal:AnyObject?
            let json = JSON(response)
            //print("Json response : " , json)
            if json[APIConstants.statusCode.rawValue].stringValue == Validate.invalidAccessToken.rawValue{
                self?.tokenExpired()
                return
            }
            let responseType = Validate(rawValue: json[APIConstants.statusCode.rawValue].stringValue) ?? .failure
            if responseType == Validate.success{
                completion(Response.success(mappedModal))
                return
            }
            else{  completion(Response.failure(json[APIConstants.message.rawValue].stringValue))
                
            }
            
            
            
        }) { [weak self](message) in
            Utility.functions.removeLoader()
            
            
            completion(Response.failure(message))
        }
    }
    
    func tokenExpired(){
        AlertsClass.shared.showAlertController(withTitle: TitleType.alert.rawValue, message: AlertMessage.sessionExpired.rawValue, buttonTitles: [TitleType.login.rawValue]) { [weak self](tag) in
            let type = tag as AlertTag
            switch type{
            case .yes:
                UserSingleton.shared.loggedInUser = nil
                let initialNavVC = StoryboardScene.Login.initialViewController()
                let VC = StoryboardScene.Login.instantiateLoginSignUpVC()
                initialNavVC.viewControllers = [VC]
                UIView.transition(with: UIApplication.shared.keyWindow!, duration: 0.5, options: .transitionFlipFromLeft , animations: { () -> Void in
                    UIApplication.shared.keyWindow?.rootViewController = initialNavVC
                }) { (completed) -> Void in
                }
            //Alerts.shared.show(alert: .error, message: "Session Expired", type: .info)
            default:
                break
            }
            
        }
    }
    
}

extension UIViewController : NVActivityIndicatorViewable{
    
}
