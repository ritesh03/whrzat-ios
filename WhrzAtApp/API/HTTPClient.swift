//
//  HTTPClient.swift
//  BusinessDirectory
//
//  Created by Aseem 13 on 04/01/17.
//  Copyright © 2017 . All rights reserved.
//

import Foundation
import Alamofire

typealias HttpClientSuccess = (Any?) -> ()
typealias HttpClientFailure = (String) -> ()

class HTTPClient {
    
    func JSONObjectWithData(data: NSData) -> Any? {
        do { return try JSONSerialization.jsonObject(with: data as Data, options: []) }
        catch { return .none }
    }
    
    func postRequest(withApi api : Router  , success : @escaping HttpClientSuccess , failure : @escaping HttpClientFailure ){
        
        var params : [String : Any]?
        let fullPath = api.baseURL + api.route
        let method = api.method
        let header : [String : String]?
        switch api.route
        {
        default:
            params = api.parameters
        }
       
        
        switch api.route{
        case APIConstants.hotspotMapView.rawValue , APIConstants.hotspotDetails.rawValue , APIConstants.logout.rawValue , APIConstants.updateProfile.rawValue , APIConstants.createEvent.rawValue , APIConstants.getEvents.rawValue , APIConstants.checkin.rawValue , APIConstants.checkout.rawValue , APIConstants.getImages.rawValue , APIConstants.reportImage.rawValue , APIConstants.likeImage.rawValue , APIConstants.syncContact.rawValue , APIConstants.getFeeds.rawValue , APIConstants.blockUser.rawValue , APIConstants.getBlockedList.rawValue , APIConstants.deletepic.rawValue , APIConstants.updateFeed.rawValue , APIConstants.notificationslisting.rawValue , APIConstants.editNotifications.rawValue , APIConstants.getProfile.rawValue , APIConstants.getNotifications.rawValue , APIConstants.chatHistory.rawValue , APIConstants.messageListing.rawValue , APIConstants.deleteChat.rawValue , APIConstants.updateToken.rawValue , APIConstants.clear.rawValue , APIConstants.changePassword.rawValue,APIConstants.happeningEvent.rawValue,APIConstants.promoteEvent.rawValue, APIConstants.getFavourites.rawValue,APIConstants.favUnfav.rawValue,APIConstants.deleteUser.rawValue,APIConstants.verifyOtp.rawValue,APIConstants.verifyReferralCode.rawValue,APIConstants.getRewardDetail.rawValue,APIConstants.redeemRequest.rawValue :
            header = getHeader()
            
//        case APIConstants.verifyOtp.rawValue:
//            header = loginSignUpHeader()
            
        default:
            header = nil
            
        }
        print(fullPath)
        print(params ?? "")
        print(method)
    
        Alamofire.request(fullPath, method: method, parameters: params, encoding: params!.isEmpty ? URLEncoding.default : JSONEncoding.default, headers: header).responseJSON { (response) in
            
            switch response.result {
            case .success(let data):
                success(data)
            case .failure(let error):
                failure(error.localizedDescription)
            }
        }
        
    }
    
    
    func postRequestWithSingleImage(withApi api : Router,image : UIImage?, success : @escaping HttpClientSuccess , failure : @escaping HttpClientFailure){
        
        guard let params = api.parameters else {failure("empty"); return}
        let fullPath = api.baseURL + api.route
        let header : [String : String]?
        
        switch api.route{
        case APIConstants.createHotspot.rawValue , APIConstants.addImages.rawValue , APIConstants.updateProfile.rawValue:
            header = getHeader()
        default:
            header = nil
        }
        
        print(fullPath)
        print(params)
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            if let img = image {
             //   guard let imgFinal = img.resize(with: 800) else {return}
                let imageData = img.jpegData(compressionQuality: 0.3)
                multipartFormData.append(imageData!, withName: "image", fileName: "image.jpeg", mimeType: "image/jpeg")
            }
            
            for (key, value) in params {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
            
        }, to: fullPath , headers : header) { (encodingResult) in
            switch encodingResult {
            case .success(let upload,_,_):
                
                upload.responseJSON { response in
                    switch response.result {
                    case .success(let data):
                        success(data)
                    case .failure(let error):
                        failure(error.localizedDescription)
                    }
                }
            case .failure(let encodingError):
                print(encodingError)
            }
        }
    }
    
    func postRequestWithDocument(withApi api : Router,documentUrl : URL?, success : @escaping HttpClientSuccess , failure : @escaping HttpClientFailure){
        
        guard let params = api.parameters else {failure("empty"); return}
        let fullPath = api.baseURL + api.route
        print(fullPath)
        print(params)
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            if let url = documentUrl {
                guard let data = try? Data(contentsOf: url) else {
                    return
                }
                multipartFormData.append(data, withName: "file", fileName: "Notes.pdf", mimeType: "application/pdf")
            }
            
            for (key, value) in params {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue
                    )!, withName: key)
            }
            
        }, to: fullPath) { (encodingResult) in
            switch encodingResult {
            case .success(let upload,_,_):
                
                upload.responseJSON { response in
                    switch response.result {
                    case .success(let data):
                        success(data)
                    case .failure(let error):
                        failure(error.localizedDescription)
                    }
                }
            case .failure(let encodingError):
                print(encodingError)
            }
        }
    }
    
    //MARK:- Get Header
    func getHeader(contentType : Bool = false) -> [String : String]? {
        let header  = [ "authorization" : "bearer " + getAccessToken(), "Response Content Type" : "application/json"]
        return header
    }
    
//    func loginSignUpHeader() ->  [String : String]?{
//         let token =  UserDefaults.standard.string(forKey: "authToken")
//         let header  = [ "authorization" : "bearer " + /token, "Response Content Type" : "application/json"]
//              print(header)
//              return header
//    }
    
    //MARK:- GetAccessToken
    func getAccessToken() -> String{
        guard let user = UserSingleton.shared.loggedInUser else{return String()}
        guard let accessToken = user.accessToken else{return String()}
        return accessToken
    }
    
  }


//MARK::- UIIMAGE EXTENSION
extension UIImage {
    func resize(with width: CGFloat) -> UIImage? {
        var imageView = UIImageView(frame: CGRect(origin: .zero, size: CGSize(width: width, height: CGFloat(ceil(width/self.size.width * self.size.height)))))
        imageView.contentMode = .scaleAspectFit
        imageView.image = self
        UIGraphicsBeginImageContextWithOptions(imageView.bounds.size, false, scale)
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        imageView.layer.render(in: context)
        guard let result = UIGraphicsGetImageFromCurrentImageContext() else { return nil }
        UIGraphicsEndImageContext()
        return result
    }
}


