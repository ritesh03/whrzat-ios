//
//  LocationManager.swift
//  BusinessDirectory
//
//  Created by Aseem 13 on 18/01/17.
//  Copyright © 2017 Taran. All rights reserved.
//

import UIKit
import CoreLocation
import EZSwiftExtensions
import Contacts


class LocationManager: NSObject,CLLocationManagerDelegate {
    
    var locationManager : CLLocationManager?
    var currentLoc : CLLocation?
    var latitude = CLLocationDegrees()
    var longitude = CLLocationDegrees()
    var address : String?
    var name : String?
    var valBool = false
    
    override init() {
        super.init()
        locationInitializer()
        updateLocation()
    }
    
    static let shared = LocationManager()
    
    func updateUserLocation(){
        valBool = false
        locationInitializer()
        updateLocation()
    }

    func updateHotspotLocation(){
        valBool = true
        locationInitializer()
        updateLocation()
    }

    
    func updateLocation(){
        locationManager?.delegate = self
        locationManager?.startUpdatingLocation()
    }
    
    func locationInitializer(){
        locationManager = CLLocationManager()
        locationManager?.delegate = self
        locationManager?.desiredAccuracy = kCLLocationAccuracyBest
        //locationManager?.allowsBackgroundLocationUpdates = true
        locationManager?.distanceFilter = 5
       // locationManager?.pausesLocationUpdatesAutomatically = false
        
        locationManager?.requestWhenInUseAuthorization()
        
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .authorizedWhenInUse,.authorizedAlways:
         locationManager?.startUpdatingLocation()
        case .notDetermined:
            locationManager?.requestWhenInUseAuthorization()
        case .restricted,.denied:
         //  settingsAlert()
            break
        }
    }
    
    func settingsAlert(){
        guard let vc = ez.topMostVC else {return}
        if vc is ExploreMapVC{
            return
        }
        
        AlertsClass.shared.showAlertController(withTitle: TitleType.permissionRequired.rawValue, message: TitleType.locationSetting.rawValue, buttonTitles: [TitleType.setting.rawValue,TitleType.Cancel.rawValue]) { (value) in
            let type = value as AlertTag
            switch type {
            case .yes:
                UIApplication.shared.openURL(NSURL(string:UIApplication.openSettingsURLString)! as URL)
            default:
//                if CLLocationManager.authorizationStatus() == .denied{
//                    self.settingsAlert()}
                break
            }
        }
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        locationManager?.startUpdatingLocation()
        currentLoc = locations.last
      //  Alerts.shared.show(alert: .error, message: String(currentLoc?.coordinate.latitude ?? 0.0), type: .info)
        if let lat = currentLoc?.coordinate.latitude ,let lng = currentLoc?.coordinate.longitude {
            if lat == 0.0 && lng == 0.0{
                latitude = CLLocationDegrees(/UserSingleton.shared.lat) ?? 0.0
                longitude = CLLocationDegrees(/UserSingleton.shared.long) ?? 0.0
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "LocationReceived"), object: nil, userInfo: nil)
            }else{
                latitude = lat
                longitude = lng
                UserSingleton.shared.lat = lat.description
                UserSingleton.shared.long = lng.description
                Utility.functions.calculateAddress(lat: lat, long: lng, responseBlock: { [weak self] (coordinates, address,name,_,_,_) in
                    self?.name = name
                    self?.address = address
                    UserSingleton.shared.location = address
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: TitleType.locationSetting.rawValue), object: nil)
                    if !(self?.valBool ?? false ){
                        self?.locationManager?.stopUpdatingLocation()
                        self?.locationManager?.delegate = nil}
                })
//                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "LocationReceived"), object: nil, userInfo: nil)
            }
        }
    }
    
   
}
