
import UIKit
import SwiftyJSON
import ObjectMapper

class UserSingleton: NSObject {
    
    class var shared: UserSingleton {
        
        struct Static {
            static let instance: UserSingleton = UserSingleton()
        }
        return Static.instance
    }
    
    override init(){
        super.init()
    }
    
    deinit {
    }
//    var checkNotification: Int?
//    var notificationData : JSON?
    var isPush = false
    var isLikeNotification = false
    var isHotspotNotification = false
    var id : String?
    
    enum UDKeys: String{
        
        case WhrzAtUser = "WhrzAtUser"
        case WhrzAtUserLat = "WhrzAtUserLat"
        case WhrzAtUserLong = "WhrzAtUserLong"
        case WhrzAtUserLocation = "WhrzAtUserLocation"
        case notificationCount = "NotificationCount"

        case WhrzAtToken  = "WhrzAtToken"
        case contact = "WhrzAtContact"
        case code = "WhrzAtCode"
        
        func save(_ value: Any) {
            
            switch self{
                
            default:
                UserDefaults.standard.set(value, forKey: self.rawValue)
                UserDefaults.standard.synchronize()
            }
        }
        
        func fetch() -> Any? {
            
            switch self{
                default:
                guard let value = UserDefaults.standard.value(forKey: self.rawValue) else { return nil}
                return value
            }
        }
        
        func remove() {
            UserDefaults.standard.removeObject(forKey: self.rawValue)
        }
        
    }
    
    var loggedInUser : RegisterUser? {
        get{
            guard let data = UDKeys.WhrzAtUser.fetch() else {
                //let mappedModel = Mapper<RegisterUser>().map(JSON: [:] as! [String : Any])
                //return mappedModel
                return nil
            }
            let mappedModel = Mapper<RegisterUser>().map(JSON: data as! [String : Any])
            return mappedModel
        }
        set{
            if let value = newValue {
                UDKeys.WhrzAtUser.save(value.toJSON())
            }else{
                UDKeys.WhrzAtUser.remove()
                
            }
        }
    }
    
    
    var lat : String?{
        get{
            guard let lat = UDKeys.WhrzAtUserLat.fetch() else {return "0.0"}
            return lat as? String ?? "0.0"
        }
        set{
            if let value = newValue{
                UDKeys.WhrzAtUserLat.save(value)
            }else{
                UDKeys.WhrzAtUserLat.remove()
            }
        }
    }
    var long : String?{
        get{
            guard let long = UDKeys.WhrzAtUserLong.fetch() else{return "0.0"}
            return long as? String ?? "0.0"
        }
        set{
            if let value = newValue{
                UDKeys.WhrzAtUserLong.save(value)
            }else{
                UDKeys.WhrzAtUserLong.remove()
            }
        }
    }
    
    var location : String?{
        get{
            guard let location = UDKeys.WhrzAtUserLocation.fetch() else{return ""}
            return location as? String ?? ""
        }
        set{
            if let value = newValue{
                UDKeys.WhrzAtUserLocation.save(value)
            }else{
                UDKeys.WhrzAtUserLocation.remove()
            }
        }
    }
    
    var notificationCount : String?{
        get{
            guard let location = UDKeys.notificationCount.fetch() else{return ""}
            return location as? String ?? ""
        }
        set{
            if let value = newValue{
                UDKeys.notificationCount.save(value)
            }else{
                UDKeys.notificationCount.remove()
            }
        }
    }
    
    
    var token : String?{
        get{
            guard let token = UDKeys.WhrzAtToken.fetch() else{return ""}
            return token as? String ?? ""
        }
        set{
            if let value = newValue{
                UDKeys.WhrzAtToken.save(value)
            }else{
                UDKeys.WhrzAtToken.remove()
            }
        }
    }

    
}

