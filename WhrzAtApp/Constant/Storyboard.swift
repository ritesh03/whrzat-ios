// Generated using SwiftGen, by O.Halligon — https://github.com/SwiftGen/SwiftGen

import Foundation
import UIKit

// swiftlint:disable file_length
// swiftlint:disable line_length
// swiftlint:disable type_body_length

protocol StoryboardSceneType {
  static var storyboardName: String { get }
}

extension StoryboardSceneType {
  static func storyboard() -> UIStoryboard {
    return UIStoryboard(name: self.storyboardName, bundle: Bundle(for: BundleToken.self))
  }

  static func initialViewController() -> UIViewController {
    guard let vc = storyboard().instantiateInitialViewController() else {
      fatalError("Failed to instantiate initialViewController for \(self.storyboardName)")
    }
    return vc
  }
}

extension StoryboardSceneType where Self: RawRepresentable, Self.RawValue == String {
  func viewController() -> UIViewController {
    return Self.storyboard().instantiateViewController(withIdentifier: self.rawValue)
  }
  static func viewController(identifier: Self) -> UIViewController {
    return identifier.viewController()
  }
}

protocol StoryboardSegueType: RawRepresentable { }

extension UIViewController {
  func perform<S: StoryboardSegueType>(segue: S, sender: Any? = nil) where S.RawValue == String {
    performSegue(withIdentifier: segue.rawValue, sender: sender)
  }
}

enum StoryboardScene {
  enum Home: String, StoryboardSceneType {
    static let storyboardName = "Home"

    static func initialViewController() -> UINavigationController {
      guard let vc = storyboard().instantiateInitialViewController() as? UINavigationController else {
        fatalError("Failed to instantiate initialViewController for \(self.storyboardName)")
      }
      return vc
    }

    case addEventVCScene = "AddEventVC"
    static func instantiateAddEventVC() -> WhrzAtApp.AddEventVC {
      guard let vc = StoryboardScene.Home.addEventVCScene.viewController() as? WhrzAtApp.AddEventVC
      else {
        fatalError("ViewController 'AddEventVC' is not of the expected class WhrzAtApp.AddEventVC.")
      }
      return vc
    }
    case homeVCScene = "HomeVC"
    static func instantiateHomeVC() -> WhrzAtApp.HomeVC {
      guard let vc = StoryboardScene.Home.homeVCScene.viewController() as? WhrzAtApp.HomeVC
      else {
        fatalError("ViewController 'AddHotspotVC' is not of the expected class WhrzAtApp.AddHotspotVC.")
      }
      return vc
    }

    case addHotspotVCScene = "AddHotspotVC"
    static func instantiateAddHotspotVC() -> WhrzAtApp.AddHotspotVC {
      guard let vc = StoryboardScene.Home.addHotspotVCScene.viewController() as? WhrzAtApp.AddHotspotVC
      else {
        fatalError("ViewController 'AddHotspotVC' is not of the expected class WhrzAtApp.AddHotspotVC.")
      }
      return vc
    }

    case addVCScene = "AddVC"
    static func instantiateAddVC() -> WhrzAtApp.AddVC {
      guard let vc = StoryboardScene.Home.addVCScene.viewController() as? WhrzAtApp.AddVC
      else {
        fatalError("ViewController 'AddVC' is not of the expected class WhrzAtApp.AddVC.")
      }
      return vc
    }

    case blockedContactVCScene = "BlockedContactVC"
    static func instantiateBlockedContactVC() -> WhrzAtApp.BlockedContactVC {
      guard let vc = StoryboardScene.Home.blockedContactVCScene.viewController() as? WhrzAtApp.BlockedContactVC
      else {
        fatalError("ViewController 'BlockedContactVC' is not of the expected class WhrzAtApp.BlockedContactVC.")
      }
      return vc
    }

    case changePasswordVCScene = "ChangePasswordVC"
    static func instantiateChangePasswordVC() -> WhrzAtApp.ChangePasswordVC {
      guard let vc = StoryboardScene.Home.changePasswordVCScene.viewController() as? WhrzAtApp.ChangePasswordVC
      else {
        fatalError("ViewController 'ChangePasswordVC' is not of the expected class WhrzAtApp.ChangePasswordVC.")
      }
      return vc
    }

    case rewardsVCScene = "RewardsVC"
    static func instantiateRewardsVC() -> WhrzAtApp.RewardsVC {
      guard let vc = StoryboardScene.Home.rewardsVCScene.viewController() as? WhrzAtApp.RewardsVC
      else {
        fatalError("ViewController 'RewardsVC' is not of the expected class WhrzAtApp.RewardsVC.")
      }
      return vc
    }
    
    case chatUserVCScene = "ChatUserVC"
    static func instantiateChatUserVC() -> WhrzAtApp.ChatUserVC {
      guard let vc = StoryboardScene.Home.chatUserVCScene.viewController() as? WhrzAtApp.ChatUserVC
      else {
        fatalError("ViewController 'ChatUserVC' is not of the expected class WhrzAtApp.ChatUserVC.")
      }
      return vc
    }

    case chatVCScene = "ChatVC"
    static func instantiateChatVC() -> WhrzAtApp.ChatVC {
      guard let vc = StoryboardScene.Home.chatVCScene.viewController() as? WhrzAtApp.ChatVC
      else {
        fatalError("ViewController 'ChatVC' is not of the expected class WhrzAtApp.ChatVC.")
      }
      return vc
    }

    case editProfileVCScene = "EditProfileVC"
    static func instantiateEditProfileVC() -> WhrzAtApp.EditProfileVC {
      guard let vc = StoryboardScene.Home.editProfileVCScene.viewController() as? WhrzAtApp.EditProfileVC
      else {
        fatalError("ViewController 'EditProfileVC' is not of the expected class WhrzAtApp.EditProfileVC.")
      }
      return vc
    }

    case eventListVCScene = "EventListVC"
    static func instantiateEventListVC() -> WhrzAtApp.EventListVC {
      guard let vc = StoryboardScene.Home.eventListVCScene.viewController() as? WhrzAtApp.EventListVC
      else {
        fatalError("ViewController 'EventListVC' is not of the expected class WhrzAtApp.EventListVC.")
      }
      return vc
    }

    case exploreListVCScene = "ExploreListVC"
    static func instantiateExploreListVC() -> WhrzAtApp.ExploreListVC {
      guard let vc = StoryboardScene.Home.exploreListVCScene.viewController() as? WhrzAtApp.ExploreListVC
      else {
        fatalError("ViewController 'ExploreListVC' is not of the expected class WhrzAtApp.ExploreListVC.")
      }
      return vc
    }

    case exploreMapVCScene = "ExploreMapVC"
    static func instantiateExploreMapVC() -> WhrzAtApp.ExploreMapVC {
      guard let vc = StoryboardScene.Home.exploreMapVCScene.viewController() as? WhrzAtApp.ExploreMapVC
      else {
        fatalError("ViewController 'ExploreMapVC' is not of the expected class WhrzAtApp.ExploreMapVC.")
      }
      return vc
    }

    case feedVCScene = "FeedVC"
    static func instantiateFeedVC() -> WhrzAtApp.FeedVC {
      guard let vc = StoryboardScene.Home.feedVCScene.viewController() as? WhrzAtApp.FeedVC
      else {
        fatalError("ViewController 'FeedVC' is not of the expected class WhrzAtApp.FeedVC.")
      }
      return vc
    }
    case detailsInfoVCScene = "DetailsInfoVC"
    static func instantiateDetailsInfoVC() -> WhrzAtApp.DetailsInfoVC {
      guard let vc = StoryboardScene.Home.detailsInfoVCScene.viewController() as? WhrzAtApp.DetailsInfoVC
      else {
        fatalError("ViewController 'HotspotDetailVC' is not of the expected class WhrzAtApp.HotspotDetailVC.")
      }
      return vc
    }

    case hotspotDetailVCScene = "HotspotDetailVC"
    static func instantiateHotspotDetailVC() -> WhrzAtApp.HotspotDetailVC {
      guard let vc = StoryboardScene.Home.hotspotDetailVCScene.viewController() as? WhrzAtApp.HotspotDetailVC
      else {
        fatalError("ViewController 'HotspotDetailVC' is not of the expected class WhrzAtApp.HotspotDetailVC.")
      }
      return vc
    }
    
    case promotedVCScene = "PromotedVC"
      static func instantiatePromotedVC() -> WhrzAtApp.PromotedVC {
        guard let vc = StoryboardScene.Home.promotedVCScene.viewController() as? WhrzAtApp.PromotedVC
        else {
          fatalError("ViewController 'HotspotDetailVC' is not of the expected class WhrzAtApp.HotspotDetailVC.")
        }
        return vc
      }

    case locationMapVCScene = "LocationMapVC"
    static func instantiateLocationMapVC() -> WhrzAtApp.LocationMapVC {
      guard let vc = StoryboardScene.Home.locationMapVCScene.viewController() as? WhrzAtApp.LocationMapVC
      else {
        fatalError("ViewController 'LocationMapVC' is not of the expected class WhrzAtApp.LocationMapVC.")
      }
      return vc
    }

    case notificationsVCScene = "NotificationsVC"
    static func instantiateNotificationsVC() -> WhrzAtApp.NotificationsVC {
      guard let vc = StoryboardScene.Home.notificationsVCScene.viewController() as? WhrzAtApp.NotificationsVC
      else {
        fatalError("ViewController 'NotificationsVC' is not of the expected class WhrzAtApp.NotificationsVC.")
      }
      return vc
    }

    case otherUserProfileVCScene = "OtherUserProfileVC"
    static func instantiateOtherUserProfileVC() -> WhrzAtApp.OtherUserProfileVC {
      guard let vc = StoryboardScene.Home.otherUserProfileVCScene.viewController() as? WhrzAtApp.OtherUserProfileVC
      else {
        fatalError("ViewController 'OtherUserProfileVC' is not of the expected class WhrzAtApp.OtherUserProfileVC.")
      }
      return vc
    }

    case profileVCScene = "ProfileVC"
    static func instantiateProfileVC() -> WhrzAtApp.ProfileVC {
      guard let vc = StoryboardScene.Home.profileVCScene.viewController() as? WhrzAtApp.ProfileVC
      else {
        fatalError("ViewController 'ProfileVC' is not of the expected class WhrzAtApp.ProfileVC.")
      }
      return vc
    }

    case settingsVCScene = "SettingsVC"
    static func instantiateSettingsVC() -> WhrzAtApp.SettingsVC {
      guard let vc = StoryboardScene.Home.settingsVCScene.viewController() as? WhrzAtApp.SettingsVC
      else {
        fatalError("ViewController 'SettingsVC' is not of the expected class WhrzAtApp.SettingsVC.")
      }
      return vc
    }

    case tabBarVCScene = "TabBarVC"
    static func instantiateTabBarVC() -> WhrzAtApp.TabBarVC {
      guard let vc = StoryboardScene.Home.tabBarVCScene.viewController() as? WhrzAtApp.TabBarVC
      else {
        fatalError("ViewController 'TabBarVC' is not of the expected class WhrzAtApp.TabBarVC.")
      }
      return vc
    }

    case webPageViewControllerScene = "WebPageViewController"
    static func instantiateWebPageViewController() -> WhrzAtApp.WebPageViewController {
      guard let vc = StoryboardScene.Home.webPageViewControllerScene.viewController() as? WhrzAtApp.WebPageViewController
      else {
        fatalError("ViewController 'WebPageViewController' is not of the expected class WhrzAtApp.WebPageViewController.")
      }
      return vc
    }
  }
  enum LaunchScreen: StoryboardSceneType {
    static let storyboardName = "LaunchScreen"
  }
  enum Login: String, StoryboardSceneType {
    static let storyboardName = "Login"

    static func initialViewController() -> UINavigationController {
      guard let vc = storyboard().instantiateInitialViewController() as? UINavigationController else {
        fatalError("Failed to instantiate initialViewController for \(self.storyboardName)")
      }
      return vc
    }
    
    case splashViewControllerScene = "SplashViewController"
     static func instantiateSplashViewController() -> WhrzAtApp.SplashViewController {
       guard let vc = StoryboardScene.Login.splashViewControllerScene.viewController() as? WhrzAtApp.SplashViewController
       else {
         fatalError("ViewController 'SplashViewController' is not of the expected class WhrzAtApp.SplashViewController.")
       }
       return vc
     }

     case tutorialVCViewControllerScene = "TutorialVCViewController"
     static func instantiateTutorialVCViewController() -> WhrzAtApp.TutorialVCViewController {
       guard let vc = StoryboardScene.Login.tutorialVCViewControllerScene.viewController() as? WhrzAtApp.TutorialVCViewController
       else {
         fatalError("ViewController 'TutorialVCViewController' is not of the expected class WhrzAtApp.TutorialVCViewController.")
       }
       return vc
     }
    case loginSignUpVCScene = "LoginSignUpVC"
    static func instantiateLoginSignUpVC() -> WhrzAtApp.LoginSignUpVC {
      guard let vc = StoryboardScene.Login.loginSignUpVCScene.viewController() as? WhrzAtApp.LoginSignUpVC
      else {
        fatalError("ViewController 'LoginSignUpVC' is not of the expected class WhrzAtApp.LoginSignUpVC.")
      }
      return vc
    }

    case loginVCScene = "LoginVC"
    static func instantiateLoginVC() -> WhrzAtApp.LoginVC {
      guard let vc = StoryboardScene.Login.loginVCScene.viewController() as? WhrzAtApp.LoginVC
      else {
        fatalError("ViewController 'LoginVC' is not of the expected class WhrzAtApp.LoginVC.")
      }
      return vc
    }

    case signUpSecondVCScene = "SignUpSecondVC"
    static func instantiateSignUpSecondVC() -> WhrzAtApp.SignUpSecondVC {
      guard let vc = StoryboardScene.Login.signUpSecondVCScene.viewController() as? WhrzAtApp.SignUpSecondVC
      else {
        fatalError("ViewController 'SignUpSecondVC' is not of the expected class WhrzAtApp.SignUpSecondVC.")
      }
      return vc
    }

    case forgetPasswordVCScene = "ForgetPasswordVC"
    static func instantiateForgetPasswordVC() -> WhrzAtApp.ForgetPasswordVC {
      guard let vc = StoryboardScene.Login.forgetPasswordVCScene.viewController() as? WhrzAtApp.ForgetPasswordVC
      else {
        fatalError("ViewController 'ForgetPasswordVC' is not of the expected class WhrzAtApp.ForgetPasswordVC.")
      }
      return vc
    }
    
    case otpVCScene = "OtpVC"
       static func instantiateOTPVC() -> WhrzAtApp.OtpVC {
         guard let vc = StoryboardScene.Login.otpVCScene.viewController() as? WhrzAtApp.OtpVC
         else {
           fatalError("ViewController 'OtpVC' is not of the expected class WhrzAtApp.OtpVC.")
         }
         return vc
       }
    
    case referralCodeVCScene = "ReferralCodeVC"
          static func instantiateReferralCodeVC() -> WhrzAtApp.ReferralCodeVC {
            guard let vc = StoryboardScene.Login.referralCodeVCScene.viewController() as? WhrzAtApp.ReferralCodeVC
            else {
              fatalError("ViewController 'ReferralCodeVC' is not of the expected class WhrzAtApp.ReferralCodeVC.")
            }
            return vc
          }
  }
  enum Main: StoryboardSceneType {
    static let storyboardName = "Main"
  }
}

enum StoryboardSegue {
}

private final class BundleToken {}
