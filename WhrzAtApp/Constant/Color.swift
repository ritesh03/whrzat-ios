//
//  Color.swift
//  cozo
//
//  Created by Sierra 4 on 09/05/17.
//  Copyright © 2017 monika. All rights reserved.
//

import Foundation
import UIKit


extension UIColor {
    
    static func overlay() -> UIColor {
        return UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
    }
    
    
    static func yelow() -> UIColor {
        return UIColor(red: 251/255, green: 203/255, blue: 103/255, alpha: 1.0)
    }
    static func blue() -> UIColor{
        return UIColor.init(hexString: "#5FABFF") ?? UIColor()
    }
    static func red() -> UIColor{
        return UIColor.init(hexString: "#EA5D4F") ?? UIColor()
    }
    static func green() -> UIColor {
        return UIColor(red: 138/255, green: 198/255, blue: 78/255, alpha: 1.0)
    }
    static func orange() -> UIColor {
        return UIColor.init(hexString: "#FB7200") ?? UIColor()
    }
    
    static func tab() -> UIColor {
        return UIColor.init(hexString: "#FF0000") ?? UIColor()
    }
    
    static func blackRang() -> UIColor{
       return UIColor.init(hexString: "#202020") ?? UIColor()

    }    

    
    func getRandomColor() -> UIColor{
        
        let randomRed:CGFloat = CGFloat(drand48())
        
        let randomGreen:CGFloat = CGFloat(drand48())
        
        let randomBlue:CGFloat = CGFloat(drand48())
        
        return UIColor(red: randomRed, green: randomGreen, blue: randomBlue, alpha: 1.0)
        
    }
}

enum CustomFont {
    static let OpenSansRegular = "OpenSans-Regular"
    static let OpenSansBold = "OpenSans-Bold"
    static let OpenSansSemiBold = "OpenSans-SemiBold"
    
    
}
