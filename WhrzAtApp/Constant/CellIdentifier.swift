

import Foundation


enum CellIdentifiers: String {
    
    
    //MARK::- TABLE VIEW CELL IDENTIFIERS

    case ExploreListCell = "ExploreListCell"
    case FeedTableCell = "FeedTableViewCell"
    case ProfileNotificationCell = "ProfileNotificationCell"
    case EventListCell = "EventListCell"
    case ChatCell = "ChatCell"
    case ChatSendCell = "ChatSendCell"
    case ChatReceiveCell = "ChatReceiveCell"
    case BlockedContactCell = "BlockedContactCell"
    case NotificationCell = "NotificationCell"
    case HotspotDetailCell = "HotspotDetailCell"
    case OtherProfileCell = "OtherProfileCell"
    case FavouritesTableViewCell = "FavouritesTableViewCell"
    
    //MARK::- COLLECTION VIEW CELL IDENTIFIERS
    

    case ExploreMapCell = "ExploreMapCell"
    case TutorialCollectionViewCell = "TutorialCollectionViewCell"
    
    //MARK::- HEADER
    case HeaderHotspot = "HeaderHotspot"

}
