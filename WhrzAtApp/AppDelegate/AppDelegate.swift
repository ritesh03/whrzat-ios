//
//  AppDelegate.swift
//  WhrzAtApp
//
//  Created by Sierra 4 on 30/08/17.
//  Copyright © 2017 com.example. All rights reserved.

import UIKit
import CoreData
import FBSDKCoreKit
import FBSDKLoginKit
import GoogleMaps
import GooglePlaces
import Firebase
import FirebaseCore
import FirebaseMessaging
import FirebaseInstanceID
import SwiftyJSON
import UserNotifications
import EZSwiftExtensions
import Fabric
import Crashlytics
import FBSDKCoreKit.FBSDKAppLinkUtility
import AppsFlyerLib
//import BranchSDK


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate, MessagingDelegate {
    
    var window: UIWindow?
    let gcmMessageIDKey = "gcm.message_id"
    var userInfo : JSON?
    //var isLaunch = false
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
      //  _ = LocationManager.shared.currentLoc
//        Branch.getInstance().initSession(launchOptions: launchOptions) { (params, error) in
//            print(params as? [String: AnyObject] ?? {})
//            let sessionParams = Branch.getInstance().getLatestReferringParams()
//            let installParams = Branch.getInstance().getFirstReferringParams()
//            guard let data = params as? [String: AnyObject] else { return }
//            guard let hotspotId = data["hotspotId"] as? String else { return }
//            self.navigateToHotspotDetail(id: hotspotId)
//        }
               
//
//            DispatchQueue.main.asyncAfter(deadline: .now() + 1)
//            {
//                if let data = params as? [String: AnyObject], let value = data["+clicked_branch_link"] as? Bool {
//                    if (value) {
//                        let hotspotId = data["hotspotId"] as? String
//
//                        self?.navigateToHotspotDetail(id: hotspotId)
//
//                                    }
//                }
//            }
          
       // }
        // Initialize Facebook SDK
        FBSDKCoreKit.ApplicationDelegate.shared.application(
            application,
            didFinishLaunchingWithOptions: launchOptions
        )
        //Set Facebook Ads tracking event
        Settings.shared.isAdvertiserTrackingEnabled = true
        
        AppLinkUtility.fetchDeferredAppLink({ (URL, error) -> Void in
            if error != nil {
                //Handle error
            }
            if URL != nil {

                application.open(URL!, options: [:], completionHandler: nil)
            }
        })
        
        //AppsFlyer
        setUpAppsFlyerSDK()
        
        //Firebase Analytics
        if #available(iOS 13.0, *) {
        } else {
            FirebaseApp.configure()
        }
        
        Fabric.with([Crashlytics.self])

        GMSServices.provideAPIKey("AIzaSyCrOyl8dQtk6Jf2swfSSYcxUzzJyWZ8F24")
        GMSPlacesClient.provideAPIKey("AIzaSyCrOyl8dQtk6Jf2swfSSYcxUzzJyWZ8F24")// fcm client account
        
       // UITabBar.appearance().tintColor = UIColor.tab()
       // UITabBar.appearance().barTintColor = .black
       UITabBar.appearance().tintColor = UIColor(named: "AppTextColor") // your color
        UITabBar.appearance().unselectedItemTintColor = UIColor(named: "UnSelectedColor")
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor(named: "UnSelectedColor")!,NSAttributedString.Key.font: UIFont(name:CustomFont.OpenSansSemiBold, size: 10)!], for: .normal)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor(named: "AppTextColor")!,NSAttributedString.Key.font: UIFont(name:CustomFont.OpenSansBold, size: 10)!], for: .selected)
        UITabBarItem.appearance()
        
        setupFireBase(application)
        var data = [String: Any]()
        data["uid"] = /UserSingleton.shared.loggedInUser?._id
       // SocketIOManager.sharedInstance.requestCount(data)
        // isLaunch = true
        return true
    }
    
    func application(_ application: UIApplication, continue userActivity: NSUserActivity,
                     restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
        var handled:Bool = false
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5)
        {
            handled = DynamicLinks.dynamicLinks()
                .handleUniversalLink(userActivity.webpageURL!) { dynamiclink, error in
                    if dynamiclink != nil {
                        let urlToString = "\(dynamiclink!.url!)"
                        let hotspotId = urlToString.split("=")
                        if UserSingleton.shared.loggedInUser?.accessToken != nil{
                            self.navigateToHotspotDetail(id: hotspotId.last!)
                        }
                    }
                    
                }
        }

      return handled
    }

    func application(_ application: UIApplication, open url: URL, sourceApplication: String?,
                     annotation: Any) -> Bool {
      if let dynamicLink = DynamicLinks.dynamicLinks().dynamicLink(fromCustomSchemeURL: url) {
          print(dynamicLink)
       
        return true
      }
      return false
    }
    
    func setUpAppsFlyerSDK() {
        AppsFlyerLib.shared().appsFlyerDevKey = AppKeys.AppsFlyer.DevKey
        AppsFlyerLib.shared().appleAppID = AppKeys.AppsFlyer.AppleAppId
        AppsFlyerLib.shared().delegate = self
        AppsFlyerLib.shared().isDebug = true
    }
    

//func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
//      // Handler for Universal Links
//    Branch.getInstance().continue(userActivity)
//    return true
//}


    
  

//    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
//          // Handler for Universal Links
//        Branch.getInstance().continue(userActivity)
//        return true
//    }

//    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
//      // Handler for Push Notifications
//      Branch.getInstance().handlePushNotification(userInfo)
//    }
    
    //--Facebook
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
       // Utility.functions.loader()
        // Branch.getInstance().application(app, open: url, options: options)
        
        if ApplicationDelegate.shared.application(app, open: url, options: options){return true}
       
        
        return application(app, open: url,
                           sourceApplication: options[UIApplication.OpenURLOptionsKey
                             .sourceApplication] as? String,
                           annotation: "")
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        Messaging.messaging().shouldEstablishDirectChannel = false
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5)
        {
//
//           // NotificationCenter.default.post(name: NSNotification.Name(rawValue: "LocationReceived"), object: nil, userInfo: nil)
//            
    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "fetchContact"), object: nil, userInfo: nil)
       }
        //LocationManager.shared.updateUserLocation()
      //  SocketIOManager.sharedInstance.establishConnection()
    }
    
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        //Starting the AppsFlyer iOS SDK
        AppsFlyerLib.shared().start()
        //Facebook app events
        AppEvents.shared.logEvent(.subscribe)
        // AppEvents.shared.logEvent(AppEvents.Name.submitApplication)
        
        SocketIOManager.sharedInstance.establishConnection() //need to uncomment
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.activeApp.rawValue), object: nil)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "chatAgain"), object: nil)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5)
        {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "LocationReceived"), object: nil, userInfo: nil)
        }
        
       
        if application.applicationIconBadgeNumber > 0 {
            application.applicationIconBadgeNumber = 0
        }
        connectToFcm()
        //checkForContact()
    }

    func applicationWillTerminate(_ application: UIApplication) {
        SocketIOManager.sharedInstance.closeConnection()
    }
    
    func navigateToHotspotDetail(id:String?) {
        let vc = StoryboardScene.Home.instantiateHotspotDetailVC()
        vc.hotspotId = id
        vc.linkTab = true
        let centerViewController = UINavigationController(rootViewController: vc)
        if UIApplication.shared.keyWindow == nil {
            self.window = UIWindow(frame: UIScreen.main.bounds)
            //self.window?.layer.add(transition, forKey: kCATransition)
            self.window?.rootViewController = centerViewController
            self.window?.makeKeyAndVisible()
        }
        else {
//            UIApplication.shared.keyWindow?.layer.add(transition, forKey: kCATransition)
            UIApplication.shared.keyWindow?.rootViewController = centerViewController
            UIApplication.shared.keyWindow?.makeKeyAndVisible()
        }
    }

}

//MARK::- EXTENSION FirebasE SetuP

extension AppDelegate{
    func setupFireBase(_ application: UIApplication) {
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            Messaging.messaging().delegate = self
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        
        FirebaseApp.configure()
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.tokenRefreshNotification),
                                               name: NSNotification.Name(rawValue: "firInstanceIDTokenRefresh"),
                                               object: nil)
    }
    func test()  {
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instange ID: \(error)")
            } else if let result = result {
                
                UserSingleton.shared.token = result.token
                self.updateToken(token : result.token)
                print("Remote instance ID token: \(result.token)")
            }
        }
        
        connectToFcm()
    }
    @objc func tokenRefreshNotification(_ notification: Notification) {
        
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instange ID: \(error)")
            } else if let result = result {
                
                UserSingleton.shared.token = result.token
                self.updateToken(token : result.token)
                print("Remote instance ID token: \(result.token)")
            }
        }
        
        connectToFcm()
    }
    //MARK::- CONNECT TO FCM
    func connectToFcm() {
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                return
            } else if let result = result {
                print("Remote instance ID token: \(result.token)")
            }
        }
        
        Messaging.messaging().shouldEstablishDirectChannel = false
        if Messaging.messaging().isDirectChannelEstablished {
            print("Connected to FCM.")
            InstanceID.instanceID().instanceID { (result, error) in
                       if let error = error {
                           return
                       } else if let result = result {
                        
                        UserSingleton.shared.token = result.token
                        self.updateToken(token : result.token)
                           print("Remote instance ID token: \(result.token)")
                       }
                   }
           }
            
    }
    // [END connect_to_fcm]
    
    
    public func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String){
    }
    
    public func application(received remoteMessage: MessagingRemoteMessage) {
        print(remoteMessage.appData)
    }
    
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
       // Branch.getInstance().handlePushNotification(userInfo)

        print(userInfo)
    }
//
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                    fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {

        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        print(userInfo)
        var data = [String: Any]()
        data["uid"] = /UserSingleton.shared.loggedInUser?._id
        SocketIOManager.sharedInstance.requestCount(data)
        if UIApplication.shared.applicationState == .active  {
            if #available(iOS 10.0, *)  {
                handleNotifcation(userInfo: JSON(userInfo))
            } else {
                showNotification(userInfo: JSON(userInfo))
            }
        } else if UIApplication.shared.applicationState == .inactive {
            guard let _ =  UserSingleton.shared.loggedInUser else { return }
            let temp = JSON(userInfo)
            let type = temp["gcm.notification.type"].stringValue
            let dict = temp["Id"].stringValue
            if type == "0"{
                UserSingleton.shared.isPush = true
                if ez.topMostVC != nil{
                    if ez.topMostVC  is ExploreMapVC || ez.topMostVC  is ExploreListVC || ez.topMostVC  is FeedVC || ez.topMostVC  is ChatVC || ez.topMostVC  is ProfileVC{
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.tabPage.rawValue), object: nil)
                    }else{
                        //UserSingleton.shared.isPush = true
                        ez.topMostVC?.popToRootVC()
                        let when = DispatchTime.now() + 1 // change 2 to desired number of seconds
                        DispatchQueue.main.asyncAfter(deadline: when) {
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.tabPage.rawValue), object: nil)
                        }
                        
                    }
                }
            }else if type == "2"{
                UserSingleton.shared.isLikeNotification = true
                if ez.topMostVC != nil{
                    if ez.topMostVC  is ExploreMapVC || ez.topMostVC  is ExploreListVC || ez.topMostVC  is FeedVC || ez.topMostVC  is ChatVC || ez.topMostVC  is ProfileVC{
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.profileNotification.rawValue), object: nil)
                    }else{
                        //UserSingleton.shared.isLikeNotification = true
                        ez.topMostVC? .popToRootVC()
                        let when = DispatchTime.now() + 1 // change 2 to desired number of seconds
                        DispatchQueue.main.asyncAfter(deadline: when) {
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.profileNotification.rawValue), object: nil)
                        }
                        
                    }
                }
            }else{
                UserSingleton.shared.id = /dict
                if ez.topMostVC != nil{
                    let when = DispatchTime.now() + 1 // change 2 to desired number of seconds
                    DispatchQueue.main.asyncAfter(deadline: when) {
                        let vc = StoryboardScene.Home.instantiateHotspotDetailVC()
                        vc.hotspotId = /dict
                        ez.topMostVC?.pushVC(vc)
                    }
                }else{
                    UserSingleton.shared.isHotspotNotification = true
                }
                
            }
        }
        completionHandler(UIBackgroundFetchResult.newData)
    }
    
    @available(iOS 10.0, *)
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        let info = JSON(notification.request.content.userInfo)
        let topVC = ez.topMostVC
        if topVC is ChatUserVC{
            if (info["gcm.notification.type"].stringValue == "0")// && info["Id"].stringValue == (topVC as? ChatUserVC)?.id)
            {
                //                completionHandler([UNNotificationPresentationOptions.sound])}
                completionHandler([])}
                
            else{
                completionHandler([UNNotificationPresentationOptions.alert,
                                   UNNotificationPresentationOptions.sound,
                                   UNNotificationPresentationOptions.badge])
            }
        }
        else {
            
            completionHandler([UNNotificationPresentationOptions.alert,
                               UNNotificationPresentationOptions.sound,
                               UNNotificationPresentationOptions.badge])
        }
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        let chars = (deviceToken as NSData).bytes.bindMemory(to: CChar.self, capacity: deviceToken.count)
        var token = ""
        for i in 0..<deviceToken.count {
            token += String(format: "%02.2hhx", arguments: [chars[i]])
        }
        print("Device Token = ", token)
      
        Messaging.messaging()
        .setAPNSToken(deviceToken, type: MessagingAPNSTokenType.prod)
        test()
        //InstanceID.instanceID().setAPNSToken(deviceToken as Data, type: .sandbox )
    }
    
}

//MARK: AppsFlyerTrackerDelegate
extension AppDelegate: AppsFlyerLibDelegate{
    // Handle Organic/Non-organic installation
    func onConversionDataSuccess(_ installData: [AnyHashable: Any]) {
        print("onConversionDataSuccess data:")
        for (key, value) in installData {
            print(key, ":", value)
        }
        if let status = installData["af_status"] as? String {
            if (status == "Non-organic") {
                if let sourceID = installData["media_source"],
                   let campaign = installData["campaign"] {
                    print("This is a Non-Organic install. Media source: \(sourceID)  Campaign: \(campaign)")
                }
            } else {
                print("This is an organic install.")
            }
            if let is_first_launch = installData["is_first_launch"] as? Bool,
               is_first_launch {
                print("First Launch")
            } else {
                print("Not First Launch")
            }
        }
    }
    func onConversionDataFail(_ error: Error) {
        print(error)
    }
    //Handle Deep Link
    func onAppOpenAttribution(_ attributionData: [AnyHashable : Any]) {
        //Handle Deep Link Data
        print("onAppOpenAttribution data:")
        for (key, value) in attributionData {
            print(key, ":",value)
        }
    }
    func onAppOpenAttributionFailure(_ error: Error) {
        print(error)
    }
}
