
import SwiftyJSON
import EZSwiftExtensions

extension AppDelegate{
    
    func showNotification(userInfo : JSON) {
        let topVC = ez.topMostVC
        if topVC is ChatUserVC{
            if (userInfo["gcm.notification.type"].stringValue == "0") {
                return}
        }
        let obj = ISMessages.cardAlert(withTitle: TitleType.WhrzAt.rawValue, message: userInfo["body"].stringValue, iconImage: #imageLiteral(resourceName: "logoo.png"), duration: 2.0, hideOnSwipe: true, hideOnTap: false, alertType: .custom, alertPosition: .top)
        let tapToOpen = UITapGestureRecognizer(target: self, action:  #selector(self.tappedOnNotification(_:)))
        obj?.alertViewBackgroundColor = UIColor.blackRang()
        obj?.view.addGestureRecognizer(tapToOpen)
        obj?.show(nil, didHide: nil)
        self.userInfo = userInfo
        ez.dispatchDelay(2.0, closure: {
            ISMessages.hideAlert(animated: true)
        })
    }
    
    //MARK::- Token Refresh Api
    func updateToken(token : String?){
        guard let _ = UserSingleton.shared.loggedInUser else {return}
        if UserSingleton.shared.token == "" {return}
        APIManager.shared.request(with: HomeEndpoint.updateToken, completion : {  (response) in
            switch response{
            case .success( _):
                break
            case .failure(_):
                break
            }
        } , isLoaderNeeded : false)
    }
    
    //MARK: - Tap on IOS9 notification action
   @objc func tappedOnNotification(_ sender: UIGestureRecognizer) {
        ISMessages.hideAlert(animated: true)
        guard let data = userInfo else { return }
        handleNotifcation(userInfo: data)
    }
    
    //MARK::- HANDLE PUSH
    func handleNotifcation(userInfo : JSON) {
        guard let _ =  UserSingleton.shared.loggedInUser else { return }
        guard let tvc = ez.topMostVC else { return }
        print(userInfo)
        
        let type = userInfo["gcm.notification.type"].stringValue
        let dict = userInfo["Id"].stringValue
        switch type{
        case "0": //New Chat Message
            if tvc is ChatUserVC{
                break
            }else if tvc is ExploreMapVC || tvc is ExploreListVC || tvc is ProfileVC || tvc is FeedVC{
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.tabPage.rawValue), object: nil)
                break
            }else if tvc is ChatVC{
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.refreshChat.rawValue), object: nil)
                break
            }
            let vc = StoryboardScene.Home.instantiateChatUserVC()
            vc.id = /dict
            ez.topMostVC?.pushVC(vc)
            
        case "2": //Photo Like Notification
            if tvc is ExploreMapVC || tvc is ExploreListVC || tvc is FeedVC || tvc is ChatVC || tvc is ProfileVC{
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.profileNotification.rawValue), object: nil)
                break
            }else{
                // UserSingleton.shared.isLikeNotification = true
                tvc.popToRootVC()
                //ez.runThisAfterDelay(seconds: 1, after: {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.profileNotification.rawValue), object: nil)
                    
               // })
            }
            
        default :
            
            if tvc is ExploreMapVC || tvc is ExploreListVC || tvc is FeedVC || tvc is ChatVC || tvc is ProfileVC{
                UserSingleton.shared.id = /dict
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.hotspotNotification.rawValue), object: nil)
                return
            }else{
                // UserSingleton.shared.isHotspotNotification = true
                UserSingleton.shared.id = /dict
                tvc.popToRootVC()
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.hotspotNotification.rawValue), object: nil)
//                ez.runThisAfterDelay(seconds: 1, after: {
//                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationName.hotspotNotification.rawValue), object: nil)
//                })
            }}
    }
    
    //MARK::- CONTACT SYNC
    func checkForContact(){
        if UserSingleton.shared.loggedInUser != nil{
            Utility.functions.contactFetch(completionHandler: { [weak self] (res) in
                let res = res ?? []
                // print(res )
                var addressDict = [String : Any]()
                addressDict["userId"] = /UserSingleton.shared.loggedInUser?._id
                addressDict["contacts"] = String(describing: res)
                print((NSTimeZone.local as NSTimeZone).secondsFromGMT.description)
                
                APIManager.shared.request(with: HomeEndpoint.syncContact(body: addressDict, id : /UserSingleton.shared.loggedInUser?._id, arr : String(describing: res)), completion: { [weak self]  (res) in
                    switch res{
                    case .success(_):
                        guard let user = UserSingleton.shared.loggedInUser else{return}
                        user.contactSync = 1
                        UserSingleton.shared.loggedInUser = user
                        
                    case .failure(_): break
                       // Alerts.shared.show(alert: .error, message: AlertMessage.unableToFetchContct.rawValue, type: .info)
                    }
                    }, isLoaderNeeded: false)
                
            })
        }
    }
}
