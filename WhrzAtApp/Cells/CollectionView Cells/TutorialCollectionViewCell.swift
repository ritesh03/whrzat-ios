//
//  TutorialCollectionViewCell.swift
//  WhrzAtApp
//
//  Created by Sierra 4 on 18/09/17.
//  Copyright © 2017 com.example. All rights reserved.
//

import UIKit

class TutorialCollectionViewCell: UICollectionViewCell {
    
    //MARK::- PROPERTIES
    var arrDataHead = [TitleType.tutorialHead1.rawValue, TitleType.tutorialHead2.rawValue,TitleType.tutorialHead3.rawValue]
    var arrDataDesc  = [TitleType.tutorialDesc1.rawValue,TitleType.tutorialDesc2.rawValue,TitleType.tutorialDesc3.rawValue]
    var arrImage = [#imageLiteral(resourceName: "image_walkthrough_1") , #imageLiteral(resourceName: "image_walkthrough_2") , #imageLiteral(resourceName: "image_walkthrough_3")]
    
    //MARK::- OUTLETS
    @IBOutlet weak var img: UIImageView?
    @IBOutlet weak var heading: UILabel?
    @IBOutlet weak var lblDescription: UILabel?
    
    //MARK::- FUNCTIONS
    func configureCell(index : Int){
        img?.image = self.arrImage[index]
        lblDescription?.text = self.arrDataDesc[index]
        heading?.text = self.arrDataHead[index]
    }
    
}
