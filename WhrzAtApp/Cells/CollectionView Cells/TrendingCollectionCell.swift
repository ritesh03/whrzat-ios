//
//  EventListCollectionCell.swift
//  WhrzAtApp
//
//  Created by Subhash Mehta on 25/06/21.
//  Copyright © 2021 com.example. All rights reserved.
//

import UIKit

class TrendingCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var lblName: UILabel?
    @IBOutlet weak var lblTags: UILabel?
    @IBOutlet weak var lblPopularity: UILabel?
    @IBOutlet weak var imgIcon: UIImageView?
    @IBOutlet weak var imgTrend: UIImageView?
    
    func configureCell(data : Any?){
       // guard let data = data as? MapListData else {return}
      
        if let object = data as? MapListData{
            lblName?.text = object.name
            if let arr = object.tags{
              let arr1 = arr.map{ $0.trimmingCharacters(in: .whitespaces) }
                lblTags?.text =  "#\(String(describing: arr1.joined(separator: " #")))"
            }
            imgTrend?.backgroundColor = UIColor.lightGray
            imgTrend?.kf.setImage(with: URL(string : object.picture?.original ?? ""))
        }
    }
    
}
