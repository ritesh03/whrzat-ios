//
//  ExploreMapCell.swift
//  WhrzAtApp
//
//  Created by Sierra 4 on 31/08/17.
//  Copyright © 2017 com.example. All rights reserved.
//

import UIKit

class ExploreMapCell: UICollectionViewCell {
    
    //MARK::- OUTLETS
    @IBOutlet weak var imgProfile: UIImageView?
    @IBOutlet weak var lblName: UILabel?
    @IBOutlet weak var lblDesc: UILabel?
    @IBOutlet weak var imgPopular: UIImageView?
    @IBOutlet weak var lblPopularity: UILabel?
    
    //MARK::- FUNCTION
    func configureCell(data : Any?){
        guard let data = data as? MapListData else {return}
        lblName?.text = /data.name
        lblDesc?.text = /data.description
        imgProfile?.backgroundColor = UIColor.lightGray
        imgProfile?.kf.setImage(with: URL(string : /data.picture?.original))
        lblPopularity?.text = TitleType.popular.rawValue
        imgPopular?.image = Utility.functions.getHotnessIcon(val: /data.hotness)
        lblPopularity?.textColor = Utility.functions.getHotnessColor(val: /data.hotness)
        lblPopularity?.text = Utility.functions.getHotnessTitle(val: /data.hotness)
    }
}
