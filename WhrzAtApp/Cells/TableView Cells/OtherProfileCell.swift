//
//  OtherProfileCell.swift
//  WhrzAtApp
//
//  Created by Sierra 4 on 28/09/17.
//  Copyright © 2017 com.example. All rights reserved.
//

import UIKit
protocol OtherProfileDelegate{
    func like(index : Int? , isLike : Bool?)
    func report(index : Int?)
    func tapLocation(index : Int?)
}

class OtherProfileCell: UITableViewCell {
    
    //MARK::- OUTLETS
    
    @IBOutlet weak var imgIcon: UIImageView?
    @IBOutlet weak var lblPlace: UILabel?
    @IBOutlet weak var btnPost: UIButton?
    @IBOutlet weak var lblLove: UILabel?
    @IBOutlet weak var btnLove: UIButton?
    @IBOutlet weak var imgPost: UIImageView?
    @IBOutlet weak var btnReport: UIButton?
    
    //MARK::- PROPERTIES
    var delegate : OtherProfileDelegate?
    var lovecount = 0
    var id : String?
    
    //MARK::- CELL CYCLE
    override func awakeFromNib() {
        super.awakeFromNib()
        let tapGesture1: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(OtherProfileCell.tapResponseName))
        tapGesture1.numberOfTapsRequired = 1
        lblPlace?.isUserInteractionEnabled =  true
        lblPlace?.addGestureRecognizer(tapGesture1)
    }
    
    //MARK::- BUTTON ACTION
    @IBAction func btnActionReport(_ sender: UIButton) {
       // delegate?.report(index: btnPost?.tag ?? 0)
        AlertsClass.shared.showAlertController(withTitle: TitleType.Confirmation.rawValue, message: TitleType.addBlocked.rawValue, buttonTitles: [TitleType.report.rawValue , TitleType.Cancel.rawValue]) { (tag) in
            let type = tag as AlertTag
            switch type{
            case .yes:
                APIManager.shared.request(with: HomeEndpoint.reportImage(imageId: /self.id , id : /UserSingleton.shared.loggedInUser?._id), completion: { (res) in
                    switch res{
                    case .success(_):
                        Alerts.shared.show(alert: .success, message: AlertMessage.reportSuccess.rawValue, type: .success)
                    case .failure(let msg):
                        Alerts.shared.show(alert: .error, message: msg, type: .info)
                        
                    }
                }, isLoaderNeeded: true)
            default:
                break
                
            }
        }
    }
    
    @IBAction func btnActionLove(_ sender: UIButton) {
        
        btnLove?.springAnnimate()
        lovecount += ((!(btnLove?.isSelected ?? true)) ? 1 : (-1))
        lblLove?.text = lovecount == 1 ? (lovecount.toString + TitleType.love.rawValue) :  (lovecount.toString + TitleType.loves.rawValue)
        if sender.isSelected{
            sender.setImage(#imageLiteral(resourceName: "heart-empty-icon"), for: .normal)
        }else{
            sender.setImage(Utility.functions.HeartColor(val: lovecount), for: .normal)
        }
        //sender.isSelected.toggle() vivek
      //  delegate?.like(index: btnPost?.tag ?? 0 , isLike : (btnLove?.isSelected) ?? true )
        delegate?.like(index: btnPost?.tag ?? 0 , isLike : !(btnLove!.isSelected) )

        btnLove?.isSelected = !(btnLove!.isSelected)
        
    }
    
    //MARK::- FUNCTIONS
    
    func configureCell(data : FeedData? , index : Int?){
        btnPost?.tag = index ?? 0
        self.id = /data?.imageId?._id
        btnLove?.isSelected = (data?.isLike) ?? false
        btnLove?.setImage(Utility.functions.HeartColor(val: data?.imageId?.likeCount ?? 1), for: .normal)
        self.lovecount = data?.imageId?.likeCount ?? 0
        lblLove?.text = lovecount == 1 ? (lovecount.toString + TitleType.love.rawValue) : ( lovecount.toString + TitleType.loves.rawValue)
        imgIcon?.kf.setImage(with: URL(string : (data?.createdBy?.img?.original)!), placeholder: #imageLiteral(resourceName: "ic_profile"), options: nil, progressBlock: nil, completionHandler: nil)
       // imgIcon?.image = Utility.functions.getHotnessIcon(val: /data?.hotspotId?.hotness)
       // lblPlace?.textColor = Utility.functions.getHotnessColor(val: /data?.hotspotId?.hotness)
        //lblPlace?.text = data?.hotspotId?.name
        let place = " " + (data?.hotspotId?.name)! + " "
//        let name = data?.createdBy?.name
        let name = data?.createdBy?.name

        getPlace(name : name , place : place , img : Utility.functions.getHotnessIcon(val: (data?.hotspotId?.hotness)!) , color: Utility.functions.getHotnessColor(val: (data?.hotspotId?.hotness)!) , isCheckin : data?.type !=  TitleType.imagePost.rawValue)
        imgPost?.kf.setImage(with: URL(string : /data?.imageId?.picture?.original))
    }
    
    @objc func tapResponseName(){
        
        delegate?.tapLocation(index: btnPost?.tag ?? 0)
    }
    
    func getPlace(name : String? , place : String? , img : UIImage? , color : UIColor , isCheckin : Bool){
        let myString = NSMutableAttributedString(string: "")
        let temp = AttributedString.shared.attributeString(value: /name, size: 16, color:  UIColor(named: "AppTextColor") ?? UIColor.black)
//        let simple = AttributedString.shared.attributeString(value:( isCheckin ? TitleType.checkinAt.rawValue : TitleType.post.rawValue), size: 16, color: UIColor(named: "AppTextColor") ?? UIColor.darkGray)
        let simple = AttributedString.shared.attributeString(value:( isCheckin ? TitleType.checkinAt.rawValue : TitleType.post.rawValue), size: 16, color: UIColor(named: "AppTextColor") ?? UIColor.black)
        myString.append(temp)
        myString.append(simple)
        let attachment = NSTextAttachment()
        attachment.image = img
        attachment.bounds = CGRect(x: 0, y: -8, width: 24, height: 24)
        let attachmentStr = NSAttributedString(attachment: attachment)
        myString.append(attachmentStr)
        let temp1 = AttributedString.shared.attributeString(value:  /place, size: 16, color: color)
        myString.append(temp1)
        lblPlace?.attributedText = myString
    }
}

//guard let data = data as? FeedData else{return}
//self.place = " " + /data.hotspotId?.name + " "
//self.name = /data.createdBy?._id == /UserSingleton.shared.loggedInUser?._id ? "You" : /data.createdBy?.name
//getPlace(name : self.name , place : /self.place , img : Utility.functions.getHotnessIcon(val: /data.hotspotId?.hotness) , color: Utility.functions.getHotnessColor(val: /data.hotspotId?.hotness) , isCheckin : /data.type !=  TitleType.imagePost.rawValue)
//lblLike?.text = loveCount == 1 ? (loveCount.toString + TitleType.love.rawValue ) : ( loveCount.toString + TitleType.loves.rawValue)
//imgProfile?.kf.setImage(with: URL(string : /data.createdBy?.img?.original), placeholder: #imageLiteral(resourceName: "ic_profile"), options: nil, progressBlock: nil, completionHandler: nil)
//imgPost?.kf.setImage(with: URL(string : /data.imageId?.picture?.thumbnail), placeholder: UIImage(), options: nil, progressBlock: nil, completionHandler: nil)
