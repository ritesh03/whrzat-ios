//
//  NotificationCell.swift
//  WhrzAtApp
//
//  Created by Sierra 4 on 15/09/17.
//  Copyright © 2017 com.example. All rights reserved.
//

import UIKit

protocol DelegateNotification{
    func valueChange(index :Int? , val : Bool?)
}


class NotificationCell: UITableViewCell {
    
    //MARK::- OUTLETS
    @IBOutlet weak var lbl: UILabel?
    @IBOutlet weak var `switch`: UISwitch?
    
    //MARK::- PROPERTIES
    var delegate : DelegateNotification?
    
    //MARK::- CELL  CYCLE
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    //MARK::- BUTTON ACTION
    @IBAction func switchAction(_ sender: UISwitch) {
        delegate?.valueChange(index : sender.tag , val : sender.isOn )
    }

    //MAK::- FUNCTION
    func configureCell(index : Int , val : String){
        self.switch?.tag = index
        lbl?.text = val
    }

}
