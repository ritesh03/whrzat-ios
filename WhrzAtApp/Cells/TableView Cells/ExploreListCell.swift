//
//  ExploreListCell.swift
//  WhrzAtApp
//
//  Created by Sierra 4 on 31/08/17.
//  Copyright © 2017 com.example. All rights reserved.
//

import UIKit

class ExploreListCell: UITableViewCell {
    
    //MARK::- OUTLETS
    @IBOutlet weak var lblName: UILabel?
    @IBOutlet weak var lblDesc: UILabel?
    @IBOutlet weak var lblPopularity: UILabel?
    @IBOutlet weak var imgIcon: UIImageView?
    @IBOutlet weak var imgProfile: UIImageView?
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblForTime: UILabel!
    
    //MARK::- LIFECYCLE
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    //MARK::- CONFIGURECELL
    func configureCell(data : Any?){
        guard let data = data as? MapListData else {return}
        lblName?.text = /data.name
        lblDesc?.text = /data.description
        imgProfile?.backgroundColor = UIColor.lightGray
        imgProfile?.kf.setImage(with: URL(string : /data.picture?.original))
    }
    
}
