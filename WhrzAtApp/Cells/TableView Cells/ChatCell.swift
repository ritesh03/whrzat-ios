//
//  ChatCell.swift
//  WhrzAtApp
//
//  Created by Sierra 4 on 03/09/17.
//  Copyright © 2017 com.example. All rights reserved.
//

import UIKit
import IBAnimatable
class ChatCell: UITableViewCell {

    //MARK::- OUTLETS
    
    @IBOutlet weak var imgProfile: UIImageView?
    @IBOutlet weak var lblTime: UILabel?
    @IBOutlet weak var lblName: UILabel?
    @IBOutlet weak var lblMsgDesc: UILabel?
    @IBOutlet weak var lblUnread: AnimatableLabel?
    
    //MRK::- CELL CYCLE
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK::- FUNCTION
    
    func configureCell(data : MessageDataa?){
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT")
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        
        if UserSingleton.shared.loggedInUser?._id == data?.message?.receiverDetails?.senderId {
            imgProfile?.kf.setImage(with: URL(string : /data?.message?.senderDetails?.profilePicURL?.thumbnail), placeholder: #imageLiteral(resourceName: "ic_profile"), options: nil, progressBlock: nil, completionHandler: nil)
            lblName?.text = /data?.message?.senderDetails?.name
        }else{
            imgProfile?.kf.setImage(with: URL(string : /data?.message?.receiverDetails?.profilePicURL?.thumbnail), placeholder: #imageLiteral(resourceName: "ic_profile"), options: nil, progressBlock: nil, completionHandler: nil)
            lblName?.text = /data?.message?.receiverDetails?.name
        }
        
        let date = Utility.functions.dateFromMilliseconds(ms : Int64((data?.message?.timeStamp ?? 0.0) ))
        lblTime?.text = calculateTimeSince(time: /date , isChat : true)
        lblMsgDesc?.text = /data?.message?.message
        lblUnread?.text = /data?.unread?.toString
        lblUnread?.isHidden = (data?.unread ?? 0) == 0
    }

}
