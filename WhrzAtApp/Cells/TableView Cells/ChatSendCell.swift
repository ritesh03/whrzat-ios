//
//  ChatSendCell.swift
//  WhrzAtApp
//
//  Created by Sierra 4 on 03/09/17.
//  Copyright © 2017 com.example. All rights reserved.
//

import UIKit

class ChatSendCell: UITableViewCell {

    //MARK::- OUTLETS
    
    @IBOutlet weak var lbl: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    
    //MARK::- CELL CYCLE
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configureCell(item : Message?){
        lbl.text = /item?.msg
        lblTime.text = Utility.functions.getChatTime(timestamp: ((item?.time ?? 0) / 1000))
    }

}
