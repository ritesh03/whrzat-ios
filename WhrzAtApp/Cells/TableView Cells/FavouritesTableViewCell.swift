//
//  FavouritesTableViewCell.swift
//  WhrzAtApp
//
//  Created by Vivek Dogra on 02/03/20.
//  Copyright © 2020 com.example. All rights reserved.
//

import UIKit

class FavouritesTableViewCell: UITableViewCell {

     //MARK::- OUTLETS
    @IBOutlet weak var lblName: UILabel?
    @IBOutlet weak var lblDesc: UILabel?
    @IBOutlet weak var lblPopularity: UILabel?
    @IBOutlet weak var imgIcon: UIImageView?
    @IBOutlet weak var imgProfile: UIImageView?
    
    //MARK::- LIFECYCLE
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    //MARK::- CONFIGURECELL
    func configureCell(data : Any?){
        guard let data = data as? ListFavorite else {return}
        lblName?.text = /data.name
        lblDesc?.text = /data.description
        imgProfile?.backgroundColor = UIColor.lightGray
        imgProfile?.kf.setImage(with: URL(string : /data.picture?.original))
    }

}
