//
//  FeedTableViewCell.swift
//  WhrzAtApp
//
//  Created by Sierra 4 on 03/09/17.
//  Copyright © 2017 com.example. All rights reserved.
//

import UIKit
import EZSwiftExtensions

protocol FeedDelegate{
    func chat(index : Int?)
    func report(index : Int?)
    func like(index : Int? , isLike : Bool?)
    func locationTap(index :Int?)
    func profileTap(index : Int?)
}

class FeedTableViewCell: UITableViewCell {
    
    //MARK::- OUTLETS
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var eventNameLabel: UILabel!
    @IBOutlet weak var imgPopular: UIImageView!
    @IBOutlet weak var imgProfile: UIImageView?
    @IBOutlet weak var lblCheckin: UILabel?
    @IBOutlet weak var imgPost: UIImageView?
    @IBOutlet weak var lblLike: UILabel?
    @IBOutlet weak var btnLove: UIButton?
    @IBOutlet weak var btnPost: UIButton?
    
    @IBOutlet weak var imgChat: UIImageView?
    @IBOutlet weak var btnChat: UIButton?
    @IBOutlet weak var btnReport: UIButton?
    @IBOutlet weak var btnFlame: UIButton!
    
    //MARK::- PROPERTIES
    var loveCount = 0
    var delegate : FeedDelegate?
    var place : String?
    var name : String?
    var likeCallBack : (() -> Void)? = nil
    var chatCalllBack : (() -> Void)? = nil
    var reportCalllBack : (() -> Void)? = nil
     var profileCalllBack : (() -> Void)? = nil
    var eventPicCallBack : (() -> Void)? = nil
    var eventNameCallBack : (() -> Void)? = nil
    //MARK::- CELL CYCLE
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let tapGesture1: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(FeedTableViewCell.tapResponseName))
       // tapGesture1.numberOfTapsRequired = 1
        lblCheckin?.isUserInteractionEnabled =  true
        lblCheckin?.addGestureRecognizer(tapGesture1)
        let tapGesture2: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(FeedTableViewCell.tapImage))
        //tapGesture2.numberOfTapsRequired = 1
        imgProfile?.isUserInteractionEnabled =  true
        imgProfile?.addGestureRecognizer(tapGesture2)
        let tapGesture3: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(FeedTableViewCell.tapPost))
        //tapGesture2.numberOfTapsRequired = 1
        imgPost?.isUserInteractionEnabled =  true
        imgPost?.addGestureRecognizer(tapGesture3)
        if let data = eventNameLabel {
            let tapGesture4: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(FeedTableViewCell.tapeventName))
                          //tapGesture2.numberOfTapsRequired = 1
                          eventNameLabel.isUserInteractionEnabled =  true
                          eventNameLabel.addGestureRecognizer(tapGesture4)
        }
       
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func btnActionChat(_ sender: Any) {
        if let data = chatCalllBack {
            chatCalllBack!()
        }
        
      //  delegate?.chat(index: btnPost?.tag ?? 0)
       // let vc = StoryboardScene.Home.instantiateChatUserVC()
       // ez.topMostVC?.pushVC(vc)
        
        
    }
    
    @IBAction func btnActionReport(_ sender: Any) {
        if let data = reportCalllBack {
             reportCalllBack!()
        }
       
//        AlertsClass.shared.showAlertController(withTitle: TitleType.Confirmation.rawValue, message: TitleType.addBlocked.rawValue, buttonTitles: [TitleType.report.rawValue , TitleType.Cancel.rawValue]) { (tag) in
//            let type = tag as AlertTag
//            switch type{
//            case .yes:
//                self.delegate?.report(index: self.btnPost?.tag ?? 0)
//            default:
//                break                
//            }
//        }
    }
    
    
    @IBAction func btnActionLive(_ sender: UIButton) {
        if let data = likeCallBack {
            likeCallBack!()
        }
        sender.springAnnimate()
    }
}

//MARK::- FUNCTION
extension FeedTableViewCell{
    
    func configureCell(data : Any?){
        guard let data = data as? FeedData else{return}
        self.place = " " + /data.hotspotId?.name + " "
        self.name = /data.createdBy?._id == /UserSingleton.shared.loggedInUser?._id ? "You" : /data.createdBy?.name
        getPlace(name : self.name , place : /self.place , img : Utility.functions.getHotnessIcon(val: /data.hotspotId?.hotness) , color: Utility.functions.getHotnessColor(val: /data.hotspotId?.hotness) , isCheckin : /data.type !=  TitleType.imagePost.rawValue)
        lblLike?.text = loveCount == 1 ? (loveCount.toString + TitleType.love.rawValue ) : ( loveCount.toString + TitleType.loves.rawValue)
        imgProfile?.kf.setImage(with: URL(string : /data.createdBy?.img?.original), placeholder: #imageLiteral(resourceName: "ic_profile"), options: nil, progressBlock: nil, completionHandler: nil)
        imgPost?.kf.setImage(with: URL(string : /data.imageId?.picture?.thumbnail), placeholder: UIImage(), options: nil, progressBlock: nil, completionHandler: nil)
    }
    
    
//    func configureData(data : Any? , index : Int){
//        guard let item = data as? FeedData else{return}
//        self.loveCount = item.imageId?.likeCount ?? 0
//        self.btnLove?.isSelected = (item.isLike) ?? false
//        self.btnPost?.tag = index
//        btnChat?.isHidden = item.createdBy?._id == /UserSingleton.shared.loggedInUser?._id
//        imgChat?.isHidden = item.createdBy?._id == /UserSingleton.shared.loggedInUser?._id
//        btnReport?.isHidden = item.createdBy?._id == /UserSingleton.shared.loggedInUser?._id
//        btnLove?.isUserInteractionEnabled = item.createdBy?._id != /UserSingleton.shared.loggedInUser?._id//
//        btnLove?.setImage(Utility.functions.HeartColor(val: item.imageId?.likeCount ?? 1), for: .normal)
//        configureCell(data : item)
//    }
    
    
//    func configureProfileData(data : Any? , index : Int){
//        guard let item = data as? FeedData else{return}
//        loveCount = item.imageId?.likeCount ?? 0
//        btnLove?.isSelected = (item.isLike) ?? false
//        btnLove?.isUserInteractionEnabled = false//
//        btnPost?.tag = index
//        configureCell(data: item)
//        btnLove?.setImage(Utility.functions.HeartColor(val: item.imageId?.likeCount ?? 0), for: .normal)
//    }

    
    @objc func tapResponseName(recognizer: UITapGestureRecognizer) {
        if let data = profileCalllBack {
            profileCalllBack!()
        }
        guard let text = lblCheckin?.text as? NSString? else {return}
        let placeRange = text?.range(of: /self.place)
        let nameRange = text?.range(of: /self.name)

        
        if recognizer.didTapAttributedTextInLabel(label: lblCheckin ?? UILabel(), inRange: placeRange ?? NSRange.init()) {
           delegate?.locationTap(index: btnPost?.tag ?? 0)
        } else if recognizer.didTapAttributedTextInLabel(label: lblCheckin ?? UILabel(), inRange: nameRange ?? NSRange.init()) {
            delegate?.profileTap(index: btnPost?.tag ?? 0)
        } else{
            
        }
    }
    
    @objc func tapImage(){
        if let data = profileCalllBack {
            profileCalllBack!()
        }
        
       // delegate?.profileTap(index: btnPost?.tag ?? 0)
    }
    @objc func tapPost(){
        if let data = eventPicCallBack {
            eventPicCallBack!()
        }
          
          // delegate?.profileTap(index: btnPost?.tag ?? 0)
       }
    @objc func tapeventName(){
           if let data = eventNameCallBack {
               eventNameCallBack!()
           }
             
             // delegate?.profileTap(index: btnPost?.tag ?? 0)
          }
 
    
    func getPlace(name : String? , place : String? , img : UIImage? , color : UIColor , isCheckin : Bool){
        let myString = NSMutableAttributedString(string: "")
        let temp = AttributedString.shared.attributeString(value: /name, size: 16, color:  UIColor(named: "AppTextColor") ?? UIColor.black)
//        let simple = AttributedString.shared.attributeString(value:( isCheckin ? TitleType.checkinAt.rawValue : TitleType.post.rawValue), size: 16, color: UIColor(named: "AppTextColor") ?? UIColor.darkGray)
        let simple = AttributedString.shared.attributeString(value:( isCheckin ? TitleType.checkinAt.rawValue : TitleType.post.rawValue), size: 16, color: UIColor(named: "AppTextColor") ?? UIColor.black)
        myString.append(temp)
        myString.append(simple)
        let attachment = NSTextAttachment()
        attachment.image = img
        attachment.bounds = CGRect(x: 0, y: -8, width: 24, height: 24)
        let attachmentStr = NSAttributedString(attachment: attachment)
        myString.append(attachmentStr)
        let temp1 = AttributedString.shared.attributeString(value:  /place, size: 16, color: color)
        myString.append(temp1)
        lblCheckin?.attributedText = myString
    }
    
    
    }

