//
//  EventListCell.swift
//  WhrzAtApp
//
//  Created by Sierra 4 on 03/09/17.
//  Copyright © 2017 com.example. All rights reserved.
//

import UIKit

class EventListCell: UITableViewCell {

    //MARK::- OUTLETS
    @IBOutlet weak var lblEventName: UILabel?
    @IBOutlet weak var lblEventDesc: UILabel?
    @IBOutlet weak var lblEventTime: UILabel?
    
    //MARK::- CELL CYCLE
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    
    func configureCell(data  : EventArr?){
        
        self.lblEventDesc?.text = /data?.description
        self.lblEventName?.text = /data?.name
    }
}
