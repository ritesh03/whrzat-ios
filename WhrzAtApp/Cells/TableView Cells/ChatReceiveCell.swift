//
//  ChatReceiveCell.swift
//  WhrzAtApp
//
//  Created by Sierra 4 on 03/09/17.
//  Copyright © 2017 com.example. All rights reserved.
//

import UIKit

class ChatReceiveCell: UITableViewCell {

    //MARK::- OUTLETS
    
    @IBOutlet weak var lbl: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    
    
    //MRK::- CELL CYCLE
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK::- FUNCTION
    func configureCell(item : Message?){
        lbl.text = /item?.msg
        lblTime.text = Utility.functions.getChatTime(timestamp: ((item?.time ?? 0) / 1000))
    }

}
