//
//  ProfileNotificationCell.swift
//  WhrzAtApp
//
//  Created by Sierra 4 on 04/09/17.
//  Copyright © 2017 com.example. All rights reserved.
//

import UIKit

class ProfileNotificationCell: UITableViewCell {
    
    //MARK::- OUTLETS

    @IBOutlet weak var imgProfile: UIImageView?
    @IBOutlet weak var imgPost: UIImageView?
    @IBOutlet weak var lblText: UILabel?
    @IBOutlet weak var btnProfile: UIButton!
    
    //MARK::- CELL CYCLE
    override func awakeFromNib() {
        super.awakeFromNib()
        let tapGesture1: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ProfileNotificationCell.tapResponseName))
        tapGesture1.numberOfTapsRequired = 1
        lblText?.isUserInteractionEnabled =  false
        lblText?.addGestureRecognizer(tapGesture1)
    }
}

extension ProfileNotificationCell{
    
    func configureCell(data : NotificationDataa , index : Int) {
        if let profileImage = data.likedBy?.pic?.original  {
        imgProfile?.kf.setImage(with: URL(string : profileImage), placeholder: #imageLiteral(resourceName: "ic_profile"), options: nil, progressBlock: nil, completionHandler: nil)
        } else {
        imgProfile?.kf.setImage(with: URL(string : /data.userId?.pic?.original), placeholder: #imageLiteral(resourceName: "ic_profile"), options: nil, progressBlock: nil, completionHandler: nil)
        }
        
        if /data.event != "13" {
        if let hotspotImage = data.imageId?.picture?.original  {
        imgPost?.kf.setImage(with: URL(string : hotspotImage))
        } else {
        imgPost?.kf.setImage(with: URL(string : /data.hotspotId?.picture?.original))
        }
        } else {
            imgPost?.isHidden =  true
        }
        
        if let username = data.likedBy?.name  {
            getText(name : username , place : /data.hotspotId?.name , img : Utility.functions.getHotnessIcon(val: /data.hotspotId?.hotness) , color: Utility.functions.getHotnessColor(val: /data.hotspotId?.hotness) , event : /data.event)
        } else if let username = data.userId?.name {
            getText(name : username , place : /data.hotspotId?.name , img : Utility.functions.getHotnessIcon(val: /data.hotspotId?.hotness) , color: Utility.functions.getHotnessColor(val: /data.hotspotId?.hotness) , event : /data.event)
        } else {
            getText(name : "WhrzAt" , place : /data.hotspotId?.name , img : Utility.functions.getHotnessIcon(val: /data.hotspotId?.hotness) , color: Utility.functions.getHotnessColor(val: /data.hotspotId?.hotness) , event : /data.event)
        }
        
        btnProfile?.tag = index

    }

    @objc func tapResponseName(recognizer: UITapGestureRecognizer) {

    }
    
    func getText(name : String? , place : String? , img : UIImage? , color : UIColor , event : String?){
        let myString = NSMutableAttributedString(string: "")
        var temp = AttributedString.shared.attributeString(value: /name, size: 16, color: UIColor(named: "AppTextColor") ?? UIColor.black)
        var simple = AttributedString.shared.attributeString(value: TitleType.checkinAt.rawValue, size: 16, color: UIColor(named: "AppTextColor") ?? UIColor.black)

       // AppTextColorWithOpacity
        switch (/event){
        case "1" :
            temp = NSMutableAttributedString(string: "")
            simple = AttributedString.shared.attributeString(value: "Since the \(/place) is popular today! Check them out.", size: 16, color: UIColor(named: "AppTextColor") ?? UIColor.black)
        
        case "2":
            
            temp = AttributedString.shared.attributeString(value: /name, size: 16, color: UIColor(named: "AppTextColor") ?? UIColor.black)
            simple = AttributedString.shared.attributeString(value: TitleType.lovesyourphotopostedto.rawValue, size: 16, color: UIColor(named: "AppTextColor") ?? UIColor.black)

        case "3":
             temp = AttributedString.shared.attributeString(value: "", size: 16, color: UIColor(named: "AppTextColor") ?? UIColor.black)
             simple = AttributedString.shared.attributeString(value: "Your picture posted to \(/place) is very popular.", size: 16, color: UIColor(named: "AppTextColor") ?? UIColor.black)
            
        case "4":
            temp = AttributedString.shared.attributeString(value: /name, size: 16, color: UIColor(named: "AppTextColor") ?? UIColor.black)
            simple = AttributedString.shared.attributeString(value: TitleType.postedAt.rawValue, size: 16, color: UIColor(named: "AppTextColor") ?? UIColor.black)
            
        case "5":
            temp = AttributedString.shared.attributeString(value: /name, size: 16, color: UIColor(named: "AppTextColor") ?? UIColor.black)
            simple = AttributedString.shared.attributeString(value: " put \(/place) on the map! Find out why.", size: 16, color: UIColor(named: "AppTextColor") ?? UIColor.black)
            
        case "13":
            temp =   NSMutableAttributedString(string: "")
            simple = AttributedString.shared.attributeString(value: TitleType.referralReward.rawValue, size: 16, color: UIColor(named: "AppTextColor") ?? UIColor.black)
      
        case "14":
            temp =   NSMutableAttributedString(string: "")
            simple = AttributedString.shared.attributeString(value: TitleType.firstHotspotReward.rawValue, size: 16, color: UIColor(named: "AppTextColor") ?? UIColor.black)
            
        case "15":
            temp =   NSMutableAttributedString(string: "")
            simple = AttributedString.shared.attributeString(value: TitleType.firstPhotoReward.rawValue, size: 16, color:  UIColor(named: "AppTextColor") ?? UIColor.black)
            

        default:
            temp = AttributedString.shared.attributeString(value: /name, size: 16, color: UIColor(named: "AppTextColor") ?? UIColor.black)
            simple = AttributedString.shared.attributeString(value: TitleType.lovesyourphotopostedto.rawValue, size: 16, color: UIColor(named: "AppTextColor") ?? UIColor.black)
        }
        
        myString.append(temp)
        myString.append(simple)
        
        if /event != "1" && /event != "5" && /event != "13"{
        let attachment = NSTextAttachment()
        attachment.image = img
        attachment.bounds = CGRect(x: 0, y: -8, width: 24, height: 24)
        let attachmentStr = NSAttributedString(attachment: attachment)
        myString.append(attachmentStr)
        let temp1 = AttributedString.shared.attributeString(value: (" " + /place), size: 16, color: color)
        myString.append(temp1)
        }
        
        lblText?.attributedText = myString
    }
    
}


//event - 0 <username> sent you a message
//event - 1 Daily Notification,
//event - 2 Like Notification, "<useraname> loves your photo posted to <username>"
//event - 3 Like Notification , "Your picture posted to <username> is very popular."
//event - 4 WhrzAt Notification, "<useraname> posted a picture to your favourite place. "
//event - 5 WhrzAt Notification , "<useraname> put <hotspotname> on the map! Find out why."
//event - 6 Hotspot Notification , "<useraname> checked out your hotspot."
//event - 7 WhrzAt Notification , "<useraname> put <hotspotname> on the map! Find out why."
//event -9 WhrzAt Notification , "<useraname> added an event to your favourite place."
//event -11 Favourite Notification , "Your hotspot has been favourite by <useraname> "
//event -12 Like Notification , "<useraname> loves your photo posted to <username>"
//event -13 Invite Notification , "You have earned a reward for your reference"
//event -14 Add Image Reward Notification , "You have got a reward for creating new hotspot"
//event -15 Hotspot Reward Notification , "You have got a reward for posting first picture
