//
//  HotspotDetailCell.swift
//  WhrzAtApp
//
//  Created by Sierra 4 on 03/09/17.
//  Copyright © 2017 com.example. All rights reserved.
//

import UIKit
import EZSwiftExtensions

protocol HotspotDetail{
    func chat(index : Int?)
    func report(index : Int?)
    func like(index : Int? , isLike : Bool?)
    func delete(index : Int?)
    func tapName(index : Int?)
}

class HotspotDetailCell: UITableViewCell {

    //MARK::- OUTLETS
    @IBOutlet weak var imgProfile: UIImageView?
    @IBOutlet weak var lblCheckin: UILabel?
    @IBOutlet weak var imgPost: UIImageView?
    @IBOutlet weak var lblLike: UILabel?
    @IBOutlet weak var btnLike: UIButton?
    @IBOutlet weak var lblTIme: UILabel?
    @IBOutlet weak var btnPost: UIButton?
    @IBOutlet weak var btnChat: UIButton?
    @IBOutlet weak var btnReport: UIButton?
    @IBOutlet weak var imgChat: UIImageView?
    @IBOutlet weak var btnRemove: UIButton?

    //MARK::- PROPERTIES
    
    var delegate : HotspotDetail?
    var loveCount = 0
    var id : String?
    var hotness : String?
    
    //MARK::- CELL CYCLE
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let tapGesture1: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(HotspotDetailCell.tapResponseName))
        tapGesture1.numberOfTapsRequired = 1
        lblCheckin?.isUserInteractionEnabled =  true
        lblCheckin?.addGestureRecognizer(tapGesture1)
        let tapGesture2: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(HotspotDetailCell.tapResponseName))
        tapGesture1.numberOfTapsRequired = 1
        imgProfile?.isUserInteractionEnabled =  true
        imgProfile?.addGestureRecognizer(tapGesture2)

    }
    
    //MARK::- BUTTON ACTION
    @IBAction func btnActionChat(_ sender: Any) {
        delegate?.chat(index: btnPost?.tag ?? 0)
    }
    
    @IBAction func btnActionRemove(_ sender: UIButton) {
        delegate?.delete(index: btnPost?.tag)
    }
    
    @IBAction func btnActionReport(_ sender: Any) {
        AlertsClass.shared.showAlertController(withTitle: TitleType.Confirmation.rawValue, message: TitleType.addBlocked.rawValue, buttonTitles: [TitleType.report.rawValue , TitleType.Cancel.rawValue]) { (tag) in
            let type = tag as AlertTag
            switch type{
                case .yes:
                    guard let user = UserSingleton.shared.loggedInUser else {return}

                    APIManager.shared.request(with: HomeEndpoint.reportImage(imageId: /self.id , id : /UserSingleton.shared.loggedInUser?._id), completion: { (res) in
                        switch res{
                        case .success(_):
                            Alerts.shared.show(alert: .success, message: AlertMessage.reportSuccess.rawValue, type: .success)

                        case .failure(let msg):
                            Alerts.shared.show(alert: .error, message: msg, type: .info)

                        }
                    }, isLoaderNeeded: true)
                default:
                break
                
            }
        }
        
    }
    @IBAction func btnActionLove(_ sender: Any) {

        //guard let sender = sender as? btnLike else{return}
        btnLike?.springAnnimate()
        loveCount += ((!(btnLike?.isSelected ?? true)) ? 1 : (-1))
        lblLike?.text = loveCount == 1 ? (loveCount.toString + TitleType.love.rawValue) :  (loveCount.toString + TitleType.loves.rawValue)
        if btnLike?.isSelected ?? true{
            btnLike?.setImage(#imageLiteral(resourceName: "heart-empty-icon"), for: .normal)
        }else{
            btnLike?.setImage(Utility.functions.HeartColor(val: loveCount), for: .normal)
        }
        //btnLike?.isSelected.toggle() vivek
        
        delegate?.like(index: btnLike?.tag ?? 0 , isLike : !(btnLike!.isSelected) )
        btnLike?.isSelected = !(btnLike!.isSelected)
    }
    
    @objc func tapResponseName(){
        delegate?.tapName(index : btnLike?.tag ?? 0)
    }
        
   }

//MARK::- CONFIGURE CELL
extension HotspotDetailCell{
    func configureCell(data : Images){
        
    }
}

