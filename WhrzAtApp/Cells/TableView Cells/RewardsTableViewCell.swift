//
//  RewardsTableViewCell.swift
//  WhrzAtApp
//
//  Created by Ritesh on 04/05/21.
//  Copyright © 2021 com.example. All rights reserved.
//

import Foundation
import UIKit



class RewardsTableViewCell: UITableViewCell,UITextFieldDelegate {
    
    
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var referralCodeField: UITextField!
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var subheadingLabel: UILabel!
    @IBOutlet weak var locationButton: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}
