//
//  BlockedContactCell.swift
//  WhrzAtApp
//
//  Created by Sierra 4 on 15/09/17.
//  Copyright © 2017 com.example. All rights reserved.
//

import UIKit

protocol BlockDelegate{
    func unblockUser(index : Int?)
}

class BlockedContactCell: UITableViewCell {

    //MARK::- OUTLETS
    
    @IBOutlet weak var img: UIImageView?
    @IBOutlet weak var lblName: UILabel?
    @IBOutlet weak var btnUnblock: UIButton?
    
    //MARK::- PROPERTIES
    
    var delegate : BlockDelegate?
    
    //MARK::- CELL CYCLE
    override func awakeFromNib() {
        super.awakeFromNib()
    }


    //MARK::- BUTTON ACTION
    @IBAction func btnActionUnblock(_ sender: UIButton) {
        delegate?.unblockUser(index : sender.tag )
    }
    
    //MARK::- FUNCTION
    func configureCell(data : Any?){
        guard let data = data as? Blockedby else {return}
        lblName?.text = data.name
        img?.kf.setImage(with: URL(string : /data.profilePicURL?.original), placeholder: #imageLiteral(resourceName: "ic_profile"), options: nil, progressBlock: nil, completionHandler: nil)

    
    }
}
